<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BasicTest extends TestCase
{

    /**
     * Basic setup before testing
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        
		// Generate Tables + Seeds
        $this->artisan('migrate:refresh');
        $this->artisan('db:seed');
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testHomepage()
    {
        $this->visit('/')
             ->see('LaraAdmin');
    }
}

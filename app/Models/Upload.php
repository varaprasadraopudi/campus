<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Upload extends Model
{
    use SoftDeletes;
	
	protected $table = 'uploads';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

    protected $dates = ['deleted_at'];
    
    public static function update_local_upload_paths() {
        $uploads = Upload::all();
        $storage_local_path = storage_path('');
        $path = "";
        foreach ($uploads as $upload) {
            $path = $upload->path;
            $path = substr($path, strpos($path, "uploads"));
            $path = $storage_local_path."/".$path;
            $upload->path = $path;
            $upload->save();
        }
		return redirect(config('laraadmin.adminRoute') . '/uploads');
    }

	/**
     * Get the user that owns upload.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Get File path
     */
    public function path()
    {
        //return url("files/".$this->hash."/".$this->name);
        return url("storage/uploads/".basename($this->path));
    }
}

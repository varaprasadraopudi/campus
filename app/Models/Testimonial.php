<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\LALog;

class Testimonial extends Model
{
    use SoftDeletes;
    use Searchable;
    
    protected $table = 'testimonials';
    
    protected $hidden = [
    
    ];
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'testimonials_index';
    }

    /**
     * Get all events happened on Module
     *
     * @return mixed
     */
    public function timeline()
    {
        $moduleConfigs = config('laraadmin.log.Testimonials');
        $moduleConfigsArr = array_keys($moduleConfigs);
        return LALog::where("context_id", $this->id)->whereIn("type", $moduleConfigsArr)->orderBy("created_at", "desc")->get();
    }

    public function authorImage()
    {
        if($this->author_image != 0) {
            return "<img src=".Upload::find($this->author_image)->path()." alt='Author image' draggable='false'>";
        } else {
            return "<img src=".asset('la-assets/img/post_banner_large.jpg')." alt='Author image' draggable='false'>";
        }
    }

    public function contentImage()
    {
        if($this->content_image != 0) {
            return "<img src=".Upload::find($this->content_image)->path()." alt='Content image' draggable='false' class='mt10'>";
        } else {
            return "";
        }
    }
}

<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\LALog;

class BlogPost extends Model
{
    use SoftDeletes;
    use Searchable;
    
    protected $table = 'blog_posts';
    
    protected $hidden = [
    
    ];
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'blog_posts_index';
    }

    public function bannerImage($large = false)
    {
        if(!empty(json_decode($this->album))) {
            $album = json_decode($this->album);
            return Upload::find($album[0])->path();
        } else {
            if($large) {
                return asset('la-assets/img/post_banner_large.jpg');
            } else {
                return asset('la-assets/img/post_banner.png');
            }
        }
        return null;
    }
    
    // public function bannerImage($large = false)
    // {
    //     if($this->banner != 0) {
    //         return Upload::find($this->banner)->path();
    //     } else {
    //         if($large) {
    //             return asset('la-assets/img/post_banner_large.jpg');
    //         } else {
    //             return asset('la-assets/img/post_banner.png');
    //         }
    //     }
    //     return null;
    // }

    /**
     * Get the user that owns post
     */
    public function author()
    {
        return $this->belongsTo('App\Models\Employee', 'author_id');
    }

    /**
     * Get the user that owns post
     */
    public function date()
    {
        return date("M d, Y", strtotime($this->post_date));
    }

    /**
     * Get the category that has post
     */
    public function category()
    {
        return $this->belongsTo('App\Models\BlogCategory', 'category_id');
    }

    /**
     * Get all events happened on Module
     *
     * @return mixed
     */
    public function timeline()
    {
        $moduleConfigs = config('laraadmin.log.Blog_posts');
        $moduleConfigsArr = array_keys($moduleConfigs);
        return LALog::where("context_id", $this->id)->whereIn("type", $moduleConfigsArr)->orderBy("created_at", "desc")->get();
    }
}

<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Module;
use App\Models\LALogUser;

class LALog extends Model
{
    protected $table = 'lalogs';
    
    protected $hidden = [
    
    ];
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];

    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'lalog_user', 'lalog_id', 'user_id');
    }

    public static function make($log_type, $log)
    {
        // log_type starts with "laraadmin.log.". Then we should append that to get config
        if(!starts_with($log_type, "laraadmin.log.")) {
            $log_type = "laraadmin.log.".$log_type;
        }

        // Check if log_type really exists in config file.
        // If not then skip adding LALog
        if(null !== config($log_type.'.name')) {
            // Get Title if not present in parameters
            if(!isset($log['title'])) {
                $log['title'] = config($log_type.'.name');
            }

            // Get Type if not present in parameters
            if(!isset($log['type'])) {
                $log['type'] = config($log_type.'.code');
            }

            // If module_id is passes as Module Name, then need to convert that to ID
            if(is_string($log['module_id'])) {
                $module = Module::get($log['module_id']);
                // Check if this Module Exists
                if(isset($module->id)) {
                    $log['module_id'] = $module->id;
                } else {
                    $log['module_id'] = NULL;
                }
            }

            // Check Type of Log "content"
            $content = "{}";
            if(is_string($log['content'])) {
                $content = $log['content'];
            } else if(is_object($log['content'])) {
                // This can be Model Object / Request Object
                $content = json_encode($log['content']);
            } else if(is_array($log['content']) && count($log['content']) == 2) {
                // This can be array containing old and new data changes
                // Now compare two arrays
                $old = $log['content']['old'];
                $new = $log['content']['new'];
                $old = json_decode(json_encode($old));
                $new = json_decode(json_encode($new));

                $diff = (object) array();
                foreach ($new as $key => $value) {
                    if(isset($old->{$key})) {
                        if($old->{$key} != $new->{$key}) {
                            $diff->{$key} = [
                                'old' => $old->{$key},
                                'new' => $new->{$key},
                            ];
                        }
                    } else {
                        if(isset($new->{$key})) {
                            $diff->{$key} = [
                                'old' => NULL,
                                'new' => $new->{$key},
                            ];
                        }
                    }
                }
                $content = json_encode($diff);
                if($content == "{}") {
                    return;
                }
            }
            $log['content'] = $content;
            
            // Handle Notify To
            $notify_to = $log['notify_to'];
            unset($log['notify_to']);

            // Create LALogs
            $log = LALog::create($log);
            
            // Send Appropriate Notifications
            LALog::handle_log_notif($log, $notify_to);
        }
    }

    public static function handle_log_notif($log, $notify_to)
    {
        if(is_array($notify_to)) {
            foreach ($notify_to as $notif) {
                $notif = (object) $notif;
                LALogUser::create([
                    'lalog_id' => $log->id,
                    'user_id' => $notif->user_id,
                    'notif_mail' => $notif->notif_mail,
                    'notif_socket' => $notif->notif_socket
                ]);
            }
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class HttpsProtocol {

    public function handle($request, Closure $next)
    {
            if (!$request->secure() && App::environment() === 'production') {
                $new_url = $request->fullUrl();
                // \Log::debug("old_url: " . $new_url);
                // $new_url = str_replace("dwij.in", "imepl.in", $new_url);
                $new_url = str_replace("http://", "https://", $new_url);
                // \Log::debug("new_url: " . $new_url);
                return redirect($new_url);
            }

            return $next($request); 
    }
}

?>
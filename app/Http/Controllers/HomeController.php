<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Log;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Role;
use App\Models\BlogPost;
use App\Models\Booking;
use App\Models\Enquiry;
use App\Models\Testimonial;
use App\Models\Upload;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $posts = BlogPost::where("status", "Published")->orderBy("post_date", "desc")->get();
        return view('home', [
            'posts' => $posts
        ]);
    }

    public function save_session_booking(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $organization = $request->organization;
        $message_str = $request->message;
        $source = $request->source;

        $booking = Booking::create([
            "name" => $name,
            "email" => $email,
            "phone_no" => $phone,
            "organization" => $organization,
            "message" => $message_str,
            "training" => $source
        ]);
        $booking->save();

        if (isset($booking->id)) {

            \Mail::send('emails.save_booking', ['name' => $name, 'email' => $email, 'phone' => $phone, 'organization' => $organization, 'message_str' => $message_str, "source" => $source], function ($m) use ($name, $email) {
                $m->from("info@empowercamp.com", $name);
                $m->replyTo($email, $name);
                $m->to('info@empowercamp.com', 'Empower Activity Camp')->bcc('digital@empowercamp.com', 'Empower Activity Camp')->subject('Booking - ' . $name);
            });

            return response()->json([
                'status' => 'success',
                'message' => 'Session Booking done successfully!'
            ]);
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => 'Error in save booking.'
            ]);
        }
    }

    public function save_enquiry(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $organization = $request->organization;
        $message_str = $request->message;

        $enquiry = Enquiry::create([
            "name" => $name,
            "email" => $email,
            "phone_no" => $phone,
            "organization" => $organization,
            "message" => $message_str
        ]);
        $enquiry->save();

        if (isset($enquiry->id)) {

            \Mail::send('emails.send_inquiry', ['name' => $name, 'email' => $email, 'phone' => $phone, 'organization' => $organization, 'message_str' => $message_str], function ($m) use ($name, $email) {
                $m->from("info@empowercamp.com", $name);
                $m->replyTo($email, $name);
                $m->to('info@empowercamp.com', 'Empower Activity Camp')->bcc('digital@empowercamp.com', 'Empower Activity Camp')->subject('Inquiry - ' . $name);
            });

            return response()->json([
                'status' => 'success',
                'message' => 'Done successfully!'
            ]);
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => 'Error in save enquiry.'
            ]);
        }
    }

    public function save_testimonial(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $designation = $request->designation;
        $organization = $request->organization;
        $category = $request->category;
        $title = $request->title;
        $content = $request->content;
        $youtube = $request->youtube;

        $author_image_id = NULL;

        $testimonial = Testimonial::create([
            "title" => $title,
            "category" => $category,
            "author" => $name,
            "email" => $email,
            "phone" => $phone,
            "designation" => $designation . ", " . $organization,
            "content" => $content,
            "author_image" => $author_image_id,
            "content_image" => NULL,
            "youtube" => $youtube,
            "post_date" => date("Y-m-d"),
            "status" => "Posted"
        ]);

        if ($request->hasFile('author_image')) {
            $file = $request->file('author_image');
            $path = $file->getRealPath();
            Log::debug("save_testimonial - author_image: " . $path);
            $destinationPath = storage_path('uploads');
            $ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $filename = "Testimonial-author-" . $testimonial->id . "." . $ext;
            $file->move($destinationPath, $filename);

            $upload = Upload::create([
                "name" => $filename,
                "path" => $destinationPath . DIRECTORY_SEPARATOR . $filename,
                "extension" => $ext,
                "caption" => "",
                "hash" => "",
                "public" => true,
                "user_id" => 1
            ]);
            $author_image_id = $upload->id;

            // apply unique random hash to file
            while (true) {
                $hash = strtolower(str_random(20));
                if (!Upload::where("hash", $hash)->count()) {
                    $upload->hash = $hash;
                    break;
                }
            }
            $upload->save();
            $testimonial->author_image = $upload->id;

            $testimonial->save();
        } else {
            Log::debug("save_testimonial - author_image not found");
        }

        return redirect("/testimonials");
    }
}
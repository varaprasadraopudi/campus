<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use App\Models\Module;
use App\Models\ModuleFields;
use App\Models\LALog;

use App\Models\Enquiry;

class EnquiriesController extends Controller
{
    public $show_action = true;
    
    /**
     * Display a listing of the Enquiries.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('Enquiries');
        
        if(Module::hasAccess($module->id)) {
            return View('la.enquiries.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('Enquiries'),
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for creating a new enquiry.
     *
     * @return mixed
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created enquiry in database.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Enquiries", "create")) {
            
            $rules = Module::validateRules("Enquiries", $request);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                if(isset($request->quick_add) && $request->quick_add) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Validation error',
                        'errors' => $validator->messages()
                    ]);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            
            $insert_id = Module::insert("Enquiries", $request);

            $enquiry = Enquiry::find($insert_id);

            // Add LALog
            LALog::make("Enquiries.ENQUIRY_CREATED", [
                'title' => "Enquiry Created",
                'module_id' => 'Enquiries',
                'context_id' => $enquiry->id,
                'content' => $enquiry,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'success',
                    'insert_id' => $insert_id
                ]);
            } else {
                return redirect()->route(config('laraadmin.adminRoute') . '.enquiries.index');
            }
        } else {
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ]);
            } else {
                return redirect(config('laraadmin.adminRoute')."/");
            }
        }
    }
    
    /**
     * Display the specified enquiry.
     *
     * @param int $id enquiry ID
     * @return mixed
     */
    public function show($id)
    {
        if(Module::hasAccess("Enquiries", "view")) {
            
            $enquiry = Enquiry::find($id);
            if(isset($enquiry->id)) {
                $module = Module::get('Enquiries');
                $module->row = $enquiry;
                
                return view('la.enquiries.show', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('enquiry', $enquiry);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("enquiry"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for editing the specified enquiry.
     *
     * @param int $id enquiry ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        if(Module::hasAccess("Enquiries", "edit")) {
            $enquiry = Enquiry::find($id);
            if(isset($enquiry->id)) {
                $module = Module::get('Enquiries');
                
                $module->row = $enquiry;
                
                return view('la.enquiries.edit', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                ])->with('enquiry', $enquiry);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("enquiry"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Update the specified enquiry in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id enquiry ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Enquiries", "edit")) {
            if($request->ajax()) {
                $request->merge((array)json_decode($request->getContent()));
            }
            $rules = Module::validateRules("Enquiries", $request, true);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                if($request->ajax()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Validation error',
                        'errors' => $validator->messages()
                    ], 400);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            
            $enquiry_old = Enquiry::find($id);

            if(isset($enquiry_old->id)) {

                // Update Data
                Module::updateRow("Enquiries", $request, $id);
                
                $enquiry_new = Enquiry::find($id);

                // Add LALog
                LALog::make("Enquiries.ENQUIRY_UPDATED", [
                    'title' => "Enquiry Updated",
                    'module_id' => 'Enquiries',
                    'context_id' => $enquiry_new->id,
                    'content' => [
                        'old' => $enquiry_old,
                        'new' => $enquiry_new
                    ],
                    'user_id' => Auth::user()->id,
                    'notify_to' => "[]"
                ]);

                if($request->ajax()) {
                    return response()->json([
                        'status' => 'success',
                        'object' => $enquiry_new,
                        'message' => 'Enquiry updated successfully!',
                        'redirect' => url(config('laraadmin.adminRoute') . '/enquiries')
                    ], 200);
                } else {
                    return redirect()->route(config('laraadmin.adminRoute') . '.enquiries.index');
                }
            } else {
                if($request->ajax()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Record not found'
                    ], 404);
                } else {
                    return view('errors.404', [
                        'record_id' => $id,
                        'record_name' => ucfirst("enquiry"),
                    ]);
                }
            }
            
        } else {
            if($request->ajax()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ], 403);
            } else {
                return redirect(config('laraadmin.adminRoute') . "/");
            }
        }
    }

    /**
     * Update status the specified booking in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id booking ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status_update(Request $request, $id, $status)
    {
        if(Module::hasAccess("Enquiries", "edit")) {
            $enquiry_old = Enquiry::find($id);

            if(isset($enquiry_old->id)) {

                // Update Data
                $enquiry_new = Enquiry::find($id);
                $enquiry_new->status = $status;
                if(!isset($enquiry_new->assigned_to) || $enquiry_new->assigned_to == NULL) {
                    $user = Auth()->user();
                    $context = $user->context();
                    $enquiry_new->assigned_to = $context->id;
                }
                $enquiry_new->save();

                // Add LALog
                LALog::make("Enquiries.ENQUIRY_UPDATED", [
                    'title' => "Enquiry Updated",
                    'module_id' => 'Enquiries',
                    'context_id' => $enquiry_new->id,
                    'content' => [
                        'old' => $enquiry_old,
                        'new' => $enquiry_new
                    ],
                    'user_id' => Auth::user()->id,
                    'notify_to' => "[]"
                ]);

                if($request->ajax()) {
                    return response()->json([
                        'status' => 'success',
                        'object' => $enquiry_new,
                        'message' => 'Enquiry updated successfully!',
                        'redirect' => url(config('laraadmin.adminRoute') . '/enquiries')
                    ], 200);
                } else {
                    return redirect()->route(config('laraadmin.adminRoute') . '.enquiries.index');
                }
            } else {
                if($request->ajax()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Record not found'
                    ], 404);
                } else {
                    return view('errors.404', [
                        'record_id' => $id,
                        'record_name' => ucfirst("booking"),
                    ]);
                }
            }
            
        } else {
            if($request->ajax()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ], 403);
            } else {
                return redirect(config('laraadmin.adminRoute') . "/");
            }
        }
    }
    
    /**
     * Remove the specified enquiry from storage.
     *
     * @param int $id enquiry ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Enquiries", "delete")) {
            $enquiry = Enquiry::find($id);
            $enquiry->delete();

            // Add LALog
            LALog::make("Enquiries.ENQUIRY_DELETED", [
                'title' => "Enquiry Deleted",
                'module_id' => 'Enquiries',
                'context_id' => $enquiry->id,
                'content' => $enquiry,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.enquiries.index');
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Server side Datatable fetch via Ajax
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function dtajax(Request $request)
    {
        $module = Module::get('Enquiries');
        $listing_cols = Module::getListingColumns('Enquiries');
        if(isset($request->date_from) && $request->date_from != "" && isset($request->date_to) && $request->date_to != "") {
            $values = DB::table('enquiries')->whereBetween("created_at", [$request->date_from." 00:00:00", $request->date_to." 00:00:00"])->select($listing_cols)->whereNull('deleted_at');
        } else {
            $values = DB::table('enquiries')->select($listing_cols)->whereNull('deleted_at');
        }
        $out = Datatables::of($values)->make();
        $data = $out->getData();
        
        $fields_popup = ModuleFields::getModuleFields('Enquiries');
        
        for($i = 0; $i < count($data->data); $i++) {

            $enquiry = Enquiry::find($data->data[$i][0]);

            for($j = 0; $j < count($listing_cols); $j++) {
                $col = $listing_cols[$j];
                if(isset($fields_popup[$col]) && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $module->view_col) {
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/enquiries/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
                } else if($col == "status") {
                    if(Module::hasFieldAccess("Enquiries", "status", "write")) {
                        $data->data[$i][$j] = '<a href="#" class="update_field" id="'.$enquiry->id.'" field_name="status" data-type="select" data-pk="1" data-placement="top" data-placeholder="Required" data-original-title="Select Status" data-value="'.$enquiry->status.'">' . $enquiry->status . '</a>'.'<br>'.date("M d", strtotime($enquiry->created_at));
                    }
                }
            }
            
            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Enquiries", "edit")) {
                    // $output .= '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/enquiries/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
                }
                
                if(Module::hasAccess("Enquiries", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.enquiries.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }
        $out->setData($data);
        return $out;
    }
}

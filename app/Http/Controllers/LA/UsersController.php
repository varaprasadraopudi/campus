<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use App\Models\Module;
use App\Models\ModuleFields;

use App\User;
use App\Role;

class UsersController extends Controller
{
	public $show_action = false;
	
	/**
	 * Display a listing of the Users.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Users');
		
		if(Module::hasAccess($module->id)) {
			return View('la.users.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => Module::getListingColumns('Users'),
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Display the specified user.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Users", "view")) {
			$user = User::findOrFail($id);
			$context = $user->context();
			if(isset($user->id) && isset($context->id)) {
				if($context->context_type == "Employee") {
					return redirect(config('laraadmin.adminRoute') . '/employees/'.$context->id);
				} else if($context->context_type == "Customer") {
					return redirect(config('laraadmin.adminRoute') . '/clients/'.$context->id);
				}
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("user"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax(Request $request)
	{
		$module = Module::get('Users');
		$listing_cols = Module::getListingColumns('Users');

		$listing_cols_arr = array();

		foreach($listing_cols as $listing_col) {
			$listing_cols_arr[] = "users.".$listing_col;
		}

		if(isset($request->filter_column) && $request->filter_column == "role_id" && $request->filter_column_value) {
			$values = Role::find($request->filter_column_value)->users()->select($listing_cols_arr);
		} else {
			$values = DB::table('users')->select($listing_cols)->whereNull('deleted_at');
		}
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Users');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($listing_cols); $j++) { 
				$col = $listing_cols[$j];
				if(isset($fields_popup[$col]) && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $module->view_col) {
					$data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="'.url(config('laraadmin.adminRoute') . '/users/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
		}
		$out->setData($data);
		return $out;
	}
}

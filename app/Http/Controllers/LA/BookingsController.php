<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use App\Models\Module;
use App\Models\ModuleFields;
use App\Models\LALog;

use App\Models\Booking;

class BookingsController extends Controller
{
    public $show_action = true;
    
    /**
     * Display a listing of the Bookings.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('Bookings');
        
        if(Module::hasAccess($module->id)) {
            return View('la.bookings.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('Bookings'),
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for creating a new booking.
     *
     * @return mixed
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created booking in database.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Bookings", "create")) {
            
            $rules = Module::validateRules("Bookings", $request);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                if(isset($request->quick_add) && $request->quick_add) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Validation error',
                        'errors' => $validator->messages()
                    ]);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            
            $insert_id = Module::insert("Bookings", $request);

            $booking = Booking::find($insert_id);

            // Add LALog
            LALog::make("Bookings.BOOKING_CREATED", [
                'title' => "Booking Created",
                'module_id' => 'Bookings',
                'context_id' => $booking->id,
                'content' => $booking,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'success',
                    'insert_id' => $insert_id
                ]);
            } else {
                return redirect()->route(config('laraadmin.adminRoute') . '.bookings.index');
            }
        } else {
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ]);
            } else {
                return redirect(config('laraadmin.adminRoute')."/");
            }
        }
    }
    
    /**
     * Display the specified booking.
     *
     * @param int $id booking ID
     * @return mixed
     */
    public function show($id)
    {
        if(Module::hasAccess("Bookings", "view")) {
            
            $booking = Booking::find($id);
            if(isset($booking->id)) {
                $module = Module::get('Bookings');
                $module->row = $booking;
                
                return view('la.bookings.show', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('booking', $booking);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("booking"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for editing the specified booking.
     *
     * @param int $id booking ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        if(Module::hasAccess("Bookings", "edit")) {
            $booking = Booking::find($id);
            if(isset($booking->id)) {
                $module = Module::get('Bookings');
                
                $module->row = $booking;
                
                return view('la.bookings.edit', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                ])->with('booking', $booking);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("booking"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Update the specified booking in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id booking ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Bookings", "edit")) {
            if($request->ajax()) {
                $request->merge((array)json_decode($request->getContent()));
            }
            $rules = Module::validateRules("Bookings", $request, true);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                if($request->ajax()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Validation error',
                        'errors' => $validator->messages()
                    ], 400);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            
            $booking_old = Booking::find($id);

            if(isset($booking_old->id)) {

                // Update Data
                Module::updateRow("Bookings", $request, $id);
                
                $booking_new = Booking::find($id);

                // Add LALog
                LALog::make("Bookings.BOOKING_UPDATED", [
                    'title' => "Booking Updated",
                    'module_id' => 'Bookings',
                    'context_id' => $booking_new->id,
                    'content' => [
                        'old' => $booking_old,
                        'new' => $booking_new
                    ],
                    'user_id' => Auth::user()->id,
                    'notify_to' => "[]"
                ]);

                if($request->ajax()) {
                    return response()->json([
                        'status' => 'success',
                        'object' => $booking_new,
                        'message' => 'Booking updated successfully!',
                        'redirect' => url(config('laraadmin.adminRoute') . '/bookings')
                    ], 200);
                } else {
                    return redirect()->route(config('laraadmin.adminRoute') . '.bookings.index');
                }
            } else {
                if($request->ajax()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Record not found'
                    ], 404);
                } else {
                    return view('errors.404', [
                        'record_id' => $id,
                        'record_name' => ucfirst("booking"),
                    ]);
                }
            }
            
        } else {
            if($request->ajax()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ], 403);
            } else {
                return redirect(config('laraadmin.adminRoute') . "/");
            }
        }
    }

    /**
     * Update status the specified booking in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id booking ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status_update(Request $request, $id, $status)
    {
        if(Module::hasAccess("Bookings", "edit")) {
            $booking_old = Booking::find($id);

            if(isset($booking_old->id)) {

                // Update Data
                $booking_new = Booking::find($id);
                $booking_new->status = $status;
                if(!isset($booking_new->assigned_to) || $booking_new->assigned_to == NULL) {
                    $user = Auth()->user();
                    $context = $user->context();
                    $booking_new->assigned_to = $context->id;
                }
                $booking_new->save();

                // Add LALog
                LALog::make("Bookings.BOOKING_UPDATED", [
                    'title' => "Booking Updated",
                    'module_id' => 'Bookings',
                    'context_id' => $booking_new->id,
                    'content' => [
                        'old' => $booking_old,
                        'new' => $booking_new
                    ],
                    'user_id' => Auth::user()->id,
                    'notify_to' => "[]"
                ]);

                if($request->ajax()) {
                    return response()->json([
                        'status' => 'success',
                        'object' => $booking_new,
                        'message' => 'Booking updated successfully!',
                        'redirect' => url(config('laraadmin.adminRoute') . '/bookings')
                    ], 200);
                } else {
                    return redirect()->route(config('laraadmin.adminRoute') . '.bookings.index');
                }
            } else {
                if($request->ajax()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Record not found'
                    ], 404);
                } else {
                    return view('errors.404', [
                        'record_id' => $id,
                        'record_name' => ucfirst("booking"),
                    ]);
                }
            }
            
        } else {
            if($request->ajax()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ], 403);
            } else {
                return redirect(config('laraadmin.adminRoute') . "/");
            }
        }
    }
    
    /**
     * Remove the specified booking from storage.
     *
     * @param int $id booking ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Bookings", "delete")) {
            $booking = Booking::find($id);
            $booking->delete();

            // Add LALog
            LALog::make("Bookings.BOOKING_DELETED", [
                'title' => "Booking Deleted",
                'module_id' => 'Bookings',
                'context_id' => $booking->id,
                'content' => $booking,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.bookings.index');
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Server side Datatable fetch via Ajax
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function dtajax(Request $request)
    {
        $module = Module::get('Bookings');
        $listing_cols = Module::getListingColumns('Bookings');
        
        if(isset($request->date_from) && $request->date_from != "" && isset($request->date_to) && $request->date_to != "") {
            $values = DB::table('bookings')->whereBetween("created_at", [$request->date_from." 00:00:00", $request->date_to." 00:00:00"])->select($listing_cols)->whereNull('deleted_at');
        } else {
            $values = DB::table('bookings')->select($listing_cols)->whereNull('deleted_at');
        }
        $out = Datatables::of($values)->make();
        $data = $out->getData();
        
        $fields_popup = ModuleFields::getModuleFields('Bookings');
        
        for($i = 0; $i < count($data->data); $i++) {

            $booking = Booking::find($data->data[$i][0]);

            for($j = 0; $j < count($listing_cols); $j++) {
                $col = $listing_cols[$j];
                if(isset($fields_popup[$col]) && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $module->view_col) {
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/bookings/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
                } else if($col == "status") {
                    if(Module::hasFieldAccess("Bookings", "status", "write")) {
                        $data->data[$i][$j] = '<a href="#" class="update_field" id="'.$booking->id.'" field_name="status" data-type="select" data-pk="1" data-placement="top" data-placeholder="Required" data-original-title="Select Status" data-value="'.$booking->status.'">' . $booking->status . '</a>'.'<br>'.date("M d", strtotime($booking->created_at));
                    }
                }
            }
            
            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Bookings", "edit")) {
                    // $output .= '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/bookings/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
                }
                
                if(Module::hasAccess("Bookings", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.bookings.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }
        $out->setData($data);
        return $out;
    }
}

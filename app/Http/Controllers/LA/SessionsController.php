<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use App\Models\Module;
use App\Models\ModuleFields;
use App\Models\LALog;

use App\Models\Session;

class SessionsController extends Controller
{
    public $show_action = true;
    
    /**
     * Display a listing of the Sessions.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('Sessions');
        
        if(Module::hasAccess($module->id)) {
            return View('la.sessions.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('Sessions'),
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for creating a new session.
     *
     * @return mixed
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created session in database.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Sessions", "create")) {
            
            $rules = Module::validateRules("Sessions", $request);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                if(isset($request->quick_add) && $request->quick_add) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Validation error',
                        'errors' => $validator->messages()
                    ]);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            
            $insert_id = Module::insert("Sessions", $request);

            $session = Session::find($insert_id);

            // Add LALog
            LALog::make("Sessions.SESSION_CREATED", [
                'title' => "Session Created",
                'module_id' => 'Sessions',
                'context_id' => $session->id,
                'content' => $session,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'success',
                    'insert_id' => $insert_id
                ]);
            } else {
                return redirect()->route(config('laraadmin.adminRoute') . '.sessions.index');
            }
        } else {
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ]);
            } else {
                return redirect(config('laraadmin.adminRoute')."/");
            }
        }
    }
    
    /**
     * Display the specified session.
     *
     * @param int $id session ID
     * @return mixed
     */
    public function show($id)
    {
        if(Module::hasAccess("Sessions", "view")) {
            
            $session = Session::find($id);
            if(isset($session->id)) {
                $module = Module::get('Sessions');
                $module->row = $session;
                
                return view('la.sessions.show', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('session', $session);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("session"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for editing the specified session.
     *
     * @param int $id session ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        if(Module::hasAccess("Sessions", "edit")) {
            $session = Session::find($id);
            if(isset($session->id)) {
                $module = Module::get('Sessions');
                
                $module->row = $session;
                
                return view('la.sessions.edit', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                ])->with('session', $session);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("session"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Update the specified session in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id session ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Sessions", "edit")) {
            
            $rules = Module::validateRules("Sessions", $request, true);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();;
            }
            
            $session_old = Session::find($id);
            $insert_id = Module::updateRow("Sessions", $request, $id);
            $session_new = Session::find($id);

            // Add LALog
            LALog::make("Sessions.SESSION_UPDATED", [
                'title' => "Session Updated",
                'module_id' => 'Sessions',
                'context_id' => $session_new->id,
                'content' => [
                    'old' => $session_old,
                    'new' => $session_new
                ],
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            return redirect()->route(config('laraadmin.adminRoute') . '.sessions.index');
            
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Remove the specified session from storage.
     *
     * @param int $id session ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Sessions", "delete")) {
            $session = Session::find($id);
            $session->delete();

            // Add LALog
            LALog::make("Sessions.SESSION_DELETED", [
                'title' => "Session Deleted",
                'module_id' => 'Sessions',
                'context_id' => $session->id,
                'content' => $session,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.sessions.index');
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Server side Datatable fetch via Ajax
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function dtajax(Request $request)
    {
        $module = Module::get('Sessions');
        $listing_cols = Module::getListingColumns('Sessions');
        
        $values = DB::table('sessions')->select($listing_cols)->whereNull('deleted_at');
        $out = Datatables::of($values)->make();
        $data = $out->getData();
        
        $fields_popup = ModuleFields::getModuleFields('Sessions');
        
        for($i = 0; $i < count($data->data); $i++) {
            for($j = 0; $j < count($listing_cols); $j++) {
                $col = $listing_cols[$j];
                if(isset($fields_popup[$col]) && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $module->view_col) {
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/sessions/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
                }
                // else if($col == "author") {
                //    $data->data[$i][$j];
                // }
            }
            
            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Sessions", "edit")) {
                    $output .= '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/sessions/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
                }
                
                if(Module::hasAccess("Sessions", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.sessions.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }
        $out->setData($data);
        return $out;
    }
}

<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use App\Models\Module;
use App\Models\ModuleFields;
use App\Models\LALog;

use App\User;
use App\Models\Message;
use App\Events\SocketMessage;

class MessagesController extends Controller
{
    public $show_action = true;
    
    /**
     * Display a listing of the Messages.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('Messages');
        
        if(Module::hasAccess($module->id)) {
            return View('la.messages.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('Messages'),
                'module' => $module,
                'no_header' => true,
                'no_padding' => "no-padding messages",
                'disable_standby_chat' => true
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Create new message in database and broadcast it.
     * Ajax method via Vue.js from Chatting Window / Popups
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Messages", "create")) {
            
            $message = new Message();
            $message->subject = $request->subject;
            $message->content = $request->content;
            $message->from = Auth::user()->id;
            $message->to = $request->to;
            $message->is_received = false;
            $message->save();

            // Add LALog
            LALog::make("Messages.MESSAGE_CREATED", [
                'title' => "Message Created",
                'module_id' => 'Messages',
                'context_id' => $message->id,
                'content' => $message,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);

            event(new SocketMessage(env("LAPLUS_ID"), "UserMessage", $message));
            
            return response()->json([
                'message_id' => $message->id,
                'status' => 'success'
            ]);
            
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'No Message Sending Access'
            ]);
        }
    }

    /**
     * Save Message acknowledgement - From Node.js
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function messageAck($message_id)
    {
        \Log::log('debug', 'MessagesController::messageAck - '.$message_id);

        $message_old = Message::find($message_id);
        $message = Message::find($message_id);
        if(isset($message->id)) {
            
            $message->is_received = true;
            $message->updated_at = date('Y-m-d H:i:s');
            $message->save();

            // Add LALog
            LALog::make("Messages.MESSAGE_RECEIVED", [
                'title' => "Message Received",
                'module_id' => 'Messages',
                'context_id' => $message->id,
                'content' => [
                    'old' => $message_old,
                    'new' => $message
                ],
                'user_id' => $message->to,
                'notify_to' => "[]"
            ]);

            return response()->json([
                'status' => 'success'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Message not found'
            ]);
        }
    }

    /**
     * Get old chat messages of TO User with Auth User
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function get_old_messages($to)
    {
        if(Module::hasAccess("Messages", "view")) {
            
            $from = Auth::user()->id;
            $to = $to;
            $messages = DB::select('select * from messages where (`to` = '.$to.' AND `from` = '.$from.') OR (`to` = '.$from.' AND `from` = '.$to.') order by created_at asc');
            // $messages = Message::where('to', '=', $to)->where('from', '=', $from)->orderBy('created_at', 'desc')->get();
            
            return response()->json([
                'status' => 'success',
                'messages' => $messages
            ]);
            
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'No Message View Access'
            ]);
        }
    }

    /**
     * Get User Context
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function get_user_profile($user_id)
    {
        if(Module::hasAccess("Users", "view")) {
            
            $user = User::find($user_id);
            
            if(isset($user->id)) {
                $context = $user->context();
                return response()->json([
                    'status' => 'success',
                    'user_context' => $context
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'User not found'
                ]);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'No User View Access'
            ]);
        }
    }
    
    /**
     * Remove the specified message from storage.
     *
     * @param int $id message ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Messages", "delete")) {
            $message = Message::find($id);
            $message->delete();

            // Add LALog
            LALog::make("Messages.MESSAGE_DELETED", [
                'title' => "Message Deleted",
                'module_id' => 'Messages',
                'context_id' => $message->id,
                'content' => $message,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.messages.index');
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
}

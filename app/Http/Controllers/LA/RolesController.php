<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Illuminate\Support\Facades\Input;
use App\Models\Module;
use App\Models\ModuleFields;
use App\Models\LALog;
use Zizaco\Entrust\EntrustFacade as Entrust;

use App\Role;
use App\Permission;

class RolesController extends Controller
{
    public $show_action = true;
    
    /**
     * Display a listing of the Roles.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('Roles');
        
        if(Module::hasAccess($module->id)) {
            return View('la.roles.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('Roles'),
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Display a hierarchy of the Roles.
     *
     * @return mixed
     */
    public function hierarchy_view()
    {
        $module = Module::get('Roles');
        $roles = Role::orderBy('id', 'ASC')->orderBy('parent', 'ASC')->get();
        $parent_roles = Role::where('parent',NULL)->orderBy('id', 'ASC')->get();
        if(Module::hasAccess($module->id)) {
            return View('la.roles.hierarchy', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('Roles'),
                'module' => $module,
                'parent_roles' => $parent_roles
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }

    /**
     * Update Menu Hierarchy
     *
     * @return mixed
     */
    public function update_role_hierarchy()
    {
        $parents = Input::get('jsonData');
        $parent_id = null;
        
        for($i = 0; $i < count($parents); $i++) {
            $this->apply_role_hierarchy($parents[$i], $i + 1, $parent_id);
        }
        
        return $parents;
    }
    
    /**
     * Save role hierarchy Recursively
     *
     * @param $roleItem role Item Array
     * @param $num Hierarchy number
     * @param $parent_id Parent ID
     */
    function apply_role_hierarchy($roleItem, $num, $parent_id)
    {
        // echo "apply_hierarchy: ".json_encode($roleItem)." - ".$num." - ".$parent_id."  <br><br>\n\n";
        $role = Role::find($roleItem['id']);
        $role->parent = $parent_id;
        $role->save();
        
        // apply hierarchy to children if exists
        if(isset($roleItem['children'])) {
            for($i = 0; $i < count($roleItem['children']); $i++) {
                $this->apply_role_hierarchy($roleItem['children'][$i], $i + 1, $roleItem['id']);
            }
        }
    }
    
    /**
     * Show the form for creating a new role.
     *
     * @return mixed
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created role in database.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Roles", "create")) {
            
            $rules = Module::validateRules("Roles", $request);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                if(isset($request->quick_add) && $request->quick_add) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Validation error',
                        'errors' => $validator->messages()
                    ]);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            
            $request->name = str_replace(" ", "_", strtoupper(trim($request->name)));

            $insert_id = Module::insert("Roles", $request);

            $modules = Module::all();
			foreach ($modules as $module) {
				Module::setDefaultRoleAccess($module->id, $insert_id, "readonly");
			}

            $role = Role::find($insert_id);
            $perm = Permission::where("name", "ADMIN_PANEL")->first();
			$role->attachPermission($perm);

            // Add LALog
            LALog::make("Roles.ROLE_CREATED", [
                'title' => "Role Created",
                'module_id' => 'Roles',
                'context_id' => $role->id,
                'content' => $role,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            if(isset($request->quick_add) && $request->quick_add) {
				return response()->json([
					'status' => 'success',
					'insert_id' => $insert_id
				]);
			} else {
				return redirect()->route(config('laraadmin.adminRoute') . '.roles.index');
            }
        } else {
            if(isset($request->quick_add) && $request->quick_add) {
				return response()->json([
					'status' => 'error',
					'message' => 'Unauthorized Access'
				]);
			} else {
				return redirect(config('laraadmin.adminRoute')."/");
			}
        }
    }
    
    /**
     * Display the specified role.
     *
     * @param int $id role ID
     * @return mixed
     */
    public function show($id)
    {
        if(Module::hasAccess("Roles", "view")) {
            
            $role = Role::find($id);
            if(isset($role->id)) {
                $module = Module::get('Roles');
                $module->row = $role;
                
                $modules_arr = DB::table('modules')->get();
				$modules_access = array();
				foreach ($modules_arr as $module_obj) {
					$module_obj->accesses = Module::getRoleAccess($module_obj->id, $id)[0];
					$modules_access[] = $module_obj;
				}
                return view('la.roles.show', [
                    'module' => $module,
                    'module_users' => Module::get('Users'),
                    'view_col' => $module->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding",
                    'modules_access' => $modules_access
                ])->with('role', $role);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("role"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for editing the specified role.
     *
     * @param int $id role ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        if(Module::hasAccess("Roles", "edit")) {
            $role = Role::find($id);
            if(isset($role->id)) {
                $module = Module::get('Roles');
                
                $module->row = $role;
                
                return view('la.roles.edit', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                ])->with('role', $role);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("role"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Update the specified role in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id role ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Roles", "edit")) {
            
            $rules = Module::validateRules("Roles", $request, true);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();;
            }
            
            $request->name = str_replace(" ", "_", strtoupper(trim($request->name)));
			
			if($request->name == "SUPER_ADMIN") {
				$request->parent = 1;
			}
            
            $role_old = Role::find($id);
            $insert_id = Module::updateRow("Roles", $request, $id);
            $role_new = Role::find($id);

            // Add LALog
            LALog::make("Roles.ROLE_UPDATED", [
                'title' => "Role Updated",
                'module_id' => 'Roles',
                'context_id' => $role_new->id,
                'content' => [
                    'old' => $role_old,
                    'new' => $role_new
                ],
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            return redirect()->route(config('laraadmin.adminRoute') . '.roles.index');
            
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Remove the specified role from storage.
     *
     * @param int $id role ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Roles", "delete")) {
            $role = Role::find($id);
            $role->delete();

            // Add LALog
            LALog::make("Roles.ROLE_DELETED", [
                'title' => "Role Deleted",
                'module_id' => 'Roles',
                'context_id' => $role->id,
                'content' => $role,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.roles.index');
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Server side Datatable fetch via Ajax
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function dtajax(Request $request)
    {
        $module = Module::get('Roles');
        $listing_cols = Module::getListingColumns('Roles');
        
        $values = DB::table('roles')->select($listing_cols)->whereNull('deleted_at');
        $out = Datatables::of($values)->make();
        $data = $out->getData();
        
        $fields_popup = ModuleFields::getModuleFields('Roles');
        
        for($i = 0; $i < count($data->data); $i++) {
            for($j = 0; $j < count($listing_cols); $j++) {
                $col = $listing_cols[$j];
                if(isset($fields_popup[$col]) && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $module->view_col) {
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/roles/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
                }
                // else if($col == "author") {
                //    $data->data[$i][$j];
                // }
            }
            
            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Roles", "edit")) {
                    $output .= '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/roles/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
                }
                
                if(Module::hasAccess("Roles", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.roles.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }
        $out->setData($data);
        return $out;
    }

    public function save_module_role_permissions(Request $request, $id)
	{
		if(Entrust::hasRole('SUPER_ADMIN')) {
			$role = Role::find($id);
			$module = Module::get('Roles');
			$module->row = $role;
			
			$modules_arr = DB::table('modules')->get();
			$modules_access = array();
			foreach ($modules_arr as $module_obj) {
				$module_obj->accesses = Module::getRoleAccess($module_obj->id, $id)[0];
				$modules_access[] = $module_obj;
			}
		
			$now = date("Y-m-d H:i:s");
			
			foreach($modules_access as $module) {
				
				/* =============== role_module_fields =============== */
	
				foreach ($module->accesses->fields as $field) {
					$field_name = $field['colname'].'_'.$module->id.'_'.$role->id;
					$field_value = $request->$field_name;
					if($field_value == 0) {
						$access = 'invisible';
					} else if($field_value == 1) {
						$access = 'readonly';
					} else if($field_value == 2) {
						$access = 'write';
					} 
	
					$query = DB::table('role_module_fields')->where('role_id', $role->id)->where('field_id', $field['id']);
					if($query->count() == 0) {
						DB::insert('insert into role_module_fields (role_id, field_id, access, created_at, updated_at) values (?, ?, ?, ?, ?)', [$role->id, $field['id'], $access, $now, $now]);    
					} else {
						DB:: table('role_module_fields')->where('role_id', $role->id)->where('field_id', $field['id'])->update(['access' => $access]);
					}
				}
				
				/* =============== role_module =============== */
	
				$module_name = 'module_'.$module->id;
				if(isset($request->$module_name)) {
					$view = 'module_view_'.$module->id;
					$create = 'module_create_'.$module->id;
					$edit = 'module_edit_'.$module->id;
					$delete = 'module_delete_'.$module->id;
					if(isset($request->$view)) {
						$view = 1;
					} else {
						$view = 0;
					}
					if(isset($request->$create)) {
						$create = 1;
					} else {
						$create = 0;
					}
					if(isset($request->$edit)) {
						$edit = 1;
					} else {
						$edit = 0;
					}
					if(isset($request->$delete)) {
						$delete = 1;
					} else {
						$delete = 0;
					}
					
					$query = DB::table('role_module')->where('role_id', $id)->where('module_id', $module->id);
					if($query->count() == 0) {
						DB::insert('insert into role_module (role_id, module_id, acc_view, acc_create, acc_edit, acc_delete, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?)', [$id, $module->id, $view, $create, $edit, $delete, $now, $now]);
					} else {
						DB:: table('role_module')->where('role_id', $id)->where('module_id', $module->id)->update(['acc_view' => $view, 'acc_create' => $create, 'acc_edit' => $edit, 'acc_delete' => $delete]);
					}
				} else {
					DB:: table('role_module')->where('role_id', $id)->where('module_id', $module->id)->update(['acc_view' => 0, 'acc_create' => 0, 'acc_edit' => 0, 'acc_delete' => 0]);
				}
			}
        	return redirect(config('laraadmin.adminRoute') . '/roles/'.$id.'#tab-access');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
}

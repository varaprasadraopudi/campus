<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use App\Models\Module;
use App\Models\ModuleFields;
use App\Models\LALog;

use App\Models\Testimonial;

class TestimonialsController extends Controller
{
    public $show_action = true;
    
    /**
     * Display a listing of the Testimonials.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('Testimonials');
        
        if(Module::hasAccess($module->id)) {
            return View('la.testimonials.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('Testimonials'),
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for creating a new testimonial.
     *
     * @return mixed
     */
    public function create()
    {
        //
    }
    
    /**
     * Store a newly created testimonial in database.
     *
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Testimonials", "create")) {
            
            $rules = Module::validateRules("Testimonials", $request);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                if(isset($request->quick_add) && $request->quick_add) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Validation error',
                        'errors' => $validator->messages()
                    ]);
                } else {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            
            $insert_id = Module::insert("Testimonials", $request);

            $testimonial = Testimonial::find($insert_id);

            // Add LALog
            LALog::make("Testimonials.TESTIMONIAL_CREATED", [
                'title' => "Testimonial Created",
                'module_id' => 'Testimonials',
                'context_id' => $testimonial->id,
                'content' => $testimonial,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'success',
                    'insert_id' => $insert_id
                ]);
            } else {
                return redirect()->route(config('laraadmin.adminRoute') . '.testimonials.index');
            }
        } else {
            if(isset($request->quick_add) && $request->quick_add) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ]);
            } else {
                return redirect(config('laraadmin.adminRoute')."/");
            }
        }
    }
    
    /**
     * Display the specified testimonial.
     *
     * @param int $id testimonial ID
     * @return mixed
     */
    public function show($id)
    {
        if(Module::hasAccess("Testimonials", "view")) {
            
            $testimonial = Testimonial::find($id);
            if(isset($testimonial->id)) {
                $module = Module::get('Testimonials');
                $module->row = $testimonial;
                
                return view('la.testimonials.show', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('testimonial', $testimonial);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("testimonial"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Show the form for editing the specified testimonial.
     *
     * @param int $id testimonial ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        if(Module::hasAccess("Testimonials", "edit")) {
            $testimonial = Testimonial::find($id);
            if(isset($testimonial->id)) {
                $module = Module::get('Testimonials');
                
                $module->row = $testimonial;
                
                return view('la.testimonials.edit', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                ])->with('testimonial', $testimonial);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("testimonial"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Update the specified testimonial in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id testimonial ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Testimonials", "edit")) {
            
            $rules = Module::validateRules("Testimonials", $request, true);
            
            $validator = Validator::make($request->all(), $rules);
            
            if($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();;
            }
            
            $testimonial_old = Testimonial::find($id);
            $insert_id = Module::updateRow("Testimonials", $request, $id);
            $testimonial_new = Testimonial::find($id);

            // Add LALog
            LALog::make("Testimonials.TESTIMONIAL_UPDATED", [
                'title' => "Testimonial Updated",
                'module_id' => 'Testimonials',
                'context_id' => $testimonial_new->id,
                'content' => [
                    'old' => $testimonial_old,
                    'new' => $testimonial_new
                ],
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            return redirect()->route(config('laraadmin.adminRoute') . '.testimonials.index');
            
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }

    /**
     * Update status the specified Testimonial in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id Testimonial ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function status_update(Request $request, $id, $status)
    {
        if(Module::hasAccess("Testimonials", "edit")) {
            $testimonial_old = Testimonial::find($id);

            if(isset($testimonial_old->id)) {

                // Update Data
                $testimonial_new = Testimonial::find($id);
                $testimonial_new->status = $status;
                $testimonial_new->save();

                // Add LALog
                LALog::make("Testimonials.TESTIMONIAL_UPDATED", [
                    'title' => "Testimonial Updated",
                    'module_id' => 'Testimonials',
                    'context_id' => $testimonial_new->id,
                    'content' => [
                        'old' => $testimonial_old,
                        'new' => $testimonial_new
                    ],
                    'user_id' => Auth::user()->id,
                    'notify_to' => "[]"
                ]);

                if($request->ajax()) {
                    return response()->json([
                        'status' => 'success',
                        'object' => $testimonial_new,
                        'message' => 'Testimonial updated successfully!',
                        'redirect' => url(config('laraadmin.adminRoute') . '/testimonials')
                    ], 200);
                } else {
                    return redirect()->route(config('laraadmin.adminRoute') . '.testimonials.index');
                }
            } else {
                if($request->ajax()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Record not found'
                    ], 404);
                } else {
                    return view('errors.404', [
                        'record_id' => $id,
                        'record_name' => ucfirst("testimonial"),
                    ]);
                }
            }
            
        } else {
            if($request->ajax()) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Unauthorized Access'
                ], 403);
            } else {
                return redirect(config('laraadmin.adminRoute') . "/");
            }
        }
    }
    
    /**
     * Remove the specified testimonial from storage.
     *
     * @param int $id testimonial ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Testimonials", "delete")) {
            $testimonial = Testimonial::find($id);
            $testimonial->delete();

            // Add LALog
            LALog::make("Testimonials.TESTIMONIAL_DELETED", [
                'title' => "Testimonial Deleted",
                'module_id' => 'Testimonials',
                'context_id' => $testimonial->id,
                'content' => $testimonial,
                'user_id' => Auth::user()->id,
                'notify_to' => "[]"
            ]);
            
            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.testimonials.index');
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Server side Datatable fetch via Ajax
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function dtajax(Request $request)
    {
        $module = Module::get('Testimonials');
        $listing_cols = Module::getListingColumns('Testimonials');
        
        $values = DB::table('testimonials')->select($listing_cols)->whereNull('deleted_at');
        $out = Datatables::of($values)->make();
        $data = $out->getData();
        
        $fields_popup = ModuleFields::getModuleFields('Testimonials');
        
        for($i = 0; $i < count($data->data); $i++) {

            $testimonial = Testimonial::find($data->data[$i][0]);

            for($j = 0; $j < count($listing_cols); $j++) {
                $col = $listing_cols[$j];
                if(isset($fields_popup[$col]) && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                if($col == $module->view_col) {
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/testimonials/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
                } else if($col == "status") {
                    if(Module::hasFieldAccess("Testimonials", "status", "write")) {
                        $data->data[$i][$j] = '<a href="#" class="update_field" id="'.$testimonial->id.'" field_name="status" data-type="select" data-pk="1" data-placement="top" data-placeholder="Required" data-original-title="Select Status" data-value="'.$testimonial->status.'">' . $testimonial->status . '</a>';
                    }
                }
            }
            
            if($this->show_action) {
                $output = '';
                if(Module::hasAccess("Testimonials", "edit")) {
                    $output .= '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/testimonials/' . $data->data[$i][0] . '/edit') . '" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></a>';
                }
                
                if(Module::hasAccess("Testimonials", "delete")) {
                    $output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.testimonials.destroy', $data->data[$i][0]], 'method' => 'delete', 'style' => 'display:inline']);
                    $output .= ' <button class="btn btn-danger btn-xs" type="submit" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i></button>';
                    $output .= Form::close();
                }
                $data->data[$i][] = (string)$output;
            }
        }
        $out->setData($data);
        return $out;
    }
}

<?php
/**
 * Controller generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use App\Models\Module;
use App\Models\ModuleFields;

use App\Models\LALog;

class LALogsController extends Controller
{
    public $show_action = false;
    
    /**
     * Display a listing of the LALogs.
     *
     * @return mixed
     */
    public function index()
    {
        $module = Module::get('LALogs');
        
        if(Module::hasAccess($module->id)) {
            return View('la.lalogs.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => Module::getListingColumns('LALogs'),
                'module' => $module
            ]);
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
    }
    
    /**
     * Display the specified lalog.
     *
     * @param int $id lalog ID
     * @return mixed
     */
    public function show($id)
    {
        return redirect()->route(config('laraadmin.adminRoute') . '.lalogs.index');
        /*
        if(Module::hasAccess("LALogs", "view")) {
            
            $lalog = LALog::find($id);
            if(isset($lalog->id)) {
                $module = Module::get('LALogs');
                $module->row = $lalog;
                
                return view('la.lalogs.show', [
                    'module' => $module,
                    'view_col' => $module->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('lalog', $lalog);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("lalog"),
                ]);
            }
        } else {
            return redirect(config('laraadmin.adminRoute') . "/");
        }
        */
    }
    
    /**
     * Server side Datatable fetch via Ajax
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function dtajax(Request $request)
    {
        $module = Module::get('LALogs');
        $listing_cols = Module::getListingColumns('LALogs');
        $listing_cols[] = "created_at";
        
        $values = DB::table('lalogs')->select($listing_cols)->whereNull('deleted_at');
        $out = Datatables::of($values)->make();
        $data = $out->getData();
        
        $fields_popup = ModuleFields::getModuleFields('LALogs');
        
        for($i = 0; $i < count($data->data); $i++) {
            for($j = 0; $j < count($listing_cols); $j++) {
                $col = $listing_cols[$j];
                $popup_value = $data->data[$i][$j];
                if(isset($fields_popup[$col]) && starts_with($fields_popup[$col]->popup_vals, "@")) {
                    $data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
                }
                /*
                if($col == $module->view_col) {
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/lalogs/' . $data->data[$i][0]) . '">' . $data->data[$i][$j] . '</a>';
                }
                */
                if($col == "id") {
                    $data->data[$i][$j] = $data->data[$i][$j]." <a class='col_exp' log_id='".$data->data[$i][$j]."'></a>";
                } else if($col == "user_id") {
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/users/' . $popup_value) . '">' . $data->data[$i][$j] . '</a>';
                } else if($col == "module_id" && isset($popup_value) && $popup_value != 0) {
                    $module = Module::find($popup_value);
                    $data->data[$i][$j] = '<a '.config('laraadmin.ajaxload').' href="' . url(config('laraadmin.adminRoute') . '/' . $module->name_db) . '">' . $data->data[$i][$j] . '</a>';
                } else if($col == "notify_to") {
                    if($data->data[$i][$j] != "[]") {

                    } else {
                        $data->data[$i][$j] = "";
                    }
                }
            }
            
            if($this->show_action) {
                $output = '';
                
                $data->data[$i][] = (string)$output;
            }
        }
        $out->setData($data);
        return $out;
    }

    public function get_lalog_details(Request $request, $id)
    {
        $lalog = LALog::find($id);
        
        return response()->json([
            'status' => 'success',
            'lalog' => $lalog
        ]);
    }
}

<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App;

use Zizaco\Entrust\EntrustRole;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\LALog;

class Role extends EntrustRole
{
    use SoftDeletes;
    use Searchable;
    
    protected $table = 'roles';
    
    protected $hidden = [
    
    ];
    
    protected $guarded = [];
    
    protected $dates = ['deleted_at'];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'roles_index';
    }

    /**
     * Get Roles with matching context type - Employee / Customer
     *
     * @return array
     */
    public static function ctype($type, $return_type = "Object") {
        $empRoles = Role::where("context_type", $type)->get();
        if($return_type == "Object") {
            return $empRoles;
        } else if($return_type == "Name") {
            $rolesArray = array();
            foreach ($empRoles as $role) {
                $rolesArray[] = $role->name;
            }
            return $rolesArray;
        } else {
            return array();
        }
    }

    /**
     * @return mixed
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Get all events happened on Module
     *
     * @return mixed
     */
    public function timeline()
    {
        $moduleConfigs = config('laraadmin.log.Roles');
        $moduleConfigsArr = array_keys($moduleConfigs);
        return LALog::where("context_id", $this->id)->whereIn("type", $moduleConfigsArr)->orderBy("created_at", "desc")->get();
    }
}

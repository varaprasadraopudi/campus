<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Laravel\Scout\Searchable;

use App\Models\Employee;
use App\Models\Customer;

class User extends Authenticatable implements AuthorizableContract, CanResetPasswordContract
{
    use Notifiable;
    use CanResetPassword;
    // use SoftDeletes;
    use EntrustUserTrait;
    use Searchable;

    protected $table = 'users';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "role", "context_id", "type"
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    // protected $dates = ['deleted_at'];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'users_index';
    }

    /**
     * @return mixed
     */
    public function lalogs()
    {
        return $this->belongsToMany('App\Models\LALog', 'lalog_user', 'user_id', 'lalog_id');
    }

    /**
     * @return mixed
     */
    public function uploads()
    {
        return $this->hasMany('App\Models\Upload');
    }

    /**
     * Get context object return for User - Employee / Customer
     *
     * @return Object / NULL
     */
    public function context()
    {
        if($this->hasRole(Role::ctype('Employee', 'Name'))) {
            $employee = Employee::find($this->context_id);
            $employee->context_type = "Employee";
            return $employee;
        } else if($this->hasRole(Role::ctype('Customer', 'Name'))) {
            $customer = Customer::find($this->context_id);
            $customer->context_type = "Customer";
            return $customer;
        }
        return null;
    }

    /**
     * @return mixed
     */
    public static function get($context_id, $context_type)
    {
        $users = User::where('context_id', $context_id)->get();
        foreach ($users as $user) {
            if($user->hasRole(Role::ctype($context_type,'Name'))) {
                return $user;
            }
        }
        return NULL;
    }

    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
}

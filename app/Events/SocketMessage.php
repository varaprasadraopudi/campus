<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\Message;

class SocketMessage implements ShouldBroadcast
{
    use InteractsWithSockets, SerializesModels;

    public $laplus_id;
    public $type;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($laplus_id, $type, Message $message)
    {
        $this->laplus_id = $laplus_id;
        $this->type = $type;
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        \Log::log('debug', 'SocketMessage::broadcastOn - laraadmin-channel'.' - '.json_encode($this));

        return new PrivateChannel('laraadmin-channel');
    }
}

<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Observers;

use Log;
use App\Models\Module;
use App\Models\ModuleFields;
use Illuminate\Support\Facades\DB;

use App\Models\Testimonial;


class TestimonialObserver
{
    /**
     * Listen to the Record deleting event.
     *
     * @param  Upload  $upload
     * @return void
     */
    public function deleting(Testimonial $testimonial)
    {
        return Module::clearMultiselects('Testimonials', $testimonial->id);
    }
}
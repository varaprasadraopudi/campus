<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Observers;

use Log;
use App\Models\Module;
use App\Models\ModuleFields;
use Illuminate\Support\Facades\DB;

use App\Models\Department;

class DepartmentObserver
{
    /**
     * Listen to the Record deleting event.
     *
     * @param  Department  $department
     * @return void
     */
    public function deleting(Department $department)
    {
        return Module::clearMultiselects('Departments', $department->id);
    }
}
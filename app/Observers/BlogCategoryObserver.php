<?php
/**
 * Model generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

namespace App\Observers;

use Log;
use App\Models\Module;
use App\Models\ModuleFields;
use Illuminate\Support\Facades\DB;

use App\Models\BlogCategory;

class BlogCategoryObserver
{
    /**
     * Listen to the Record deleting event.
     *
     * @param  BlogCategory  $blogcategory
     * @return void
     */
    public function deleting(BlogCategory $blogcategory)
    {
        return Module::clearMultiselects('Blog_categories', $blogcategory->id);
    }
}
![LaraAdmin Plus 1.0](http://laraadmin.com/img/laraadmin-256.png)
# LaraAdmin Plus 1.0

LaraAdmin Plus is a unique Laravel Admin Panel for quick-start Admin based applications and boilerplate for CRM or CMS systems.

##### Website: [laraadmin.com](http://laraadmin.com)
##### Creator: [Ganesh Bhosale](https://github.com/gdbhosale), [Dwij IT Solutions](http://dwijitsolutions.com)

--------

### Installation:

```
composer install
```

Configure Databse in .env file. Run following command (Dont miss to Seed).

```
php artisan migrate:refresh --seed
```

Your installation is Done.

### How to code ?

We recommend you to use [Visual Studio Code](https://code.visualstudio.com) version 1.18 above and git to work flawlessly with LaraAdmin.

#### Required VSCode Plugins:
- [Git Lens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [Laravel Blade Snippets](https://marketplace.visualstudio.com/items?itemName=onecentlin.laravel-blade)
- [PHP Intelphense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client)

#### Brought to you by<br>
![Dwij IT Solutions](http://dwijitsolutions.com/wp-content/uploads/2016/01/dwij-it-solutions.png)
# Dwij IT Solutions

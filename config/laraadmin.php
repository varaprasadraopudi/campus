<?php
/**
 * Config generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    
	/*
    |--------------------------------------------------------------------------
    | General Configuration
    |--------------------------------------------------------------------------
    */
    
    'adminRoute' => 'admin',
    'blogRoute' => 'blog',
    'ajaxload' => 'data-pjax', // Ajax Page load via Pjax Library. Keep blank to disable
    
    /*
    |--------------------------------------------------------------------------
    | Uploads Configuration
    |--------------------------------------------------------------------------
    |
    | private_uploads: Uploaded file remains private and can be seen by respective owners + Super Admin only
    | default_public: Will make default uploads public / private
	| allow_filename_change: allows user to modify filenames after upload. Changes will be only in Database not on actual files.
    | 
    */
    
    'uploads' => [
        'private_uploads' => false,
        'default_public' => true,
        'allow_filename_change' => true
    ],

    /*
    |--------------------------------------------------------------------------
    | Log Types
    |--------------------------------------------------------------------------
    */

    'log' => [
        
        // Users
        'Users' => [
            'USER_CREATED' => ["name" => "User Created", "is_notif" => "false", "code" => "USER_CREATED", "module" => "Users"],
            'USER_UPDATED' => ["name" => "User Updated", "is_notif" => "false", "code" => "USER_UPDATED", "module" => "Users"],
            'USER_DELETED' => ["name" => "User Deleted", "is_notif" => "false", "code" => "USER_DELETED", "module" => "Users"],
            
            'USER_LOGIN' => ["name" => "User Login", "is_notif" => "false", "code" => "USER_LOGIN", "module" => "Users"],
            'USER_LOGOUT' => ["name" => "User Logout", "is_notif" => "false", "code" => "USER_LOGOUT", "module" => "Users"],
        ],

        // Employees
        'Employees' => [
            'EMPLOYEE_CREATED' => ["name" => "Employee Created", "is_notif" => "false", "code" => "EMPLOYEE_CREATED", "module" => "Employees"],
            'EMPLOYEE_UPDATED' => ["name" => "Employee Updated", "is_notif" => "false", "code" => "EMPLOYEE_UPDATED", "module" => "Employees"],
            'EMPLOYEE_DELETED' => ["name" => "Employee Deleted", "is_notif" => "false", "code" => "EMPLOYEE_DELETED", "module" => "Employees"],
        ],

        // Customers
        'Customers' => [
            'CUSTOMER_CREATED' => ["name" => "Customer Created", "is_notif" => "false", "code" => "CUSTOMER_CREATED", "module" => "Customers"],
            'CUSTOMER_UPDATED' => ["name" => "Customer Updated", "is_notif" => "false", "code" => "CUSTOMER_UPDATED", "module" => "Customers"],
            'CUSTOMER_DELETED' => ["name" => "Customer Deleted", "is_notif" => "false", "code" => "CUSTOMER_DELETED", "module" => "Customers"],
        ],

        // Uploads
        'Uploads' => [
            'UPLOAD_CREATED' => ["name" => "Upload Created", "is_notif" => "false", "code" => "UPLOAD_CREATED", "module" => "Uploads"],
            'UPLOAD_UPDATED' => ["name" => "Upload Updated", "is_notif" => "false", "code" => "UPLOAD_UPDATED", "module" => "Uploads"],
            'UPLOAD_DELETED' => ["name" => "Upload Deleted", "is_notif" => "false", "code" => "UPLOAD_DELETED", "module" => "Uploads"],
        ],

        // Backups
        'Backups' => [
            'BACKUP_CREATED' => ["name" => "Backup Created", "is_notif" => "false", "code" => "BACKUP_CREATED", "module" => "Backups"],
            'BACKUP_DELETED' => ["name" => "Backup Deleted", "is_notif" => "false", "code" => "BACKUP_DELETED", "module" => "Backups"],
        ],

        // Departments
        'Departments' => [
            'DEPARTMENT_CREATED' => ["name" => "Department Created", "is_notif" => "false", "code" => "DEPARTMENT_CREATED", "module" => "Departments"],
            'DEPARTMENT_UPDATED' => ["name" => "Department Updated", "is_notif" => "false", "code" => "DEPARTMENT_UPDATED", "module" => "Departments"],
            'DEPARTMENT_DELETED' => ["name" => "Department Deleted", "is_notif" => "false", "code" => "DEPARTMENT_DELETED", "module" => "Departments"],
        ],

        // Roles
        'Roles' => [
            'ROLE_CREATED' => ["name" => "Role Created", "is_notif" => "false", "code" => "ROLE_CREATED", "module" => "Roles"],
            'ROLE_UPDATED' => ["name" => "Role Updated", "is_notif" => "false", "code" => "ROLE_UPDATED", "module" => "Roles"],
            'ROLE_DELETED' => ["name" => "Role Deleted", "is_notif" => "false", "code" => "ROLE_DELETED", "module" => "Roles"],
            'MENU_ROLE_ATTACHED' => ["name" => "Menu role Attached", "is_notif" => "false", "code" => "MENU_ROLE_ATTACHED", "module" => "Roles"],
            'MENU_ROLE_DETACHED' => ["name" => "Menu role Detached", "is_notif" => "false", "code" => "MENU_ROLE_DETACHED", "module" => "Roles"],
        ],

        // Permissions
        'Permissions' => [
            'PERMISSION_CREATED' => ["name" => "Permission Created", "is_notif" => "false", "code" => "PERMISSION_CREATED", "module" => "Permissions"],
            'PERMISSION_UPDATED' => ["name" => "Permission Updated", "is_notif" => "false", "code" => "PERMISSION_UPDATED", "module" => "Permissions"],
            'PERMISSION_DELETED' => ["name" => "Permission Deleted", "is_notif" => "false", "code" => "PERMISSION_DELETED", "module" => "Permissions"],
        ],

        // Messages
        'Messages' => [
            'MESSAGE_CREATED' => ["name" => "Message Created", "is_notif" => "false", "code" => "MESSAGE_CREATED", "module" => "Messages"],
            'MESSAGE_RECEIVED' => ["name" => "Message Received", "is_notif" => "false", "code" => "MESSAGE_RECEIVED", "module" => "Messages"],
            'MESSAGE_DELETED' => ["name" => "Message Deleted", "is_notif" => "false", "code" => "MESSAGE_DELETED", "module" => "Messages"],
        ],

        // Blog_categories
        'Blog_categories' => [
            'BLOG_CATEGORY_CREATED' => ["name" => "Blog category Created", "is_notif" => "false", "code" => "BLOG_CATEGORY_CREATED", "module" => "Blog_categories"],
            'BLOG_CATEGORY_UPDATED' => ["name" => "Blog category Updated", "is_notif" => "false", "code" => "BLOG_CATEGORY_UPDATED", "module" => "Blog_categories"],
            'BLOG_CATEGORY_DELETED' => ["name" => "Blog category Deleted", "is_notif" => "false", "code" => "BLOG_CATEGORY_DELETED", "module" => "Blog_categories"],
        ],

        // Blog_posts
        'Blog_posts' => [
            'BLOG_POST_CREATED' => ["name" => "Blog post Created", "is_notif" => "false", "code" => "BLOG_POST_CREATED", "module" => "Blog_posts"],
            'BLOG_POST_UPDATED' => ["name" => "Blog post Updated", "is_notif" => "false", "code" => "BLOG_POST_UPDATED", "module" => "Blog_posts"],
            'BLOG_POST_DELETED' => ["name" => "Blog post Deleted", "is_notif" => "false", "code" => "BLOG_POST_DELETED", "module" => "Blog_posts"],
        ],

        // Sessions
        'Sessions' => [
            'SESSION_CREATED' => ["name" => "Session Created", "is_notif" => "false", "code" => "SESSION_CREATED", "module" => "Sessions"],
            'SESSION_UPDATED' => ["name" => "Session Updated", "is_notif" => "false", "code" => "SESSION_UPDATED", "module" => "Sessions"],
            'SESSION_DELETED' => ["name" => "Session Deleted", "is_notif" => "false", "code" => "SESSION_DELETED", "module" => "Sessions"],
        ],

        // Testimonials
        'Testimonials' => [
            'TESTIMONIAL_CREATED' => ["name" => "Testimonial Created", "is_notif" => "false", "code" => "TESTIMONIAL_CREATED", "module" => "Testimonials"],
            'TESTIMONIAL_UPDATED' => ["name" => "Testimonial Updated", "is_notif" => "false", "code" => "TESTIMONIAL_UPDATED", "module" => "Testimonials"],
            'TESTIMONIAL_DELETED' => ["name" => "Testimonial Deleted", "is_notif" => "false", "code" => "TESTIMONIAL_DELETED", "module" => "Testimonials"],
        ],

        // Bookings
        'Bookings' => [
            'BOOKING_CREATED' => ["name" => "Booking Created", "is_notif" => "false", "code" => "BOOKING_CREATED", "module" => "Bookings"],
            'BOOKING_UPDATED' => ["name" => "Booking Updated", "is_notif" => "false", "code" => "BOOKING_UPDATED", "module" => "Bookings"],
            'BOOKING_DELETED' => ["name" => "Booking Deleted", "is_notif" => "false", "code" => "BOOKING_DELETED", "module" => "Bookings"],
        ],

        // Enquiries
        'Enquiries' => [
            'ENQUIRY_CREATED' => ["name" => "Enquiry Created", "is_notif" => "false", "code" => "ENQUIRY_CREATED", "module" => "Enquiries"],
            'ENQUIRY_UPDATED' => ["name" => "Enquiry Updated", "is_notif" => "false", "code" => "ENQUIRY_UPDATED", "module" => "Enquiries"],
            'ENQUIRY_DELETED' => ["name" => "Enquiry Deleted", "is_notif" => "false", "code" => "ENQUIRY_DELETED", "module" => "Enquiries"],
        ],

        // More LALogs Configurations - Do not edit this line
    ]
];
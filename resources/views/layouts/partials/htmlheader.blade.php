<head>
	<meta charset="utf-8">
    <title>@yield('meta_title', 'Kolad Adventure Camp | Resort near Mumbai, Pune - Empower Activity Camps')</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="@yield('meta_description', 'Veteran ARMY Officer\'s venture Empower Activity Camps specializes in ‘Corporate training’ in Outdoor Environment, 50 acres Adventure Camp near Mumbai and Pune.')">
	<meta name="keywords" content="@yield('meta_keywords', 'corporate training, corporate offsite, empower camp, mumbai, kolad')">
    <meta name="author" content="Empower Activity Camps">

    <meta property="og:title" content="No.1 Training & Adventure Company with Own Resort in Kolad" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="@yield('meta_description', 'Veteran ARMY Officer\'s venture Empower Activity Camps specializes in ‘Corporate training’ in Outdoor Environment, 50 acres Adventure Camp near Mumbai and Pune.')" />
    
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css?v=4.4.1') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/slider-pro.min.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/superfish.css') }}" media="all" /> 
	<link rel="stylesheet" href="{{ asset('css/datepicker.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/timepicker.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/jquery.navgoco.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}" media="all" />
	<link rel="stylesheet" href="{{ asset('css/style.css?') . File::lastModified(base_path('css/style.css')) }}" media="all" >
	<link rel="stylesheet" href="{{ asset('css/custom.css?') . File::lastModified(base_path('css/custom.css')) }}" media="all" >
    <link rel="shortcut icon" href="{{ asset('img/icons/favicon.ico') }}">
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MLP2BN6');</script>
    <!-- End Google Tag Manager -->

    <meta name="google-site-verification" content="zlzf4w5Gv2xhNXymGNhqJsJGIV326AyPahll0jCz0JM" />
    
    @stack('styles')
</head>
{{-- <li><a href="{{ url('/') }}">Home</a></li> --}}
<li>
    <a href="{{ url('/about-us') }}">About Us</a>
    <ul>
        <li>
            <a href="{{ url('/about-us#our-team') }}">Our Team</a>
            <ul>
                <li><a href="{{ url('/col-naval-kohli') }}">Col. Naval Kohli</a></li>
                <li><a href="#" 2href="{{ url('/anil-bhasin') }}">Anil Bhasin</a></li>
            </ul>
        </li>
        <li><a href="{{ url('/about-us#media-coverage') }}">Media Coverage</a></li>
        <li>
            <a href="{{ url('/facilities') }}">Facilities</a>
            <ul>
                <li><a href="{{ url('/facilities#ac-cottages') }}">AC Cottages</a></li>
                <li><a href="{{ url('/facilities#swiss-cottage-tents') }}">Swiss Cottage Tents</a></li>
                <li><a href="{{ url('/facilities#dormitory-accommodation') }}">Dormitory Accommodation</a></li>
                <li><a href="{{ url('/facilities#gazebo-the-hub') }}">Gazebo-The Hub</a></li>
                <li><a href="{{ url('/facilities#camp-activity-area') }}">Camp Activity Area</a></li>
                <li><a href="{{ url('/facilities#conference-hall') }}">Conference Hall</a></li>
                <li><a href="{{ url('/facilities#safety-measures') }}">Safety Measures</a></li>
            </ul>
        </li>
    </ul>
</li>
<li>
    <a href="#">Trainings</a>
    <ul>
        <li>
            <a href="#">Corporate Training</a>
            <ul style="width:320px;">
                <li style="width:320px;"><a href="{{ url('/corporate-outbound-training') }}">Corporate Outbound Training</a></li>
                <li style="width:320px;"><a href="{{ url('/leadership-outbound-training') }}">Leadership Outbound Training</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#corporate-camps') }}">College to Corporate Programmes</a></li>
                <li style="width:320px;"><a href="{{ url('/team-building-outbound-training') }}">Team Building Outbound Training</a></li>
                <li style="width:320px;"><a href="{{ url('/induction-outbound-camps') }}">Induction Outbound Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/summer-camp') }}">Children's Camp</a></li>
{{--                <li style="width:320px;"><a href="#" 2href="{{ url('/corporate-social-responsibility') }}">Corporate Social Responsibility Camps</a></li>--}}
            </ul>
        </li>
        <li>
            <a href="{{ url('/empowering-students') }}">Empowering Students</a>
            <ul style="width:320px;">
                <li style="width:320px;"><a href="{{ url('/empowering-students#induction-camps') }}">College Induction Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#corporate-camps') }}">College to Corporate Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#school-camps') }}">School Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#summer-camps') }}">Summer Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#winter-camps') }}">Diwali & Winter Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#miscellaneous') }}">Miscellaneous</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#adventure-camps') }}">Adventure Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#leadership-camps') }}">Leadership & Self Esteem Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#educational-trips') }}">Educational Trips</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#environmental-camps') }}">Environmental Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#excursions-in-india-and-abroad') }}">Excursions in India & Abroad</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#projects') }}">CAS Curriculum / Projects</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#community-services-empowering-students') }}">Community Services Workshops in rural areas</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#seminars-in-school') }}">Camps/ Workshops/Seminars in School/ College Campuses</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#team-building') }}">Team Building for Sports Teams</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#social-get-togethers') }}">Open Residential - Summer & Diwali Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#industrial-visits') }}">Industrial Visits</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#leadership-programs') }}">Educational Leadership Programs for Principals & Teachers</a></li>
                <li style="width:320px;"><a href="{{ url('/empowering-students#offsites-for-staff') }}">Offsites for Staff</a></li>
            </ul>
        </li>
    </ul>
</li>


<li>
    <a href="#">Adventure & Leisure</a>
    <ul>
        <li><a href="{{ url('/white-water-rafting-kolad') }}">White Water Rafting</a></li>
        <li>
            <a href="{{ url('/adventure-and-leisure-camps') }}">Adventure & Leisure</a>
            <ul style="width:320px;">
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#camping') }}">Camping</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#rock-climbing') }}">Rock Climbing & Rappelling</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#flying-fox') }}">Flying Fox/Zip Line</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#burma-bridge') }}">Burma Bridge</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#wall-climbing-rappelling') }}">Artificial Wall Climbing & Rappelling</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#raft-building') }}">Raft Building & Rowing</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#lake-crossing') }}">Lake Crossing</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#trekking') }}">Trekking</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#camp-fire') }}">Camp Fire</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#adventure-sports-events') }}">Adventure & Sports Events</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#target-shooting') }}">Target Shooting</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#zoomaring') }}">Zoomaring</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#kayaking') }}">Kayaking</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#relaxing') }}">Relaxing in a serene environment</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#social-get-togethers') }}">Social Get Togethers</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#yoga-camps') }}">Yoga Camps</a></li>
                <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#wellness-camps') }}">Wellness Camps</a></li>
            </ul>        
        </li>
    </ul>
</li>

<!-- <li>
    <a href="{{ url('/adventure-and-leisure-camps') }}">Adventure & Leisure</a>
    <ul style="width:320px;">
        <li style="width:320px;"><a href="{{ url('/white-water-rafting-kolad') }}">White Water Rafting</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#camping') }}">Camping</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#rock-climbing') }}">Rock Climbing & Rappelling</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#flying-fox') }}">Flying Fox/Zip Line</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#burma-bridge') }}">Burma Bridge</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#wall-climbing-rappelling') }}">Artificial Wall Climbing & Rappelling</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#raft-building') }}">Raft Building & Rowing</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#lake-crossing') }}">Lake Crossing</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#trekking') }}">Trekking</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#camp-fire') }}">Camp Fire</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#adventure-sports-events') }}">Adventure & Sports Events</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#target-shooting') }}">Target Shooting</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#zoomaring') }}">Zoomaring</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#kayaking') }}">Kayaking</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#relaxing') }}">Relaxing in a serene environment</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#social-get-togethers') }}">Social Get Togethers</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#yoga-camps') }}">Yoga Camps</a></li>
        <li style="width:320px;"><a href="{{ url('/adventure-and-leisure-camps#wellness-camps') }}">Wellness Camps</a></li>
    </ul>
</li> -->
<!--<li>-->
<!--    <a href="#">Camps</a>-->
<!--    <ul>-->
<!--        <li><a href="{{ url('/winter-camp') }}">Winter Camp</a></li>-->
<!--        <li><a href="{{ url('/summer-camp') }}">Summer Camp</a></li>-->
<!--        <li><a href="{{ url('/special-packages-for-summer-camp-2019') }}">Summer Camp 2019</a></li>-->
<!--        <li><a href="{{ url('/monsoon-adventure-camps') }}">Monsoon Adventure Camp</a></li>-->
<!--    </ul>-->
<!--</li>-->
<li>
    <a href="{{ url('/events#upcoming') }}">Events</a>
</li>
<li>
    <a href="{{ url('/testimonials') }}">Testimonials</a>
    <ul>
        <li><a href="{{ url('/testimonials#corporate') }}">Corporate</a></li>
        <li><a href="{{ url('/testimonials#students') }}">Students</a></li>
        <li><a href="{{ url('/testimonials#adventure-leisure') }}">Adventure & Leisure</a></li>
    </ul>
</li>
<li><a href="{{ url('/blog') }}">Blog</a></li>
<li><a href="{{ url('/contact-us') }}">Contact</a></li>

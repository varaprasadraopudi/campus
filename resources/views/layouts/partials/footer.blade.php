<div class="bottom-sidebar kopa-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="widget millside-module-bottom-logo">
                    <div class="widget-content">								

                        <div class="bottom-logo">
                            <a href="">
                                <img src="{{ asset('img/empower-icon.png') }}" width="100" alt="">
                                <p class="bottom-logo-title">Empower</p>
                                <p class="bottom-logo-sub-title">Activity Camps</p>
                            </a>
                        </div>
                        <!-- col-md-4 -->
                        <div class="bottom-logo-caption">								
                            Veteran ARMY Officer's venture Empower Activity Camps specializes in ‘Corporate training’ in outdoor environment, 50 acres adventure camp near Mumbai and Pune.
                        </div>
                        <!-- col-md-8 -->							
                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-6 -->

<!--millside-module-bottom-menu -->
<!--widget millside-module-nav-bottom-->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="widget millside-module-nav-bottom">
                    <h3 class="widget-title style-13">Our Programs</h3>
                    <div class="widget-content">								
                        <ul>
                            <li><a href="{{ url('/corporate-outbound-training') }}">Corporate Training</a></li>
                            <li><a href="{{ url('/education') }}">Education</a></li>
                            <li><a href="{{ url('/summer-camps') }}">Summer Camps</a></li>
                            <li><a href="{{ url('/facilities') }}">Facilities</a></li>
                            <li><a href="{{ url('/leisure') }}">Leisure trips</a></li>
                            <li><a href="{{ url('/adventure') }}">Adventure</a></li>
                            <!--<li><a href="{{ url('/blog') }}">Blog</a></li>-->
                            <!--<li><a href="{{ url('/about') }}">About</a></li>-->
                            <!--<li><a href="{{ url('/contact') }}">Contact</a></li>-->
                        </ul>	
                        
                        </div>
                </div>
                <!-- widget --> 
            </div>
            
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="widget millside-module-nav-bottom">
                    <h3 class="widget-title style-13">About Empower</h3>
                    <div class="widget-content">
                                    <ul>
                                 <li><a href="{{ url('/blog') }}">Blog</a></li>
                            <li><a href="{{ url('/about') }}">About</a></li>
                            <li><a href="{{ url('/contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </div>
                 <!--widget -->
            </div>
             <!--col-md-6 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</div>
<!-- bottom sidebar -->

<style>
    .millside-module-nav-bottom .widget-title, .millside-module-nav-bottom .widget-content ul li a {
        color: white;
    }
</style>
    
<footer class="kopa-page-footer">
    <div class="container">
        <p class="text-center">
            Copyright © 2019, Empower Activity Camps Pvt Ltd. All Rights Reserved.
        </p>
    </div>
    <!-- container -->
</footer>
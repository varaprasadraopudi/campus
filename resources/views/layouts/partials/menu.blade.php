<div class="header-bottom">
    <div class="container" data-spy="affix" data-offset-top="197">
    <div class="row">
        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-10">
            <div class="hamburger-menu kopa-pull-left kopa-dropdown-btn">
                <span class="ti-menu"></span>
            </div>
            <nav class="mobile-main-nav">
                <ul class="mobile-main-menu">
                    {{--                        @if(env('APP_ENV') == "local")--}}
                    {{--                            @include('layouts.partials.menuitems_theme')--}}
                    {{--                        @else--}}
                    @include('layouts.partials.menuitems')
                    {{--                        @endif--}}
                </ul>
            </nav>

            <div class="kopa-logo">
                <a href="{{ url('/') }}">
                    <img src="{{ asset('img/empower-logo.png') }}" alt="">
                </a>
            </div>
        </div>
        <div class="col-lg-9 col-md-0 col-sm-0 col-xs-0">
            <nav class="main-nav ">
                <ul class="main-menu">
                    {{--                        @if(env('APP_ENV') == "local")--}}
                    {{--                            @include('layouts.partials.menuitems_theme')--}}
                    {{--                        @else--}}
                    @include('layouts.partials.menuitems')
                    {{--                        @endif--}}
                </ul>
            </nav>
        </div>
        <div class="col-lg-1  col-md-6  col-sm-6  col-xs-2 text-right" style="padding:0px 5px;">
            <a href="https://www.tripadvisor.in/Hotel_Review-g1209190-d1510560-Reviews-Empower_Activity_Camp-Kolad_Maharashtra.html"
               target="_blank"><img src="{{ asset('img/icons/seal-trip-adv.png') }}" alt=""
                                    style="width:80px;margin-top:10px;"></a>
            {{--
            <div class="search-box search-box-01">
                <div class="preSearch kopa-dropdown-btn"><span class="ti-search"></span></div>
                <form method="GET" action="#">
                    <input class="search-input" placeholder="Search..." type="text" value="" name="s" id="search">
                    <button type="submit" class="search-submit">
                        <span class="ti-search"></span>
                    </button>
                </form>
            </div>
            --}}
        </div>
    </div>
</div>
</div>
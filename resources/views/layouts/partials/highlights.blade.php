<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="widget millside-module-slider-1">
            <div class="widget-content">
                <div class="owl-carousel owl-carousel-trainings">
                    <div class="item">
                        <p>170</p>
                        <p>Corporate<br>Trainings</p>
                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                    </div>
                    <div class="item">
                        <p>310</p>
                        <p>Outbound Events</p>
                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                    </div>
                    <div class="item">
                        <p>200</p>
                        <p>Accomodation<br>Strength</p>
                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                    </div>
                    <div class="item">
                        <p>175</p>
                        <p>Satisfied<br>Corporates</p>
                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                    </div>
                </div>
            </div>
            <!-- widget-content -->
        </div>
        <!-- widget --> 
    </div>
    <!-- col-md-12 -->
</div>	
<!-- row --> 

@push('scripts')
<script>
(function($) {
    var owl_t = jQuery('.owl-carousel-trainings');
	if(owl_t.length){
		owl_t.owlCarousel({
			items: 1,
			loop: true,
			nav: true,
			navText: ["<span class='ti-angle-left'></span>","<span class='ti-angle-right'></span>"],
			dots: false,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			slideSpeed: 2000,
		}); 
	}
})(window.jQuery);
</script>
@endpush
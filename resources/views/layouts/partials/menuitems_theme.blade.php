<li>
    <a href="{{ url('/') }}">Home</a>
    <ul>
        <li>
            <a href="index-1.html">home style 1</a>												
        </li>
        <li>
            <a href="index-2.html">home style 2</a>		
        </li>
        <li>
            <a href="index-3.html">home style 3</a>		
        </li>
        <li>
            <a href="index-4.html">home style 4</a>		
        </li>
    </ul>
</li>
<li>
    <a href="{{ url('/about') }}">About</a>
</li>
<li>
    <a href="#">the course</a>
    <ul>
        <li>
            <a href="course-1.html">book a tee time</a>
        </li>
        <li>
            <a href="#">photo Gallery</a>
            <ul>
                <li>
                    <a href="course-2.html">photo Gallery style 1</a>
                </li>
                <li>
                    <a href="course-3.html">photo Gallery style 2</a>
                </li>
            </ul>
        </li>	
        <li>
            <a href="course-4.html">Rates</a>
        </li>		
        <li>
            <a href="#">hole by hole</a>
            <ul>
                <li>
                    <a href="course-5.html">hole by hole fullwidth</a>
                </li>
                <li>
                    <a href="course-6.html">hole by hole left sidebar</a>
                </li>
            </ul>
        </li>	
        <li>
            <a href="course-7.html">Instruction</a>
        </li>							
    </ul>
</li>
<li>
    <a href="#">About</a>
    <ul>
        <li>
            <a href="club-1.html">About</a>
        </li>
        <li>
            <a href="club-2.html">club history</a>
        </li>
        <li>
            <a href="club-3.html">club news</a>
        </li>
        <li>
            <a href="club-4.html">special programs</a>
        </li>
        <li>
            <a href="club-5.html">the club house</a>
        </li>																
    </ul>
</li>
<li>
    <a href="#">membership</a>
    <ul>
        <li>
            <a href="membership-1.html">Enquiry Form</a>
        </li>
        <li>
            <a href="membership-2.html">Membership fees</a>
        </li>
    </ul>
</li>
<li>
    <a href="#">Blog</a>
    <ul>
        <li>
            <a href="#">news list</a>
            <ul>
                <li>
                    <a href="news-1.html">news right sidebar style 1</a>
                </li>
                <li>
                    <a href="news-2.html">news right sidebar style 2</a>
                </li>
                <li>
                    <a href="news-3.html">news right sidebar style 3</a>
                </li>
                <li>
                    <a href="news-4.html">news fullwidth</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="news-5.html">news single post</a>
            
        </li>
        <li>
            <a href="event-1.html">upcoming event</a>
            <ul>
                <li>
                    <a href="event-1.html">upcoming event style 1</a>
                </li>
                <li>
                    <a href="event-5.html">upcoming event style 2</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="event-2.html">Meetings &amp; Gatherings</a>
        </li>
        <li>
            <a href="event-3.html">Request Information</a>
        </li>
        <li>
            <a href="event-4.html">Tournaments/G.O.</a>
        </li>											
        <li>
            <a href="#">Weddings at Millside</a>
            <ul>
                <li>
                    <a href="event-7.html">Weddings at Millside</a>
                </li>
                <li>
                    <a href="event-6.html">Weddings at Millside detail</a>
                </li>
            </ul>
        </li>										
    </ul>
</li>
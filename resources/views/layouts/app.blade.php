<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MLP2BN6"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="main-container">
    <header class="kopa-page-header-1" style="
    background-color: white;
    position: fixed;
    top: 0px;
    width: 100%;
    z-index: 999;
">
        <div class="header-top">
            <div class="container">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-md-4 col-sm-4 col-xs-0 text-left header-top-left hidden-xs hidden-sm">
                        <ul>
                            <li>
                                <a href="{{ url('/about-us#media-coverage') }}">Media Coverage</a>
                            </li>
                            <li>
                                <a href="{{ url('/contact-us') }}">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-12 text-right header-top-right">
                        <ul>
                            <li>
                                <a href="tel:+919422691325">+91 9422691325</a>
                                <a href="tel:+917720873330">+91 7720873330</a>
                            </li>
                            <li class="hidden-xs hidden-sm hidden-md">
                                <a target="_blank" href="https://www.facebook.com/EmpowerActivityCamps/"><i
                                            class="fa fa-facebook"></i></a>
                                <a target="_blank" href="https://twitter.com/EmpowerCamps"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="https://www.linkedin.com/company/empower-activity-camps/"><i
                                            class="fa fa-linkedin"></i></a>
                                <a target="_blank" href="https://www.youtube.com/channel/UCtIG_K9h9yegTxONtP_XUTA"><i
                                            class="fa fa-youtube"></i></a>
                                <a target="_blank" href="https://www.instagram.com/empowercamp/"><i
                                            class="fa fa-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.partials.menu')
    </header>

    <div class="main-content" style="margin-top: 7.5%;">
        @yield('main-content')

    </div>
    <!-- main content -->

    @hasSection("no_footer")
    @else
        @include('layouts.partials.footer')
    @endif
</div>

<script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/superfish.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.sliderPro.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.matchHeight.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvaUg89uMNUQ3CSkUpio6dD0IudZ2ZWmQ"></script>
<script src="{{ asset('js/gmaps.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-timepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.navgoco.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('la-assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

<script type="text/javascript"
        src="{{ asset('js/custom.js?') . File::lastModified(base_path('js/custom.js')) }}"></script>

@stack('scripts')

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "+91 7720873330", // WhatsApp number
            call_to_action: "Need Assistance? Simply WhatsApp us", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () {
            WhWidgetSendButton.init(host, proto, options);
        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->
</body>
</html>
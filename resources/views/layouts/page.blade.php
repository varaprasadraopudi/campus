@extends('layouts.app')

@section('main-content')
    <section class="kopa-area kopa-area-10 kopa-area-no-space">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                @yield('breadcrumb-image')
                <div class="bg-green">
                    <div class="container">
                        <div class="kopa-breadcrumb">
                            @yield('breadcrumb-content')
                        </div>
                        <!-- kopa-breadcrumb -->
                        <div class="sidebar col-md-6 col-sm-6 col-xs-12 float-right" style="float: right;
                         background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.3); /* Black background with 0.5 opacity */
  top: 5%;">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div id="widget-booking" class="widget">
                                        {{--                                        <h3 class="widget-title style-04">Make Your Booking</h3>--}}
                                        <span id="msg" style="color:red"></span>
                                        <div class="widget-content">
                                            <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10"
                                                  method="POST" role="form" novalidate="novalidate">
                                                <input type="hidden" name="source"
                                                       value="Corporate Outbound Team building activities">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                        <input type="text" class="form-control" name="name"
                                                               placeholder="Name" required autocomplete="on">
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                        <input type="email" class="form-control" name="email"
                                                               placeholder="Email" required autocomplete="on">
                                                    </div>
                                                </div>
                                                {{--                                                <div class="form-group">--}}
                                                {{--                                                </div>--}}
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                        <input type="text" class="form-control" name="phone"
                                                               placeholder="Phone" required autocomplete="on">
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                        <input type="text" class="form-control" name="organization"
                                                               placeholder="Organization" required autocomplete="on">
                                                    </div>
                                                </div>
                                                {{--                                                <div class="form-group">--}}

                                                {{--                                                </div>--}}
                                                <div class="form-group">
                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                        <textarea class="form-control" rows="3" name="message"
                                                                  placeholder="Message"></textarea>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                        <button id="submit" type="submit" class="btn ct-btn-7">Make Your
                                                            Booking
                                                        </button>
                                                    </div>
                                                </div>
                                                {{--                                                <div class="form-group">--}}

                                                {{--                                                </div>--}}

                                            </form>
                                        </div>
                                        <!-- widget-content -->
                                    </div>
                                    <!-- widget -->
                                </div>
                                <!-- col-md-12 -->
                            </div>
                            <!-- row -->
                        </div>
                    </div>
                    <!-- container -->
                </div>
            </div>
            <!-- col-md-12 -->
        </div>
        <!-- row -->
    </section>
    <!-- kopa-area-10 -->

    @yield('page-content')
    <!-- kopa-area-11 -->

@endsection

@push('styles')
    <style>
        #bookButton {
            float: right;
            display: inline-block;
            margin-top: 13px;
        }
    </style>
@endpush


@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
    <script>
        var wTitleTop = 0;
        var wTitleBottom = 0;

        function updateFormPos() {
            var scrollTop = $(this).scrollTop();
            // console.log('scrollTop', scrollTop);
            $("#widget-booking").removeClass('widget-fixed');

            if ($(window).width() > 481) {
                if (scrollTop > wTitleTop) {
                    $("#widget-booking").addClass('widget-fixed');
                } else {
                    $("#widget-booking").removeClass('widget-fixed');
                }
            }
        }

        (function ($) {
            // var scrollTop = $(this).scrollTop();
            // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
            // wTitleTop = wTitleTop - scrollTop;

            wTitleTop = 388;
            wTitleBottom = 4950;

            // console.log('wTitleTop', wTitleTop);
            $(window).scroll(function () {
                // updateFormPos();
            });

            // create testimonials slider
            $('.cd-testimonials-wrapper').flexslider({
                selector: ".cd-testimonials > li",
                animation: "slide",
                controlNav: true,
                slideshow: true,
                smoothHeight: true,
                start: function () {
                    $('.cd-testimonials').children('li').css({
                        'opacity': 1,
                        'position': 'relative'
                    });
                }
            });

            $("#bookButton,#bookButton2").on("click", function () {
                $("input[name=name]").focus();
            });

            $("#bookingForm").validate({
                rules: {},
                errorPlacement: function (error, element) {
                    error.insertAfter(element);
                },
                submitHandler: function (form) {
                    $.ajax({
                        url: "{{ url('/save_session_booking') }}",
                        method: 'POST',
                        data: $(form).serialize(),
                        beforeSend: function () {
                            $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submitting...');
                            $('#submit').prop('disabled', true);
                        },
                        success: function (data) {
                            console.log(data);
                            if (data.status == "success") {
                                $('#msg').html(data.message);
                                $('#submit').prop('disabled', false);
                                $("#bookingForm")[0].reset();
                                document.location.href = "https://empowercamp.com/thankyou";
                            } else {
                                // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                                $('#msg').html("Try Again :(");
                                $('#submit').prop('disabled', false);
                            }
                        }
                    });
                    return false;
                }
            });

            // updateFormPos();

            jQuery('.slider-pro-trainings').sliderPro({
                arrows: true,
                buttons: true,
                waitForLayers: false,
                autoplay: true,
                fadeArrows: false,
                fadeOutPreviousSlide: true,
                autoScaleLayers: true,
                responsive: true,
                slideDistance: 25,
                autoplayDelay: 5000,
                width: 550,
                height: 400,
                thumbnailWidth: 214,
                thumbnailHeight: 130,
                visibleSize: '100%',
                breakpoints: {
                    1023: {
                        width: 500,
                        height: 238,
                        thumbnailWidth: 155,
                        thumbnailHeight: 94,
                    },
                },
                init: function () {
                    jQuery(".slider-pro").show();
                }
            });
        })(window.jQuery);
    </script>
@endpush

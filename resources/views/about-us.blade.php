@extends('layouts.app')

@section('meta_title') Empower Activity Camps - About Us @endsection
@section('meta_description')About Empower Camp Empower Camp is a passion driven company enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is Leisure with Learning, and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment.@endsection
@section('meta_keywords')  @endsection

@section('main-content')
<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/adventure-training.jpg') }}" alt="Adventure Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">About Us</h3>
                        <h4 class="subtitle">“Empowering people by helping them discover their own strengths”</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/home') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/about') }}">
                                    <span itemprop="title">About</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 sidebar sidebar-about">
                
                <div class="widget millside-module-event-1 mt90">
                    <h3 class="widget-title style-01">
                        <img src="{{ asset('img/icons/list.png') }}" alt="">Recent Events
                    </h3>
                    <div class="widget-content">
                        @php
                        $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                        @endphp
                        <ul>
                            @foreach($sessions as $session)
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        @php
                                        $day = date("d", strtotime($session->post_date));
                                        $month = date("M", strtotime($session->post_date));
                                        @endphp
                                        <p>{{ $day }}</p>
                                        <p>{{ $month }}</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">{{ $session->title }}</a>
                                        </h4>
                                        <p>{{ $session->excerpt }}</p> 
                                    </div>
                                </article>											
                            </li>
                            @endforeach
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        <p>24</p>
                                        <p>sep</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                                        </h4>
                                        <p>
                                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                                        </p> 
                                    </div>
                                </article>											
                            </li>
                        </ul>

                        <div>
                            {{-- <a href="#" class="more-link style-01 "><span class="ti-arrow-right"></span>More</a> --}}
                        </div>

                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-4 -->
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11">About Empower Camp</h3>
                            <div class="widget-content">
                                <p>
                                    Empower Camp is a passion driven company enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is Leisure with Learning, and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment. Each of our programme modules have been custom developed considering the participants and training objectives.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30"> Our Philosophy</h3>
                            <div class="widget-content">
                                <p>
                                    “Empowering people by helping them discover their own strengths”. This is the philosophy behind Empower Activity Camps that has benefited more than 400 organizations. Our primary means of achieving this objective is experiential learning through outbound training. Put plainly, it is super-learning while having super-fun with adventure and outdoor activities.
                                </p>
                                <p>
                                    We have specially developed modules for all ages in all spheres – the corporate as well as the academic world, which are customized for each organization that we work with. Empower Camp is custom-built for our programs and located at the picturesque and adventure friendly Kolad, at a short drive from Mumbai and Pune. Clients get to choose the training venue; indoors or outdoors, at our camp or some other location. Whatever changes, one thing remains constant in our training programs: We Transform Mindsets and Enhance Effectiveness.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30"> Empower Camp – Break Free to Bring Out Your Best!</h3>
                            <div class="widget-content">
                                <p>
                                    You could be a high flying corporate executive, or a student still figuring out life, a stock broker or an artist. Whoever you are, and whatever you do, there comes a time when you need a break. Not just a lazy break to some boring vacation spot which does nothing to give you a fresh perspective. What you really yearn for is a place that takes you away ‘from it all’.
                                </p>
                                <p>Where you do a lot of exciting things in a lovely location; where the fresh air reaches not just your lungs, but your soul.</p>
                                <p> When you choose Empower, your journey doesn’t stop at reaching Empower Camp. From here, we will take you on another journey, much more meaningful, where you will discover the heights of your abilities and the depths of your inner strengths</p>
                                <p>Trust us, you will be amazed at your own hidden potential.</p>
                                <p>Camp with us and discover yourself, truly empowered!</p>
                                <p>
                                    We at Empower understand your needs, both of creature comforts as well as your drive to succeed. The former is taken care of by our location and well-appointed accommodation; our custom designed experiential learning
                                    programs cater to the latter. Our wide range of activities has been developed considering different age groups  and preferences. We have something for everyone, and we assure you that you will return rejuvenated and empowered.
                                </p>
                                <p>
                                    We have specially developed modules for all ages in all spheres – the corporate as well as the academic world, which are customized for each organization that we work with. Empower Camp is custom-built for our programs and located at the picturesque and adventure friendly Kolad, at a short drive from Mumbai and Pune. Clients get to choose the training venue; indoors or outdoors, at our camp or some other location. Whatever changes, one thing remains constant in our training programs: We Transform Mindsets and Enhance Effectiveness.
                                </p>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!--Team section-->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Our Team</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget-content">
                    <p>
                        Before we get into specifics, we would like to mention one quality of our team that really sets us apart. Our team has a Army Veteran at the helm. Defence personnel are known for their discipline, leadership and team building skills. But there is more to being an army officer. 
                    </p>
                    <p>
                        The onerous task of safeguarding the sovereignty of the nation is fraught with risks; and perhaps there is no other profession where trust, teamwork and effective communication can mean the difference between losing lives and saving them. Our founding team has joined forces and combined their learnings from their highly demanding, yet deeply enriching profession to come up with programs that can benefit one and all.
                    </p>
                </div>
                <div id="our-team" class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center team-text">
                        <div class="img-circular" style="background-image: url('{{ asset('img/team/col-naval-kohli.jpg') }}');">
                            <span></span>
                        </div>
                        <h5>Col Naval Kohli</h5>
                        <span>Managing Director</span>
                    </div>
                    <div class="col-md-8 team-text">
                        <h6>Col Naval Kohli</h6>
                        <div class="widget-content">
                            <p>
                                He is the Managing Director and co-founder of Empower Activity Camps. Endowed with a rich Experience, he has served the <b>Indian Army for 24 years</b>. He is a post graduate in Senior Level Defence Management  and Computer software & Technology. He has served in UNITED NATIONS PEACE KEEPING FORCE in Somalia and has been awarded the <b>French National Medal of Defence by Govt of France for distinguished service.</b> There as a result of his interaction with <b>International Armed Forces</b> he gained expertise in Training, logistics, macro & micro planning, coordination and execution of plans under adverse conditions.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12 text-center team-text">
                            <div class="img-circular" style="background-image: url('{{ asset('img/team/anil-bhasin-empower-1.jpg') }}');"><span></span></div>
                        <h5>Mr. Anil Bhasin</h5>
                        <span>Managing Director</span>
                    </div>
                    <div class= "col-md-8 team-text">
                        <h6>Mr. Anil Bhasin</h6>
                        <div>
                            <ul>
                                <li>Took voluntary retirement as Manager, Inflight Service Department, Air India after a glorious service of 36 years.</li>
                                <li>Post-graduate in Specialized Hotel Management.</li>
                                <li>Long and varied experience in travel and human relations, having, on occasions, dealt with difficult situations and emergencies. Travelled extensively around the globe for work and leisure to over 35 countries, spanning all the continents.</li>
                                <li>Was conferred with the Best Employee award for his dedication and contribution to the airline</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class ="col-md-4 col-sm-12 col-xs-12 text-center team-text">
                        <h5 class ="ml40">Bhavna Sahai</h5>
                        <span class ="ml30">Senior Marketing Manager</span>
                    </div>
                    <div class ="col-md-8 col-sm-12 col-xs-12 text-center team-text">
                        <h5>MENTORS & TRAINERS</h5>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <ul>
                                <li><b>Col Naval Kohli</b></li>
                                <li><b>Col IP Singh</b></li>
                                <li><b>Anand Mehta</b></li>
                                <li><b>Neelu Grover</b></li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <ul>
                                <li><b>Anil Bhasin</b></li>
                                <li><b>Col Bharat Haladi</b></li>
                                <li><b>Anup Talwar</b></li>
                                <li><b>Jennifer Shetty</b></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--Team section end-->
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 --><!--MEDIA COVERAGE-- > <!- row --> 
<section id="media-coverage" class="kopa-area kopa-area-12 kopa-area-no-space">
    <div class="container"> 
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="widget millside-module-ads-5">
                    <div class="widget-content">
                        <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                        <div class="bg-green-2">
                            <div class="row mtm30">
                                <div class="col-md-12">
                                    <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;text-align: center;">Media Coverage</h3>
                                    <h3 class="mt20 mb20" style="padding-left:30px;line-height:20px;text-align: center;">Empower Activity Camps in News</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="kopa-area kopa-area-0">
    <div class="container">
        <div class="widget millside-module-slider-pro-4">
            {{-- <h3 class="widget-title text-center style-11">Empower Activity Camps in News</h3> --}}
            <div class="widget-content">
                <div class="slider-pro slider-pro-4">
                    <div class="sp-slides">
                        <div class="sp-slide">
                            <img class="sp-image" alt="" src="{{ asset('img/media-coverage/Article-Lokhandwala-TOI-1-thumb11.jpg') }}">
                            <img class="sp-thumbnail" alt="" src="{{ asset('img/media-coverage/Article-Lokhandwala-TOI-1-thumb11.jpg') }}">
                        </div>
                        <div class="sp-slide">
                            <img class="sp-image" alt="" src="{{ asset('img/media-coverage/Article-Lokhandwala-TOI-1-thumb21.jpg') }}">
                            <img class="sp-thumbnail" alt="" src="{{ asset('img/media-coverage/Article-Lokhandwala-TOI-1-thumb21.jpg') }}">
                        </div>
                        <div class="sp-slide">
                            <img class="sp-image" alt="" src="{{ asset('img/media-coverage/A-Weekend-at-the-Boot-Camp-Empower-Camp-Mail-Today1.jpg') }}">
                            <img class="sp-thumbnail" alt="" src="{{ asset('img/media-coverage/A-Weekend-at-the-Boot-Camp-Empower-Camp-Mail-Today1.jpg') }}">
                        </div>
                        <div class="sp-slide">
                            <img class="sp-image" alt="" src="{{ asset('img/media-coverage/citypluse_lokmat_18april2007-thumb1.jpg') }}">
                            <img class="sp-thumbnail" alt="" src="{{ asset('img/media-coverage/citypluse_lokmat_18april2007-thumb1.jpg') }}">
                        </div>
                        <div class="sp-slide">
                            <img class="sp-image" alt="" src="{{ asset('img/media-coverage/dna_19april2007-thumb1.jpg') }}">
                            <img class="sp-thumbnail" alt="" src="{{ asset('img/media-coverage/dna_19april2007-thumb1.jpg') }}">
                        </div>
                        <div class="sp-slide">
                            <img class="sp-image" alt="" src="{{ asset('img/media-coverage/MID-DAY-on-21-Apr-11-Summar-Camp-thumb1.jpg') }}">
                            <img class="sp-thumbnail" alt="" src="{{ asset('img/media-coverage/MID-DAY-on-21-Apr-11-Summar-Camp-thumb1.jpg') }}">
                        </div>
                    </div>
                </div>
            </div>
            <!-- widget-content --> 
        </div>
        <!-- widget --> 
    </div>
    <!-- container -->
</section>

{{-- <section class="kopa-area kopa-area-12 kopa-area-no-space">
    <div class="container">
        <div class="widget millside-module-ads-5">
            <div class="widget-content">
                <img src="http://placehold.it/1170x153" alt="">
                <div class="bg-green-2">
                    <div class="row">
                        <div class="col-md-2 col-sm-3 col-xs-12 ">
                            <div class="part-1">
                                <p>Cloudy</p>
                                <p>15<span>o</span><span>F</span></p>
                            </div>
                        </div>
                        <!-- col-md-2 -->
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <div class=" part-2 ul-mh">
                                <div class="icon-weather">
                                    <img src="http://placehold.it/32x27" alt="">
                                </div>
                                <div>
                                    <p>Take advantage of tee times at a special price!</p>
                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                </div>
                            </div>
                        </div>
                        <!-- col-md-10 -->
                    </div>
                    <!-- row --> 
                </div>
            </div>
        </div>
        <!-- widget --> 
    </div>
    <!-- container -->
</section> --}}
<!-- kopa-area-12 -->


<!-- kopa-area-3 -->
@endsection

@push('styles')
<style>


</style>
@endpush


@push('scripts')
<script>

(function($) {
    var owl_t = jQuery('.owl-carousel-trainings');
	if(owl_t.length){
		owl_t.owlCarousel({
			items: 1,
			loop: true,
			nav: true,
			navText: ["<span class='ti-angle-left'></span>","<span class='ti-angle-right'></span>"],
			dots: false,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			slideSpeed: 2000,
		}); 
	}
})(window.jQuery);
</script>
</script>
@endpush
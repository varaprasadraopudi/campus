@extends('layouts.app')

@section('meta_title') BLOG &#124; empower @endsection
@section('meta_description') Veteran ARMY Officer's venture Empower Activity Camps is a passion driven company dedicated to enhancing human effectiveness through \u2018experiential learning\u2019 in outdoor environment. Our motto is \u201cLeisure and Learn\u201d and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment. Our programme modules are customized considering partcipant requirements - be it corporates, students or leisure and adventure seekers.@endsection
@section('meta_keywords')  @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Blog</h3>
                        <h4 class="subtitle">Team building is awesome</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">blog</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->
                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="main-col col-md-8 col-sm-8 col-xs-12">
                <div class="row">
                @php
                $blog = $blog_post;
                @endphp
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-6">
                            <header class="wiget-header">
                                <h3 class="widget-title style-07">{{ $blog->title}}</h3>
                            </header>
                            <div>
                                <span>@php
                                        $day = date("d", strtotime($blog->post_date));
                                        $month = date("M", strtotime($blog->post_date));
                                        @endphp
                                        {{ $day }}
                                        {{ $month }}
                                        {{ $blog->category->name or ""}}
                                        <a>Posted by {{ $blog->author->name or ""}}</a>
                                </span>
                            </div>
                            <div class="widget-content">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="widget millside-module-slider-pro-trainings">                                                    
                                                    <div class="widget-content">
                                                        <div class="slider-pro slider-pro-trainings">
                                                            <div class="sp-slides">
                                                                @foreach(json_decode($blog->album) as $banner)
                                                                    @php $path = App\Models\Upload::find($banner)->path(); @endphp
                                                                    <div class="sp-slide">
                                                                        <img class="sp-image" alt="" src="{{ $path }}">
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- widget-content --> 
                                                </div>
                                                <!-- widget --> 
                                            </div>
                                        </div>									        
                                    </div>
                                    <div class="entry-content">
                                        <p>
                                            <a><?php echo ($blog->content) ?></a>
                                        </p>
                                    </div>
                                </article>
                            </div>
                            <!-- widget-content -->
                            <footer>
                                
                            </footer>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row -->

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-event-13">
                            <h3 class="widget-title style-04">Related</h3>
                            <div class="widget-content">
                                <ul class="row ul-mh">
                                    <li class="col-md-4 col-sm-4 col-xs-12">
                                        <article class="entry-item">
                                            <div class="entry-content">
                                                <h4 class="entry-title style-12">
                                                    <a href="#">Mother's Day Sunday Lunch offer</a>
                                                </h4>
                                                <p>24th Feb, 2:30Pm</p> 
                                            </div>
                                        </article>
                                    </li>
                                    <li class="col-md-4 col-sm-4 col-xs-12">
                                        <article class="entry-item">
                                            <div class="entry-content">
                                                <h4 class="entry-title style-12">
                                                    <a href="#">Our best ever full membership 	offer!!</a>
                                                </h4>
                                                <p>24th Feb, 2:30Pm</p> 
                                            </div>
                                        </article>
                                    </li>
                                    <li class="col-md-4 col-sm-4 col-xs-12">
                                        <article class="entry-item">
                                            <div class="entry-content">
                                                <h4 class="entry-title style-12">
                                                    <a href="#">Fantastic Start to Sunday Lunch Promotion</a>
                                                </h4>
                                                <p>24th Feb, 2:30Pm</p> 
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="BLOG">
                                    {{ csrf_field() }}
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
										</div>
									</div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
										</div>
                                    </div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
										</div>
									</div>

								</form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();

    jQuery( '.slider-pro-trainings' ).sliderPro({
        arrows: true,
        buttons: false,
        waitForLayers: false,
        autoplay: true,
        fadeArrows: false,
        fadeOutPreviousSlide: true,
        autoScaleLayers: true,
        responsive: true,
        slideDistance: 25,
        autoplayDelay: 5000,

        width: 692,
        height: 430,
        thumbnailWidth: 214,
        thumbnailHeight: 130,

        visibleSize: '100%',
        breakpoints: {
            1023: {
                width: 500,
                height: 238,
                thumbnailWidth: 155,
                thumbnailHeight: 94,
            },
        },

        init: function(){
            jQuery(".slider-pro").show();   
        }
    });
})(window.jQuery);
</script>
@endpush
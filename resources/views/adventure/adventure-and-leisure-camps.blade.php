@extends('layouts.app')

@section('meta_title')Adventure and Leisure Capms @endsection
@section('meta_description') Book white water rafting on Kundalika River, Kolad. Call now for bookings / enquiries. Rafting with Empower is a different experience altogether. @endsection
@section('meta_keywords') White water rafting,Burma Bridge,Flying Fox/Zip Line,Raft Building,Rock Climbing,Rappelling,Target Shooting,Trekking,Zoomaring @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/adventure/adventure-activities-empower.png') }}" alt="Adventure activities near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Adventure</h3>
                        <h4 class="subtitle">Team building is awesome</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Adventure</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">

            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Adventure</h3>
                            <div class="widget-content">
                                <p>
                                    <b>Pursue your passion for adventure as we take adventure to the next level at Empower!</b>
                                </p>
                                <p>
                                    The thrill. The Adrenaline Rush. Euphoria from the triumph over a challenge. The surge in self-confidence that makes you feel like you can take on anything, absolutely anything in life.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30">Welcome to the world of adventure!</h3>
                            <div class="widget-content">
                                <p>
                                    For some, it is very obvious. For others, challenges bring out their true courage. Empower’s Adventure Camp offers a range of adventure activities for all sorts of adventurers- trekking, river rafting, obstacle courses, nature walks, rock climbing and para-sailing. Whether you are a water baby or a wannabe mountaineer, whether you feel like feeling liberated up in the air when para-sailing or you like feeling one with the nature on a trekking expedition or a nature walk, we have an option for you*.
                                </p>
                                <p>
                                    Set in the picture perfect Kolad area, surrounded by greenery and flanked by a lake and mountain, Empower Camp is an ideal location for a whole range of adventure activities. Need bigger challenges? We can take you on a fun camp that includes a visit to Kumaon National Park and takes you on lake tours and nature walks.
                                </p>
                                <p>
                                    Are you a serious trekker? Then our Himalayan Treks are the one for you. Adventure, fun and thrills. For us, this is serious business. We make sure that each activity is safe, provide the participants with prescribed safety gear, and run them through safety instructions before each activity. (*Some of the adventure activities are seasonal, and some of them need a minimum number of participants. Please check with our office before planning for an activity during your trip).  
                                </p>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Adventure Camp Activities</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-article-list-5">
                            <div class="widget-content">
                                <ul>
                                    <!--
                                    <li id="camping">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Camping">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Camping</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample Content
                                                    </p>
                                                    <p><b>Sample Content</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>-->
                                    <li id="rock-climbing">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/adventure/m15-300x200.jpg') }}" alt="Rock Climbing">
                                                <img class="mt20" src="{{ asset('img/adventure/m16-300x192.jpg') }}" alt="Rappelling">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Rock Climbing & Rappelling</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Empower Camp facilitates rock climbing as its a physically and mentally demanding sport that tests a climber’s strength, endurance, agility and balance along with mental control. Rock climbing here, at Empower Camps has objectives of team building and we have variations for different groups. Most of all this activity gives all those involved with it an up-close view of incredible scenery of nature allowing them to interact with nature along with rock climbing.
                                                    </p>
                                                    <p>
                                                        Rappelling, generally known as “to rope down” is done over a natural rock face that brings a powerful sense of accomplishment and is a vital source in bringing about the realization that not every perceived risk is undefeatable. The natural environment and the remoteness of locations that Empower takes you to enhance the challenge you accept when you take up this activity. Confidence building, fear management, trust, risk taking capability is determined. We introduce rappelling activity with information about climbing and rappelling equipment like gloves, helmets, harnesses, rappelling ropes etc.. We give a fair demonstration of rappelling techniques and safety systems followed with attempts by participants. Fifteen minutes per head is the time we give which individual effectiveness & a sense of achievement.
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="flying-fox">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/adventure/Launch-Kraft-ACTIVITY-300x204.jpg') }}" alt="Flying Fox">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Flying Fox/Zip Line</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        A flying fox is a common way to return participants to the ground at the end of a ropes adventure course. In past days in the Australian outback, flying foxes were occasionally used for delivering food, cigarettes or tools to people working on the other side of an obstacle such as a gully or river.At Empower its a simple ropeway built across two trees wherein the participants need to climb up to a height of 25 ft by rope ladder and come down sliding on a rope inclined at 30 degrees, with the help of a pulley. The climb is moderately difficult. The start off is the decision point. Safety is ensured. Builds self-confidence, identifies fears & most importantly the team work support helps gather courage.
                                                    </p>
                                                    <p><b>Where are my fears gone??</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="burma-bridge">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/adventure/Burma-Bridge1-300x225.jpg') }}" alt="Burma-Bridge">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Burma Bridge</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        A semblance of improvised bridges used in Burma – a land full of water channels and rivers. This is a bridge that consists of a thick rope for walking upon, and two ropes to hold on to. The bridge stretches across a gorge. Here its a semblance of the river. Its anchored on to artificial structures with a pond below, which is filled with water only during the monsoons. You are ready to cross when you have put on the harness and connected yourself to the safety rope. All that remains is a 18-metre suspended walk in natural surroundings.An exciting activity for those willing to try something new.
                                                    </p>
                                                    <p><b>Take me across</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <!--
                                    <li id="wall-climbing-rappelling">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Artificial Wall Climbing & Rappelling">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Artificial Wall Climbing & Rappelling</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample Content 
                                                    </p>
                                                    <p><b>Sample Content</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    -->
                                    <li id="raft-building">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/adventure/DSC_1081-300x199.jpg') }}" alt="Raft Building">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Raft Building & Rowingng</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        It is a 150 – 220 minutes team event wherein every team is required to design and build a raft with given material. Thereafter the teams race by rowing the rafts built by them. This activity involves planning and strategizing, risk management, team work and dynamism. A very challenging activity that’s way too interesting in rains.
                                                    </p>
                                                    <p><b>We built our own raft</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <!--
                                    <li id="lake-crossing">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Lake Crossing">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Lake Crossing</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                    <p><b>Sample content</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>-->
                                    <li id="trekking">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/adventure/Nature-Trail-300x225.jpg') }}" alt="Trekking">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Trekking</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Trekking – the best activity for those who are fascinated by the unexplored locations. This can help introduce those individuals new to trekking to the special skills required to successfully make a journey of significant length. Empower team takes you through enthralling routes that you could explore in an adventurous and athletic way.You have an option of trekking up a hill (from moderate to tough), along wooded area on a level ground to walking along the lakeside (when there is water (usually Jun to Dec)
                                                    </p>
                                                    <p><b>Extreme Ecology adventure with Empower!</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <!--
                                    <li id="camp-fire">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Lake Crossing">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Camp Fire</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                    <p><b>Sample content</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="adventure-sports-events">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Lake Crossing">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Adventure & Sports Events</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                    <p><b>Sample content</b></p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>-->
                                    <li id="target-shooting">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/adventure/Target-Shooting1-300x199.jpg') }}" alt="Target Shooting">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Target Shooting </a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Target Shooting at Empower is an amateur’s way of getting thrills and sense of achievement by trying out your concentration power. Using simple Air guns, but multi sized tin target that give u a nice tingling sound when the pallet hits the target, is fun for many.
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="zoomaring">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/adventure/DSC04892-300x225.jpg') }}" alt="Zoomaring">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Zoomaring </a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        A simple looking activity which isn’t so simple. It involves climbing up a vertical rope with the help of a ‘zoomar’ – an instrument that can move up but doesn’t slip or move down. It, therefore, provides a firm foothold.
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <!--
                                    <li id="kayaking">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Kayaking">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Kayaking </a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="relaxing">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Relaxing in a serene environment">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Relaxing in a serene environment</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="social-get-togethers">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Social Get Togethers">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Social Get Togethers</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="yoga-camps">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Yoga Camps">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Yoga Camps</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="wellness-camps">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/300x250.png') }}" alt="Wellness Camps">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">Wellness Camps</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Sample content
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    -->
                                </ul>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                    <!-- row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-event-1 mt90">
                            <h3 class="widget-title style-01">Adventure Camp Activities</h3>
                            <div class="widget-content">
                                <ul>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Burma Bridge</a>
                                                </h4>
                                                <span>Take me across</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Cycling</a>
                                                </h4>
                                                <span>Experience the wheel life!</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Flying Fox/Zip Line</a>
                                                </h4>
                                                <span>Where are my fears gone??</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Raft Building</a>
                                                </h4>
                                                <span>We built our own raft</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">White Water Rafting</a>
                                                </h4>
                                                <span>Move through the endless joy of thrills and spills</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Rock Climbing/Rappelling</a>
                                                </h4>
                                                <span>Sea to sky adventure!</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Target Shooting</a>
                                                </h4>
                                                <span>An amateur’s way of getting thrills </span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Trekking</a>
                                                </h4>
                                                <span>Extreme Ecology adventure with Empower!</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Valley Crossing</a>
                                                </h4>
                                                <span>It tests individual effectiveness</span>
                                            </div>
                                        </article>											
                                    </li>
                                    <li>
                                        <article class="entry-item">
                                            <div class="entry-content ml30">
                                                <h4 class="entry-title style-03 ">
                                                    <a href="#">Zoomering</a>
                                                </h4>
                                                <span>It tests individual effectiveness</span>
                                            </div>
                                        </article>											
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                    <!-- row --> 
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="Adventure and Leisure Capms">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
            </div>
                <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();

    jQuery( '.slider-pro-trainings' ).sliderPro({
        arrows: true,
        buttons: false,
        waitForLayers: false,
        autoplay: true,
        fadeArrows: false,
        fadeOutPreviousSlide: true,
        autoScaleLayers: true,
        responsive: true,
        slideDistance: 25,
        autoplayDelay: 5000,

        width: 692,
        height: 430,
        thumbnailWidth: 214,
        thumbnailHeight: 130,

        visibleSize: '100%',
        breakpoints: {
            1023: {
                width: 500,
                height: 238,
                thumbnailWidth: 155,
                thumbnailHeight: 94,
            },
        },

        init: function(){
            jQuery(".slider-pro").show();   
        }
    });
})(window.jQuery);
</script>
@endpush
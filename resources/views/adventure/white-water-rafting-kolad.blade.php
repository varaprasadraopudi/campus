@extends('layouts.page')

@section('meta_title') White Water Rafting at Kolad @endsection
@section('meta_description') Book white water rafting on Kundalika River, Kolad. Call now for bookings / enquiries. Rafting with Empower is a different experience altogether. @endsection
@section('meta_keywords') White water rafting,Burma Bridge,Flying Fox/Zip Line,Raft Building,Rock Climbing,Rappelling,Target Shooting,Trekking,Zoomaring @endsection
@section('no_footer', false)

@section('breadcrumb-image')
    <img src="{{ asset('img/adventure/Rafting6.JPG') }}" alt="Adventure activities near Pune">
@endsection

@section('breadcrumb-content')
    <h3 class="with-subtitle">White Water Rafting in Kolad</h3>
    <h4 class="subtitle">A Never Ending Adventure Activity </h4>
    <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Adventure</span>
                                </a>
                            </span>
    </div>
@endsection

@section('page-content')
    <section class="kopa-area sub-page kopa-area-25">
        <div class="container">
            <div class="row">
                <div class="main-col col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-intro-9">
                                <h3 class="widget-title style-11 ">White Water Rafting</h3>
                                <div class="widget-content">
                                    <p>
                                        When we think about White Water Rafting(WWR), our adrenalin pushes faster with
                                        the thought of this widely popular sport as it delivers excitement, thrill and
                                        health benefits. The first white water rafting
                                        expedition took place at Snake River in Wyoming in 1811, however it was not
                                        successful due to non-availability of suitable gear or training. As of today,
                                        when it comes to adventure, no other sport can beat
                                        the thrill of sailing in the midst of foamy water in a raft that is something no
                                        adventurer could afford to miss.
                                    </p>
                                </div>
                                <p></p>
                                <h3 class="widget-title style-11">benefits of white-water rafting :-</h3>
                                <!-- <h4 class="widget-title style-11 mt20 mb20">Apart from the thrills and spills there are some wonderful benefits of white-water rafting: -</h4> -->
                                <p class="ul-li-title"><b>Apart from the thrills and spills there are some wonderful
                                        benefits of white-water rafting:-</b></p>
                                <ul class="ul-checked custom1">
                                    <li><i class="fa fa-check-circle"></i><span>It impacts a lot on our mind and body, as a result this sport reduces stress, anxiety and depression by releasing happy hormones.</span>
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><span>This sport requires endurance and stamina for successful completion. The participants need to exercise their physical abilities to cut through the water stream which puts a positive effect in their cardiovascular system and also the overall health.</span>
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><span>Rafting boosts our self esteem and self-confidence. This sport requires the participants to navigate a flowing river, so after successful accomplishment it gives a sense of well being and increase in confidence to handle various other difficult tasks as they appear in regular life.</span>
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><span>Talking about group bonding, it is an excellent opportunity to bond with the team mates as it is a team sport to achieve a similar goal. While participating in this task, participants get to learn many social skills and life changing values.</span>
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><span>The breath-taking excitement of white water rafting gives you an experiential and unforgettable boost to the adrenalin rush.</span>
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><span>We all know that rafting requires intense physical activity, which creates a fit body and as we conquer the wild water, the entire body and mind experience a great workout.</span>
                                    </li>

                                </ul>
                            </div>
                            <!-- widget -->
                        </div>
                        <!-- col-md-12 -->
                    </div>
                    <!-- row -->
                <!-- <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Gallery</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-slider-pro-trainings">
                                <!-- <h3 class="widget-title text-center style-11">Course Photo Gallery</h3> -->
                                <div class="widget-content">
                                    <div class="slider-pro slider-pro-trainings">
                                        <div class="sp-slides">
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/White-Water-Rafting-1.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/White-Water-Rafting-2.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/White-Water-Rafting-3.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/White-Water-Rafting-4.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/White-Water-Rafting-5.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/White-Water-Rafting-6.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/White-Water-Rafting-7.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/adventure/white-water-rafting.png') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- widget-content -->
                            </div>
                            <!-- widget -->
                        </div>
                    </div>
                    <div class="widget millside-module-intro-9">
                        <h3 class="widget-title style-11 ">Kundalika river and Empower Activity Camps</h3>
                        <div class="widget-content">
                            <p>
                                Kundalika river and Empower Activity Camps is surrounded by blissful presence of mother
                                nature,which creates a therapeutic treatment for all souls, especially during the
                                monsoon. Overcoming the
                                obstacles and completing the activities successfully brings a positive psychological
                                change in an individual. Our excellence in adventure activities and experiential
                                learning will make you a pro in
                                white water rafting providing you the best experience you could have with safety,
                                training and world class gears provided by our trainers and experts. The only wahite
                                water rafting location in this part
                                of the country.
                            </p>
                            <p>
                                The source of Kundalika River is from Tamhini Ghat and it flows through Kolad village
                                and is a perfect place for white water rafting. This rafting activity is conducted
                                between two dams which are
                                about 12Kms apart. Rafting starts at the lower side of Bhira Dam and runs into Dolwahal
                                Dam since there is no unfamiliar change in the water level, this river is highly safe
                                and has been recognized as
                                water sports hub. Enjoy rafting in this 12Kms water stretch which is full of exciting
                                rapids making it a fun filled 2hrs activity.
                            </p>
                        </div>
                    </div>

                    <iframe width="560" height="315" src="https://www.youtube.com/embed/feoY6uvv5L4" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                </div>
                <!-- col-md-8 --><!-- class="widget widget-fixed"-->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-11 -->
@endsection

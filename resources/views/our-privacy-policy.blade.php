@extends('layouts.app')

@section('meta_title') Empower Activity Camps - privacy-policy @endsection
@section('meta_description')About Empower Camp Empower Camp is a passion driven company enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is Leisure with Learning, and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment.@endsection
@section('meta_keywords')  @endsection

@section('main-content')
<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/adventure-training.jpg') }}" alt="Adventure Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Privacy policy</h3>
                        <h4 class="subtitle">“Empowering people by helping them discover their own strengths”</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/home') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/privacy-policy') }}">
                                    <span itemprop="title">Privacy policy</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 sidebar ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-slider-1">
                            <div class="widget-content">
                                <div class="owl-carousel owl-carousel-trainings">
                                    <div class="item">
                                        <p>170</p>
                                        <p>Corporate<br>Trainings</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                    <div class="item">
                                        <p>310</p>
                                        <p>Outbound Events</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                    <div class="item">
                                        <p>200</p>
                                        <p>Accomodation<br>Strength</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                    <div class="item">
                                        <p>175</p>
                                        <p>Satisfied<br>Corporates</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                </div>
                            </div>
                            <!-- widget-content -->
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>

                <div class="widget millside-module-event-1 ">
                    <h3 class="widget-title style-01">
                        <img src="{{ asset('img/icons/list.png') }}" alt="">Recent Sessions
                    </h3>
                    <div class="widget-content">
                        @php
                        $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                        @endphp
                        <ul>
                            @foreach($sessions as $session)
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        @php
                                        $day = date("d", strtotime($session->post_date));
                                        $month = date("M", strtotime($session->post_date));
                                        @endphp
                                        <p>{{ $day }}</p>
                                        <p>{{ $month }}</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">{{ $session->title }}</a>
                                        </h4>
                                        <p>{{ $session->excerpt }}</p> 
                                    </div>
                                </article>											
                            </li>
                            @endforeach
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        <p>24</p>
                                        <p>sep</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                                        </h4>
                                        <p>
                                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                                        </p> 
                                    </div>
                                </article>											
                            </li>
                        </ul>

                        <div>
                            {{-- <a href="#" class="more-link style-01 "><span class="ti-arrow-right"></span>More</a> --}}
                        </div>

                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-4 -->
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Privacy policy</h3>
                            <div class="widget-content">
                                {{-- <p>
                                    Empower Camp is a passion driven company enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is Leisure with Learning, and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment. Each of our programme modules have been custom developed considering the participants and training objectives.
                                </p> --}}
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>



<section class="kopa-area kopa-area-12 kopa-area-no-space">
    <div class="container">
        <div class="widget millside-module-ads-5">
            <div class="widget-content">
                <img src="http://placehold.it/1170x153" alt="">
                <div class="bg-green-2">
                    <div class="row">
                        <div class="col-md-2 col-sm-3 col-xs-12 ">
                            <div class="part-1">
                                <p>Cloudy</p>
                                <p>15<span>o</span><span>F</span></p>
                            </div>
                        </div>
                        <!-- col-md-2 -->
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <div class=" part-2 ul-mh">
                                <div class="icon-weather">
                                    <img src="http://placehold.it/32x27" alt="">
                                </div>
                                <div>
                                    <p>Take advantage of tee times at a special price!</p>
                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                </div>
                            </div>
                        </div>
                        <!-- col-md-10 -->
                    </div>
                    <!-- row --> 
                </div>
            </div>
        </div>
        <!-- widget --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-12 -->


<!-- kopa-area-3 -->
@endsection

@push('styles')
<style>


</style>
@endpush


@push('scripts')
<script>

(function($) {
    var owl_t = jQuery('.owl-carousel-trainings');
	if(owl_t.length){
		owl_t.owlCarousel({
			items: 1,
			loop: true,
			nav: true,
			navText: ["<span class='ti-angle-left'></span>","<span class='ti-angle-right'></span>"],
			dots: false,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			slideSpeed: 2000,
		}); 
	}
})(window.jQuery);
</script>
</script>
@endpush
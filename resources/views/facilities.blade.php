@extends('layouts.app')

@section('meta_title') FACILITIES &#124; empower @endsection
@section('meta_description') FACILITIES Our 6 (AC) Cottages (on twin to quad sharing basis) and 12(air cooled) Swiss Cottage Tents (on quad sharing basis) have been designed for your comfort and safety. For groups and those seeking effective team bonding, we have two AC dormitories (20 beds each). All accommodation has attached @endsection
@section('meta_keywords')  @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">FACILITIES</h3>
                        {{-- <h4 class="subtitle">Team building is awesome</h4> --}}
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">facilities</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Facilities</h3>
                            <div class="widget-content">
                                <p>
                                    Our 6 (AC) Cottages (on twin to quad sharing basis) and 12(air cooled) Swiss Cottage Tents (on quad sharing basis) have been designed for your comfort and safety. For groups and those seeking effective team bonding, we have two AC dormitories (20 beds each). All accommodation has attached toilets and our impeccable standards of maintenance ensure that they are always sparkling clean. Whether you are an individual seeking privacy for yourself or with your family, or someone who loves to laugh and share with a group, we have an accommodation option for you. And yes, our prices offer total value for money.
                                </p>
                            </div>
                            <div class="row">
                                    <h3 class="widget-title style-11 mt20 mb20">Arrival and Departure Policy</h3>
                                <ul class="ul-checked">
                                    <li><i class="fa fa-check-circle"></i>Check in – 1400 HoursEarly arrival is subject to availability.</li>
                                    <li><i class="fa fa-check-circle"></i>Check out – 1200 HoursLate check-outs are available on request and subject to availability</li>
                                    <li><i class="fa fa-check-circle"></i>Corporate groups- Check In- Check out will vary as per program design</li>
                                </ul>
                                <h3 class="widget-title style-11 mt20 mb20">Cancellation Policy</h3>
                                <ul class="ul-checked">
                                    <li><i class="fa fa-check-circle"></i>As per booking note issued.</li>
                                </ul>
                                <h3 class="widget-title style-11 mt20 mb20">Child Policy</h3>
                                <ul class="ul-checked">
                                    <li><i class="fa fa-check-circle"></i>One child over 6 but less than 12 years can stay in the parent’s room. A child’s bed is provided at no additional charge.</li>
                                    <li><i class="fa fa-check-circle"></i>For two children between 6 and 12 years an additional room is necessary. The rate of this room will be 50% of the parent’s applicable room rate.</li>
                                    <li><i class="fa fa-check-circle"></i>If a child is above 12 years of age, a separate room will be required at the parent’s applicable room rate. (An extra bed shall not be provided for anyone over 12 years of age.)</li>
                                </ul>    
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-photo">
                            <h3 class="widget-title style-05"></h3>
                            <div class="widget-content">
                                <ul class="row ct-row-2 ul-mh">
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/ac-cottage-empower.jpg') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/swiss-cottage-tents.jpg') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/tents-int.png') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/gazebo-21-300x225.jpg') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/Surya-mandal.png') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/dorms-accommodation.jpg') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/AB-121-150x150-1.jpg') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/Gazebo1-150x150-1.jpg') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                    <li class="col-md-4 col-sm-6 col-xs-6">
                                        <img class="sp-image" alt="" src="{{ asset('img/facilities/gol-hut-a-1.png') }}">
                                    </li>
                                    <!-- col-md-3 -->
                                </ul>
                                <!-- row -->           
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Accommodation Options</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-article-list-5">
                            <div class="widget-content">
                                <ul>
                                    <li id="ac-cottages">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/facilities/cottage-3-300x225.jpg') }}" alt="Burma-Bridge">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">AC COTTAGES</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        We know that some of our guests need a bit of pampering after all the rugged adventure and close encounters with nature. We have six well-appointed AC Cottages precisely for this segment of the adventurous yet luxury seeking nature lovers who value their privacy. Each cottage is Air Conditioned Double occupancy. Spacious enough to comfortably accommodate 4 pax when needed. Part of a unit with 2 AC Cottages, which has a dressing room and a toilet with all modern amenities.
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="swiss-cottage-tents">
                                        <article class="entry-item ct-item-3">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/facilities/tents-int.png') }}" alt="Burma-Bridge">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">SWISS COTTAGE TENTS</a></h4>
                                                <div class="clearfix">
                                                    <p><b>Swiss Tents for Luxury Camping at Empower</b></p>
                                                    <p>
                                                        Swiss Cottage Tents depict the uniqueness of our camp. If you really want the a camp experience without foregoing your privacy and creature comforts, our Swiss Tents are the choice of accommodation for you.We have 12 tents. Each tent has:Air cooling 2 Pedestal fans Shaded sit-out area Dressing room with luggage counters Four beds and space for a fifth bed Attached toilet with an additional wash basin outside.Many guests prefer these tents for their novel and unusual character. They are very comfortable and give you a nice campish feeling. The tents have been recently been redone.
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                    <li id="dormitory-accommodation">
                                        <article class="entry-item ct-item-2">
                                            <div class="entry-thumb">
                                                <img src="{{ asset('img/facilities/dorms-accommodation.jpg') }}" alt="Flying Fox">													
                                            </div>
                                            <div class="entry-content">
                                                <h4><a href="#">DORMITORY ACCOMMODATION</a></h4>
                                                <div class="clearfix">
                                                    <p>
                                                        Well-appointed and ideal for large groups, Dormitory accomodation at Empower have become a preferred option for our corporate clients. They also offer an extended opportunity for team building, as the group of adventurers can talk on about the day thrills and chills till they doze off.Both the dormitories have: Air Conditioning 20 bed capacity Attached toilet with 3 washbasins, 2 toilets and 3 bathrooms Open air showers
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="gazebo-the-hub" class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="widget millside-module-ads-5">
                                    <div class="widget-content">
                                        <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                        <div class="bg-green-2">
                                            <div class="row mtm30">
                                                <div class="col-md-12">
                                                    <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Gazebo-The Hub</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <h3 class="widget-title style-11 mt20 mb20">Gazebo – The Hub at Empower</h3>
                            <p>
                                Waking up to the chirping of birds and breathing in the fresh air abundant with oxygen is going to whet your appetite for a sumptuous breakfast. The rigors of the activities and the calories consumed by the sheer fun of adventure need to be nourished with well-balanced meals.
                            </p>
                            <h3 class="widget-title style-11 mt20 mb20">Gazebo – The Community Dining Lounge</h3>
                            <p>
                                Our community dining lounge, Gazebo is designed to be in tune with the outdoors theme all around. A Gazebo is synonymous with rest and recuperation, and our Gazebo makes sure you are well nourished for all the fun and learning ahead.Besides being a Food Pavilion, the Gazebo is a happening place at the camp. It has the Reception, a Tuck Shop, a Telephone Booth, a Cold Drink counter and a First Aid Centre. The Gazebo also adorns, with pride and a little humility, the logos of our elite clientele.During inclement weather or otherwise we have an option of conducting activities in the 40 Ft the hexagon shaped Gazebo.
                            </p>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <ul class="row">
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/gazebo-21-300x225.jpg') }}" alt="Gazebo">
                                        </div>
                                    </article>
                                </li>
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/Gazebo3-300x225.jpg') }}" alt="Gazebo3">
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="camp-activity-area" class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="widget millside-module-ads-5">
                                    <div class="widget-content">
                                        <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                        <div class="bg-green-2">
                                            <div class="row mtm30">
                                                <div class="col-md-12">
                                                    <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Camp Activity Area</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <h3 class="widget-title style-11 mt20 mb20">Activity Area at Empower Camp</h3>
                            <p>
                                Most of the 50 acres vast and open landscape forms the Activity Area of Empower Camps. The place where all action takes place.The Activity Area has open grounds with sports like Foot Cricket, Jungle Golf, Volley Ball etc. It also has a DCC (Dynamic Challenge Course – a very powerful Leadership and Team Building tool), Rope course with Flying Fox, Burma Bridge, a Fast Track Obstacle course, A Target Shooting Range, a Trust Track, Paint Ball Arena, Para Sailing Ground etc. We have a dynamic development of activities in this area.
                            </p>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <ul class="row">
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/Surya-mandal.png') }}" alt="Surya-Mandal">
                                        </div>
                                    </article>
                                </li>
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/Lake-Side-Gazebo.png') }}" alt="Lake-Side-Gazebo">
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <h3 class="widget-title style-11 mt20 mb20">Beautiful Lake at Empower Camp</h3>
                            <p>
                                The Western end of this beautiful landscape is adorned by the lake. A vast water body where we conduct a host of water activities – both for training and for leisure. This lake goes dry from Dec/Jan to Jun when we use a lake about 6 km away. River Kunadalika, which is in close proximity, permits us another adventure sport – White Water Rafting, round the year.The Activity Area or the Training Area, as you may like to call it, is the modern Gurukul. Going by the Gurukul traditions, our unique class rooms under trees or open air shelters called Gazebos. These are the ones where we sit down after an outdoor activity to share our experiences and learn through a process called the debriefing.
                            </p>
                            <p><b>We have the following Gazebos:-</b></p>
                            {{-- <ul class="ul-checked">
                                <li><i class="fa fa-check-circle"></i>Waterfall Gazebo</li>
                                <li><i class="fa fa-check-circle"></i>Gol Hut</li>
                                <li><i class="fa fa-check-circle"></i>Rendezvous (RV)</li>
                                <li><i class="fa fa-check-circle"></i>Surya Mandal</li>
                                <li><i class="fa fa-check-circle"></i>Lake Side Gazeb</li>
                            </ul> --}}
                            <ul>
                                <li>Waterfall Gazebo</li>
                                <li>Gol Hut</li>
                                <li>Rendezvous (RV)</li>
                                <li>Surya Mandal</li>
                                <li>Lake Side Gazeb</li>
                            </ul>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <ul class="row">
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/Surya-mandal.png') }}" alt="Surya-Mandal">
                                        </div>
                                    </article>
                                </li>
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/Lake-Side-Gazebo.png') }}" alt="Lake-Side-Gazebo">
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="conference-hall" class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="widget millside-module-ads-5">
                                    <div class="widget-content">
                                        <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                        <div class="bg-green-2">
                                            <div class="row mtm30">
                                                <div class="col-md-12">
                                                    <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Conference Hall</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <h3 class="widget-title style-11 mt20 mb20">Conference Hall, Perferct Venue for Corporate Events</h3>
                            <p>
                                A new state-of-art conference hall has recently been commissioned. A 1200 sq ft hall, has a facility of being partitioned into one -third and two-third. It can accommodate about 180 people in theatre style and about 80 people in cluster arrangement. We also have arrangements of seating on the floor on mattresses.
                            </p>
                        </div>
                        <div class="widget-content mt30 mb30">
                            <ul class="row">
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/20140207_000700.jpg') }}" alt="Gazebo">
                                        </div>
                                    </article>
                                </li>
                                <li class="col-md-6 col-sm-12 col-xs-12">
                                    <article class="entry-item">
                                        <div class="entry-thumb">
                                            <img src="{{ asset('img/facilities/20140207_000719-Copy.jpg') }}" alt="Gazebo3">
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                        <h3 id="safety-measures" class="widget-title style-11 mt20 mb20">SAFETY MEASURES</h3>
                        <p>
                            At Empower Camps, safety of participants is a prime concern and we make no compromises on that. We use high quality safety equipment and gears. Our instructors are highly qualified and trained in various skills like mountaineering, life saving in water activities and First Aid.We have medical facilities in close proximity and can provide a doctor at site on special demand.
                        </p>
                    </div>
                </div>
                    <!-- col-md-12 -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/testimonials-for-outbound-training.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Testimonials</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="testimonial-container">
                    <div class="dk-container">
                        <div class="cd-testimonials-wrapper cd-container">
                            <ul class="cd-testimonials">
                                @php
                                $testimonials = App\Models\Testimonial::where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                @endphp
                                @foreach($testimonials as $testimonial)
                                <li>
                                    <div class="testimonial-content">
                                        <p>
                                            @php
                                            echo $testimonial->content;
                                            if($testimonial->content_image) {
                                                echo $testimonial->contentImage();
                                            }
                                            @endphp
                                        </p>
                                        <div class="cd-author">
                                            @php
                                            echo $testimonial->authorImage();
                                            @endphp
                                            
                                            {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                            <ul class="cd-author-info">
                                                <li>{{ $testimonial->author }},<br> <span>@php echo $testimonial->designation @endphp</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- cd-testimonials -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="FACILITIES">
                                    {{ csrf_field() }}
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
										</div>
									</div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
										</div>
                                    </div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
										</div>
									</div>

								</form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();
})(window.jQuery);
</script>
@endpush
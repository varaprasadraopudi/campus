@extends('layouts.app')

@section('meta_title') Empower Activity Camps - FAQS @endsection
@section('meta_description')About Empower Camp Empower Camp is a passion driven company enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is Leisure with Learning, and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment.@endsection
@section('meta_keywords')  @endsection

@section('main-content')
<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/adventure-training.jpg') }}" alt="Adventure Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">FAQS</h3>
                        <h4 class="subtitle">“Empowering people by helping them discover their own strengths”</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/home') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/faq') }}">
                                    <span itemprop="title">Frequently Asked Questions</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 sidebar ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget " style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="kids adventure summer camp 2018">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>

                <div class="widget millside-module-event-1 ">
                    <h3 class="widget-title style-01">
                        <img src="{{ asset('img/icons/list.png') }}" alt="">Recent Sessions
                    </h3>
                    <div class="widget-content">
                        @php
                        $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                        @endphp
                        <ul>
                            @foreach($sessions as $session)
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        @php
                                        $day = date("d", strtotime($session->post_date));
                                        $month = date("M", strtotime($session->post_date));
                                        @endphp
                                        <p>{{ $day }}</p>
                                        <p>{{ $month }}</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">{{ $session->title }}</a>
                                        </h4>
                                        <p>{{ $session->excerpt }}</p> 
                                    </div>
                                </article>											
                            </li>
                            @endforeach
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        <p>24</p>
                                        <p>sep</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                                        </h4>
                                        <p>
                                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                                        </p> 
                                    </div>
                                </article>											
                            </li>
                        </ul>

                        <div>
                            {{-- <a href="#" class="more-link style-01 "><span class="ti-arrow-right"></span>More</a> --}}
                        </div>

                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-4 -->
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Frequently Asked Questions</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class ="col-md-12 mb20">
                        <h5 style="text-align: left;">What makes Empower different from other camps and training programmes?</h5>
                        <span> Everything – the superb location, passionate and experienced team, our standard of service and accommodation, and to top it all, the incredible prices. Believe us, there is nothing else like Empower.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">Are your training programmes only at your camp?</h5>
                        <span> Everything – the superb location, passionate and experienced team, our standard of service and accommodation, and to top it all, the incredible prices. Believe us, there is nothing else like Empower.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">What makes Empower different from other camps and training programmes?</h5>
                        <span> Not at all. Empower also organises on-site training programmes for clients and external training programmes at other locations as per client request. We offer visiting lectures and workshops as well.<br>
                            We have conducted outbound programmes at Barog (Shimla Hills), Bangalore, Hyderabad, Delhi NCR, Jaipur, Kolkatta, Goa, Raipur etc. Being an adventurous lot, we are always game to explore new locations. Just get in touch with us and we will figure out the rest.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">Do you customize your training programmes?</h5>
                        <span> Yes, all training programmes are customised to suit organisational requirements. In fact that’s our greatest strength. We do extensive pre-work to ascertain the client’s requirement and then offer follow up sessions to reinforce transfer of learnings from our camp site to your work place.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">Where is Empower?</h5>
                        <span> Empower Camp is easily accessible from both Mumbai and Pune. Our camp site is located off Kolad on Mumbai Goa Highway (NH 17), about 140 km from Mumbai and about 105 km on SH 60 from Chandni Chowk Pune. For exact location, please take a look at our Location Map.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">What can we do here?</h5>
                        <span> Empower has designed and developed leisure ‘n’ learn activities for every age group, fitness level and taste. The camp itself is built on 50 acres of lush green table-land, with a lake on one side and a hill on the other. River Kundalika is at a short distance. Kolad is well-known for its natural foliage and luxuriant greenery. Need more proof? Take a look at our photos and videos.<br>
                            We make it a policy to combine fun with real learning. Here you can choose from a range of options, from serious & intense outbound training garnished with the flavour of fun, to management games, adventure camps, leisure get-aways, conferences and offsites. Or, you can just holiday in this blissfully beautiful location. There are so many options that you are sure to find one, or more likely, a lot of options that appeal to you.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">What adventure activities are available?</h5>
                        <span> We offer a diverse range of activities, including white-water rafting, hot-air balloon rides, rock-climbing, trekking and para-sailing. Some activities require suitable weather and some need minimum numbers. To know which activities will be available at the time of your visit, simply drop us a mail, and we will let you know.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">What do you mean by experiential learning?</h5>
                        <span> Simply put, experiential learning is what we learn by experience, hands on. Just the way we never forget how to swim or cycle once we learn it, some things are best learnt in a real world setting, where all the senses come into play. This ensures that the learning is hard wired in our brains, and becomes a part of us. To read more about experiential learning, you can take a look at this article.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">What kind of activities can we participate in?</h5>
                        <span> Empower offers a huge range of activities to choose from. You can write to us with details of your group size and interests and we can suggest an existing package or custom design something for you.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">What measures do you have in place to ensure safety of the participants?</h5>
                        <span> Safety and Security are given a very high priority at EMPOWER. We use the highest quality of certified equipment, highly qualified and experienced instructors, and have stringent rules and processes that participants need to follow. There is no compromise on this.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">Who has Empower trained so far?</h5>
                        <span> We are happy and proud to have worked with esteemed clients like M tv, ICICI Bank, Pepsi and Kotak. To know more, you can take a look at our clients page.</span>
                    </div>
                    <div class ="col-md-12 mb20">
                        <h5 class ="mt30" style="text-align: left;">Who has Empower trained so far?</h5>
                        <span> TripAdvisor, Facebook and our testimonial page.</span>
                    </div>
                </div>
                <!--Team section end-->
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 --><!--MEDIA COVERAGE-- > <!- row --> 


{{-- <section class="kopa-area kopa-area-12 kopa-area-no-space">
    <div class="container">
        <div class="widget millside-module-ads-5">
            <div class="widget-content">
                <img src="http://placehold.it/1170x153" alt="">
                <div class="bg-green-2">
                    <div class="row">
                        <div class="col-md-2 col-sm-3 col-xs-12 ">
                            <div class="part-1">
                                <p>Cloudy</p>
                                <p>15<span>o</span><span>F</span></p>
                            </div>
                        </div>
                        <!-- col-md-2 -->
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <div class=" part-2 ul-mh">
                                <div class="icon-weather">
                                    <img src="http://placehold.it/32x27" alt="">
                                </div>
                                <div>
                                    <p>Take advantage of tee times at a special price!</p>
                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                </div>
                            </div>
                        </div>
                        <!-- col-md-10 -->
                    </div>
                    <!-- row --> 
                </div>
            </div>
        </div>
        <!-- widget --> 
    </div>
    <!-- container -->
</section> --}}
<!-- kopa-area-12 -->


<!-- kopa-area-3 -->
@endsection

@push('styles')
<style>


</style>
@endpush


@push('scripts')
<script>

(function($) {
    var owl_t = jQuery('.owl-carousel-trainings');
	if(owl_t.length){
		owl_t.owlCarousel({
			items: 1,
			loop: true,
			nav: true,
			navText: ["<span class='ti-angle-left'></span>","<span class='ti-angle-right'></span>"],
			dots: false,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			slideSpeed: 2000,
		}); 
	}
})(window.jQuery);
</script>
</script>
@endpush
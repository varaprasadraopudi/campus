@extends('layouts.app')

@section('meta_title') Empower Activity Camps - About Us @endsection
@section('meta_description')About Empower Camp Empower Camp is a passion driven company enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is Leisure with Learning, and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment.@endsection
@section('meta_keywords')  @endsection

@section('main-content')
<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/adventure-training.jpg') }}" alt="Adventure Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Our Team</h3>
                        <h4 class="subtitle">“Empowering people by helping them discover their own strengths”</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/home') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/about') }}">
                                    <span itemprop="title">About</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 sidebar ">
                <div class="widget millside-module-event-1 mt90">
                    <h3 class="widget-title style-01">
                        <img src="{{ asset('img/icons/list.png') }}" alt="">Recent Sessions
                    </h3>
                    <div class="widget-content">
                        @php
                        $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                        @endphp
                        <ul>
                            @foreach($sessions as $session)
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        @php
                                        $day = date("d", strtotime($session->post_date));
                                        $month = date("M", strtotime($session->post_date));
                                        @endphp
                                        <p>{{ $day }}</p>
                                        <p>{{ $month }}</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">{{ $session->title }}</a>
                                        </h4>
                                        <p>{{ $session->excerpt }}</p> 
                                    </div>
                                </article>											
                            </li>
                            @endforeach
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        <p>24</p>
                                        <p>sep</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                                        </h4>
                                        <p>
                                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                                        </p> 
                                    </div>
                                </article>											
                            </li>
                        </ul>

                        <div>
                            {{-- <a href="#" class="more-link style-01 "><span class="ti-arrow-right"></span>More</a> --}}
                        </div>

                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-4 -->
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                {{-- <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Our Team</h3>
                            <div class="widget-content">
                                <p>
                                    Before we get into specifics, we would like to mention one quality of our team that really sets us apart. Our team has a Army Veteran at the helm. Defence personnel are known for their discipline, leadership and team building skills. But there is more to being an army officer. 
                                </p>
                                <p>
                                    The onerous task of safeguarding the sovereignty of the nation is fraught with risks; and perhaps there is no other profession where trust, teamwork and effective communication can mean the difference between losing lives and saving them. Our founding team has joined forces and combined their learnings from their highly demanding, yet deeply enriching profession to come up with programs that can benefit one and all.
                                </p>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div> --}}
                <!--Team section-->
                <div class="row">
                    <div class="col-md-7">
                        <h3 class="widget-title style-11 ">Mr. Anil Bhasin</h3>
                        <h6>Managing Director</h6>
                        <div class="widget-content">
                            <p>
                                Long and varied experience in travel and human relations, having, on occasions, dealt with difficult situations and emergencies. Travelled extensively around the globe for work and leisure to over 35 countries, spanning all the continents.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-5 text-center">
                        <div class="img-circular" style="background-image: url('{{ asset('img/team/anil-bhasin-empower-1.jpg') }}');">
                            
                        </div>
                        <h5>Mr. Anil Bhasin</h5>
                        <span>Managing Director</span>
                    </div>
                </div>
                <div>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p> 
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                        
                    </p>                 
                </div>

                {{-- <div>
                    <ul>
                        <li>Took voluntary retirement as Manager, Inflight Service Department, Air India after a glorious service of 36 years.</li>
                        <li>Post-graduate in Specialized Hotel Management.</li>
                        <li>Was conferred with the Best Employee award for his dedication and contribution to the airline</li>
                        <li>Attended a marketing workshop conducted by Boeing</li>
                        <li>Vast experience in administration, management and human relations.</li>
                        <li>For the past 5 years, actively involved in Corporate Outbound Training, College to corporate programs and children’s camps</li>
                    </ul>
                </div> --}}
                <!--Team section end-->
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-3 -->
@endsection

@push('styles')
<style>


</style>
@endpush


@push('scripts')
<script>

(function($) {
    var owl_t = jQuery('.owl-carousel-trainings');
	if(owl_t.length){
		owl_t.owlCarousel({
			items: 1,
			loop: true,
			nav: true,
			navText: ["<span class='ti-angle-left'></span>","<span class='ti-angle-right'></span>"],
			dots: false,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			slideSpeed: 2000,
		}); 
	}
})(window.jQuery);
</script>
</script>
@endpush
@extends('layouts.app')

@section('meta_title') Empower Activity Camps - About Us @endsection
@section('meta_description')About Empower Camp Empower Camp is a passion driven company enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is Leisure with Learning, and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment.@endsection
@section('meta_keywords')  @endsection

@section('main-content')
<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/adventure-training.jpg') }}" alt="Adventure Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Our Team</h3>
                        <h4 class="subtitle">“Empowering people by helping them discover their own strengths”</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/home') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/about') }}">
                                    <span itemprop="title">About</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 sidebar ">
                {{-- <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-slider-1">
                            <div class="widget-content">
                                <div class="owl-carousel owl-carousel-trainings">
                                    <div class="item">
                                        <p>170</p>
                                        <p>Corporate<br>Trainings</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                    <div class="item">
                                        <p>310</p>
                                        <p>Outbound Events</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                    <div class="item">
                                        <p>200</p>
                                        <p>Accomodation<br>Strength</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                    <div class="item">
                                        <p>175</p>
                                        <p>Satisfied<br>Corporates</p>
                                        <p><a href="{{ url('/corporate-outbound-training') }}">View all</a></p>
                                    </div>
                                </div>
                            </div>
                            <!-- widget-content -->
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div> --}}

                <div class="widget millside-module-event-1 mt90">
                    <h3 class="widget-title style-01">
                        <img src="{{ asset('img/icons/list.png') }}" alt="">Recent Sessions
                    </h3>
                    <div class="widget-content">
                        @php
                        $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                        @endphp
                        <ul>
                            @foreach($sessions as $session)
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        @php
                                        $day = date("d", strtotime($session->post_date));
                                        $month = date("M", strtotime($session->post_date));
                                        @endphp
                                        <p>{{ $day }}</p>
                                        <p>{{ $month }}</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">{{ $session->title }}</a>
                                        </h4>
                                        <p>{{ $session->excerpt }}</p> 
                                    </div>
                                </article>											
                            </li>
                            @endforeach
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        <p>24</p>
                                        <p>sep</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                                        </h4>
                                        <p>
                                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                                        </p> 
                                    </div>
                                </article>											
                            </li>
                        </ul>

                        <div>
                            {{-- <a href="#" class="more-link style-01 "><span class="ti-arrow-right"></span>More</a> --}}
                        </div>

                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-4 -->
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                {{-- <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Our Team</h3>
                            <div class="widget-content">
                                <p>
                                    Before we get into specifics, we would like to mention one quality of our team that really sets us apart. Our team has a Army Veteran at the helm. Defence personnel are known for their discipline, leadership and team building skills. But there is more to being an army officer. 
                                </p>
                                <p>
                                    The onerous task of safeguarding the sovereignty of the nation is fraught with risks; and perhaps there is no other profession where trust, teamwork and effective communication can mean the difference between losing lives and saving them. Our founding team has joined forces and combined their learnings from their highly demanding, yet deeply enriching profession to come up with programs that can benefit one and all.
                                </p>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div> --}}
                <!--Team section-->
                <div class="row">
                    <div class="col-md-7">
                        <h3 class="widget-title style-11 ">Col Naval Kohli</h3>
                        <h6>Managing Director</h6>
                        <div class="widget-content">
                            <p>
                                He is the Managing Director and co-founder of Empower Activity Camps. Endowed with a rich Experience, he has served the <b>Indian Army for 24 years</b>. He is a post graduate in Senior Level Defence Management  and Computer software & Technology. He has served in UNITED NATIONS PEACE KEEPING FORCE in Somalia and has been awarded the <b>French National Medal of Defence by Govt of France for distinguished service.</b> There as a result of his interaction with <b>International Armed Forces</b> he gained expertise in Training, logistics, macro & micro planning, coordination and execution of plans under adverse conditions.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-5 text-center">
                        <div class="img-circular" style="background-image: url('{{ asset('img/team/col-naval-kohli.jpg') }}');">
                            
                        </div>
                        <h5>Col Naval Kohli</h5>
                        <span>Managing Director</span>
                    </div>
                </div>
                <div>
                    <p>
                        He has commanded various units in the Indian Army & having served as an instructor in Army College of instruction for training officers, he has developed impeccable training skills & mastered the art of applying theoretical knowledge into execution of a plan in an outdoor environment to obtain optimum results.He has experience of military leadership with vast and rich training experience in subjects related to human behavior such as Strategic Alignment, Leadership, Team work, Decision making, Trust Building, Effective Communications, Time Management, Resource Management, Goal and Target setting. Col Naval Kohli is an avid football player and a sportsman.
                    </p>
                    <p>
                        He being an outdoor man brings his love for outbound training to Empower. As a lead Facilitator of Empower where  above 400 companies have been trained he has been a very popular trainer amongst all trainees.He has  rich and vast experience in human resource development and  in the last eight  years has Trained and Empowered a large number of executives from various companies across the corporate world & students from schools & colleges. He facilitated Senior level to middle and grass root level teams across the corporate spectrum of various industries. He has the unique ability of facilitating outbound programmes where he brings out the core issues from the participants during the activities, correlating them to the market and workplace scenario.His passion for performance & ability to propel others’ thoughts & actions make him an inspiring leader. With his strong belief in humanity & spiritual pursuits for self-development, he stands out as a patriotic Indian with the spirit to reach out & touch lives for a better tomorrow.
                    </p>
                </div>
                <!--Team section end-->
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-3 -->
@endsection

@push('styles')
<style>


</style>
@endpush


@push('scripts')
<script>

(function($) {
    var owl_t = jQuery('.owl-carousel-trainings');
	if(owl_t.length){
		owl_t.owlCarousel({
			items: 1,
			loop: true,
			nav: true,
			navText: ["<span class='ti-angle-left'></span>","<span class='ti-angle-right'></span>"],
			dots: false,
			autoplay: true,
			autoplayTimeout: 2000,
			autoplayHoverPause: true,
			slideSpeed: 2000,
		}); 
	}
})(window.jQuery);
</script>
</script>
@endpush
@extends('layouts.app')

@section('meta_title') LEISURE TIPS &#124; empower @endsection
@section('meta_description') What can you do on a Leisure Getaway at EmpowerCamp? Leisure Activities at Empower Camp: Picnic and play Reconnect with extended family Have a mini college reunion Have a learn ‘n’ leisure office offsite Adventure with old friends A yoga camp Chill out and restock on @endsection
@section('meta_keywords') Leisure trip,picnic place,family fun,Adventure with old friends,Chill out place @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Leisure Trips</h3>
                        <h4 class="subtitle">Team building is awesome</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Leisure Trips</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="LEISURE TIPS">
                                    {{ csrf_field() }}
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
										</div>
									</div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
										</div>
                                    </div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
										</div>
									</div>

								</form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->

            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">LEISURE TRIPS</h3>
                            <div class="widget-content">
                                <h3>What can you do on a Leisure Getaway at Empower Camps?</h3>
                                <p>
                                    There is one thing that can cure boredom, stress and fatigue. The same thing can also make you feel good, increase productivity and decrease your cortisol levels and body mass index. Oh no, we are not taking about some latest medical innovation or some miracle cure of a Godman.Believe us, it is a leisure getaway, to a place which offers the right mix of Leisure, Play and Recreation. Without burning a hole in your pocket. Picnics, Leisure Getaways, Offsites.  The names change, but the benefits are always there. From relaxing and recharging to providing bonding time with your colleagues; from catching up with your extended family to getting lost in the tranquil surroundings and finding yourself, a leisure getaway can do wonders for your mind, body and soul. Not to mention your productivity and energy levels.
                                </p>
                                <p>
                                    Empower Camp is set up at an idyllic location with a magnificent backdrop. This is a great place for nature lovers to escape for a quick getaway from the noise and hustle bustle of Mumbai or Pune. Our camp is surrounded by lush greenery, hills, a lake and a river.
                                </p>
                                <p>
                                    We offer a serene, quiet environment of peace and tranquillity for people who like to just chill; and for people who find adventure to be the real stress-buster, we offer a range of adventure activities to choose from.
                                </p>
                            </div>
                            <h3 class=" mt20 mb20">Leisure Activities at Empower Camp:</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="ul-checked">
                                        <li><i class="fa fa-check-circle"></i>Picnic and play</li>
                                        <li><i class="fa fa-check-circle"></i>Reconnect with extended family</li>
                                        <li><i class="fa fa-check-circle"></i>Have a mini college reunion</li>
                                        <li><i class="fa fa-check-circle"></i>Have a learn ‘n’ leisure office offsite</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="ul-checked">
                                        <li><i class="fa fa-check-circle"></i>Adventure with old friends</li>
                                        <li><i class="fa fa-check-circle"></i>A yoga camp</li>
                                        <li><i class="fa fa-check-circle"></i>Chill out and restock on energy</li>
                                        <li><i class="fa fa-check-circle"></i>Gorge on lungfuls of fresh air and green therapy to the eyes</li>
                                    </ul>
                                </div>
                                <div class="widget-content mt30 mb30">
                                    <ul class="row">
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/m13-300x2021.png') }}" alt="Teacher's training">
                                                </div>
                                            </article>
                                        </li>
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/j15-300x2251.png') }}" alt="">
                                                </div>
                                            </article>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <h3 class="widget-title style-11 mt20 mb20"> Why take a quick getaway to Empower Camp from Mumbai or Pune ?</h3>
                            <ul class="ul-checked">
                                <li><i class="fa fa-check-circle"></i>Instant stress buster</li>
                                <li><i class="fa fa-check-circle"></i>Can be fit into a weekend</li>
                                <li><i class="fa fa-check-circle"></i>No need for expensive flights or hotels</li>
                                <li><i class="fa fa-check-circle"></i>Affordable</li>
                                <li><i class="fa fa-check-circle"></i>An ideal location for groups of 10+ pax</li>
                            </ul>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/testimonials-for-outbound-training.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Testimonials</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="testimonial-container">
                    <div class="dk-container">
                        <div class="cd-testimonials-wrapper cd-container">
                            <ul class="cd-testimonials">
                                @php
                                $testimonials = App\Models\Testimonial::where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                @endphp
                                @foreach($testimonials as $testimonial)
                                <li>
                                    <div class="testimonial-content">
                                        <p>
                                            @php
                                            echo $testimonial->content;
                                            if($testimonial->content_image) {
                                                echo $testimonial->contentImage();
                                            }
                                            @endphp
                                        </p>
                                        <div class="cd-author">
                                            @php
                                            echo $testimonial->authorImage();
                                            @endphp
                                            
                                            {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                            <ul class="cd-author-info">
                                                <li>{{ $testimonial->author }},<br><span>{{ $testimonial->designation }}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- cd-testimonials -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();
})(window.jQuery);
</script>
@endpush
@extends('layouts.app')

@section('meta_title') How to reach &#124; empower @endsection
@section('meta_description')"Book Summer Camp 2019 at Kolad, Adventure Resort Built on 50 acre plot. Build Confidence, Care &amp; Courage.@endsection
@section('meta_keywords') @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Summer Camp">
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <h3 class="widget-title style-11 mt30 mb20">How to Reach Empower Camp</h3>
                <div class="row" style= "height: 500px; bottom:20px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="kopa-map-bg">
                            <div class="widget millside-module-map">
                                <div class="widget-content">
                                    <div id="kopa-map" class="kopa-map" data-place="Empower Activity Camps, 263, Roha - Kolad Rd, Sutarwadi, Taluka Kolad, Maharashtra, India" data-latitude="18.403900" data-longitude="73.320315">				  	
                                    </div>    
                                </div>
                            </div>
                            <!-- widget -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="How to reach">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
.mt40 {
    margin-bottom: 40px !important;
}
</style>
@endpush



@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}


(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 388;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton,#bookButton2").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submitting...');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();

    jQuery( '.slider-pro-trainings' ).sliderPro({
        arrows: true,
        buttons: false,
        waitForLayers: false,
        autoplay: true,
        fadeArrows: false,
        fadeOutPreviousSlide: true,
        autoScaleLayers: true,
        responsive: true,
        slideDistance: 25,
        autoplayDelay: 5000,

        width: 550,
        height: 400,
        thumbnailWidth: 214,
        thumbnailHeight: 130,

        visibleSize: '100%',
        breakpoints: {
            1023: {
                width: 500,
                height: 238,
                thumbnailWidth: 155,
                thumbnailHeight: 94,
            },
        },

        init: function(){
            jQuery(".slider-pro").show();   
        }
    });
})(window.jQuery);
</script>
@endpush








<!-- Recent Events -->
<div class="widget millside-module-event-1 ">
    <h3 class="widget-title style-01 mt90">
        <img src="{{ asset('img/icons/list.png') }}" alt="">Recent Events
    </h3>
    <div class="widget-content">
        @php
        $sessions = App\Models\Session::orderBy("post_date", "desc")->limit(5)->get();
        @endphp
        <ul>
            @foreach($sessions as $session)
            <li>
                <article class="entry-item">
                    <div class="entry-date-1">
                        @php
                        $day = date("d", strtotime($session->post_date));
                        $month = date("M", strtotime($session->post_date));
                        @endphp
                        <p>{{ $day }}</p>
                        <p>{{ $month }}</p>
                    </div>
                    <div class="entry-content">
                        <h4 class="entry-title style-03 ">
                            <a href="{{ url('/event/'.$session->pageurl) }}" >{{ $session->title }}</a>
                        </h4>
                        <p>Posted in <a href="{{ url('/event-category/' . $session->training) }}" >{{ $session->training }}</a></p> 
                    </div>
                </article>											
            </li>
            @endforeach
            <!-- <li>
                <article class="entry-item">
                    <div class="entry-date-1">
                        <p>24</p>
                        <p>sep</p>
                    </div>
                    <div class="entry-content">
                        <h4 class="entry-title style-03 ">
                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                        </h4>
                        <p>
                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                        </p> 
                    </div>
                </article>											
            </li> -->
        </ul>
    </div>
</div>
<!-- Upcoming Events --> 
<div class="widget millside-module-event-1 ">
    <h3 class="widget-title style-01">
        <img src="{{ asset('img/icons/list.png') }}" alt="">Upcoming Events
    </h3>
    <div class="widget-content">
    @php
    $sessions = App\Models\Session::where("post_date", ">=", date("Y-m-d"))->orderBy("post_date", "desc")->limit(5)->get();
    @endphp
        <ul>
            @if(count($sessions) != 0)
            @foreach($sessions as $session)
            <li>
                <article class="entry-item">
                    <div class="entry-date-1">
                        @php
                        $day = date("d", strtotime($session->post_date));
                        $month = date("M", strtotime($session->post_date));
                        @endphp
                        <p>{{ $day }}</p>
                        <p>{{ $month }}</p>
                    </div>
                    <div class="entry-content">
                        <h4 class="entry-title style-03 ">
                            <a href="{{ url('/event/'.$session->pageurl) }}" >{{ $session->title }}</a>
                        </h4>
                        <p>Posted in <a href="{{ url('/event-category/' . $session->training) }}" >{{ $session->training }}</a></p> 
                    </div>
                </article>											
            </li>
            @endforeach
            @else
                <p style="text-align:center;">No Upcoming Events</p>
            @endif
            <!-- <li>
                <article class="entry-item">
                    <div class="entry-date-1">
                        <p>24</p>
                        <p>sep</p>
                    </div>
                    <div class="entry-content">
                        <h4 class="entry-title style-03 ">
                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                        </h4>
                        <p>
                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                        </p> 
                    </div>
                </article>											
            </li> -->
        </ul>
    </div>
</div>
<!-- row -->
<!-- Upcoming Events --> 
<div class="widget millside-module-event-1 ">
    <h3 class="widget-title style-01">
        <img src="{{ asset('img/icons/list.png') }}" alt="">Event Categories
    </h3>
    @php
    $categories = App\Models\Session::select('training')->distinct()->get();
    @endphp
    <ul>
        @foreach ($categories as $category)
            <li style="border-width:0px;padding:5px;list-style:circle;margin-left:20px;"><a href="{{ url('/event-category/' . $category->training) }}" >{{ $category->training }}</a></li>
        @endforeach
    </ul>
</div>
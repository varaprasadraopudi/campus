@extends('layouts.app')

@section('meta_title'){{ $session->title }}@endsection
@section('meta_description'){{ $session->excerpt }}@endsection
@section('meta_keywords')  @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Event</h3>
                        <h4 class="subtitle">Adventure Never Stops at Empower Camp</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Event</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->
                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        @php
                        $day = date("d", strtotime($session->post_date));
                        $month = date("M", strtotime($session->post_date));
                        @endphp
                        <article class="entry-item">
                            <div class="entry-content">
                                <header class="entry-header style-01 ">
                                    <div class="entry-date-2">
                                        <p>{{ $day }}</p>
                                        <p>{{ $month }}</p>
                                    </div>
                                    <div>
                                        <h4 class="entry-title style-08 ">
                                            <a href="#">{{ $session->title}}</a>
                                        </h4>
                                        <p>
                                            Posted in <a href="{{ url('/event-category/' . $session->training) }}" >{{ $session->training }}</a>
                                        </p>
                                    </div>
                                </header>
                                <div class="clearfix">
                                    <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">

                                    <p class="mt40 mb30">
                                        {!! ($session->content) !!}
                                    </p>
                                </div>
                                <div class="widget millside-module-event-11">
                                    <div class="row mb40" >
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="widget millside-module-ads-5" style="min-height: 50px;">
                                                <div class="widget-content">
                                                    {{-- <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt=""> --}}
                                                    <div class="bg-green-2" style="padding: 30px 0;">
                                                        <div class="row mtm30">
                                                            <div class="col-md-12">
                                                                <h3 id="recent" class="widget-title mt20 mb20" style="padding-left:30px;line-height:20px; font-style: normal; font-weight: normal;">Upcoming Events of {{ $session->training }}</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="row ul-mh">
                                            @php
                                            $sessions = App\Models\Session::where("training", $session->training)->where("post_date", ">=", date("Y-m-d"))->orderBy("post_date", "desc")->limit(4)->get();
                                            @endphp
                                            @foreach($sessions as $session)
                                            <li class="col-md-6 col-sm-12 col-xs-12">
                                                <article class="entry-item">
                                                    <div class="entry-thumb">
                                                        <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                                    </div>
                                                    <div class="entry-content">
                                                        <header class="entry-header style-01 ">
                                                            <div class="entry-date-2">
                                                                @php
                                                                $day = date("d", strtotime($session->post_date));
                                                                $month = date("M", strtotime($session->post_date));
                                                                @endphp
                                                                <p>{{ $day }}</p>
                                                                <p>{{ $month }}</p>
                                                            </div>
                                                            <div>
                                                                <h4 class="entry-title style-05 ">
                                                                    <a href="{{ url('/event/'.$session->pageurl) }}" >{{ $session->title }}</a>
                                                                </h4>
                                                                <p>
                                                                    <a href="#">Posted in {{ $session->training }}</a>
                                                                </p>
                                                            </div>
                                                        </header>
                                                        <div class="clearfix">
                                                            <p>
                                                                {{ $session->excerpt }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            @endforeach
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="Winter Camp for Kids 2018">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
                <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();

    jQuery( '.slider-pro-trainings' ).sliderPro({
        arrows: true,
        buttons: false,
        waitForLayers: false,
        autoplay: true,
        fadeArrows: false,
        fadeOutPreviousSlide: true,
        autoScaleLayers: true,
        responsive: true,
        slideDistance: 25,
        autoplayDelay: 5000,

        width: 692,
        height: 430,
        thumbnailWidth: 214,
        thumbnailHeight: 130,

        visibleSize: '100%',
        breakpoints: {
            1023: {
                width: 500,
                height: 238,
                thumbnailWidth: 155,
                thumbnailHeight: 94,
            },
        },

        init: function(){
            jQuery(".slider-pro").show();   
        }
    });
})(window.jQuery);
</script>
@endpush










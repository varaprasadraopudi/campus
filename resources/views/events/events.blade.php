@extends('layouts.app')

@section('meta_title') EVENTS &#124; empower @endsection
@section('meta_description') Veteran ARMY Officer's venture Empower Activity Camps is a passion driven company dedicated to enhancing human effectiveness through \u2018experiential learning\u2019 in outdoor environment. Our motto is \u201cLeisure and Learn\u201d and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment. Our programme modules are customized considering partcipant requirements - be it corporates, students or leisure and adventure seekers.@endsection
@section('meta_keywords')  @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Events</h3>
                        <h4 class="subtitle">Adventure Never Stops at Empower Camp</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/events') }}" class="current-page">
                                    <span itemprop="title">Events</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->
                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <!--Upcoming Events-->
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="widget millside-module-event-11">
                    <div class="row mb40" >
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5" style="min-height: 50px;">
                                <div class="widget-content">
                                    {{-- <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt=""> --}}
                                    <div class="bg-green-2" style="padding: 30px 0;">
                                        <div class="row mtm30">
                                            <div class="col-md-12">
                                                <h3 id="upcoming" class="widget-title mt20 mb20" style="padding-left:30px;line-height:20px; font-style: normal; font-weight: normal">Upcoming Events</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content">
                        <ul class="row ul-mh">
                            @php
                            $sessions = App\Models\Session::where("post_date", ">=", date("Y-m-d"))->orderBy("post_date", "desc")->limit(6)->get();
                            @endphp
                            @if(count($sessions) != 0)
                            @foreach($sessions as $session)
                            <li class="col-md-6 col-sm-12 col-xs-12">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                    </div>
                                    <div class="entry-content">
                                        <header class="entry-header style-01 ">
                                            <div class="entry-date-2">
                                                @php
                                                $day = date("d", strtotime($session->post_date));
                                                $month = date("M", strtotime($session->post_date));
                                                @endphp
                                                <p>{{ $day }}</p>
                                                <p>{{ $month }}</p>
                                            </div>
                                            <div>
                                                <h4 class="entry-title style-05 ">
                                                    <a href="{{ url('/event/'.$session->pageurl) }}" >{{ $session->title }}</a>
                                                </h4>
                                                <p>
                                                    <a href="#">Posted in {{ $session->training }}</a>
                                                </p>
                                            </div>
                                        </header>
                                        <div class="clearfix">
                                            <p>
                                                {{ $session->excerpt }}
                                            </p>
                                        </div>
                                    </div>
                                </article>
                            </li>
                            @endforeach
                            @else
                                <p style="text-align:center;">No Upcoming Events</p>
                            @endif
                        </ul>
                    </div>
                </div>
                <!-- widget --> 
                                    <!--  Recent Events -->
                <div class="widget millside-module-event-11">
                    <div class="row mb40" >
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5" style="min-height: 50px;">
                                <div class="widget-content">
                                    {{-- <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt=""> --}}
                                    <div class="bg-green-2" style="padding: 30px 0;">
                                        <div class="row mtm30">
                                            <div class="col-md-12">
                                                <h3 id="recent" class="widget-title mt20 mb20" style="padding-left:30px;line-height:20px; font-style: normal; font-weight: normal;">Recent Events</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content">
                        <ul class="row ul-mh">
                            @php
                            $sessions = App\Models\Session::where("post_date", "<", date("Y-m-d"))->orderBy("post_date", "desc")->limit(6)->get();
                            @endphp
                            @foreach($sessions as $session)
                            <li class="col-md-6 col-sm-12 col-xs-12">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                    </div>
                                    <div class="entry-content">
                                        <header class="entry-header style-01 ">
                                            <div class="entry-date-2">
                                                @php
                                                $day = date("d", strtotime($session->post_date));
                                                $month = date("M", strtotime($session->post_date));
                                                @endphp
                                                <p>{{ $day }}</p>
                                                <p>{{ $month }}</p>
                                            </div>
                                            <div>
                                                <h4 class="entry-title style-05 ">
                                                    <a href="{{ url('/event/'.$session->pageurl) }}" >{{ $session->title }}</a>
                                                </h4>
                                                <p>
                                                    <a href="#">Posted in {{ $session->training }}</a>
                                                </p>
                                            </div>
                                        </header>
                                        <div class="clearfix">
                                            <p>
                                                {{ $session->excerpt }}
                                            </p>
                                        </div>
                                    </div>
                                </article>
                            </li>
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                @include('events.sidebar')
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();
})(window.jQuery);
</script>
@endpush
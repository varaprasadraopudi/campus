@extends('layouts.app')

@section('meta_title') Sample &#038; Empower Activity Camps @endsection
@section('meta_description')  @endsection
@section('meta_keywords')  @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Samples</h3>
                        <h4 class="subtitle">Team building is awesome</h4>
                        <button id="bookButton2" class="btn btn-default">Book Now</button>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Trainings</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Corporate Training</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            

            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Sample</h3>
                            <div class="widget-content">
                                <p>
                                    It is often said that employees are the most valuable resource of a company.Are you getting the best out of your employees?
                                </p>
                                <p>
                                    The performance of your employees is highly dependent on teamwork, trust and effective communication skills. To perform at their optimum levels and achieve organizational objectives, the team needs time out and trust building exercises.
                                </p>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-slider-pro-trainings">
                            {{-- <h3 class="widget-title text-center style-11">Course Photo Gallery</h3> --}}
                            <div class="widget-content">
                                <div class="slider-pro slider-pro-trainings">
                                    <div class="sp-slides">
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-4.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-4.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-5.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-5.jpg') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
                <div class="widget millside-module-intro-9">
                    <h3 class="widget-title style-11 ">Sample</h3>
                    <div class="widget-content">
                        <p>
                            Seating them in an A/C room and lecturing while pointing towards a power-point presentation is going to achieve nothing, apart from boring them so much that they either go to sleep or start day-dreaming in their cushy chairs.
                        </p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Sample</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="img-circular" style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}');">
                            <span></span>
                        </div>
                        <h6>Residential Leadership and Team Building Camps at our camp site at Kolad.</h6>
                    </div>
                    <div class="col-md-4 text-center">
                            <div class="img-circular" style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}');"></div>
                        <h6>Residential Leadership and Team Building Camps at a venue of your choice.</h6>
                    </div>
                    <div class="col-md-4 text-center">
                            <div class="img-circular" style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}');"><span></span></div>
                        <h6>Corporate Sales Meets and Conferences with a value adding element of Team Building.</h6>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->

            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                @include('layouts.partials.highlights')

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                        <input type="hidden" name="source" value="Sample">
                                    {{ csrf_field() }}
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
										</div>
									</div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
										</div>
                                    </div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
										</div>
									</div>

								</form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton,#bookButton2").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submitting...');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();

    jQuery( '.slider-pro-trainings' ).sliderPro({
        arrows: true,
        buttons: false,
        waitForLayers: false,
        autoplay: true,
        fadeArrows: false,
        fadeOutPreviousSlide: true,
        autoScaleLayers: true,
        responsive: true,
        slideDistance: 25,
        autoplayDelay: 5000,

        width: 692,
        height: 430,
        thumbnailWidth: 214,
        thumbnailHeight: 130,

        visibleSize: '100%',
        breakpoints: {
            1023: {
                width: 500,
                height: 238,
                thumbnailWidth: 155,
                thumbnailHeight: 94,
            },
        },

        init: function(){
            jQuery(".slider-pro").show();   
        }
    });
})(window.jQuery);
</script>
@endpush
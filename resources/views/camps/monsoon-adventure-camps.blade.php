@extends('layouts.app')

@section('meta_title') Monsoon Adventure Camp &#124; empower @endsection
@section('meta_description')"Monsoon Adventure Camp &quot; Limited seats available Book Now! OVERVIEW ITINERARY GALLERY MAP CHARGES WHY US? Camp Overview Veteran ARMY Officer's venture Empower Activity Camps is a passion driven company".@endsection
@section('meta_keywords') @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
        <img src="{{ asset('img/adventure/adventure-activities-empower.png') }}" alt="Adventure activities near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Monsoon Adventure Camp</h3>
                        <h4 class="subtitle">“To Make Good Human Beings”</h4>
                        {{-- <button id="bookButton2" class="btn btn-default">Book Now</button> --}}
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Camps</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Monsoon Adventure Camp</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Camp Overview</h3>
                            <div class="widget-content">
                                <p>
                                    Veteran ARMY Officer’s venture Empower Activity Camps is a passion driven company dedicated to enhancing human effectiveness through ‘experiential learning’ in outdoor environment. Our motto is “Leisure and Learn” and we give our heart and soul to make you experience nature to the fullest, within a safe and peaceful environment. Our programme modules are customized considering participant requirements – be it corporates, students or leisure and adventure seekers.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30 mb20">Monsoon Adventure Camp</h3>
                            <div class="widget-content">
                                <p>
                                    Empower Activity Camps Kolad has managed to win hearts of nature lovers, adventure seekers (especially white-water rafters) and travelers looking to relax in the lap of mother nature. The Campsite
                                    receives a tropical climate and is a delight all around the year, however during monsoon the whole area including the nearest villages become paradise due to its natural beauty. The monsoon season in Kolad
                                    lasts from June to September when heavy rains lash out and create more beauty by lush green surroundings and stark white waterfalls. This makes the ideal time to participate in white water rafting
                                    in Kundalika river and trekking through the magnificent natural beauty of Kolad while witnessing its abundant flora and fauna.
                                </p>
                            </div>
                                <!-- <p>
                                    Empower Activity Camps is the no. 1 adventure and training resort in Kolad sprawling on 50 acres land with lake and hill on either side. It is a passion driven company lead by
                                    veteran army officers and hotel management specialists with a motto of “Learn ‘n Leisure”. It is one of the most happening places during the monsoon due to its infrastructure and professional trainers to
                                    give you the best experience if you are looking for adventure and ready to get wet in the wild. Apart from this, you may also visit various water falls and enjoy building rafts and crossing lake on those rafts.
                                    This is one of a kind of experience which every individual should have once during the life time.
                                </p> -->

                                <ul class="ul-checked custom1 mt40 mb30">
                                    <li><i class="fa fa-check-circle"></i><span>Empower Activity Camps is the no. 1 adventure and training resort in Kolad sprawling on 50 acres land with lake and hill on either side. </span></li>
                                    <li><i class="fa fa-check-circle"></i><span>It is a passion driven company lead by veteran army officers and hotel management specialists with a motto of “Learn ‘n Leisure”.</span></li>
                                    <li><i class="fa fa-check-circle"></i><span>It is one of the most happening places during the monsoon due to its infrastructure and professional trainers to give you the best experience if you are looking for adventure and ready to get wet in the wild.</span></li>
                                    <li><i class="fa fa-check-circle"></i><span>Apart from this, you may also visit various water falls and enjoy building rafts and crossing lake on those rafts. </span></li>
                                    <li><i class="fa fa-check-circle"></i><span>This is one of a kind of experience which every individual should have once during the life time.</span></li>
                                </ul>

                                <p>The Campsite has its own beauty amidst the pollution free natural surroundings which attract the adventure seekers and nature lovers with its organic farming of Cashew, Star fruit, Mango, Pineapple etc,
                                    surrounded by lush greenery everywhere our eyes could see. The accommodation is unique in relation to its amenities, service and simplicity which includes Cottages(AC), Swiss Cottage Tents Deluxe (AC)
                                    Swiss Cottage Tents (Air cooled), and Dormitories(Modern, AC). The Dining area called “Gazebo” ads a bit extra spice to the campsite. You can live your life in army style with lots of fun and adventure or you
                                    can just relax and chill amidst the natural beauty while enjoying the beautiful monsoon.</p>
                            
                            <!-- <h3 class="widget-title style-11 mt30 mb20">Accommodation</h3>
                            <div class="widget-content">
                                <p>
                                    Children will be staying in AC dormitories or air cooled Swiss cottage tents, all with attached wash room facilities. Boys and girls will be staying in segregated accommodation. Girls will be under close supervision of a lady trainer/ care taker. Camp has round the clock security.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30 mb20">Activities</h3>
                            <div class="widget-content">
                                <p>
                                    ActivitiesTo enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, ‘Processed Adventure’, Creativity, Practical education in an outdoor environment, and some cultural activities.Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.
                                </p>
                                <p>
                                    Going by our motto ‘learns ‘n leisure’ we have redefined the spelling of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN….N.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30 mb20">Camping at Empower starts with –</h3>
                            <ul class="ul-checked">
                                <li><i class="fa fa-check-circle"></i><b>INTRODUCTIONS :</b> Knowing self and other members of the team</li>
                                <li><i class="fa fa-check-circle"></i><b>TEAM FORMULATION AND FLAG MAKING :</b> Building brotherhood and a sense of belonging to a team and the importance of a flag</li>
                                <li><i class="fa fa-check-circle"></i><b> MORNING PRAYERS :</b> The day starts with a universal prayer.</li>
                                <li><i class="fa fa-check-circle"></i><b> HARIYALI :</b> An entertaining way to introduce the environment and physical training</li>
                            </ul>
                            {{-- <h3 class="widget-title style-11 mt30 ">And the Campers gradually experience</h3> --}}
                            <div class="widget-content mt30">
                                <img src="{{ asset('img/Campers-gradually-experience.png') }}" alt="Campers Gradually Experience">
                            </div> -->
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- <h3 class="widget-title style-11 mt50">Duration and Commercials</h3>
                <div class="widget millside-module-intro-9">
                    <p style=" font-size: 18px;" class="mb30">
                        <span><b>2 N/3 DAYS</b></span><br>
                        <span><b>Rs. 5,999/-PER CHILD</b></span><br>
                        <b>22
                        <span style="position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;">nd
                        </span>&nbsp;to 24
                        <span style="position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;">th
                        </span>&nbsp;Dec 2018</b>
                    </p>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="ul-checked">
                                <li class="mb30"><b>Cost Includes:</b></li>
                                <li><i class="fa fa-check-circle"></i>Stay in Tents/ Dormitory </li>
                                <li><i class="fa fa-check-circle"></i>Meals from Day one lunch to Day last lunch</li>
                                <li><i class="fa fa-check-circle"></i>Veg Meals & Activities</li>
                                <li><i class="fa fa-check-circle"></i>Expert, Experienced & Certified Team</li>
                                <li><i class="fa fa-check-circle"></i>Purified water for entire camp period</li>
                                <li><i class="fa fa-check-circle"></i>All Tested & Safety gears for adventure Activities</li>
                                <li><i class="fa fa-check-circle"></i>First aid Facility throughout the camp</li>
                                <li><i class="fa fa-check-circle"></i>Transport included</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="ul-checked">
                                <li class="mb30"><b>Cost Excludes:</b></li>
                                <li><i class="fa fa-check-circle"></i>Mineral Water / Cold Drinks, etc.</li>
                                <li><i class="fa fa-check-circle"></i>Food charges during Bus Journey.</li>
                                <li><i class="fa fa-check-circle"></i>Personal expenditure of any kind.</li>
                                <li><i class="fa fa-check-circle"></i>Medical expenses other than First aid.</li>
                                <li><i class="fa fa-check-circle"></i>White Water Rafting Rs 1000/-per head</li>
                                <li><i class="fa fa-check-circle"></i>Any other charges not specifically mentioned in “Fees Include”.</li>
                                <li><i class="fa fa-check-circle"></i>Tax is applicable on the final Invoice.</li>
                            </ul>
                        </div>
                    </div>

                </div> -->
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Camp Gallery</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb30">
                    <p>
                        <b>The Campsite has its own beauty amidst the pollution free natural surroundings which attract the adventure seekers and nature lovers.</b>
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-slider-pro-trainings">
                            <div class="widget-content">
                                <div class="slider-pro slider-pro-trainings">
                                    <div class="sp-slides">
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-1.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-1.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-2.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-2.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-3.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-3.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-4.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-4.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-5.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-5.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-6.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-6.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-7.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-7.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-8.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-8.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-9.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-9.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-10.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-10.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-11.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/Monsoon-Adventure-Camps-11.jpg') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                    <div class="bg-green-2">
                                        <div class="row mtm30">
                                            <div class="col-md-12">
                                                <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Camp Informations</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <p style=" font-size: 18px;" class="mb30 mt40">
                            <span><b>Campsite distance from Mumbai – 130 Kms;</b></span><br>
                            <span><b>Panvel – 70 kms;</b></span><br>
                            <span><b>Harihareshwar – 80kms;</b></span><br>
                            <span><b>Nagothane – 25 kms;</b></span><br>
                            <span><b> Roha – 25 kms;</b></span><br>
                            <span><b>MurudJanjira – 80 kms;</b></span><br>
                        </p>
                    </div>
                    <div>
                        <h3 class="widget-title style-11 mt30 mb20">Safety</h3>
                        <ul class="ul-checked">
                            <li><i class="fa fa-check-circle"></i>Utmost care is taken to ensure the participants have a great time at the campus and no compromise is made on the safety and hygiene aspects.</li>
                            <li><i class="fa fa-check-circle"></i>Our junior trainers are trained and certified.</li>
                            <li><i class="fa fa-check-circle"></i>Equipment used for adventure is of European standards.</li>
                        </ul>
                        <h3 class="widget-title style-11 mt30 mb20">Propriety</h3>
                        <div class="widget-content">
                            <p>
                                The data furnished in this document and its attachments are not expected to be disclosed outside the account and may not be duplicated, used or disclosed in whole or part for any purpose other than to evaluate this information for conduct of this program.
                            </p>
                        </div>
                        <h3 class="widget-title style-11 mt30 mb20">Medical</h3>
                        <div class="widget-content">
                            <p>
                                We are fully equipped to provide First aid facilities at all activity locations along withtrained staff. Two local doctors (non MBBS) are available in a village 4 kms away.
                            </p>
                            <p>
                                We have appropriate arrangements to transfer a patient to Gandhi Hospital,Koladwhich is 20 km away and takes approx. 30 mins to reach.
                            </p>
                        </div>
                    </div>
                </div>
                <h3 class="widget-title style-11 mt30 mb20">How to Reach Empower Camp</h3>
                <div class="row" style= "height: 500px; bottom:20px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="kopa-map-bg">
                            <div class="widget millside-module-map">
                                <div class="widget-content">
                                    <div id="kopa-map" class="kopa-map" data-place="Empower Activity Camps, 263, Roha - Kolad Rd, Sutarwadi, Taluka Kolad, Maharashtra, India" data-latitude="18.403900" data-longitude="73.320315">				  	
                                    </div>    
                                </div>
                            </div>
                            <!-- widget -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="Winter Camp for Kids 2018">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
.mt40 {
    margin-bottom: 40px !important;
}
.ul-checked.custom1 li i {
    display: inline-block;
    vertical-align: top;
    margin-top: 3px;
}
.ul-checked.custom1 li span {
    display: inline-block;
    width: 90%;
    margin-left: 5px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 388;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton,#bookButton2").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submitting...');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();

    jQuery( '.slider-pro-trainings' ).sliderPro({
        arrows: true,
        buttons: false,
        waitForLayers: false,
        autoplay: true,
        fadeArrows: false,
        fadeOutPreviousSlide: true,
        autoScaleLayers: true,
        responsive: true,
        slideDistance: 25,
        autoplayDelay: 5000,

        width: 550,
        height: 400,
        thumbnailWidth: 214,
        thumbnailHeight: 130,

        visibleSize: '100%',
        breakpoints: {
            1023: {
                width: 500,
                height: 238,
                thumbnailWidth: 155,
                thumbnailHeight: 94,
            },
        },

        init: function(){
            jQuery(".slider-pro").show();   
        }
    });
})(window.jQuery);
</script>
@endpush
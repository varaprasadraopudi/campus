@extends('layouts.app')

@section('meta_title') Special Packages for Summer Camp 2019 &#124; empower @endsection
@section('meta_description')"Book Summer Camp for Kids at Kolad, Adventure Resort Built on 50 acre plot. Build Confidence, Care &amp; Courage.@endsection
@section('meta_keywords') @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/summer-camp.jpg') }}" alt="Summer Camp">
            {{-- <img src="{{ asset('img/Summer-Camp-Special-Packages-2019.gif') }}" alt="Summer Camp"> --}}
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Summer Camp 2019</h3>
                        <h4 class="subtitle">“Empower Activity Camps”</h4>
                        {{-- <button id="bookButton2" class="btn btn-default">Book Now</button> --}}
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Camps</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Summer Camps 2019</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Camp Overview</h3>
                            <div class="widget-content">
                                <p>
                                    Empower Activity Camps is a passion driven training and adventure resort built on a 50 acre plot of picturesque expanse of table land adjoining a hill on one side and a lake on the other. Beautiful landscaped surroundings bring you in the lap of mother nature to energize , rejuvenate and relieve you from stresses of city life.The resort is owned and managed by retired senior army officers and hotel industry professionals, assisted by a team of dedicated trained staff.
                                </p>
                            </div>
                            <img class="mt30" src="{{ asset('img/Summer-Camp-Special-Packages-2019.gif') }}" alt="Summer Camp">
                            <h3 class="widget-title style-11 mt30 mb20">Notes:</h3>
                            <div class="widget-content">
                                <p>
                                    White water rafting is an outsourced activity and is dependent on release of water from a dam. If it gets cancelled or anyone doesn’t want to do, Empower will not be responsible and no refund will take place. Transport for rafting from camp to rafting site & back will be extra.
                                </p>
                                <p>
                                    All inclusions are part of package & no refund will be given if any facility is not availed/activity is not done.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30 mb20">Children Policy:</h3>
                            <div class="widget-content">
                                <p>
                                    Up to 5 years complimentary (No extra Bed)
                                </p>
                                <P>
                                    6-8 years half rate.
                                </P>
                                <p>
                                    8 years and above full rate. Kids who are under 14 cannot do rafting but can-do Zip line instead of that.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30 mb20">Optional:</h3>
                            <div class="widget-content">
                                <p>
                                    Starters, Mineral water & Juices available at extra cost
                                </p>
                                <P>
                                    DJ Rs 12000/- and Music Rs. 5,000/-per night
                                </P>
                                <p>
                                    Driver’s/cleaners meals Rs. 1,000/-per person per day (buffet) and Rs 700/-per head (staff food).
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30 mb20">Note:</h3>
                            <div class="widget-content">
                                <p>
                                    Above package is applicable for the Corporate outings/offsites too.
                                </p>
                                <P>
                                    Any other Adventure Activity/ Training prog/Team Building prog will be charged extra and GST on it will be applicable.
                                </P>
                                <p>
                                    Full Payment in advance
                                </p>
                            </div>
                            {{-- <h3 class="widget-title style-11 mt30 mb20">Camping at Empower starts with –</h3>
                            <ul class="ul-checked">
                                <li><i class="fa fa-check-circle"></i><b>INTRODUCTIONS :</b> Knowing self and other members of the team</li>
                                <li><i class="fa fa-check-circle"></i><b>TEAM FORMULATION AND FLAG MAKING :</b> Building brotherhood and a sense of belonging to a team and the importance of a flag</li>
                                <li><i class="fa fa-check-circle"></i><b> MORNING PRAYERS :</b> The day starts with a universal prayer.</li>
                                <li><i class="fa fa-check-circle"></i><b> HARIYALI :</b> An entertaining way to introduce the environment and physical training</li>
                            </ul> --}}
                            {{-- <h3 class="widget-title style-11 mt30 ">And the Campers gradually experience</h3> --}}
                            {{-- <div class="widget-content mt30">
                                <img src="{{ asset('img/Campers-gradually-experience.png') }}" alt="Campers Gradually Experience">
                            </div> --}}
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <h3 class="widget-title style-11 mt50">Accommodation:</h3>
                <div class="widget millside-module-intro-9">
                    <p style=" font-size: 18px;" class="mb30">
                        <span><b>Swiss Cottage Tents(Air Cooled)- Rs. 2.950/-Per Head</b></span><br>
                        <span><b>Cottage/Dlx Swiss Cottage Tents(AC)- Rs. 3950/-Per Head</b></span><br>
                        <span><b>Additional Rs. 800/-Per Head for triple Occupancy</b></span><br>
                        <span><b>Rs. 1000/-Per Head for double occupancy. Inclusion will be same.</b></span><br>
                    </p>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="ul-checked">
                                <li class="mb30"><b>Cost Includes:</b></li>
                                <li><i class="fa fa-check-circle"></i>1600 hrs Check in to Check out 1600 hrs</li>
                                <li><i class="fa fa-check-circle"></i>One cycle of Meals(tea,Dinner,Breakfast,lunch)</li>
                                <li><i class="fa fa-check-circle"></i>Trekking, Treasure hunt, Foot Cricket</li>
                                <li><i class="fa fa-check-circle"></i>Burma Bridge, Wall Climbing and Rappelling</li>
                                <li><i class="fa fa-check-circle"></i>Target Shooting, Dodge Ball, Camp fire.</li>
                                <li><i class="fa fa-check-circle"></i>White Water rafting on Day two.</li>
                                <li><i class="fa fa-check-circle"></i>Inclusive of Taxes.</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="ul-checked">
                                <li class="mb30"><b>Cost Excludes:</b></li>
                                
                                <li><i class="fa fa-check-circle"></i>Personal expenditure of any kind.</li>
                                <li><i class="fa fa-check-circle"></i>Medical expenses other than First aid.</li>
                                <li><i class="fa fa-check-circle"></i>Any other charges not specifically mentioned in “Fees Include”.</li>
                                
                            </ul>
                        </div>
                    </div>

                </div>
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Camp Gallery</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb30">
                    {{-- <p>
                        <b>Winter holidays is time for your  child/children  to enjoy, have fun, explore new things, make new friends as well as it’s a great time to learn new things.</b>
                    </p> --}}
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-slider-pro-trainings">
                            <div class="widget-content">
                                <div class="slider-pro slider-pro-trainings">
                                    <div class="sp-slides">
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/summer_camp.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/summer_camp.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/summer_camp1.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/summer_camp1.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/summer_camp2.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/summer_camp2.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/summer_camp3.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/summer_camp3.jpg') }}">
                                        </div>
                                        <div class="sp-slide">
                                            <img class="sp-image" alt="" src="{{ asset('img/events/summer_camp4.jpg') }}">
                                            <img class="sp-thumbnail" alt="" src="{{ asset('img/events/summer_camp4.jpg') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                    <div class="bg-green-2">
                                        <div class="row mtm30">
                                            <div class="col-md-12">
                                                <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Camp Informations</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <p style=" font-size: 18px;" class="mb30 mt40">
                            <span><b>Campsite distance from Mumbai – 130 Kms;</b></span><br>
                            <span><b>Panvel – 70 kms;</b></span><br>
                            <span><b>Harihareshwar – 80kms;</b></span><br>
                            <span><b>Nagothane – 25 kms;</b></span><br>
                            <span><b> Roha – 25 kms;</b></span><br>
                            <span><b>MurudJanjira – 80 kms;</b></span><br>
                        </p>
                    </div>
                    {{-- <div>
                        <h3 class="widget-title style-11 mt30 mb20">Safety</h3>
                        <ul class="ul-checked">
                            <li><i class="fa fa-check-circle"></i>Utmost care is taken to ensure the participants have a great time at the campus and no compromise is made on the safety and hygiene aspects.</li>
                            <li><i class="fa fa-check-circle"></i>Our junior trainers are trained and certified.</li>
                            <li><i class="fa fa-check-circle"></i>Equipment used for adventure is of European standards.</li>
                        </ul>
                        <h3 class="widget-title style-11 mt30 mb20">Propriety</h3>
                        <div class="widget-content">
                            <p>
                                The data furnished in this document and its attachments are not expected to be disclosed outside the account and may not be duplicated, used or disclosed in whole or part for any purpose other than to evaluate this information for conduct of this program.
                            </p>
                        </div>
                        <h3 class="widget-title style-11 mt30 mb20">Medical</h3>
                        <div class="widget-content">
                            <p>
                                We are fully equipped to provide First aid facilities at all activity locations along withtrained staff. Two local doctors (non MBBS) are available in a village 4 kms away.
                            </p>
                            <p>
                                We have appropriate arrangements to transfer a patient to Gandhi Hospital,Koladwhich is 20 km away and takes approx. 30 mins to reach.
                            </p>
                        </div>
                    </div> --}}
                </div>
                <h3 class="widget-title style-11 mt30 mb20">How to Reach Empower Camp</h3>
                <div class="row" style= "height: 500px; bottom:20px;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="kopa-map-bg">
                            <div class="widget millside-module-map">
                                <div class="widget-content">
                                    <div id="kopa-map" class="kopa-map" data-place="Empower Activity Camps, 263, Roha - Kolad Rd, Sutarwadi, Taluka Kolad, Maharashtra, India" data-latitude="18.403900" data-longitude="73.320315">				  	
                                    </div>    
                                </div>
                            </div>
                            <!-- widget -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="source" value="Special Packages for Summer Camp 2019">
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn ct-btn-7">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
.mt40 {
    margin-bottom: 40px !important;
}
</style>
@endpush



@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}


(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 388;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton,#bookButton2").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submitting...');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();

    jQuery( '.slider-pro-trainings' ).sliderPro({
        arrows: true,
        buttons: false,
        waitForLayers: false,
        autoplay: true,
        fadeArrows: false,
        fadeOutPreviousSlide: true,
        autoScaleLayers: true,
        responsive: true,
        slideDistance: 25,
        autoplayDelay: 5000,

        width: 550,
        height: 400,
        thumbnailWidth: 214,
        thumbnailHeight: 130,

        visibleSize: '100%',
        breakpoints: {
            1023: {
                width: 500,
                height: 238,
                thumbnailWidth: 155,
                thumbnailHeight: 94,
            },
        },

        init: function(){
            jQuery(".slider-pro").show();   
        }
    });
})(window.jQuery);
</script>
@endpush








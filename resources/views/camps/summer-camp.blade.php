@extends('layouts.page')

@section('meta_title') kids adventure summer camp &#124; empower @endsection
@section('meta_description')"Book Summer Camp for Kids at Kolad, Adventure Resort Built on 50 acre plot. Build Confidence, Care &amp; Courage.@endsection
@section('meta_keywords') @endsection
@section('no_footer', false)

@section('breadcrumb-image')
    <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}"
         alt="Corporate Outbound Trainings near Pune">
@endsection

@section('breadcrumb-content')
    <h3 class="with-subtitle text-center">Summer Camps</h3>
    <h4 class="subtitle text-center">“Empower Activity Camps”</h4>
    {{-- <button id="bookButton2" class="btn btn-default">Book Now</button> --}}
    <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Camps</span>
                                </a>
                            </span>
        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Summer Camps</span>
                                </a>
                            </span>
    </div>
@endsection

@section('page-content')
    <section class="kopa-area sub-page kopa-area-25">
        <div class="container">
            <div class="row">
                <div class="main-col col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-intro-9">
                                <h3 class="widget-title style-11 ">Camp Overview</h3>
                                <div class="widget-content">
                                    <p>
                                        Empower Activity Camps is a passion driven training and adventure resort built
                                        on a 50 acre plot of picturesque expanse of table land adjoining a hill on one
                                        side and a lake on the other. Beautiful landscaped surroundings bring you in the
                                        lap of mother nature to energize , rejuvenate and relieve you from stresses of
                                        city life.The resort is owned and managed by retired senior army officers and
                                        hotel industry professionals, assisted by a team of dedicated trained staff.
                                    </p>
                                </div>
                                <h3 class="widget-title style-11 mt30 mb20">Kids Summer Camps Announcement!</h3>
                                <div class="widget-content">
                                    <p>
                                        Summer holidays is time for your child/children to enjoy, have fun, explore new
                                        things, make new friends as well as it’s a great time to learn new things.
                                    </p>
                                    <p>
                                        The greatest gifts that parents can give their children are independence and
                                        resiliency, and by selecting OUR CAMPS, you can give both. We can assure you all
                                        will see growth, maturity and confidence when he/she will return home. They will
                                        be more engaged, giving, and confident.
                                    </p>
                                    <p>
                                        A program which is being loved by many children and parents over the past years.
                                        A program that helps your child to open up, learns new skills, prepares them for
                                        challenges and overcome them with less pressure & more abilities and designed in
                                        lines with your children age group and learning capabilities; both physical and
                                        mental.
                                    </p>
                                </div>
                                <h3 class="widget-title style-11 mt30 mb20">Accommodation</h3>
                                <div class="widget-content">
                                    <p>
                                        Children will be staying in AC dormitories or air cooled Swiss cottage tents,
                                        all with attached wash room facilities. Boys and girls will be staying in
                                        segregated accommodation. Girls will be under close supervision of a lady
                                        trainer/ care taker. Camp has round the clock security.
                                    </p>
                                </div>
                                <h3 class="widget-title style-11 mt30 mb20">Activities</h3>
                                <div class="widget-content">
                                    <p>
                                        To enrich the overall personality of participants by giving them exposure to
                                        Natural Environment, Experiential Learning, ‘Processed Adventure’, Creativity,
                                        Practical education in an outdoor environment, and some cultural activities.
                                    </p>
                                    <P>
                                        Some of the Activities planned are given below. They may not be followed rigidly
                                        as mood and energy levels of participants as well as weather conditions
                                        necessitate some modifications.
                                    </P>
                                    <p>
                                        Going by our motto ‘learns ‘n leisure’ we have redefined the spelling of FUN. In
                                        Empower we spell FUN as FUNNNNNNNNNNNN….N.
                                    </p>
                                </div>
                                <h3 class="widget-title style-11 mt30 mb20">Camping at Empower starts with –</h3>
                                <ul class="ul-checked">
                                    <li><i class="fa fa-check-circle"></i><b>INTRODUCTIONS :</b> Knowing self and other
                                        members of the team
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><b>TEAM FORMULATION AND FLAG MAKING :</b>
                                        Building brotherhood and a sense of belonging to a team and the importance of a
                                        flag
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><b> MORNING PRAYERS :</b> The day starts with
                                        a universal prayer.
                                    </li>
                                    <li><i class="fa fa-check-circle"></i><b> HARIYALI :</b> An entertaining way to
                                        introduce the environment and physical training
                                    </li>
                                </ul>
                                {{-- <h3 class="widget-title style-11 mt30 ">And the Campers gradually experience</h3> --}}
                                <div class="widget-content mt30">
                                    <img src="{{ asset('img/Campers-gradually-experience.png') }}"
                                         alt="Campers Gradually Experience">
                                </div>
                            </div>
                            <!-- widget -->
                        </div>
                        <!-- col-md-12 -->
                    </div>

                    <h4 class="entry-title style-02 text-center mt20 mb50 pb20">
                        Camp Gallery
                    </h4>
                    <div class="mb30">
                        <p>
                            <b>Winter holidays is time for your child/children to enjoy, have fun, explore new things,
                                make new friends as well as it’s a great time to learn new things.</b>
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-slider-pro-trainings">
                                <div class="widget-content">
                                    <div class="slider-pro slider-pro-trainings">
                                        <div class="sp-slides">
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/events/summer_camp.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/events/summer_camp1.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/events/summer_camp2.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/events/summer_camp3.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/events/summer_camp4.jpg') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- widget-content -->
                            </div>
                            <!-- widget -->
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="widget millside-module-ads-5">
                                    <div class="widget-content">
                                        <h3 class="widget-title style-11 mt20 mb20"
                                            style="padding-left:30px;line-height:20px;">Camp
                                            Informations</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <p style=" font-size: 18px;" class="mb30 mt40">
                                <span><b>Campsite distance from Mumbai – 130 Kms;</b></span><br>
                                <span><b>Panvel – 70 kms;</b></span><br>
                                <span><b>Harihareshwar – 80kms;</b></span><br>
                                <span><b>Nagothane – 25 kms;</b></span><br>
                                <span><b> Roha – 25 kms;</b></span><br>
                                <span><b>MurudJanjira – 80 kms;</b></span><br>
                            </p>
                        </div>
                        <div>
                            <h3 class="widget-title style-11 mt30 mb20">Safety</h3>
                            <ul class="ul-checked">
                                <li><i class="fa fa-check-circle"></i>Utmost care is taken to ensure the participants
                                    have a great time at the campus and no compromise is made on the safety and hygiene
                                    aspects.
                                </li>
                                <li><i class="fa fa-check-circle"></i>Our junior trainers are trained and certified.
                                </li>
                                <li><i class="fa fa-check-circle"></i>Equipment used for adventure is of European
                                    standards.
                                </li>
                            </ul>
                            <h3 class="widget-title style-11 mt30 mb20">Propriety</h3>
                            <div class="widget-content">
                                <p>
                                    The data furnished in this document and its attachments are not expected to be
                                    disclosed outside the account and may not be duplicated, used or disclosed in whole
                                    or part for any purpose other than to evaluate this information for conduct of this
                                    program.
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt30 mb20">Medical</h3>
                            <div class="widget-content">
                                <p>
                                    We are fully equipped to provide First aid facilities at all activity locations
                                    along withtrained staff. Two local doctors (non MBBS) are available in a village 4
                                    kms away.
                                </p>
                                <p>
                                    We have appropriate arrangements to transfer a patient to Gandhi Hospital,Koladwhich
                                    is 20 km away and takes approx. 30 mins to reach.
                                </p>
                            </div>
                        </div>
                    </div>
                    <h3 class="widget-title style-11 mt30 mb20">How to Reach Empower Camp</h3>
                    <div class="row" style="height: 500px; bottom:20px;">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="kopa-map-bg">
                                <div class="widget millside-module-map">
                                    <div class="widget-content">
                                        <div id="kopa-map" class="kopa-map"
                                             data-place="Empower Activity Camps, 263, Roha - Kolad Rd, Sutarwadi, Taluka Kolad, Maharashtra, India"
                                             data-latitude="18.403900" data-longitude="73.320315">
                                        </div>
                                    </div>
                                </div>
                                <!-- widget -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- col-md-8 -->
{{--                <div class="sidebar col-md-12 col-sm-12 col-xs-12">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                            <div id="widget-booking" class="widget" style="background: #ebebeb; padding: 15px 15px;">--}}
{{--                                <h3 class="widget-title style-04">Make Your Booking</h3>--}}
{{--                                <span id="msg" style="color:red"></span>--}}
{{--                                <div class="widget-content">--}}
{{--                                    <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST"--}}
{{--                                          role="form" novalidate="novalidate">--}}
{{--                                        <input type="hidden" name="source" value="kids adventure summer camp">--}}
{{--                                        {{ csrf_field() }}--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                                                <input type="text" class="form-control" name="name" placeholder="Name"--}}
{{--                                                       required autocomplete="on">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                                                <input type="email" class="form-control" name="email"--}}
{{--                                                       placeholder="Email" required autocomplete="on">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                                                <input type="text" class="form-control" name="phone" placeholder="Phone"--}}
{{--                                                       required autocomplete="on">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                                                <input type="text" class="form-control" name="organization"--}}
{{--                                                       placeholder="Organization" required autocomplete="on">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                                                <textarea class="form-control" rows="3" name="message"--}}
{{--                                                          placeholder="Message"></textarea>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                                                <button id="submit" type="submit" class="btn ct-btn-7">Submit</button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </form>--}}
{{--                                </div>--}}
{{--                                <!-- widget-content -->--}}
{{--                            </div>--}}
{{--                            <!-- widget -->--}}
{{--                        </div>--}}
{{--                        <!-- col-md-12 -->--}}
{{--                    </div>--}}
{{--                    <!-- row -->--}}
{{--                </div>--}}
                <!-- col-md-4 -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-11 -->

@endsection








@extends('layouts.app')

@section('meta_title') Empower Activity Camps - Testimonials @endsection
@section('meta_description')Veteran ARMY Officer's venture Empower Activity Camps is a passion driven company dedicated to enhancing human effectiveness through 2018 experiential learning 2019 in outdoor environment.@endsection
@section('meta_keywords')  @endsection

@section('main-content')
<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/adventure-training.jpg') }}" alt="Adventure Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Testimonials</h3>
                        <h4 class="subtitle">“Empowering people by helping them discover their own strengths”</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/home') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/about') }}">
                                    <span itemprop="title">About</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-12 sidebar  sidebar-about">
                <div class="widget millside-module-event-1 mt90">
                    <h3 class="widget-title style-01">
                        <img src="{{ asset('img/icons/list.png') }}" alt="">Recent Sessions
                    </h3>
                    <div class="widget-content">
                        @php
                        $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                        @endphp
                        <ul>
                            @foreach($sessions as $session)
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        @php
                                        $day = date("d", strtotime($session->post_date));
                                        $month = date("M", strtotime($session->post_date));
                                        @endphp
                                        <p>{{ $day }}</p>
                                        <p>{{ $month }}</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">{{ $session->title }}</a>
                                        </h4>
                                        <p>{{ $session->excerpt }}</p> 
                                    </div>
                                </article>											
                            </li>
                            @endforeach
                            <li>
                                <article class="entry-item">
                                    <div class="entry-date-1">
                                        <p>24</p>
                                        <p>sep</p>
                                    </div>
                                    <div class="entry-content">
                                        <h4 class="entry-title style-03 ">
                                            <a href="#">Mother's Day Sunday Lunch <br>Offer</a>
                                        </h4>
                                        <p>
                                            Whether it is a welcome break to escape the pressures of modern living, a reward for staff, a business event.
                                        </p> 
                                    </div>
                                </article>											
                            </li>
                        </ul>

                        <div>
                            {{-- <a href="#" class="more-link style-01 "><span class="ti-arrow-right"></span>More</a> --}}
                        </div>

                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-4 -->
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 mt30"> Our Philosophy</h3>
                            <div class="widget-content">
                                <p>
                                    “Empowering people by helping them discover their own strengths”. This is the philosophy behind Empower Activity Camps that has benefited more than 400 organizations. Our primary means of achieving this objective is experiential learning through outbound training. Put plainly, it is super-learning while having super-fun with adventure and outdoor activities.
                                </p>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!--Team section-->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 id="corporate" class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Corporate</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testimonial-container">
                    <div class="dk-container">
                        <div class="cd-testimonials-wrapper cd-container">
                            <ul class="cd-testimonials">
                                @php
                                $testimonials = App\Models\Testimonial::where("category", "Corporate")->where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                @endphp
                                @foreach($testimonials as $testimonial)
                                <li>
                                    <div class="testimonial-content">
                                        <p>
                                            @php
                                            echo $testimonial->content;
                                            if($testimonial->content_image) {
                                                echo $testimonial->contentImage();
                                            }
                                            @endphp
                                        </p>
                                        {{-- <p>
                                            @php
                                            if($testimonial->youtube != "") {
                                                $link = $testimonial->youtube;
                                                $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
                                                if (empty($video_id[1]))
                                                    $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

                                                $video_id = explode("&", $video_id[1]); // Deleting any other params
                                                $video_id = $video_id[0];
                                                echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$video_id.'"></iframe>';
                                            }
                                            @endphp
                                        </p> --}}
                                        <div class="cd-author">
                                            @php
                                            echo $testimonial->authorImage();
                                            @endphp
                                            
                                            {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                            <ul class="cd-author-info">
                                                <li>{{ $testimonial->author }},<br> <span>@php echo $testimonial->designation @endphp</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- cd-testimonials -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 id="students" class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Students</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testimonial-container">
                    <div class="dk-container">
                        <div class="cd-testimonials-wrapper cd-container">
                            <ul class="cd-testimonials">
                                @php
                                $testimonials = App\Models\Testimonial::where("category", "Students")->where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                @endphp
                                @foreach($testimonials as $testimonial)
                                <li>
                                    <div class="testimonial-content">
                                        <p>
                                            @php
                                            echo $testimonial->content;
                                            if($testimonial->content_image) {
                                                echo $testimonial->contentImage();
                                            }
                                            @endphp
                                        </p>
                                        {{-- <p>
                                            @php
                                            if($testimonial->youtube != "") {
                                                $link = $testimonial->youtube;
                                                $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
                                                if (empty($video_id[1]))
                                                    $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

                                                $video_id = explode("&", $video_id[1]); // Deleting any other params
                                                $video_id = $video_id[0];
                                                echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$video_id.'"></iframe>';
                                            }
                                            @endphp
                                        </p> --}}
                                        <div class="cd-author">
                                            @php
                                            echo $testimonial->authorImage();
                                            @endphp
                                            
                                            {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                            <ul class="cd-author-info">
                                                <li>{{ $testimonial->author }},<br> <span>@php echo $testimonial->designation @endphp</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- cd-testimonials -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 id="adventure-leisure" class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Adventure & Leisure</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testimonial-container">
                    <div class="dk-container">
                        <div class="cd-testimonials-wrapper cd-container">
                            <ul class="cd-testimonials">
                                @php
                                $testimonials = App\Models\Testimonial::where("category", "Adventure")->where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                @endphp
                                @foreach($testimonials as $testimonial)
                                <li>
                                    <div class="testimonial-content">
                                        <p>
                                            @php
                                            echo $testimonial->content;
                                            if($testimonial->content_image) {
                                                echo $testimonial->contentImage();
                                            }
                                            @endphp
                                        </p>
                                        {{-- <p>
                                            @php
                                            if($testimonial->youtube != "") {
                                                $link = $testimonial->youtube;
                                                $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
                                                if (empty($video_id[1]))
                                                    $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

                                                $video_id = explode("&", $video_id[1]); // Deleting any other params
                                                $video_id = $video_id[0];
                                                echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$video_id.'"></iframe>';
                                            }
                                            @endphp
                                        </p> --}}
                                        <div class="cd-author">
                                            @php
                                            echo $testimonial->authorImage();
                                            @endphp
                                            
                                            {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                            <ul class="cd-author-info">
                                                <li>{{ $testimonial->author }},<br> <span>@php echo $testimonial->designation @endphp</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- cd-testimonials -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <h3 class="widget-title style-11 mt30 text-center">Submit your Testimonial</h3>
                        <form id="testimonialform" action="{{ url('/save_testimonial') }}" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" enctype="multipart/form-data" accept-charset="UTF-8">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" name="designation" placeholder="Designation" required autocomplete="on">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Service</label>
                                    <select class="form-control" name="category" title="Service">
                                        <option value="Corporate">Corporate Training</option>
                                        <option value="Adventure">Adventure & Leisure</option>
                                        <option value="Students">Students Camps</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" class="form-control" name="title" placeholder="Title of Testimonial" required autocomplete="on">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="form-control" rows="3" name="content" required minlength="10" placeholder="Content of Testimonial"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Photo of Author (Optional)</label>
                                    <input type="file" class="form-control" name="author_image" placeholder="Author Image" style="line-height:20px;height:34px;">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Youtube Link (Optional)</label>
                                    <input type="text" class="form-control" name="youtube" placeholder="Youtube Link" autocomplete="on">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn ct-btn-7">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
{{-- <section class="kopa-area kopa-area-12 kopa-area-no-space">
    <div class="container">
        <div class="widget millside-module-ads-5">
            <div class="widget-content">
                <img src="http://placehold.it/1170x153" alt="">
                <div class="bg-green-2">
                    <div class="row">
                        <div class="col-md-2 col-sm-3 col-xs-12 ">
                            <div class="part-1">
                                <p>Cloudy</p>
                                <p>15<span>o</span><span>F</span></p>
                            </div>
                        </div>
                        <!-- col-md-2 -->
                        <div class="col-md-10 col-sm-9 col-xs-12">
                            <div class=" part-2 ul-mh">
                                <div class="icon-weather">
                                    <img src="http://placehold.it/32x27" alt="">
                                </div>
                                <div>
                                    <p>Take advantage of tee times at a special price!</p>
                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                </div>
                            </div>
                        </div>
                        <!-- col-md-10 -->
                    </div>
                    <!-- row --> 
                </div>
            </div>
        </div>
        <!-- widget --> 
    </div>
    <!-- container -->
</section> --}}
<!-- kopa-area-12 -->


<!-- kopa-area-3 -->
@endsection

@push('styles')
<style>


</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
    (function($) {
        // create testimonials slider
        $('.cd-testimonials-wrapper').flexslider({
            selector: ".cd-testimonials > li",
            animation: "slide",
            controlNav: true,
            slideshow: true,
            smoothHeight: true,
            start: function() {
                $('.cd-testimonials').children('li').css({
                    'opacity': 1,
                    'position': 'relative'
                });
            }
        });

        $("#testimonialform").validate({
            
        });

        var owl_t = jQuery('.owl-carousel-trainings');
        if(owl_t.length){
            owl_t.owlCarousel({
                items: 1,
                loop: true,
                nav: true,
                navText: ["<span class='ti-angle-left'></span>","<span class='ti-angle-right'></span>"],
                dots: false,
                autoplay: true,
                autoplayTimeout: 2000,
                autoplayHoverPause: true,
                slideSpeed: 2000,
            }); 
        }
    })(window.jQuery);
</script>

@endpush
@extends('layouts.app')

@section('meta_title')Kolad Adventure Camp | Resort near Mumbai, Pune - Empower Activity Camps @endsection
@section('meta_description')Veteran ARMY Officer's venture Empower Activity Camps specializes in ‘Corporate training’ in Outdoor Environment, 50 acres Adventure Camp near Mumbai and Pune. @endsection
@section('meta_keywords')corporate development training, adventure resorts near mumbai, corporate team building activities, team building activities for employees, white water rafting kolad, kids adventure camp, outbound training team building programs, outbound team building activities, outbound activities for employees, leadership development training activities @endsection

@section('main-content')
    <section class="slide-area">
        <div class="widget millside-module-slider">
            <div class="widget-content">
                <div class="slider-pro slider-pro-1">
                    <div class="sp-slides">

                        <div class="sp-slide">
                            <img class="sp-image sp-image-bg" src="{{ asset('img/slides/empower-camp-1.jpg') }}"
                                 data-src="{{ asset('img/slides/empower-camp-1.jpg') }}" alt="">
                            <div class="sp-layer bg-gray">

                            </div>

                            <p class="sp-layer sp-txt-2" style="color:white;" data-horizontal="7.1%" data-vertical="37%"
                               data-show-transition="right" data-hide-transition="up" data-show-delay="1400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">Empower
                                Activity Camps</p>

                            {{-- <p class="sp-layer sp-txt-1a" data-horizontal="7.1%" data-vertical="37%" data-show-transition="right" data-hide-transition="up" data-show-delay="1800" data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000"></p> --}}

                            <p class="sp-layer sp-txt-22" style="color:white;" data-horizontal="7.1%"
                               data-vertical="50%" data-show-transition="right" data-hide-transition="up"
                               data-show-delay="1000" data-hide-delay="200" data-show-duration="1000"
                               data-hide-duration="1000">No. 1 Training & Adventure Company with Own Resort in Kolad</p>

                            <p class="sp-layer sp-link-1" data-horizontal="7.1%" data-vertical="65.23%"
                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">
                                <a href="{{ url('/about-us') }}" class="">Visit Today
                                </a>
                            </p>

                            {{--                            <p class="sp-layer sp-link-1" data-horizontal="23%" data-vertical="65.23%"--}}
                            {{--                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"--}}
                            {{--                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">--}}
                            {{--                                <a href="tel:772-087-3330" class="">Request Callback</a>--}}
                            {{--                            </p>--}}

                            {{-- <div class="sp-layer sp-link-2" data-horizontal="62%" data-vertical="56.29%" data-show-transition="up" data-hide-transition="fade" data-show-delay="3000" data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">
                                <div>
                                    <p>
                                        <span>NO</span><br>
                                        <span class="span-2">PRE-BOOKING</span><br>
                                        <span class="span-3">REQUIRED</span><br>
                                    </p>
                                    <a href="#">
                                        Book Now
                                    </a>
                                </div>
                            </div> --}}
                        </div>

                        <div class="sp-slide">
                            <img class="sp-image sp-image-bg"
                                 src="{{ asset('img/slides/empower-2-team-building-leadership-training.jpg') }}"
                                 data-src="{{ asset('img/slides/empower-2-team-building-leadership-training.jpg') }}"
                                 alt="">
                            <div class="sp-layer bg-gray">

                            </div>

                            <p class="sp-layer sp-txt-2" style="color:white;" data-horizontal="7.1%" data-vertical="30%"
                               data-show-transition="right" data-hide-transition="up" data-show-delay="1400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">Corporate
                                Outbound Trainings</p>

                            <p class="sp-layer sp-txt-22" style="color:white;" data-horizontal="7.1%"
                               data-vertical="45%" data-show-transition="right" data-hide-transition="up"
                               data-show-delay="1000" data-hide-delay="200" data-show-duration="1000"
                               data-hide-duration="1000">Let your teams learn Planning & Strategising through <br>OUTBOUND
                                Training straight from Senior Army Veterans.</p>

                            <p class="sp-layer sp-link-1" data-horizontal="7.1%" data-vertical="73.23%"
                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">
                                <a href="{{ url('/corporate-outbound-training') }}" class="">Learn More
                                </a>
                            </p>

                            {{--                            <p class="sp-layer sp-link-1" data-horizontal="23%" data-vertical="73.23%"--}}
                            {{--                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"--}}
                            {{--                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">--}}
                            {{--                                <a href="tel:772-087-3330" class="">Request Callback</a>--}}
                            {{--                            </p>--}}
                        </div>

                        <div class="sp-slide">
                            <img class="sp-image sp-image-bg"
                                 src="{{ asset('img/slides/empower-3-learn-strategize.jpg') }}"
                                 data-src="{{ asset('img/slides/empower-3-learn-strategize.jpg') }}" alt="">
                            <div class="sp-layer bg-gray">

                            </div>

                            <p class="sp-layer sp-txt-2" style="color:white;" data-horizontal="7.1%" data-vertical="30%"
                               data-show-transition="right" data-hide-transition="up" data-show-delay="1400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">Team Building
                                &amp; Leadership Training</p>

                            <p class="sp-layer sp-txt-22" style="color:white;" data-horizontal="7.1%"
                               data-vertical="45%" data-show-transition="right" data-hide-transition="up"
                               data-show-delay="1000" data-hide-delay="200" data-show-duration="1000"
                               data-hide-duration="1000">Build High Performance Team Through <br>Outbound Training &
                                Experiential Learning.</p>

                            <p class="sp-layer sp-link-1" data-horizontal="7.1%" data-vertical="73.23%"
                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">
                                <a href="{{ url('/corporate-outbound-training') }}" class="">Learn More
                                </a>
                            </p>

                            {{--                            <p class="sp-layer sp-link-1" data-horizontal="23%" data-vertical="73.23%"--}}
                            {{--                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"--}}
                            {{--                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">--}}
                            {{--                                <a href="tel:772-087-3330" class="">Request Callback</a>--}}
                            {{--                            </p>--}}
                        </div>

                        <div class="sp-slide">
                            <img class="sp-image sp-image-bg"
                                 src="{{ asset('img/slides/empower-4-special-camps-for-children.jpg') }}"
                                 data-src="{{ asset('img/slides/empower-4-special-camps-for-children.jpg') }}" alt="">
                            <div class="sp-layer bg-gray">

                            </div>

                            <p class="sp-layer sp-txt-2" style="color:white;" data-horizontal="7.1%" data-vertical="30%"
                               data-show-transition="right" data-hide-transition="up" data-show-delay="1400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">Special Camps
                                for Children</p>

                            <p class="sp-layer sp-txt-22" style="color:white;" data-horizontal="7.1%"
                               data-vertical="45%" data-show-transition="right" data-hide-transition="up"
                               data-show-delay="1000" data-hide-delay="200" data-show-duration="1000"
                               data-hide-duration="1000">The greatest gifts that parents can give their children are
                                independence <br>
                                and resiliency, and by selecting OUR CAMPS, you can give both.</p>

                            <p class="sp-layer sp-link-1" data-horizontal="7.1%" data-vertical="73.23%"
                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"
                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">
                                <a href="{{ url('/special-packages-for-summer-camp-2019') }}" class="">Learn More
                                </a>
                            </p>

                            {{--                            <p class="sp-layer sp-link-1" data-horizontal="23%" data-vertical="73.23%"--}}
                            {{--                               data-show-transition="left" data-hide-transition="fade" data-show-delay="2400"--}}
                            {{--                               data-hide-delay="200" data-show-duration="1000" data-hide-duration="1000">--}}
                            {{--                                <a href="tel:772-087-3330" class="">Request Callback</a>--}}
                            {{--                            </p>--}}
                        </div>

                    </div>
                    <!-- sp-slides -->
                </div>
                <!-- slider-pro -->
            </div>
        </div>
        <!--<a href="#kopa-area-1" class="kopa-scroll style-01 ">-->
        <!--    <span class="ti-arrow-down"></span>-->
        <!--</a>-->
    </section>
    <!-- slide-area -->

    <!--kopa-area-1-->
    <section class="kopa-area" id="kopa-area-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget millside-module-article-list-9">
                        <!--<h3 class="widget-title style-14">Browse by category</h3>-->
                        <div class="widget-content" style="padding : 20px;">
                            <h4 class="entry-title style-02 text-center mt20 mb50 pb20">
                                Enhancing Human Effectiveness through Outbound Training and Experiential Learning
                            </h4>
                            <ul class="row ul-mh ct-list-4 ct-row-8">
                                <li class="col-md-4 col-sm-6 col-xs-12">
                                    <article class="entry-item ct-item-8 thumbnail">
                                        <div class="entry-thumb">
                                            <a target="_blank" href="https://empowercamp.com/corporate-outbound-training">
                                                <img class="img-rounded"
                                                     src="https://empowercamp.com/img/trainings/corporate-outbound-training.JPG"
                                                     width="271px" height="180px" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-content caption">
                                            <h4 class="entry-title kopa-pull-left">
                                                <a target="_blank" href="https://empowercamp.com/corporate-outbound-training">
                                                    Corporate Outbound <br> Trainings</a>
                                            </h4>

                                            <a target="_blank" href="https://empowercamp.com/corporate-outbound-training"
                                               class="more-link style-10 kopa-pull-right"><span
                                                        class="ti-arrow-right"></span></a>
                                            <div class="clear"></div>
                                        </div>
                                    </article>
                                </li>
                                <!-- col-md-3 -->

                                <li class="col-md-4 col-sm-6 col-xs-12">
                                    <article class="entry-item ct-item-8 thumbnail">
                                        <div class="entry-thumb">
                                            <a target="_blank" href="https://empowercamp.com/team-building-outbound-training">
                                                <img class="img-rounded"
                                                     src="https://empowercamp.com/img/trainings/corporate-team-building-programme.jpg"
                                                     width="271px" height="180px" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-content">
                                            <h4 class="entry-title kopa-pull-left">
                                                <a target="_blank" href="https://empowercamp.com/team-building-outbound-training">Corporate
                                                    Team Building <br> Programmes</a>
                                            </h4>

                                            <a target="_blank" href="https://empowercamp.com/team-building-outbound-training"
                                               class="more-link style-10 kopa-pull-right"><span
                                                        class="ti-arrow-right"></span></a>
                                            <div class="clear"></div>
                                        </div>
                                    </article>
                                </li>
                                <!-- col-md-3 -->
                                <li class="col-md-4 col-sm-6 col-xs-12">
                                    <article class="entry-item ct-item-8 thumbnail">
                                        <div class="entry-thumb">
                                            <a target="_blank" href="https://empowercamp.com/empowering-students#corporate-camps">
                                                <img class="img-rounded"
                                                     src="https://empowercamp.com/img/trainings/college-to-corporate.JPG"
                                                     width="271px" height="180px" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-content">
                                            <h4 class="entry-title kopa-pull-left">
                                                <a target="_blank" href="https://empowercamp.com/empowering-students#corporate-camps">College to
                                                    Corporate <br> Programmes</a>
                                            </h4>

                                            <a target="_blank" href="https://empowercamp.com/empowering-students#corporate-camps"
                                               class="more-link style-10 kopa-pull-right"><span
                                                        class="ti-arrow-right"></span></a>
                                            <div class="clear"></div>
                                        </div>
                                    </article>
                                </li>
                                <!-- col-md-3 -->
                                <li class="col-md-4 col-sm-6 col-xs-12">
                                    <article class="entry-item ct-item-8 thumbnail">
                                        <div class="entry-thumb">
                                            <a target="_blank" href="https://empowercamp.com/white-water-rafting-kolad">
                                                <img class="img-rounded"
                                                     src="https://empowercamp.com/img/trainings/white-water-rafting.jpg"
                                                     width="271px" height="180px" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-content">
                                            <h4 class="entry-title kopa-pull-left">
                                                <a target="_blank" href="https://empowercamp.com/white-water-rafting-kolad">
                                                    White Water Rafting</a>
                                            </h4>

                                            <a target="_blank" href="https://empowercamp.com/white-water-rafting-kolad"
                                               class="more-link style-10 kopa-pull-right"><span
                                                        class="ti-arrow-right"></span></a>
                                            <div class="clear"></div>
                                        </div>
                                    </article>
                                </li>

                                <li class="col-md-4 col-sm-6 col-xs-12">
                                    <article class="entry-item ct-item-8 thumbnail">
                                        <div class="entry-thumb">
                                            <a target="_blank" href="https://empowercamp.com/summer-camp">
                                                <img class="img-rounded"
                                                     src="https://empowercamp.com/img/trainings/Kids-Summer-Camps.jpg"
                                                     width="271px" height="180px" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-content">
                                            <h4 class="entry-title kopa-pull-left">
                                                <a target="_blank" href="https://empowercamp.com/summer-camp">Children
                                                    Camps</a>
                                            </h4>

                                            <a target="_blank" href="https://empowercamp.com/summer-camp"
                                               class="more-link style-10 kopa-pull-right"><span
                                                        class="ti-arrow-right"></span></a>
                                            <div class="clear"></div>
                                        </div>
                                    </article>
                                </li>

                                <li class="col-md-4 col-sm-6 col-xs-12">
                                    <article class="entry-item ct-item-8 thumbnail">
                                        <div class="entry-thumb">
                                            <a target="_blank" href="https://empowercamp.com/adventure-and-leisure-camps#camping">
                                                <img class="img-rounded"
                                                     src="https://empowercamp.com/img/trainings/leisure-getaway.jpg"
                                                     width="271px" height="180px" alt="">
                                            </a>
                                        </div>
                                        <div class="entry-content">
                                            <h4 class="entry-title kopa-pull-left">
                                                <a target="_blank" href="https://empowercamp.com/adventure-and-leisure-camps#camping">Leisure
                                                    Getaway</a>
                                            </h4>

                                            <a target="_blank" href="https://empowercamp.com/adventure-and-leisure-camps#camping"
                                               class="more-link style-10 kopa-pull-right"><span
                                                        class="ti-arrow-right"></span></a>
                                            <div class="clear"></div>
                                        </div>
                                    </article>
                                </li>
                                <!-- col-md-3 -->

                            </ul>
                            <!-- row -->
                        </div>
                        <!-- widget-content -->
                    </div>
                    <!-- widget -->
                </div>
                <!-- col-md-12 -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-1 -->

    <section class="kopa-area kopa-area-2">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="widget millside-module-tab">
                        <div class="widget-content">                                    <!--  -->
                            <ul class="row nav nav-tabs">
                                <li class="col-md-4 col-sm-4 col-xs-12 text-center active">
                                    <a data-toggle="tab" href="#tab1">Corporate Training</a>
                                </li>
                                <li class="col-md-4 col-sm-4 col-xs-12 text-center ">
                                    <a data-toggle="tab" href="#tab2">Adventure</a>
                                </li>
                                <li class="col-md-4 col-sm-4 col-xs-12 text-center ">
                                    <a data-toggle="tab" href="#tab3">Summer Camps</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div id="tab1" class="tab-pane fade in active">
                                    <h3 class="tab-title">Excellent outbound Trainings</h3>
                                    <div class="tab-thumb">
                                        <img src="{{ asset('img/trainings/DCC-Dynamic-Challenge-Course-for-corporates.jpg') }}"
                                             alt="">
                                    </div>
                                    <div class="tab-detail">
                                        <p>
                                            It is often said that employees are the most valuable resource of a company.
                                            Are you getting the best out of your employees ?
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            Training Partners with 600+ Organisations
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            Collective Training Experience of 60+ years
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            Diverse range of Activities
                                        </p>
                                    </div>
                                </div>
                                <!-- tab1 -->
                                <div id="tab2" class="tab-pane fade">
                                    <h3 class="tab-title">Advenure, Treks &amp; Camps</h3>
                                    <div class="tab-thumb">
                                        <img src="{{ asset('img/trainings/white-water-rafting.jpg') }}" alt="">
                                    </div>
                                    <div class="tab-detail">
                                        <p>
                                            For some, it is very obvious. For others, challenges bring out their true
                                            courage. Empower’s Adventure Camp offers a range of adventure activities for
                                            all sorts of adventurers- trekking, river rafting, obstacle courses, nature
                                            walks, rock climbing and para-sailing.
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            Raft Building &amp; White Water Rafting
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            Valley Crossing &amp; Trekking
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            Rock Climbing and much more...
                                        </p>
                                    </div>
                                </div>
                                <!-- tab2 -->
                                <div id="tab3" class="tab-pane fade">
                                    <h3 class="tab-title">Kids Summer Camps</h3>
                                    <div class="tab-thumb">
                                        <img src="{{ asset('img/trainings/Kids-Summer-Camps.jpg') }}" alt="">
                                    </div>
                                    <div class="tab-detail">
                                        <p>To enrich the overall personality of participants by giving them exposure to
                                            Natural Environment, Experiential Learning, ‘Processed Adventure’,
                                            Creativity, Practical education in an outdoor environment, and some cultural
                                            activities</p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            50+ Activities
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            All Tested Safety gears for Adventure
                                        </p>
                                        <p class="pcheck">
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            Proper stay options with Healthy Food
                                        </p>
                                    </div>
                                </div>
                                <!-- tab3 -->

                            </div>
                            <!--  -->
                        </div>
                    </div>
                    <!-- widget -->
                </div>
                <!-- col-md-8 -->

            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-2 -->

    <section class="kopa-area kopa-area-2 hidden-xs hidden-sm">
        <div class="container">
            <div class="widget millside-module-action-2">
                <div class="widget-content" style="padding : 20px;">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h4 class="entry-title style-02 text-center mt20 mb50">What <b>our customers</b> are saying
                            </h4>
                            <?php
                            $testimonials = \App\Models\Testimonial::where("status", "Approved")->get();
                            $chunks = $testimonials->chunk(2);
                            ?>
                            <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000">
                                <!-- Carousel indicators -->
                                <ol class="carousel-indicators">
                                    <?php $chunks->each(function ($chunk, $index) { ?>
                                    <li data-target="#myCarousel" data-slide-to="<?= $index ?>"
                                        class="<?php if ($index == 0) {
                                            echo "active";
                                        } ?>"></li>
                                    <?php }); ?>
                                </ol>
                                <!-- Wrapper for carousel items -->
                                <div class="carousel-inner">
                                    <?php $chunks->each(function ($chunk, $index1) { ?>
                                    <div class="item carousel-item <?php if ($index1 == 0) {
                                        echo "active";
                                    } ?>">
                                        <div class="row">
                                            <?php
                                            $class = "col-sm-6";
                                            if ($chunk->count() == 1) {
                                                $class = "col-sm-12";
                                            }
                                            ?>
                                            <?php $chunk->each(function ($testimonial, $index2) use ($class) { ?>
                                            <div class="<?= $class ?>">
                                                <div class="media">
                                                    <div class="media-body">
                                                        <div class="testimonial">
                                                            <p><?= $testimonial->content ?></p>
                                                            <p class="overview">
                                                                <b><?= $testimonial->author ?></b>, <?= $testimonial->designation ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php }, $class); ?>
                                        </div>
                                    </div>
                                    <?php }); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="kopa-area kopa-area-2 mt20 mb40">
        <div class="container">
            <div class="widget millside-module-action-2">
                <div class="widget-content">
                    <div class="row">
                        <h4 class="entry-title style-02 text-center mt20 mb50">Why Us</h4>
                        <div class="col-md-8 col-sm-12 col-xs-8">
                            <article class="entry-item">
                                <div class="entry-content">
                                    <p>
                                        Veteran ARMY Officer's venture Empower Activity Camps is a passion driven
                                        company dedicated to enhancing human effectiveness through ‘experiential
                                        learning’ in outdoor environment. Our motto is “Leisure and Learn” and we give
                                        our heart and soul to make you experience nature to the fullest, within a safe
                                        and peaceful environment. Our programme modules are customized considering
                                        participant requirements - be it corporates, students or leisure and adventure
                                        seekers.</p>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-4">
                            <ul class="">
                                <li class=""><b>650+ Corporate Training Partners</b></li>
                                <li style="margin-top: 0;"><b>60+ Years of Training Experience</b></li>
                                <li style="margin-top: 0;"><b>Above 100 Outdoor Activities</b></li>
                                <li style="margin-top: 0;"><b>2 Lacs + Satisfied Customers</b></li>
                            </ul>
                        </div>
                    </div>
                    <!-- row -->
                </div>
            </div>
            <!-- widget -->
        </div>
        <!-- container -->
    </section>
    <!-- review section -->
    <section class="kopa-area kopa-area-2">
        <div class="container">
            <div class="widget millside-module-action-2">
                <div class="widget-content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <h4 class="entry-title style-02 text-center mt20 mb50">
                                Our Training Partners
                            </h4>
                            <div class="widget client-boxes mb30">
                                <div class="widget-content" style="padding : 20px;">
                                    <div class="row brands">
                                        <div class="col-md-12">
                                            <ul class="row brands-box">
                                                <li class="item col-md-2 col-sm-3 col-xs-6 ">
                                                    <img src="{{ asset('img/companies/abn-anro-bank.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/airtel.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/bp-client.gif') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/CITI Bank.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/convergys-client.gif') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/deutsche-Bank.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/dhl-express.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/godrej-empower-client.gif') }}"
                                                         alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/hdfc-bank.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/indigo-consulting.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/indra-networks.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/infosys.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/kotak-empower-client.gif') }}"
                                                         alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/sbi-life-insurance.jpg') }}"
                                                         alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/SHOPPERS-STOP.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/stanchart-empower-client.gif') }}"
                                                         alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/tata-docomo.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/tata-chemicals.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/uti-mutual-fund.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/ZAPAK.jpg') }}" alt=""
                                                         class="full-width">
                                                </li>

                                                <li class="item col-md-2 col-sm-3 col-xs-6">
                                                    <img src="{{ asset('img/companies/zensar-technologies.jpg') }}"
                                                         alt=""
                                                         class="full-width">
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- widget-content -->
                            </div>
                            <!-- widget -->
                        </div>
                        <!-- col-md-12 -->
                    </div>
                    <!-- row -->
                </div>
            </div>
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-7 -->

    <section class="kopa-area kopa-area-3 white-text-style">
        <div class="kopa-map-bg">
            <div class="widget millside-module-map">
                <div class="widget-content">
                    <div id="kopa-map" class="kopa-map"
                         data-place="Empower Activity Camps, 263, Roha - Kolad Rd, Sutarwadi, Taluka Kolad, Maharashtra, India"
                         data-latitude="18.403900" data-longitude="73.320315">
                    </div>
                </div>
            </div>
            <!-- widget -->
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
                    <div class="widget millside-module-contact">
                        <div class="widget-content">

                            <article class="entry-item">
                                <div class="entry-icon">
                                    <span class="ti-arrow-right arr-1"></span>
                                </div>
                                <div class="entry-content">
                                    <h4 class="entry-title style-02">
                                        Get in touch
                                    </h4>
                                    <p class="yellow-text">
                                        <a href="tel:+919422691325" style="color: #d2ad5e;">9422 691 325</a><br>
                                        <a href="tel:+917720873330" style="color: #d2ad5e;">7720 87 3330</a>
                                    </p>
                                    <p>
                                        Empower Activity Camps,<br>
                                        263, Roha - Kolad Rd,<br>
                                        Sutarwadi, Taluka Kolad, Maharashtra,<br>
                                        India - 402304
                                    </p>
                                </div>
                            </article>

                            <article class="entry-item">
                                <div class="entry-icon">
                                    <span class="ti-email"></span>
                                </div>
                                <div class="entry-content">
                                <span>
                                    YOUR E-MAIL:
                                </span>
                                    <p>
                                        <a href="mailto:info@empowercamp.com">info@empowercamp.com</a>
                                    </p>
                                </div>
                            </article>

                        </div>
                    </div>
                    <!-- widget -->
                </div>
                <!-- col-md-5 -->
            </div>
            <!-- row -->
        </div>
    </section>
    <!-- kopa-area-3 -->

@endsection

@push('styles')
    <style>

        .brands {
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        .brands-box {

            display: flex;
            -webkit-box-align: center;
            /* -ms-flex-align: center; */
            align-items: center;
            /* -ms-flex-wrap: wrap; */
            flex-wrap: wrap;
        }

        .brands-box .item:nth-child(1n+1) {
            border-left: none;
            border-top: none;
        }

        .brands-box .item:nth-child(4n+4) {
            border-right: none;
        }

        .brands-box .item:nth-child(17n+1) {
            border-bottom: none;
        }


        .brands-box .item {
            padding: 20px;
            text-align: center;
            position: relative;
            background: #FFF;
            /* cursor: pointer; */
            -moz-transition: all 0.3s;
            -webkit-transition: all 0.3s;
            transition: all 0.3s;
            /* border: 1.5px solid #EEE; */
        }

        .brands-box li {
            display: block;
        }

        /* .brands-box .item ::after, ::before {
            box-sizing: border-box;
        } */


        /* .widget.client-boxes ul li {
            height: 200px;
        }
        .widget.client-boxes ul li img {
            display: inline-block;
            vertical-align: middle;
        } */

        .testimonial-carousel-1 .owl-nav > div {
            font-size: 30px;
            font-weight: normal;
            position: absolute;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -moz-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            -o-transform: translateY(-50%);
            transform: translateY(-50%);
            width: 30px;
            height: 50px;
            line-height: 46px;
            text-align: center;
            background: #383938;
        }

        .testimonial-carousel-1 .owl-prev {
            left: -10px;
        }

        .testimonial-carousel-1 .owl-next {
            right: 10px;
        }

        .carousel {
            margin: 50px auto;
            padding: 0 70px;
        }

        .carousel .item {
            color: #999;
            overflow: hidden;
            min-height: 120px;
            font-size: 13px;
        }

        .carousel .media img {
            width: 80px;
            height: 80px;
            display: block;
            border-radius: 50%;
        }

        .carousel .testimonial {
            padding: 0 15px 0 60px;
            position: relative;
        }

        .carousel .testimonial::before {
            content: open-quote;
            color: #849b0d;
            font-weight: bold;
            font-size: 68px;
            line-height: 54px;
            position: absolute;
            left: 15px;
            top: 0;
        }

        .carousel .overview b {
            text-transform: uppercase;
            color: #1c47e3;
        }

        .carousel .carousel-indicators {
            bottom: -40px;
        }

        .carousel-indicators li, .carousel-indicators li.active {
            width: 18px;
            height: 18px;
            border-radius: 50%;
            margin: 1px 3px;
        }

        .carousel-indicators li {
            background: #e2e2e2;
            border: 4px solid #fff;
        }

        .carousel-indicators li.active {
            color: #fff;
            background: #849b0d;
            border: 5px double;
        }
    </style>
@endpush


@push('scripts')
    <script>
        (function ($) {
            var tc_1 = jQuery('.testimonial-carousel-1');
            if (tc_1.length) {
                tc_1.owlCarousel({
                    items: 1,
                    loop: true,
                    autoHeight: true,
                    nav: false,
                    navText: ["<span class='ti-angle-left'></span>", "<span class='ti-angle-right'></span>"],
                    dots: false,
                    autoplay: true,
                    autoplayTimeout: 3000,
                    autoplayHoverPause: true,
                    slideSpeed: 3000,
                });
            }
        })(window.jQuery);
    </script>
@endpush

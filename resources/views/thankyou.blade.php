<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MLP2BN6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
	<div class="main-container">
        <header class="kopa-page-header-1">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-0 text-left header-top-left hidden-xs hidden-sm">
                            <ul>
                                <li>
                                    <a href="{{ url('/about-us#media-coverage') }}">Media Coverage</a>
                                </li>
                                <li>
                                    <a href="{{ url('/contact-us') }}">Contact us</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12 text-right header-top-right">
                            <ul>
                                <li>
                                    <a href="tel:+919422691325">+91 9422691325</a>
                                    <a href="tel:+917720873330">+91 7720873330</a>
                                </li>
                                <li class="hidden-xs hidden-sm hidden-md">
                                    <a target="_blank" href="https://www.facebook.com/EmpowerActivityCamps/"><i class="fa fa-facebook"></i></a>
                                    <a target="_blank" href="https://twitter.com/EmpowerCamps"><i class="fa fa-twitter"></i></a>
                                    <a target="_blank" href="https://www.linkedin.com/company/empower-activity-camps/"><i class="fa fa-linkedin"></i></a>
                                    <a target="_blank" href="https://www.youtube.com/channel/UCtIG_K9h9yegTxONtP_XUTA"><i class="fa fa-youtube"></i></a>
                                    <a target="_blank" href="https://www.instagram.com/empowercamp/"><i class="fa fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @include('layouts.partials.menu')
            <hr>
        </header>

        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 mt30"></div>
                <div class="main-col col-md-6 col-sm-6 col-xs-12">
                    <ul class="ul-checked1">
                        <li class="mt90"><i class="fa fa-check-circle"></i></li>
                        <li class="mt30 mb60">Thank You!!!</li>
                    </ul>  
                    <p class="lead"><strong>Please Visit Again</strong> for more Adventures.</p>
                <hr>
                <p class="lead mb30">
                    Having trouble? <a href="https://empowercamp.com/contact-us"><strong>Contact us</strong></a>
                </p>
                <p class="lead mb60">
                    <a class="btn btn-primary btn-md" href="https://empowercamp.com/facilities" role="button"><b>Know More!!</b></a>
                </p>
                </div>
            <div class="col-md-3 col-sm-3 col-xs-12"></div>
        </div>
        
        <!-- main content -->

		@hasSection("no_footer")
		@else
			@include('layouts.partials.footer')
		@endif
    </div>

    <script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/superfish.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.sliderPro.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.matchHeight.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDvaUg89uMNUQ3CSkUpio6dD0IudZ2ZWmQ"></script>
    <script src="{{ asset('js/gmaps.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-timepicker.js') }}"></script>	
    <script type="text/javascript" src="{{ asset('js/masonry.pkgd.min.js') }}"></script>	
    <script type="text/javascript" src="{{ asset('js/imagesloaded.pkgd.min.js') }}"></script>	
    <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>	
    <script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.navgoco.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('la-assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/custom.js?') . File::lastModified(base_path('js/custom.js')) }}"></script>

    @stack('scripts')

</body>
</html>




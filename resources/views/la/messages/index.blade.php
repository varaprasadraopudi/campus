@extends('la.layouts.app')

@section('htmlheader_title')
	Messages
@endsection


@section('main-content')
<div id="page-content" class="messages">
	<div class="row">
        <div id="msg-userlist" class="col-md-3 col-sm-4 col-xs-5">
            <form id="user-search-form" action="" method="post">
                <input class="user-input" type="text" placeholder="Search Users">
            </form>
            <chat-users :user_me="user_me" :chat_users="chat_users" >

            </chat-users>
        </div>
        <chatbox :chat_users="chat_users" :user_me="user_me" :user_chat="user_chat"></chatbox>
        
        <div id="msg-userinfo" class="col-md-3 hidden-xs hidden-sm">
            <div style="height:100%;">
                <chat-user-employee :user_chat="user_chat"></chat-user-employee>
                <chat-user-customer :user_chat="user_chat"></chat-user-customer>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
Vue.config.devtools = true;

var user_id = "{{ Auth()->user()->id }}";
var laplus_id = "{{ env('LAPLUS_ID') }}";
var socket_port = "{{ env('SOCKET_PORT') }}";
var socket_protocol = "{{ env('SOCKET_PROTOCOL') }}";
var socket_host = "{{ env('SOCKET_SERVER') }}";
var user_socket_channel = "private-laraadmin-" + laplus_id + "-user-" + user_id + "-channel";
var private_laraadmin_app_channel = "private-laraadmin-channel-" + laplus_id;
var private_laraadmin_channel = "private-laraadmin-channel";

Pace.options.ajax.trackWebSockets = false;

var socket = io(socket_protocol+":\/\/"+socket_host+":"+socket_port, {
    query: "user_id=" + user_id + "&laplus_id=" + laplus_id + "&_token={{ csrf_token() }}"
});

var cmp_chat_user_employee = Vue.component('chat-user-employee', {
    props: [
        'user_chat'
    ],
    template: `
        <div v-if="isActive">
            <div class="box box-widget widget-user">
                <div class="widget-user-header">
                    <h3 class="widget-user-username" v-text="user_chat.context.name"></h3>
                    <h5 class="widget-user-desc" v-text="user_chat.context.designation"></h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" :src="user_chat.profile_img">
                </div>
                <div class="box-footer info-highlight-1">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><i class='fa fa-file-text'></i></h5>
                                <span class="description-text">27</span>
                            </div>
                        </div>
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><i class='fa fa-university'></i></h5>
                                <span class="description-text">13,000</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header"><i class='fa fa-star'></i></h5>
                                <span class="description-text">8.6</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer no-padding info-highlight-2">
                    <ul class="nav nav-stacked">
                        <li><a href="#">Email <span class="pull-right" v-text="user_chat.context.email_primary"></span></a></li>
                        <li><a href="#">Phone <span class="pull-right" v-text="user_chat.context.phone_primary"></span></a></li>
                        <li><a href="#">Phone 2 <span class="pull-right" v-text="user_chat.context.phone_secondary"></span></a></li>
                        <li><a href="#">Gender <span class="pull-right" v-text="user_chat.context.gender"></span></a></li>
                        <li><a href="#">Date of Birth <span class="pull-right" v-text="user_chat.context.date_birth"></span></a></li>
                        
                        <li><a href="#">City <span class="pull-right badge bg-blue" v-text="user_chat.context.city"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="box box-widget widget-user" v-if="!isActive && isLoading" style="padding-top:100px;">
                <div class="loader center-block"></div>
            </div>
        </div>
    `,
    computed: {
        isActive() {
            return (this.user_chat != null && this.user_chat.context != null && this.user_chat.context.context_type == 'Employee');
        },
        isLoading() {
            return (this.user_chat && this.user_chat.context_loading);
        }
    }
});

var cmp_chat_user_customer = Vue.component('chat-user-customer', {
    props: [
        'user_chat'
    ],
    template: `
        <div v-if="isActive">
            <div class="box box-widget widget-user">
                <div class="widget-user-header" style="background-color:#40a544">
                    <h3 class="widget-user-username" v-text="user_chat.context.name"></h3>
                    <h5 class="widget-user-desc" v-text="user_chat.context.designation"></h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" :src="user_chat.profile_img">
                </div>
                <div class="box-footer info-highlight-1">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><i class='fa fa-file-text'></i></h5>
                                <span class="description-text">27</span>
                            </div>
                        </div>
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><i class='fa fa-university'></i></h5>
                                <span class="description-text">13,000</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header"><i class='fa fa-star'></i></h5>
                                <span class="description-text">8.6</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer no-padding info-highlight-2">
                    <ul class="nav nav-stacked">
                        <li><a href="#">Email <span class="pull-right" v-text="user_chat.context.email_primary"></span></a></li>
                        <li><a href="#">Phone <span class="pull-right" v-text="user_chat.context.phone_primary"></span></a></li>
                        <li><a href="#">Phone 2 <span class="pull-right" v-text="user_chat.context.phone_secondary"></span></a></li>
                        <li><a href="#">Gender <span class="pull-right" v-text="user_chat.context.gender"></span></a></li>
                        <li><a href="#">Date of Birth <span class="pull-right" v-text="user_chat.context.date_birth"></span></a></li>
                        
                        <li><a href="#">City <span class="pull-right badge bg-blue" v-text="user_chat.context.city"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="box box-widget widget-user" v-if="!isActive && isLoading" style="padding-top:100px;">
                <div class="loader center-block"></div>
            </div>
        </div>
    `,
    computed: {
        isActive() {
            console.log(this.user_chat);
            return (this.user_chat != null && this.user_chat.context != null && this.user_chat.context.context_type == 'Customer');
        },
        isLoading() {
            return (this.user_chat && this.user_chat.context_loading);
        }
    }
});

var cmp_chat_users = Vue.component('chat-users', {
    props: [
        'chat_users',
        'user_me'
    ],
    template: `
        <ul class='userlist'>
            <chat-user v-if="chat_user.name" v-for="(chat_user, index) in onlineUsers" :chat_user='chat_user'></chat-user>
            <chat-user v-if="chat_user.name" v-for="(chat_user, index) in offlineUsers" :chat_user='chat_user'></chat-user>
        </ul>
    `,
    computed: {
        onlineUsers() {
            return this.chat_users.filter(chat_user => chat_user.is_online && chat_user.id != this.user_me.id);
        },
        offlineUsers() {
            return this.chat_users.filter(chat_user => !chat_user.is_online && chat_user.id != this.user_me.id);
        }
    }
});

var cmp_chat_user = Vue.component('chat-user', {
    props: [
        'chat_user'
    ],
    template: `
        <li :class="{'active' : isActive}">
            <a href='#' :title="chat_user.name" @click="initUserChat(chat_user.id)">
                <img :src="chat_user.profile_img" class="user-image" v-if="isProfileImage">
                <i class="user-icon fa fa-user bg-red" v-if="!isProfileImage"></i>
                <i class="user-live-icon fa fa-circle text-green" v-if="chat_user.is_online"></i>
                <div class="user-info">
                    <span class="user-subheading" v-text="chat_user.name"></span>
					<span class="label label-danger pull-right" v-if="chat_user.unread_msg_count > 0" v-text="chat_user.unread_msg_count"></span>
                    <p v-text="chat_user.type"></p>
                </div>
            </a>
        </li>
    `,
    computed: {
        isProfileImage() {
            return this.chat_user.profile_img != "";
        },
        isActive() {
            return (this.chat_user.id == this.$parent.$parent.user_chat_id);
        }
    },
    methods: {
        initUserChat(chat_user_id) {
            // console.log("initUserChat", chat_user_id);
            this.$parent.$parent.startChatSession(chat_user_id);
        }
    },
});

var cmp_chatbox = Vue.component('chatbox', {
    props: [
        'chat_users',
        'user_me',
        'user_chat'
    ],
    template: `
        <div id="msg-messages" class="col-md-6 col-sm-8 col-xs-7">
            <div class="chat_user" v-if="is_active">
                <h3 v-text="user_chat.name"></h3>
                <div v-if="isContext" class="inline-block pull-right">
                    <a class="btn btn-msg" v-if="user_chat.context.email_primary != ''" :href="'mailto:' + user_chat.context.email_primary"><i class="fa fa-envelope-o"></i></a>
                    <a class="btn btn-msg" v-if="user_chat.context.phone_primary != ''" :href="'tel:' + user_chat.context.phone_primary"><i class="fa fa-phone"></i></a>
                </div>
            </div>
            <div class="chat_user" v-if="!is_active">
                <h3>Please select user to chat</h3>
            </div>
            <div class="chat_area" @click.self="$refs.chat_message_ta.focus()" v-if="is_active">
                <chat_message v-for="message in user_chat.messages" :chat_users="chat_users" :user_chat="user_chat" :user_id="user_chat.id" :message="message"></chat_message>
            </div>
            <div title="Type your message here" class="chat_message" v-if="is_active">
                <textarea class="chat_message_ta" placeholder="Type here..." @keyup.enter="submitChatMessage('keyup.enter')" v-model="input_chat_message" ref="chat_message_ta"></textarea>
            </div>
            <div class="chat_info" v-if="is_active">
                <div class="info"></div>
                <div class="options">
                    <a class="btn btn-xs btn-primary pull-right" href="#" @click="submitChatMessage('button')">Send</a>
                    <div class="checkbox pull-right"><label><input type="checkbox" v-model="enterSend"> Press Enter to send</label></div>
                </div>
            </div>
        </div>
    `,
    data() {
        return {
            input_chat_message: "",
            enterSend : true
        }
    },
    computed: {
        is_active() {
            return this.user_chat != null;
        },
        isContext() {
            return (this.user_chat != null && this.user_chat.context != null && this.user_chat.context.context_type == 'Employee');
        },
        user_online() {
            if(this.user_chat.is_online) {
                return "text-green";
            } else {
                return "text-white";
            }
        }
    },
    methods: {
        submitChatMessage(event) {
            if(event == "keyup.enter" && !this.enterSend) {
                return;
            }
            var messageContent = this.input_chat_message.trim();
            if(messageContent != '') {
                var msgTime = new Date();
                var message = {
                    "id": 0,
                    "subject": '',
                    "content": messageContent,
                    "from": this.$parent.user_me.id,
                    "to": this.user_chat.id,
                    "is_received": false,
                    "created_at": msgTime.toTS(),
                    "_token": '{{ csrf_token() }}'
                };
                this.input_chat_message = "";
                this.$parent.scrollToBottom(true);
                this.$parent.sendChatMessage(message);
            }
        }
    }
});

var cmp_chat_message = Vue.component('chat_message', {
    props: [
        'chat_users',
        'user_chat',
        'user_id',
        'message'
    ],
    template: `
        <div :class="{ 'msg fromMe': isMsgfromMe, 'msg fromHim' : !isMsgfromMe }" :msgid="message.id" @click.self="$parent.$refs.chat_message_ta.focus()">
            <a :href="user_path" class="tn"><img :src="getUserProfileImg"></a>
            <div><span v-html="content"></span> 
                <span class="msg_sts">@{{ time }} <i v-if="isMsgfromMe" :class="[ 'fa', message.is_received ? 'fa-circle' : 'fa-circle-o' ]"></i></span>
            </div>
        </div>
    `,
    data() {
        return {
            user_path: ""
        }
    },
    created () {
        this.user_path = '{{ url(config('laraadmin.adminRoute').'/users') }}/' + this.message.from;
    },
    computed: {
        isMsgfromMe() {
            return this.user_id == this.message.to;
        },
        content() {
            return this.message.content.replace(/\n/g, "<br>");
        },
        time() {
            var msgTime = this.message.created_at.toDate();
            var hours = msgTime.getHours() % 12 || 12;
            var ampm = 'AM';
            if(hours != msgTime.getHours()) {
                ampm = 'PM';
            }
            return "" + hours + ":" + msgTime.getMinutes() + " " + ampm;
        },
        getUserProfileImg() {
            if(this.isMsgfromMe) {
                return this.$parent.$parent.user_me.profile_img;
            } else {
                return this.user_chat.profile_img;
            }
        }
    }
});

var msgApp = new Vue({
    el: '#page-content',
    data: {
        user_me_id: {{ Auth::user()->id }},
        user_chat_id: 0,
        chat_users: [
            @foreach(App\User::all() as $user)
            {
                id: {{ $user->id }},
                name: "{{ $user->name }}",
                profile_img: '{{ $user->context()->profileImageUrl() }}',
                type: '{{ $user->type }}',
                context: null,
                context_loading: false,
                is_online: false,
                messages: [],
                unread_msg_count: 0
            }@if(!$loop->last), @endif
            @endforeach
        ]
    },
    computed: {
        user_me() {
            $users = this.chat_users.filter(chat_user => chat_user.id == this.user_me_id);
            if($users.length == 0) {
                return null;
            } else {
                return $users[0];
            }
        },
        user_chat() {
            $users = this.chat_users.filter(chat_user => chat_user.id == this.user_chat_id);
            if($users.length == 0) {
                return null;
            } else {
                return $users[0];
            }
        }
    },
    mounted: function() {
        socket.on(user_socket_channel, function(message) {
            // console.log("socket.on", message);
            if(message.type == "UserMessage") {
                this.newChatMessage(message.message);
                this.sendMsgAck(message.message);
            } else if(message.type == "UserMessageAckDone") {
                this.doneMsgAck(message);
            }   
        }.bind(this));

        socket.on(private_laraadmin_app_channel, function(message) {
            console.log("socket.on." + private_laraadmin_app_channel, message);
            if(message.type == "UserJoined") {
                var chat_user = this.getUser(message.user_id);
                chat_user.is_online = true;
            }
        }.bind(this));
    },
    methods: {
        newChatMessage: function(message) {
            if(message.from == this.user_chat_id) {
                this.user_chat.messages.push(message);
                this.scrollToBottom(true);
            } else {
                var chat_user = this.getUser(message.from);
                chat_user.messages.push(message);
                chat_user.unread_msg_count = chat_user.unread_msg_count + 1;
            }
        },
        sendMsgAck: function(message) {
            // Send acknowledgement for received message
            var ack = {
                laplus_id: laplus_id,
                type: "UserMessageAck",
                message_id: message.id,
                user_ack_to: message.from,
                user_ack_from: message.to,
                app_url: '{{ url("") }}',
                _token: '{{ csrf_token() }}'
            };
            socket.emit(private_laraadmin_channel, ack);
        },
        doneMsgAck: function(message) {
            // Called when User get acknowledgement for Messages received.
            // Set Message Status as received
            var chat_user = this.getUser(message.user);
            if(chat_user != null) {
                var messages = chat_user.messages.filter(msg_obj => msg_obj.id == message.message_id);
                if(messages.length == 1) {
                    messages[0].is_received = true;
                } else {
                    console.log("Message not found", chat_user.messages);
                }
            } else {
                console.log("User not found");
            }
        },
        sendChatMessage: function(message) {
            // console.log("sendChatMessage", message);
            // socket.emit('chat.message', message);

            this.user_chat.messages.push(message);

            this.$http.post('{{ url(config("laraadmin.adminRoute") . "/messages") }}', message).then((response) => {
                // console.log("sendChatMessage", response);
                if(response.data.status == "error") {
                    $('body').pgNotification({
                        style: 'circle',
                        title: 'LaraAdmin Chat',
                        message: response.data.message,
                        position: "top-right",
                        timeout: 0,
                        type: "error",
                        thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Auth::user()->context()->profileImageUrl() }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
                    }).show();
                } else {
                    // Set DB Message ID to message object
                    message.id = response.data.message_id;
                }
            });
        },
        startChatSession: function(user_chat_id) {
            // console.log("startChatSession", user_chat_id);

            this.user_chat_id = user_chat_id;
            this.loadChatMessages();
            this.loadUserProfile();
        },
        loadChatMessages: function() {
            this.$nextTick(function () {
                request_data = {
                    "_token": '{{ csrf_token() }}'
                };
                this.$http.get('{{ url(config("laraadmin.adminRoute") . "/messages/user") }}/'+this.user_chat_id, request_data).then((response) => {
                    // console.log("loadChatMessages - messages.user", response);
                    if(response.data.status == "error") {
                        $('body').pgNotification({
                            style: 'circle',
                            title: 'LaraAdmin Chat',
                            message: response.data.message,
                            position: "top-right",
                            timeout: 0,
                            type: "error",
                            thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Auth::user()->context()->profileImageUrl() }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
                        }).show();
                    } else {
                        // console.log("this.user_chat", this.user_chat);
                        this.user_chat.messages.clear();
                        for (index = 0; index < response.data.messages.length; ++index) {
                            var msg = response.data.messages[index];
                            if(msg.to == this.user_me_id && !msg.is_received) {
                                msg.is_received = true;
                                this.sendMsgAck(msg);
                            }
                            this.user_chat.messages.push(msg);
                        }
                        this.scrollToBottom(false);

                        // Set unread count to 0
                        this.user_chat.unread_msg_count = 0;
                    }
                });
            });
        },
        scrollToBottom: function(animate) {
            this.$nextTick(function () {
                if(animate) {
                    // jQuery Way scroll
                    $chat_area = $("#msg-messages .chat_area");
                    $chat_area.animate({"scrollTop": $chat_area[0].scrollHeight}, "slow");
                } else {
                    // Vue.js Way scroll
                    var container = this.$el.querySelector("#msg-messages .chat_area");
                    container.scrollTop = container.scrollHeight;
                }
            });
        },
        loadUserProfile: function() {
            this.$nextTick(function () {
                request_data = {
                    "_token": '{{ csrf_token() }}'
                };
                this.user_chat.context_loading = true;
                this.$http.get('{{ url(config("laraadmin.adminRoute") . "/messages/user_profile") }}/'+this.user_chat_id, request_data).then((response) => {
                    // console.log("loadUserProfile - messages.user", response);
                    if(response.data.status == "error") {
                        $('body').pgNotification({
                            style: 'circle',
                            title: 'LaraAdmin Chat',
                            message: response.data.message,
                            position: "top-right",
                            timeout: 0,
                            type: "error",
                            thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Auth::user()->context()->profileImageUrl() }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
                        }).show();
                    } else {
                        // temp
                        this.user_chat.context_loading = false;
                        this.user_chat.context = response.data.user_context;
                    }
                });
            });
        },
        getUser: function(user_id) {
            $users = this.chat_users.filter(chat_user => chat_user.id == user_id);
            if($users.length == 0) {
                return null;
            } else {
                return $users[0];
            }
        }
    },
    components: {
        'chat-users': cmp_chat_users,
        'chat-user': cmp_chat_user,
        'chat-user-employee': cmp_chat_user_employee,
        'chat-user-customer': cmp_chat_user_customer,
        'chat-box': cmp_chatbox,
        'chat-message': cmp_chat_message
    }
});

</script>
@endpush
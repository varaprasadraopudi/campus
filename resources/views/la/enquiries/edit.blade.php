@extends("la.layouts.app")

@section("contentheader_title")
    <a @ajaxload href="{{ url(config('laraadmin.adminRoute') . '/enquiries') }}">@lang('la_enquiry.enquiry')</a> :
@endsection
@section("contentheader_description", $enquiry->$view_col)
@section("section", app('translator')->get('la_enquiry.enquiries'))
@section("section_url", url(config('laraadmin.adminRoute') . '/enquiries'))
@section("sub_section", app('translator')->get('common.edit'))

@section("htmlheader_title", app('translator')->get('la_enquiry.enquiry_edit')." : ".$enquiry->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Form::model($enquiry, ['route' => [config('laraadmin.adminRoute') . '.enquiries.update', $enquiry->id ], 'method'=>'PUT', 'id' => 'enquiry-edit-form']) !!}
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'name')
					@la_input($module, 'email')
					@la_input($module, 'phone_no')
					@la_input($module, 'organization')
					@la_input($module, 'training')
					@la_input($module, 'message')
                    --}}
                    <br>
                    <div class="form-group">
                        {!! Form::submit( app('translator')->get('common.update'), ['class'=>'btn btn-success']) !!} <a @ajaxload href="{{ url(config('laraadmin.adminRoute') . '/enquiries') }}" class="btn btn-default pull-right">@lang('common.cancel')</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
    $("#enquiry-edit-form").validate({
        
    });
});
</script>
@endpush

@extends("la.layouts.app")

@section("contentheader_title", app('translator')->get('la_booking.bookings'))
@section("contentheader_description", app('translator')->get('la_booking.booking_listing'))
@section("section", app('translator')->get('la_booking.bookings'))
@section("sub_section", app('translator')->get('common.listing'))
@section("htmlheader_title", app('translator')->get('la_booking.booking_listing'))

@section("headerElems")
    From <input type="date" name="date_from" value="" placeholder="Date From"> To <input type="date" name="date_to" value="" placeholder="Date To">
@la_access("Bookings", "create")
    <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">@lang('la_booking.booking_add')</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
    <!--<div class="box-header"></div>-->
    <div class="box-body">
        <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
            @foreach( $listing_cols as $col )
            @if($col == "status")
            <th style="width:120px;">{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
            @else
            <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
            @endif
            @endforeach
            @if($show_actions)
            <th>@lang('common.actions')</th>
            @endif
        </tr>
        </thead>
        <tbody>
            
        </tbody>
        </table>
    </div>
</div>

@la_access("Bookings", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@lang('la_booking.booking_add')</h4>
            </div>
            {!! Form::open(['action' => 'LA\BookingsController@store', 'id' => 'booking-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'name')
					@la_input($module, 'email')
					@la_input($module, 'phone_no')
					@la_input($module, 'organization')
					@la_input($module, 'training')
					@la_input($module, 'message')
                    --}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('common.close')</button>
                {!! Form::submit( app('translator')->get('common.save'), ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endla_access

@endsection

@push('styles')

@endpush

@push('scripts')
<script>
var example1 = null;
$(function () {
    example1 = $("#example1").DataTable({
        processing: true,
        serverSide: true,
        order: [[ 0, "desc" ]],
        ajax: {
            "url": "{{ url(config('laraadmin.adminRoute') . '/booking_dt_ajax') }}",
            "data": function ( data_custom ) {
                data_custom.date_from = $("input[name=date_from]").val();
                data_custom.date_to = $("input[name=date_to]").val();
            }
        },
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: '@lang("common.search")'
        },
        @if($show_actions)
        columnDefs: [ { orderable: false, targets: [-1] }],
        @endif
    });
    $('input[name=date_from], input[name=date_to]').on("change", function() {
        example1.draw();
    });
    @la_access("Bookings", "edit")
    example1.on('draw', function () {
        $('.update_field').editable({
            container: 'body',
            source: [
                { value: 'Not Contacted', text: 'Not Contacted' },
                { value: 'Attempted to Contact', text: 'Attempted to Contact' },
                { value: 'Cold', text: 'Cold' },
                { value: 'Hot', text: 'Hot' },
                { value: 'Contact in Future', text: 'Contact in Future' },
                { value: 'Lost', text: 'Lost' },
                { value: 'Won', text: 'Won' }
            ],
            validate: function(value) {
                var id = $(this).attr('id');
                var field_name = $(this).attr('field_name');
                console.log(id, field_name);
                // Make your validations here
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
                var formData = {};
                formData[field_name] = value;
                $.ajax({
                    url: "{{ url(config('laraadmin.adminRoute')) }}/booking_status_update/"+id+"/"+value,
                    method: 'GET',
                    // contentType: 'json',
                    // headers: { 'X-CSRF-Token': '{{ csrf_token() }}' },
                    // data: JSON.stringify(formData),
                    success: function( data ) {
                        if(data.status == "success") {
                            show_success("Booking Update", data);
                        } else {
                            show_failure("Booking Update", data);
                        }
                        if(isset(data.redirect)) {
                            window.location.href = data.redirect;
                        }
                    },
                    error: function( data ) {
                        show_failure("Booking Update", data);
                        if(isset(data.redirect)) {
                            window.location.href = data.redirect;
                        }
                    }
                });
            }
        });
    });
    @endla_access

    $("#booking-add-form").validate({
        
    });
});
</script>
@endpush

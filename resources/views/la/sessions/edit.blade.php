@extends("la.layouts.app")

@section("contentheader_title")
    <a @ajaxload href="{{ url(config('laraadmin.adminRoute') . '/sessions') }}">@lang('la_session.session')</a> :
@endsection
@section("contentheader_description", $session->$view_col)
@section("section", app('translator')->get('la_session.sessions'))
@section("section_url", url(config('laraadmin.adminRoute') . '/sessions'))
@section("sub_section", app('translator')->get('common.edit'))

@section("htmlheader_title", app('translator')->get('la_session.session_edit')." : ".$session->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Form::model($session, ['route' => [config('laraadmin.adminRoute') . '.sessions.update', $session->id ], 'method'=>'PUT', 'id' => 'session-edit-form']) !!}
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'title')
                    @la_input($module, 'pageurl')
					@la_input($module, 'training')
					@la_input($module, 'excerpt')
					@la_input($module, 'banner')
					@la_input($module, 'post_date')
					@la_input($module, 'content')
                    --}}
                    <br>
                    <div class="form-group">
                        {!! Form::submit( app('translator')->get('common.update'), ['class'=>'btn btn-success']) !!} <a @ajaxload href="{{ url(config('laraadmin.adminRoute') . '/sessions') }}" class="btn btn-default pull-right">@lang('common.cancel')</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
    $("#session-edit-form").validate({
        
    });
});
</script>
@endpush

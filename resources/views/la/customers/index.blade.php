@extends("la.layouts.app")

@section("contentheader_title", app('translator')->get('la_customer.customers'))
@section("contentheader_description", app('translator')->get('la_customer.customer_listing'))
@section("section", app('translator')->get('la_customer.customers'))
@section("sub_section", app('translator')->get('common.listing'))
@section("htmlheader_title", app('translator')->get('la_customer.customer_listing'))

@section("headerElems")
@la_access("Customers", "create")
    <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">@lang('la_customer.customer_add')</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
    <!--<div class="box-header"></div>-->
    <div class="box-body">
        <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
            @foreach( $listing_cols as $col )
            <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
            @endforeach
            @if($show_actions)
            <th>@lang('common.actions')</th>
            @endif
        </tr>
        </thead>
        <tbody>
            
        </tbody>
        </table>
    </div>
</div>

@la_access("Customers", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@lang('la_customer.customer_add')</h4>
            </div>
            {!! Form::open(['action' => 'LA\CustomersController@store', 'id' => 'customer-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'name')
					@la_input($module, 'designation')
					@la_input($module, 'organization')
					@la_input($module, 'gender')
					@la_input($module, 'phone_primary')
					@la_input($module, 'phone_secondary')
                    @la_input($module, 'email_primary')
					@la_input($module, 'email_secondary')
					@la_input($module, 'profile_img')
					@la_input($module, 'city')
					@la_input($module, 'address')
					@la_input($module, 'about')
					@la_input($module, 'date_birth')
                    --}}
                    <div class="form-group">
                        <label for="create_user">Create user :</label>
                        <input class="form-control" name="create_user" id="create_user" type="checkbox" style="display: none;">
                        <div class="Switch Round" id="Switch"  style="vertical-align:top;margin-left:10px;">
                            <div class="Toggle"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('common.close')</button>
                {!! Form::submit( app('translator')->get('common.save'), ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endla_access

@endsection

@push('styles')

@endpush

@push('scripts')
<script>
$(function () {
    $("#example1").DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/customer_dt_ajax') }}",
        pageLength: 100,
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: '@lang("common.search")'
        },
        @if($show_actions)
        columnDefs: [ { orderable: false, targets: [-1] }],
        @endif
    });
    $("#customer-add-form").validate({
        
    });
});
</script>
@endpush

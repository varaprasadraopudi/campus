@push('scripts')
<div id="chatApp">
    <chat-boxes :chat_users="chat_users" :chat_sessions="chat_sessions">
        
    </chat-boxes>

    <!-- Chat Sidebar -->
    <aside class="chat-sidebar">
        <chat-users :user_me="user_me" :chat_users="chat_users">
            
        </chat-users>
    </aside><!-- /.chat-sidebar -->
    <div class='chat-sidebar-bg'></div>
</div>

<script>
Vue.config.devtools = true;

var socket_port = "{{ env('SOCKET_PORT') }}";
var socket_protocol = "{{ env('SOCKET_PROTOCOL') }}";
var socket_host = "{{ env('SOCKET_SERVER') }}";
var socket_channel = "private-laraadmin-{{ env('LAPLUS_ID') }}-user-{{ Auth()->user()->id }}-channel";
var private_channel = "private-laraadmin-channel-{{ env('LAPLUS_ID') }}";

Pace.options.ajax.trackWebSockets = false;

var socket = io(socket_protocol+":\/\/"+socket_host+":"+socket_port, {
    query: "user_id={{ Auth()->user()->id }}&_token={{ csrf_token() }}"
});

var cmp_chat_users = Vue.component('chat-users', {
    props: [
        'chat_users',
        'user_me'
    ],
    template: `
        <ul class='chat-sidebar-users'>
            <chat-user v-if="chat_user.name" v-for="(chat_user, index) in onlineUsers" :chat_user='chat_user'></chat-user>
            <chat-user v-if="chat_user.name" v-for="(chat_user, index) in offlineUsers" :chat_user='chat_user'></chat-user>
        </ul>
    `,
    computed: {
        onlineUsers() {
            return this.chat_users.filter(chat_user => chat_user.is_online && chat_user.id != this.user_me.id);
        },
        offlineUsers() {
            return this.chat_users.filter(chat_user => !chat_user.is_online && chat_user.id != this.user_me.id);
        }
    }
});

var cmp_chat_user = Vue.component('chat-user', {
    props: [
        'chat_user'
    ],
    template: `
        <li>
            <a :title="chat_user.name" @click="initUserChat(chat_user)">
                <img :src="chat_user.profile_img" class="user-image" v-if="isProfileImage">
                <i class="user-icon fa fa-user bg-red" v-if="!isProfileImage"></i>
                <i class="user-live-icon fa fa-circle text-green" v-if="chat_user.is_online"></i>
            </a>
        </li>
    `,
    computed: {
        isProfileImage() {
            return this.chat_user.profile_img != "";
        }
    },
    methods: {
        initUserChat(chat_user) {
            console.log("initUserChat", chat_user.id, chat_user.name);
            this.$parent.$parent.startChatSession(chat_user);
        }
    },
});

var cmp_chat_boxes = Vue.component('chat-boxes', {
    props: [
        'chat_users',
        'chat_sessions'
    ],
    template: `
        <div class="chat-boxes">
            <chatbox v-for="(chat_session, index) in chat_sessions" :chat_users="chat_users" :chat_session='chat_session' v-bind:style="{right: '' + ( (index * 250) + 50 ) +'px'}" @closeChatbox="remove(chat_session, index)"></chatbox>
        </div>
    `,
    methods: {
        remove: function(chat_session, index) {
            console.log("Remove chat session", chat_session, index);
            this.chat_sessions.splice(index, 1);
        }
    }
});

var cmp_chatbox = Vue.component('chatbox', {
    props: [
        'chat_session',
        'chat_users'
    ],
    template: `
        <div :id="'chat_user_' + chat_session.user_id" title="" :class="['chatbox', 't1', ui_state]">
            <div title="" class="header" @click="toggleChatbox">
                <p><i :class="['fa', 'fa-circle', user_online]"></i>&nbsp; <span v-text="chat_session.user_obj.name"></span></p>
                <div title="Close chat window" class="close_chatbox" @click.stop="$emit('closeChatbox')"><i class="fa fa-times"></i></div>
            </div>
            <div class="chat_area" @click.self="$refs.chat_message_ta.focus()">
                <chat_message v-for="message in chat_session.messages" :chat_users="chat_users" :chat_session="chat_session" :user_id="chat_session.user_id" :message="message"></chat_message>
            </div>
            <div class="chat_info">
                <div class="info"></div>
            </div>
            <div title="Type your message here" class="chat_message">
                <textarea class="chat_message_ta" placeholder="Type here..." @keyup.enter="submitChatMessage" v-model="input_chat_message" ref="chat_message_ta"></textarea>
            </div>
        </div>
    `,
    data() {
        return {
            ui_state: "maximize",
            input_chat_message: ""
        }
    },
    computed: {
        user_online() {
            if(this.chat_session.user_obj.is_online) {
                return "text-green";
            } else {
                return "text-white";
            }
        }
    },
    methods: {
        toggleChatbox() {
            console.log("toggleMinimize", this.ui_state);
            if(this.ui_state == "maximize") {
                this.ui_state = "minimize";
            } else {
                this.ui_state = "maximize";
            }
        },
        submitChatMessage() {
            var message = {
                "type": "UserMessage",
                "id": 0,
                "subject": '',
                "content": this.input_chat_message.trim(),
                "from": this.$parent.$parent.user_me.id,
                "to": this.chat_session.user_id,
                "is_received": false,
                "created_at": '1:20 PM',
                "_token": '{{ csrf_token() }}'
            };
            this.chat_session.messages.push(message);
            this.input_chat_message = "";
            this.$nextTick(function () {
                this.scrollToBottom();
            });
            this.$parent.$parent.sendChatMessage(message);
        },
        scrollToBottom() {
            // Vue.js Way
            // var container = this.$el.querySelector("#chat_user_"+this.chat_session.user_id+" .chat_area");
            // console.log("scrollToBottom", container, container.scrollTop, container.scrollHeight);
            // container.scrollTop = container.scrollHeight;
            // jQuery Way
            $chat_area = $("#chat_user_"+this.chat_session.user_id+" .chat_area");
            $chat_area.animate({"scrollTop": $chat_area[0].scrollHeight}, "slow");
        }
    }
});

var cmp_chat_message = Vue.component('chat_message', {
    props: [
        'chat_session',
        'user_id',
        'message',
        'chat_users'
    ],
    template: `
        <div :class="{ 'msg fromMe': isMsgfromMe, 'msg fromHim' : !isMsgfromMe }" :msgid="message.id" @click.self="$parent.$refs.chat_message_ta.focus()">
            <a :href="user_path" class="tn"><img :src="getUserProfileImg"></a>
            <div>@{{ message.content }} 
                <span class="msg_sts msg_sts_1">@{{ message.created_at }} <i v-if="isMsgfromMe" class="fa fa-circle"></i></span>
            </div>
        </div>
    `, // :src="chat_users[message.from].profile_img"
    data() {
        return {
            user_path: ""
        }
    },
    created () {
        this.user_path = '{{ url(config('laraadmin.adminRoute').'/users') }}/' + this.message.from;
    },
    computed: {
        isMsgfromMe() {
            return this.user_id == this.message.to;
        },
        getUserProfileImg() {
            if(this.isMsgfromMe) {
                return this.$parent.$parent.$parent.user_me.profile_img;
            } else {
                return this.chat_session.user_obj.profile_img;
            }
        }
    }
});

var chatApp = new Vue({
    el: '#chatApp',
    data: {
        user_me: {
            id: {{ Auth::user()->id }},
            name: "{{ Auth::user()->name }}",
            profile_img: '{{ Auth::user()->context()->profileImageUrl() }}',
            is_online: true
        },
        chat_users: [
            @foreach(App\User::all() as $user)
            {
                id: {{ $user->id }},
                name: "{{ $user->name }}",
                profile_img: '{{ $user->context()->profileImageUrl() }}',
                is_online: true
            }@if(!$loop->last), @endif
            @endforeach
        ],
        chat_sessions: [
            
        ]
    },
    mounted: function() {
        socket.on(socket_channel, function(message) {
            console.log("socket.on", message);
            if(message.type == "UserMessage") {
                var matchingSessions = this.getChatSession(message.message.from);
                if(matchingSessions.length == 0) {
                    var chatUserNew = this.getUser(message.message.from);
                    var chatSessionNew = this.startChatSession(chatUserNew[0]);

                    chatSessionNew.messages.push(message.message);
                    this.$nextTick(function () {
                        // chatSessionNew.scrollToBottom();
                    });
                } else {
                    matchingSessions[0].messages.push(message.message);
                    this.$nextTick(function () {
                        // matchingSessions[0].scrollToBottom();
                    });
                }
            }
        }.bind(this));

        // start demo chat session
        /*
        this.chat_sessions.push({
            user_id: 2,
            user_obj: this.chat_users[1],
            messages: [
                {id: 1, subject: '', content: 'Hi...', from: 1, to: 2, created_at: '1:20 PM'},
                {id: 2, subject: '', content: 'Hello', from: 2, to: 1, created_at: '1:21 PM'},
                {id: 3, subject: '', content: 'This is Testing Chat Message', from: 1, to: 2, created_at: '1:22 PM'},
                {id: 4, subject: '', content: 'Yes Ok', from: 2, to: 1, created_at: '1:22 PM'},
                {id: 5, subject: '', content: 'Thanks you for reply', from: 1, to: 2, created_at: '1:23 PM'}
            ]
        });
        */
    },
    methods: {
        sendChatMessage: function(message){
            console.log("sendChatMessage", message);
            // Message should be sent through Laravel
            // socket.emit('chat.message', message);
            
            this.$http.post('{{ url(config("laraadmin.adminRoute") . "/messages") }}', message).then((response) => {
                console.log("sendChatMessage", response);
                if(response.data.status == "error") {
                    $('body').pgNotification({
                        style: 'circle',
                        title: 'LaraAdmin Chat',
                        message: response.data.message,
                        position: "top-right",
                        timeout: 0,
                        type: "error",
                        thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Auth::user()->context()->profileImageUrl() }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
                    }).show();
                }
            });

        },
        startChatSession: function(chat_user) {
            console.log("startChatSession", chat_user);

            // Check if session exists
            var matchingSessions = this.getChatSession(chat_user.id);
            if(matchingSessions.length == 0) {
                // Create Chat Session
                this.chat_sessions.push({
                    user_id: chat_user.id,
                    user_obj: chat_user,
                    messages: []
                });
                return this.chat_sessions[this.chat_sessions.length - 1];
            }
            return matchingSessions[0];
        },
        getChatSession: function(user_id) {
            return this.chat_sessions.filter(chat_session => chat_session.user_id == user_id);
        },
        getUser: function(user_id) {
            return this.chat_users.filter(chat_user => chat_user.id == user_id);
        }
    },
    components: {
        'chat-users': cmp_chat_users,
        'chat-user': cmp_chat_user,
        'chat-boxes': cmp_chat_boxes,
        'chat-box': cmp_chatbox,
        'chat-message': cmp_chat_message
    }
});

</script>
@endpush
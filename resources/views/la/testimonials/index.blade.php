@extends("la.layouts.app")

@section("contentheader_title", app('translator')->get('la_testimonial.testimonials'))
@section("contentheader_description", app('translator')->get('la_testimonial.testimonial_listing'))
@section("section", app('translator')->get('la_testimonial.testimonials'))
@section("sub_section", app('translator')->get('common.listing'))
@section("htmlheader_title", app('translator')->get('la_testimonial.testimonial_listing'))

@section("headerElems")
@la_access("Testimonials", "create")
    <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">@lang('la_testimonial.testimonial_add')</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
    <!--<div class="box-header"></div>-->
    <div class="box-body">
        <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
            @foreach( $listing_cols as $col )
            <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
            @endforeach
            @if($show_actions)
            <th>@lang('common.actions')</th>
            @endif
        </tr>
        </thead>
        <tbody>
            
        </tbody>
        </table>
    </div>
</div>

@la_access("Testimonials", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">@lang('la_testimonial.testimonial_add')</h4>
            </div>
            {!! Form::open(['action' => 'LA\TestimonialsController@store', 'id' => 'testimonial-add-form']) !!}
            <div class="modal-body">
                <div class="box-body">
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'title')
                    @la_input($module, 'category')
					@la_input($module, 'author')
					@la_input($module, 'designation')
					@la_input($module, 'content')
					@la_input($module, 'author_image')
                    @la_input($module, 'content_image')
                    @la_input($module, 'youtube')
					@la_input($module, 'post_date')
                    --}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">@lang('common.close')</button>
                {!! Form::submit( app('translator')->get('common.save'), ['class'=>'btn btn-success']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endla_access

@endsection

@push('styles')

@endpush

@push('scripts')
<script>
var example1 = null;
$(function () {
    example1 = $("#example1").DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/testimonial_dt_ajax') }}",
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: '@lang("common.search")'
        },
        @if($show_actions)
        columnDefs: [ { orderable: false, targets: [-1] }],
        @endif
    });

    @la_access("Testimonials", "edit")
    example1.on('draw', function () {
        $('.update_field').editable({
            container: 'body',
            source: [
                { value: 'Posted', text: 'Posted' },
                { value: 'Approved', text: 'Approved' }
            ],
            validate: function(value) {
                var id = $(this).attr('id');
                var field_name = $(this).attr('field_name');
                console.log(id, field_name);
                // Make your validations here
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
                var formData = {};
                formData[field_name] = value;
                $.ajax({
                    url: "{{ url(config('laraadmin.adminRoute')) }}/testimonial_status_update/"+id+"/"+value,
                    method: 'GET',
                    // contentType: 'json',
                    // headers: { 'X-CSRF-Token': '{{ csrf_token() }}' },
                    // data: JSON.stringify(formData),
                    success: function( data ) {
                        if(data.status == "success") {
                            show_success("Testimonial Update", data);
                        } else {
                            show_failure("Testimonial Update", data);
                        }
                        if(isset(data.redirect)) {
                            window.location.href = data.redirect;
                        }
                    },
                    error: function( data ) {
                        show_failure("Testimonial Update", data);
                        if(isset(data.redirect)) {
                            window.location.href = data.redirect;
                        }
                    }
                });
            }
        });
    });
    @endla_access

    $("#testimonial-add-form").validate({
        
    });
});
</script>
@endpush

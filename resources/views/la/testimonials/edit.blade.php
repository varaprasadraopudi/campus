@extends("la.layouts.app")

@section("contentheader_title")
    <a @ajaxload href="{{ url(config('laraadmin.adminRoute') . '/testimonials') }}">@lang('la_testimonial.testimonial')</a> :
@endsection
@section("contentheader_description", $testimonial->$view_col)
@section("section", app('translator')->get('la_testimonial.testimonials'))
@section("section_url", url(config('laraadmin.adminRoute') . '/testimonials'))
@section("sub_section", app('translator')->get('common.edit'))

@section("htmlheader_title", app('translator')->get('la_testimonial.testimonial_edit')." : ".$testimonial->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                {!! Form::model($testimonial, ['route' => [config('laraadmin.adminRoute') . '.testimonials.update', $testimonial->id ], 'method'=>'PUT', 'id' => 'testimonial-edit-form']) !!}
                    @la_form($module)
                    
                    {{--
                    @la_input($module, 'title')
                    @la_input($module, 'category')
					@la_input($module, 'author')
					@la_input($module, 'designation')
					@la_input($module, 'content')
					@la_input($module, 'author_image')
                    @la_input($module, 'content_image')
                    @la_input($module, 'youtube')
					@la_input($module, 'post_date')
                    --}}
                    <br>
                    <div class="form-group">
                        {!! Form::submit( app('translator')->get('common.update'), ['class'=>'btn btn-success']) !!} <a @ajaxload href="{{ url(config('laraadmin.adminRoute') . '/testimonials') }}" class="btn btn-default pull-right">@lang('common.cancel')</a>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
    $("#testimonial-edit-form").validate({
        
    });
});
</script>
@endpush

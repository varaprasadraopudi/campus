@extends('la.layouts.app')

@section('htmlheader_title') @lang('dashboard.dashboard') @endsection
@section('contentheader_title') @lang('dashboard.dashboard') @endsection
@section('contentheader_description') @lang('dashboard.organization_overview') @endsection

@section('main-content')
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box small-box-bg blog_post bg-aqua">
        <div class="inner">
            <h3>{{ $blog_post_count }}</h3>
            <p>Posts</p>
        </div>
        <div class="icon">
            <i class="fa fa-file-text-o"></i>
        </div>
        <a href="{{ url(config('laraadmin.adminRoute') . '/blog_posts') }}" class="small-box-footer">Check All Posts <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box small-box-bg customers bg-green">
        <div class="inner">
            <h3>{{ $customers_count }}</h3>
            <p>Customers</p>
        </div>
        <div class="icon">
            <i class="fa fa-group"></i>
        </div>
        <a href="{{ url(config('laraadmin.adminRoute') . '/customers') }}" class="small-box-footer">Check All Customers <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box small-box-bg employees bg-yellow">
        <div class="inner">
            <h3>{{ $employee_count }}</h3>
            <p>Employees</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a href="{{ url(config('laraadmin.adminRoute') . '/employees') }}" class="small-box-footer">Check All Employees <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box small-box-bg uploads bg-red">
        <div class="inner">
            <h3>{{ $upload_count }}</h3>
            <p>Uploads</p>
        </div>
        <div class="icon">
            <i class="fa fa-files-o"></i>
        </div>
        <a href="{{ url(config('laraadmin.adminRoute') . '/uploads') }}" class="small-box-footer">Check All Uploads <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div><!-- ./col -->
    </div><!-- /.row -->
    <!-- Main row -->
    <div class="row">
    <!-- Left col -->
    <section class="col-lg-7 connectedSortable">
        

    </section><!-- /.Left col -->
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <section class="col-lg-5 connectedSortable">


    </section><!-- right col -->
    </div><!-- /.row (main row) -->

</section><!-- /.content -->
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

<style>
.small-box.small-box-bg {
    background-size: cover;
}
.small-box.small-box-bg > .inner {
    background: rgba(0, 0, 0, 0.3);
}
.small-box.small-box-bg > .small-box-footer {
    transition: all 0.2s linear;
    background: rgba(0, 0, 0, 0.5);
    font-size: 14px;
    color: #DDD;
}
.small-box.small-box-bg > .small-box-footer:hover {
    font-size: 14.5px;
    color: #FFF;
}
.small-box.small-box-bg .icon {
    opacity: 0;
    font-size: 80px;
    margin-top: 6px;
    margin-right: 6px;
    color: rgba(255, 255, 255, 0.25);
}
.small-box.small-box-bg:hover .icon {
    opacity: 1;
    font-size: 85px;
    margin-top: 4px;
}
.small-box.blog_post {
    background-image: url('{{ asset('la-assets/img/bg_blog.jpg') }}')
}
.small-box.customers {
    background-image: url('{{ asset('la-assets/img/bg_customers.jpg') }}')
}
.small-box.employees {
    background-image: url('{{ asset('la-assets/img/bg_employees.jpg') }}')
}
.small-box.uploads {
    background-image: url('{{ asset('la-assets/img/bg_uploads.jpg') }}')
}
</style>
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')
<script>
(function($) {
    if(document.URL.indexOf("#") == -1)
    {
        // Set the URL to whatever it was plus "#".
        url = document.URL+"#";
        location = "#";
        //Reload the page
        location.reload(true);
    }

	$('body').pgNotification({
		style: 'circle',
		title: 'LaraAdmin',
		message: "Welcome to LaraAdmin...",
		position: "top-right",
		timeout: 0,
		type: "success",
		thumbnail: '<img width="40" height="40" style="display: inline-block;" src="{{ Auth::user()->context()->profileImageUrl() }}" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar2x.jpg" alt="">'
	}).show();
})(window.jQuery);
</script>
@endpush
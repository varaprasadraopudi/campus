@extends('layouts.app')

@section('meta_title') CONTACT &#124; empower @endsection
@section('meta_description') Please fill in this form to get in touch with us: Please provide the following details * indicates required field For more information, please contact Empower Camp at the following: Email us: info@empowercamp.com Empower Marketing Team Ms Bhavna Marketing Manager +91 96713116736 @endsection
@section('meta_keywords') @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 >Contact Us</h3>
                        {{-- <h4 class="subtitle mt30">Team building is awesome</h4> --}}
                    </div>
                    <!-- kopa-breadcrumb -->
                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area kopa-area-0">
    <div class="container">	
        <div class="widget millside-module-contact-20">
            <header class="widget-header">
                <h3 class="h3 widget-title style-after10">Get in touch</h3>
                <p class="entry-sub-title text-center">If you have any questions or comments, please feel free to contact us, we will be happy to help you. </p>
            </header>
            <div class="widget-content">						
                <div class="row ct-list-02">
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <ul class="ct-list-01">
                            <li>
                                <article class="entry-item ct-item-01">
                                    <div class="entry-content">
                                        <h4 class="entry-title style-23">
                                            Address
                                        </h4>
                                        <p><u>Camp Office</u>: 263, Sutarwadi,<br>
                                            Taluka Roha, Distt Raigad,<br>
                                            Maharashtra – 402 304.
                                        </p>
                                        <p><u>Camp Site</u>: Village Kudli,<br>
                                            14 Km Off Kolad,<br>
                                            Taluka Roha, Distt Raigad,<br>
                                            Maharashtra – 402 304.
                                        </p>
                                        <h4 class="entry-title style-23">Phone</h4> +91-02194-255105<br>
                                        9226605055
                                        <h4 class="entry-title style-23">Email</h4>
                                        <p>adm@empowercamp.com</p>
                                        
                                    </div>
                                </article>
                            </li>
                            <li>
                                <article class="entry-item ct-item-01">
                                    <div class="entry-content">
                                        <h4 class="entry-title style-23">
                                            Marketing Team
                                        </h4>
                                        <p><b>Ms Bhavna</b><br>
                                            Marketing Manager<br>
                                            <i class="fa fa-phone" style="font-size:18px;"></i> 9422691325<br>
                                            <i class="fa fa-phone" style="font-size:18px;"></i> 7720873330<br>
                                            <i class="fa fa-envelope"></i> bhavna@empowercamp.com<br>
                                            <i class="fa fa-envelope"></i> info@empowercamp.com
                                        </p> 											
                                    </div>
                                </article>
                            </li>
                            
                        </ul>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <ul class="ct-list-01">
                            <li>
                                <article class="entry-item ct-item-01">
                                    <div class="entry-content">
                                        <h4 class="entry-title style-23">
                                            Management
                                        </h4>
                                        <p><b>Mr Anil Bhasin</b><br>
                                            Managing Director<br>
                                            <i class="fa fa-phone" style="font-size:18px;"></i> 9422909688<br>
                                            <i class="fa fa-phone" style="font-size:18px;"></i> 8108110855<br>
                                            <i class="fa fa-envelope"></i> anil@empowercamp.com
                                        </p>
                                        <p><b>Col Naval Kohli</b><br>
                                            Managing Director<br>
                                            <i class="fa fa-phone" style="font-size:18px;"></i> 9423091928<br>
                                            <i class="fa fa-phone" style="font-size:18px;"></i> 9312071129<br>
                                            <i class="fa fa-envelope"></i> naval@empowercamp.com
                                        </p> 											
                                    </div>
                                </article>
                            </li>
                            <li>
                                <article class="entry-item ct-item-01">
                                    <div class="entry-content">
                                        <h4 class="entry-title style-23">
                                            Head Office
                                        </h4>
                                        <p>303,New Blue Heaven,<br>
                                            Juhu Versova Link Road,<br>
                                            Andheri (W), Mumbai 400053, INDIA.
                                        </p> 											
                                    </div>
                                </article>
                            </li>
                        </ul>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div id="widget-booking" >
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="enquiryform" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn ct-btn-7">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-6 -->
                    
                </div>
                <!-- row --> 
            </div>
        </div>
        <!-- widget --> 		
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->
<section class="kopa-area kopa-area-3 white-text-style">
    <div class="kopa-map-bg">
        <div class="widget millside-module-map">
            <div class="widget-content">
                <div id="kopa-map" class="kopa-map" data-place="Empower Activity Camps, 263, Roha - Kolad Rd, Sutarwadi, Taluka Kolad, Maharashtra, India" data-latitude="18.403900" data-longitude="73.320315">				  	
                </div>    
            </div>
        </div>
        <!-- widget -->
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
                <div class="widget millside-module-contact">
                    <div class="widget-content">

                        <article class="entry-item">
                            <div class="entry-icon">
                                <span class="ti-arrow-right arr-1"></span>
                            </div>
                            <div class="entry-content">
                                <h4 class="entry-title style-02">
                                    Get in touch
                                </h4>
                                <p class="yellow-text">
                                    <a href="tel:+919422691325" style="color: #d2ad5e;">9422 691 325</a><br>
                                    <a href="tel:+917720873330" style="color: #d2ad5e;">7720 87 3330</a>
                                </p>
                                <p>
                                    Empower Activity Camps,<br>
                                    263, Roha - Kolad Rd,<br>
                                    Sutarwadi, Taluka Kolad, Maharashtra,<br>
                                    India - 402304
                                </p>
                            </div>
                        </article>

                        <article class="entry-item">
                            <div class="entry-icon">
                                <span class="ti-email"></span>
                            </div>
                            <div class="entry-content">
                                <span>
                                    YOUR E-MAIL:
                                </span>
                                <p>										
                                    <a href="mailto:info@empowercamp.com">info@empowercamp.com</a>
                                </p>
                            </div>
                        </article>

                    </div>
                </div>
                <!-- widget --> 
            </div>
            <!-- col-md-5 -->
        </div>
        <!-- row --> 
    </div>
</section>

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#enquiryform").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_enquiry') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#enquiryform")[0].reset();
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();
})(window.jQuery);
</script>
@endpush
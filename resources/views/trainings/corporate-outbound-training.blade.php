@extends('layouts.page')

@section('meta_title') Corporate Outbound Team building activities & employee development programs @endsection
@section('meta_description') Empower Camp’s core competencies in training corporate executives: Leadership Skills Team Building through experiential learning in an outbound environment.@endsection
@section('meta_keywords') outbound team building activities,corporate team building activities,team building activities for employees,employee development programs @endsection
@section('no_footer', false)

@section('breadcrumb-image')
    <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}"
         alt="Corporate Outbound Trainings near Pune">
@endsection

@section('breadcrumb-content')
<h3 class="with-subtitle">Corporate Training</h3>
<h4 class="subtitle">Team building is awesome</h4>
{{-- <button id="bookButton2" class="btn btn-default">Book Now</button> --}}
<div class="breadcrumb-content">

                                <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                    <a itemprop="url" href="{{ url('/') }}">
                                        <span itemprop="title">Trainings</span>
                                    </a>
                                </span>
    <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                    <a itemprop="url" href="{{ url('/corporate-outbound-training') }}"
                                       class="current-page">
                                        <span itemprop="title">Corporate Training</span>
                                    </a>
                                </span>
</div>
@endsection

@section('page-content')
    <section class="kopa-area sub-page kopa-area-25">
        <div class="container">
            <div class="row">
                <div class="main-col col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-intro-9">
                                <h3 class="widget-title style-11 ">About Training</h3>
                                <div class="widget-content">
                                    <p>
                                        It is often said that employees are the most valuable resource of a company.Are
                                        you getting the best out of your employees?
                                    </p>
                                    <p>
                                        The performance of your employees is highly dependent on teamwork, trust and
                                        effective communication skills. To perform at their optimum levels and achieve
                                        organizational objectives, the team needs time out and trust building exercises.
                                    </p>
                                </div>
                                <h3 class="widget-title style-11 mt20 mb20">Brush up your Skills</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul class="ul-checked">
                                            <li><i class="fa fa-check-circle"></i>Leadership Skills</li>
                                            <li><i class="fa fa-check-circle"></i>Team Building</li>
                                            <li><i class="fa fa-check-circle"></i>Self-Empowerment</li>
                                            <li><i class="fa fa-check-circle"></i>Risk Management</li>
                                            <li><i class="fa fa-check-circle"></i>Handling Failure</li>
                                            <li><i class="fa fa-check-circle"></i>Motivation</li>
                                            <li><i class="fa fa-check-circle"></i>Handling Diverse Teams</li>
                                            <li><i class="fa fa-check-circle"></i>Creativity</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="ul-checked">
                                            <li><i class="fa fa-check-circle"></i>Communication Skills</li>
                                            <li><i class="fa fa-check-circle"></i>Business Etiquette</li>
                                            <li><i class="fa fa-check-circle"></i>Social Graces</li>
                                            <li><i class="fa fa-check-circle"></i>Grooming</li>
                                            <li><i class="fa fa-check-circle"></i>Body Language</li>
                                            <li><i class="fa fa-check-circle"></i>Working with Teams</li>
                                            <li><i class="fa fa-check-circle"></i>Meeting Timelines</li>
                                        </ul>
                                    </div>
                                </div>
                                <h3 class="widget-title style-11 mt20 mb20">Why Empower Camp’s training programmes
                                    ?</h3>
                                <ul class="ul-checked">
                                    <li><i class="fa fa-check-circle"></i>Empower Camp is a custom built training and
                                        adventure resort.
                                    </li>
                                    <li><i class="fa fa-check-circle"></i>Proven expertise, having conducted over 400
                                        training programmes.
                                    </li>
                                    <li><i class="fa fa-check-circle"></i>Team brings a collective training experience
                                        of more than 60 years.
                                    </li>
                                    <li><i class="fa fa-check-circle"></i>Risk Management</li>
                                    <li><i class="fa fa-check-circle"></i>Handling Failure</li>
                                    <li><i class="fa fa-check-circle"></i>Motivation</li>
                                    <li><i class="fa fa-check-circle"></i>Handling Diverse Teams</li>
                                    <li><i class="fa fa-check-circle"></i>Creativity</li>
                                </ul>
                            </div>
                            <!-- widget -->
                        </div>
                        <!-- col-md-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-slider-pro-trainings">
                                {{-- <h3 class="widget-title text-center style-11">Course Photo Gallery</h3> --}}
                                <div class="widget-content">
                                    <div class="slider-pro slider-pro-trainings">
                                        <div class="sp-slides">
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">
                                                {{--                                                <img class="sp-thumbnail" alt=""--}}
                                                {{--                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">--}}
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}">
                                                {{--                                                <img class="sp-thumbnail" alt=""--}}
                                                {{--                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}">--}}
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">
                                                {{--                                                <img class="sp-thumbnail" alt=""--}}
                                                {{--                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">--}}
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-4.jpg') }}">
                                                {{--                                                <img class="sp-thumbnail" alt=""--}}
                                                {{--                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-4.jpg') }}">--}}
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-5.jpg') }}">
                                                {{--                                                <img class="sp-thumbnail" alt=""--}}
                                                {{--                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-5.jpg') }}">--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- widget-content -->
                            </div>
                            <!-- widget -->
                        </div>
                    </div>
                    <div class="widget millside-module-intro-9">
                        <h3 class="widget-title style-11 ">How do you do it?</h3>
                        <div class="widget-content">
                            <p>
                                Seating them in an A/C room and lecturing while pointing towards a power-point
                                presentation is going to achieve nothing, apart from boring them so much that they
                                either go to sleep or start day-dreaming in their cushy chairs.
                            </p>
                            <p><b>What you really need is to take the employees out of their comfort zone, make the
                                    ambience drastically different from their office setup, and set them to tasks where
                                    interdependence is inevitable. Stated simply, get them to an outdoor camp offering
                                    an expansive range of customizable activities that look like fun, but are actually
                                    intended to teach them something through experience, leading to self realization</b>
                            </p>
                            <p>
                                A team with better life skills and working together in a close-knit manner can do
                                wonders for your company.In fact, each of your employees is perceived as a
                                representative of your company whenever they interact with the outside world, whether it
                                is a client or a shareholder. An employee with better management skills and personality
                                will automatically bring better ROI from each of their interactions.
                            </p>
                            <p>
                                If you find training expensive, please try ignorance!
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Training Options</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img class="img-rounded"
                                 src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}"></img>
                            <h6>Residential Leadership and Team Building Camps at our camp site at Kolad.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <img
                                    class="img-rounded"
                                    src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}"></img>
                            <h6>Residential Leadership and Team Building Camps at a venue of your choice.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <img class="img-rounded"
                                 src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}"></img>
                            <h6>Corporate Sales Meets and Conferences with a value adding element of Team Building.</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img class="img-rounded"
                                 src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">
                            </img>
                            <h6>Indoor workshops/seminars/motivational lectures/psychometric assessments and analysis at
                                your premises.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <img class="img-rounded"
                                 src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}"></img>
                            <h6>Off Sites.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <img class="img-rounded"
                                 src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">
                            </img>
                            <h6>Day Packages.</h6>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Latest Events</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget millside-module-event-11 mb0">
                        <div class="mb30">
                            <p>
                                Book a Corporate Training Session with Empower, and trust us, your employees will thank
                                you for it. Not only will they remember the learnings, this camp will form a part of
                                their dinner conversations for years to come.
                            </p>
                        </div>
                        <div class="widget-content">
                            <ul class="row ul-mh">
                                @php
                                    $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                                @endphp
                                @foreach($sessions as $session)
                                    <li class="col-md-6 col-sm-12 col-xs-12">
                                        <article class="entry-item">
                                            <div class="entry-thumb">
                                                <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                            </div>
                                            <div class="entry-content">
                                                <header class="entry-header style-01 ">
                                                    <div class="entry-date-2">
                                                        @php
                                                            $day = date("d", strtotime($session->post_date));
                                                            $month = date("M", strtotime($session->post_date));
                                                        @endphp
                                                        <p>{{ $day }}</p>
                                                        <p>{{ $month }}</p>
                                                    </div>
                                                    <div>
                                                        <h4 class="entry-title style-05 ">
                                                            <a>{{ $session->title }}</a>
                                                        </h4>
                                                        <p>
                                                            <a href="#">Posted in Corporate Training</a>
                                                        </p>
                                                    </div>
                                                </header>
                                                <div class="clearfix">
                                                    <p>
                                                        {{ $session->excerpt }}
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Testimonials</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial-container">
                        <div class="dk-container">
                            <div class="cd-testimonials-wrapper cd-container">
                                <ul class="cd-testimonials">
                                    @php
                                        $testimonials = App\Models\Testimonial::where("category", "Corporate")->where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                    @endphp
                                    @foreach($testimonials as $testimonial)
                                        <li>
                                            <div class="testimonial-content">
                                                <p>
                                                    @php
                                                        echo $testimonial->content;
                                                        if($testimonial->content_image) {
                                                            echo $testimonial->contentImage();
                                                        }
                                                    @endphp
                                                </p>
                                                {{-- <p>
                                                    @php
                                                    if($testimonial->youtube != "") {
                                                        $link = $testimonial->youtube;
                                                        $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
                                                        if (empty($video_id[1]))
                                                            $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

                                                        $video_id = explode("&", $video_id[1]); // Deleting any other params
                                                        $video_id = $video_id[0];
                                                        echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$video_id.'"></iframe>';
                                                    }
                                                    @endphp
                                                </p> --}}
                                                <div class="cd-author">
                                                    @php
                                                        echo $testimonial->authorImage();
                                                    @endphp

                                                    {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                                    <ul class="cd-author-info">
                                                        <li>{{ $testimonial->author }},<br>
                                                            <span>@php echo $testimonial->designation @endphp</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- cd-testimonials -->
                        </div>
                    </div>
                </div>
                <!-- col-md-8 -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
@endsection

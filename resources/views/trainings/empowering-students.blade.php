
@extends('layouts.app')

@section('meta_title') Empowering Students &#124; Empower @endsection
@section('meta_description') Empower the Next Generation with Outbound, Experiential Learning! Kids Camp at Empower The child is the father of man, and the students are the future of our society. A student may or may not remember what is taught in the classroom, but practical learning is something that is never forgotten. Kids Camps 
@endsection
@section('meta_keywords') Kids Camp @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Empowering Students</h3>
                        <h4 class="subtitle">Empower the Next Generation with Outbound, Experiential Learning!</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Home</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Empowering Students</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Empowering Students</h3>
                            <div class="widget-content">
                                <p>
                                    The child is the father of man, and the students are the future of our society. A student may or may not remember what is taught in the classroom, but practical learning is something that is never forgotten. Kids Camps are an ideal way to teach kids and youngsters crucial life skills that can only be learnt in a practical environment.
                                </p>
                                <p>
                                    However, more often than not, kids camps are perceived as a picnic or excursion. Empower is here to change all that. Our team has rich experience in training students and their teachers, and is passionate about empowering them with life skills and a better personality. We have a range of programs on offer which cater to every possible combination of students, teachers and parents.
                                </p>
                                <p>
                                    Our aim is to make the entire learning experience fun for everyone involved in education– students, parents, teachers and principals. All our programmes focus on outbound, experiential learning. We see ourselves as an extension of the vision of a school, college or institute and customize our training modules to be in sync with the objectives of the academic institution that we are working with.
                                </p>
                            </div>
                        </div>
                        <!-- New sections -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="widget millside-module-ads-5">
                                    <div class="widget-content">
                                        <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                        <div class="bg-green-2">
                                            <div class="row mtm30">
                                                <div class="col-md-12">
                                                    <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Empowering Students</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt30">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="widget millside-module-article-list-5">
                                    <div class="widget-content">
                                        <ul>
                                            <li id="induction-camps">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="College Induction Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">College Induction Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample Content
                                                            </p>
                                                            <p><b>Sample Content</b></p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="corporate-camps">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="College to Corporate Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">College to Corporate Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample Content 
                                                            </p>
                                                            <p><b>Sample Content</b></p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="school-camps">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="School Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">School Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                            <p><b>Sample content</b></p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            
                                            <li id="summer-camps">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Summer Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Summer Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                            <p><b>Sample content</b></p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="winter-camps">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Diwali & Winter Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Diwali & Winter Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                            <p><b>Sample content</b></p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="miscellaneous">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Miscellaneous">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Miscellaneous</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="adventure-camps">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Adventure Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Adventure Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="leadership-camps">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Leadership & Self Esteem Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Leadership & Self Esteem Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="educational-trips">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Educational Trips">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Educational Trips</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="environmental-camps">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Environmental Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Environmental Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="excursions-in-india-and-abroad">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Excursions in India & Abroad">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Excursions in India & Abroad</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="projects">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="CAS Curriculum / Projects">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">CAS Curriculum / Projects</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="community-services-empowering-students">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Community Services Workshops in rural areas">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Community Services Workshops in rural areas</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="seminars-in-school">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Camps/ Workshops/Seminars in School/ College Campuses">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Camps/ Workshops/Seminars in School/ College Campuses</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="team-building">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Team Building for Sports Teams">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Team Building for Sports Teams</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="social-get-togethers">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Open Residential - Summer & Diwali Camps">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Open Residential - Summer & Diwali Camps</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="industrial-visits">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Industrial Visits">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Industrial Visits</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="leadership-programs">
                                                <article class="entry-item ct-item-3">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Educational Leadership Programs for Principals & Teachers">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Educational Leadership Programs for Principals & Teachers</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                            <li id="offsites-for-staff">
                                                <article class="entry-item ct-item-2">
                                                    <div class="entry-thumb">
                                                        <img src="{{ asset('img/300x250.png') }}" alt="Offsites for Staff">													
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4><a href="#">Offsites for Staff</a></h4>
                                                        <div class="clearfix">
                                                            <p>
                                                                Sample content
                                                            </p>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- widget --> 
                            </div>
                            <!-- col-md-12 -->
                        </div>
                        <!-- Teacher's Training -->
                        {{-- <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="widget millside-module-ads-5">
                                            <div class="widget-content">
                                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                                <div class="bg-green-2">
                                                    <div class="row mtm30">
                                                        <div class="col-md-12">
                                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Leadership Programs for Principals & Teachers</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content mt30 mb30">
                                    <p>
                                        GururDevoBhava, says our culture, which means that the teacher is like a God. There is a reason behind this saying. Teachers are in a unique position to mould and influence the young minds, apart from teaching them subjects from the syllabus. The team at Empower understands this crucial contribution of the teaching community and has designed special programmes to improve the way they carry out their noble mission.
                                    </p>
                                    <p>
                                        Our teacher’s training programmes have been developed with an objective of creating collaboration between teachers, where they can share their learnings, learn about the latest methodologies and innovations in teaching and so on, thereby making teaching more effective and interesting.
                                    </p>
                                </div>
                                <ul class="ul-checked">
                                    <li><i class="fa fa-check-circle"></i>Includes interaction with teachers of different schools.</li>
                                    <li><i class="fa fa-check-circle"></i>Leads to exchange of teaching innovations and solutions.</li>
                                    <li><i class="fa fa-check-circle"></i>Conducted for better, effective and efficient methods of teaching.</li>
                                    <li><i class="fa fa-check-circle"></i>Making teaching interesting.</li>
                                    <li><i class="fa fa-check-circle"></i>Motivational programmes for teachers.</li>
                                    <li><i class="fa fa-check-circle"></i>Self-esteem workshops.</li>
                                    <li><i class="fa fa-check-circle"></i>Experiential Learning Workshop.</li>
                                </ul>
                                <div class="widget-content mt30 mb30">
                                    <ul class="row">
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/Teachers2-300x19711.png') }}" alt="Teacher's training">
                                                </div>
                                            </article>
                                        </li>
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/principals-300x2131.jpg') }}" alt="">
                                                </div>
                                            </article>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Principal's Seminars -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="widget millside-module-ads-5">
                                            <div class="widget-content">
                                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                                <div class="bg-green-2">
                                                    <div class="row mtm30">
                                                        <div class="col-md-12">
                                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Principal's Seminars</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content mt30 mb30">
                                    <p>
                                        Principals have a special role to play in the overall success of a school and its students. A principal is more than a senior teacher or as called in the old times, head-master. The knowledge and thinking of a principal not only affects the policies and priorities of a school, it also impacts the overall morale of the teachers and students.
                                    </p>
                                    <p>
                                        Our teacher’s training programmes have been developed with an objective of creating collaboration between teachers, where they can share their learnings, learn about the latest methodologies and innovations in teaching and so on, thereby making teaching more effective and interesting.
                                    </p>
                                    <p>
                                        The transformation from a teacher to a Principal needs some training. We have ventured to create a 3 days residential ‘Education Leadership Camp.’Empower’s leadership training programs are aimed at making Principals rise up to their true potential, to be more successful and effective.
                                    </p>
                                    <p>
                                        <b>These seminars are customized to suit the group, based on their interests and any particular areas that they would like to focus on.</b>
                                    </p>
                                    <ul class="ul-checked">
                                        <li><i class="fa fa-check-circle"></i>Conducted with an aim to groom principals and prospective principals as educational leaders.</li>
                                        <li><i class="fa fa-check-circle"></i>Change their mindsets from senior teacher to the role of a CEO in a company.</li>
                                        <li><i class="fa fa-check-circle"></i>Provides assistance to principals in terms of innovation, networking, training and support.</li>
                                    </ul>
                                </div>
                                <div class="widget-content mt30 mb30">
                                    <ul class="row">
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/Soft-Skills-n21.jpg') }}" alt="Teacher's training">
                                                </div>
                                            </article>
                                        </li>
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/Teachers-Training-1-300x2251.jpg') }}" alt="Teacher's training">
                                                </div>
                                            </article>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- MOM-KID CAMPS -->
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="widget millside-module-ads-5">
                                            <div class="widget-content">
                                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                                <div class="bg-green-2">
                                                    <div class="row mtm30">
                                                        <div class="col-md-12">
                                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Mom-Kid Camps</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content mt30 mb30">
                                    <p>
                                        These camps are for children from age group of 6-12 yrs. While the children are busy in their activities, we conduct activities for mothers on ‘Effective Parenting’.
                                    </p>
                                    <p>
                                        The hectic lifestyles of today’s parents and the busy schedules of their kids, with school, tuitions and extracurricular activities, reduces meaningful, quality time that parents spend with their kids. Our programmes for parents have been developed after thorough research and are aimed to create more meaningful interactions between parents and children.
                                    </p>
                                    <p>
                                        <b>Conducted during vacations:</b>
                                    </p>
                                    <ul class="ul-checked">
                                        <li><i class="fa fa-check-circle"></i>Encourages healthy interaction between parents and children.</li>
                                        <li><i class="fa fa-check-circle"></i>Covers subjects and facts related to the Child’s Personality Development.</li>
                                    </ul>
                                </div>
                                <div class="widget-content mt30 mb30">
                                    <ul class="row">
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/mom-kid-camp-empower.png') }}" alt="Teacher's training">
                                                </div>
                                            </article>
                                        </li>
                                        <li class="col-md-6 col-sm-12 col-xs-12">
                                            <article class="entry-item">
                                                <div class="entry-thumb">
                                                    <img src="{{ asset('img/education/m17-300x2251.png') }}" alt="Teacher's training">
                                                </div>
                                            </article>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="widget millside-module-ads-5">
                                            <div class="widget-content">
                                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                                <div class="bg-green-2">
                                                    <div class="row mtm30">
                                                        <div class="col-md-12">
                                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Family Groups</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content mt30 mb30">
                                    <p>
                                        This programme is designed for close bonding and building a better understanding between all the members of a family, and is aimed at fostering positive attitudes that help create a supportive environment within the family, that benefits the children as well as the rest of the family members.
                                    </p>
                                    <p>
                                        <b>Conducted over weekends:</b>
                                    </p>
                                    <ul class="ul-checked">
                                        <li><i class="fa fa-check-circle"></i>Designed to build and strengthen the bonding amongst the family members.</li>
                                        <li><i class="fa fa-check-circle"></i>Helps to fill communication gaps amongst the family members.</li>
                                    </ul>
                                </div>
                            </div>
                        </div> --}}
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed" style="background: #ebebeb; padding: 15px 15px;">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="Empowering Students">
                                    {{ csrf_field() }}
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
										</div>
									</div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
										</div>
                                    </div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
										</div>
									</div>

								</form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 388;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();
})(window.jQuery);
</script>
@endpush
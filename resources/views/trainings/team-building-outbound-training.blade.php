@extends('layouts.page')

@section('meta_title') Team building outbound training|Empower Camp @endsection
@section('meta_description') Seeking your investment's worth, pro-team building outbound training? With Empower Camp, your search for strategised training camp ends here! Click to know more!@endsection
@section('meta_keywords') outbound team building activities,corporate team building activities,team building activities for employees,employee development programs @endsection
@section('no_footer', false)

@section('breadcrumb-image')
    <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}"
         alt="Corporate Outbound Trainings near Pune">
@endsection

@section('breadcrumb-content')
    <h3 class="with-subtitle">Team building outbound training</h3>
    {{--    <h4 class="subtitle">Team building is awesome</h4>--}}
    {{-- <button id="bookButton2" class="btn btn-default">Book Now</button> --}}
    <div class="breadcrumb-content">
                                <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                    <a itemprop="url" href="{{ url('/') }}">
                                        <span itemprop="title">Trainings</span>
                                    </a>
                                </span>
        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                    <a itemprop="url" href="{{ url('/leadership-outbound-training') }}"
                                       class="current-page">
                                        <span itemprop="title">Team building outbound training</span>
                                    </a>
                                </span>
    </div>
@endsection

@section('page-content')
    <section class="kopa-area sub-page kopa-area-25">
        <div class="container">
            <div class="row">
                <div class="main-col col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-intro-9">
                                <h3 class="widget-title style-11 ">Empower Camp Taps Potential with Team Building
                                    Outbound Training</h3>
                                <div class="widget-content">
                                    <p>
                                        Collaboration and teamwork remain as one of the most underutilised workplace
                                        tactics. Contrarily, acknowledging the importance of the power of collaboration
                                        to enhance business process, the power phrase ‘teamwork’ is receiving a massive
                                        boost.</p>
                                    <p>Empower Camp recognises this need for teams to propel the performance of an
                                        organisation and offers <b>team building outbound training</b>. However, its
                                        benefits
                                        are not limited to efficient productivity.
                                    </p>
                                </div>
                            </div>

                            <div class="widget millside-module-intro-9">
                                <h3 class="widget-title style-11 ">Why Should You Opt for Team Building Outbound
                                    Trainings?</h3>
                                <div class="widget-content">
                                    <p>
                                        At Empower Camp, we have structured a planned out series of activities that are
                                        fun to carry out and assist one in
                                    </p>

                                    <ul class="ul-checked" style="list-style: disc;">
                                        <li><i class="fa fa-check-circle"></i> Problem-solving and</li>
                                        <li><i class="fa fa-check-circle"></i> Conflicting resolutions.</li>
                                    </ul>

                                    <p>
                                        While this ensures improved productivity metrics of an enterprise and motivation
                                        of a team, it benefits extend to –
                                    </p>
                                    <ul class="ul-checked" style="list-style: disc;">
                                        <li><i class="fa fa-check-circle"></i>Teach teamwork and boost team performance
                                        </li>
                                        <li><i class="fa fa-check-circle"></i>Fostering and collaboration of innovative
                                            ideas
                                        </li>
                                        <li><i class="fa fa-check-circle"></i>Facilitating better communication</li>
                                        <li><i class="fa fa-check-circle"></i>Brushing up skills</li>
                                        <li><i class="fa fa-check-circle"></i>Developing problem-solving skills</li>
                                        <li><i class="fa fa-check-circle"></i>Building bridges among various departments
                                            & organisation and clients
                                        </li>
                                        <li><i class="fa fa-check-circle"></i>Improved morale and engagement</li>
                                    </ul>

                                    <h3 class="widget-title style-11 ">Empower Camp – The Ideal Destination for Team
                                        Building Trainings</h3>

                                    <p>
                                        At Empower Camp, we offer individuals and their corporates with the provisions
                                        of being guided by experts with years of experience behind them. Such expertise
                                        ensures assured results. Furthermore, ours is a custom-built training and
                                        adventure
                                        resort offering –
                                    </p>
                                    <ul class="ul-checked" style="list-style: disc;">
                                        <li><i class="fa fa-check-circle"></i>Risk management</li>
                                        <li><i class="fa fa-check-circle"></i>Handling outcomes</li>
                                        <li><i class="fa fa-check-circle"></i>Handling diverse team members</li>
                                    </ul>
                                    <p>Our <b>team building outbound training programs</b> are not just affordable but
                                        bring out the creativity from individuals. Such efforts at building a team
                                        ensure
                                        smooth functioning of the same, and hence yield maximum revenue for their
                                        organisation.
                                    </p>
                                </div>
                            </div>

                            <!-- widget -->
                        </div>
                        <!-- col-md-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-slider-pro-trainings">
                                {{-- <h3 class="widget-title text-center style-11">Course Photo Gallery</h3> --}}
                                <div class="widget-content">
                                    <div class="slider-pro slider-pro-trainings">
                                        <div class="sp-slides">
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-4.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-5.jpg') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- widget-content -->
                            </div>
                            <!-- widget -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Training Options</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}');">
                                <span></span>
                            </div>
                            <h6>Residential Leadership and Team Building Camps at our camp site at Kolad.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}');"></div>
                            <h6>Residential Leadership and Team Building Camps at a venue of your choice.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}');">
                                <span></span></div>
                            <h6>Corporate Sales Meets and Conferences with a value adding element of Team Building.</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}');">
                                <span></span>
                            </div>
                            <h6>Indoor workshops/seminars/motivational lectures/psychometric assessments and analysis at
                                your premises.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}');"></div>
                            <h6>Off Sites.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}');">
                                <span></span></div>
                            <h6>Day Packages.</h6>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Latest Sessions</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget millside-module-event-11 mb0">
                        <div class="mb30">
                            <p>
                                Book a Corporate Training Session with Empower, and trust us, your employees will thank
                                you for it. Not only will they remember the learnings, this camp will form a part of
                                their dinner conversations for years to come.
                            </p>
                        </div>
                        <div class="widget-content">
                            <ul class="row ul-mh">
                                @php
                                    $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                                @endphp
                                @foreach($sessions as $session)
                                    <li class="col-md-6 col-sm-12 col-xs-12">
                                        <article class="entry-item">
                                            <div class="entry-thumb">
                                                <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                            </div>
                                            <div class="entry-content">
                                                <header class="entry-header style-01 ">
                                                    <div class="entry-date-2">
                                                        @php
                                                            $day = date("d", strtotime($session->post_date));
                                                            $month = date("M", strtotime($session->post_date));
                                                        @endphp
                                                        <p>{{ $day }}</p>
                                                        <p>{{ $month }}</p>
                                                    </div>
                                                    <div>
                                                        <h4 class="entry-title style-05 ">
                                                            <a>{{ $session->title }}</a>
                                                        </h4>
                                                        <p>
                                                            <a href="#">Posted in Corporate Training</a>
                                                        </p>
                                                    </div>
                                                </header>
                                                <div class="clearfix">
                                                    <p>
                                                        {{ $session->excerpt }}
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Testimonials</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial-container">
                        <div class="dk-container">
                            <div class="cd-testimonials-wrapper cd-container">
                                <ul class="cd-testimonials">
                                    @php
                                        $testimonials = App\Models\Testimonial::where("category", "Corporate")->where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                    @endphp
                                    @foreach($testimonials as $testimonial)
                                        <li>
                                            <div class="testimonial-content">
                                                <p>
                                                    @php
                                                        echo $testimonial->content;
                                                        if($testimonial->content_image) {
                                                            echo $testimonial->contentImage();
                                                        }
                                                    @endphp
                                                </p>
                                                <p>
                                                    @php
                                                        if($testimonial->youtube != "") {
                                                            $link = $testimonial->youtube;
                                                            $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
                                                            if (empty($video_id[1]))
                                                                $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

                                                            $video_id = explode("&", $video_id[1]); // Deleting any other params
                                                            $video_id = $video_id[0];
                                                            echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$video_id.'"></iframe>';
                                                        }
                                                    @endphp
                                                </p>
                                                <div class="cd-author">
                                                    @php
                                                        echo $testimonial->authorImage();
                                                    @endphp

                                                    {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                                    <ul class="cd-author-info">
                                                        <li>{{ $testimonial->author }},<br>
                                                            <span>@php echo $testimonial->designation @endphp</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- cd-testimonials -->
                        </div>
                    </div>
                </div>
                <!-- col-md-8 -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-11 -->
@endsection

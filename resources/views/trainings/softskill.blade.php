
@extends('layouts.app')

@section('meta_title') SOFT SKILLS TRAINING &#124; empower @endsection
@section('meta_description') Some of our offerings: Attitude Boosting Self Confidence Reducing Negativity &#038; Developing Positive Attitude to Life Overcoming Fear of Failure &#038; Rejection Fear of being Ridicule / Overcoming Shyness Sustainability of @endsection
@section('meta_keywords') Stress Management,Leadership,Self Motivation,Communications Skills,Grooming and Etiquette @endsection
@section('no_footer', false)

@section('main-content')

<section class="kopa-area kopa-area-10 kopa-area-no-space">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}" alt="Corporate Outbound Trainings near Pune">
            <div class="bg-green">
                <div class="container">
                    <div class="kopa-breadcrumb">
                        <h3 class="with-subtitle">Soft Skill Training</h3>
                        <h4 class="subtitle">Team building is awesome</h4>
                        <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Trainings</span>
                                </a>
                            </span>
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/corporate-outbound-training') }}" class="current-page">
                                    <span itemprop="title">Soft Skill Training</span>
                                </a>
                            </span>
                        </div>
                    </div>
                    <!-- kopa-breadcrumb -->

                </div>
                <!-- container -->
            </div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row --> 
</section>
<!-- kopa-area-10 -->

<section class="kopa-area sub-page kopa-area-25">
    <div class="container">
        <div class="row">
            <div class="sidebar col-md-4 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div id="widget-booking" class="widget widget-fixed">
                            <h3 class="widget-title style-04">Make Your Booking</h3>
                            <span id="msg" style="color:red"></span>
                            <div class="widget-content">
                                <form id="bookingForm" class="form-horizontal kopa-form kopa-form-10" method="POST" role="form" novalidate="novalidate">
                                    <input type="hidden" name="source" value="SOFT SKILLS TRAINING">
                                    {{ csrf_field() }}
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="name" placeholder="Name" required autocomplete="on">
										</div>
									</div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="email" class="form-control" name="email" placeholder="Email" required autocomplete="on">
										</div>
                                    </div>
                                    <div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<input type="text" class="form-control" name="organization" placeholder="Organization" required autocomplete="on">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<button id="submit" type="submit" class="btn ct-btn-7">Submit</button>
										</div>
									</div>

								</form>
                            </div>
                            <!-- widget-content --> 
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row --> 
            </div>
            <!-- col-md-4 -->

            <div class="main-col col-md-8 col-sm-7 col-xs-12">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-intro-9">
                            <h3 class="widget-title style-11 ">Soft Skills</h3>
                            <div class="widget-content">
                                <p>
                                    Imagine two individuals with completely identical qualifications competing for the same job, contract or position.Who would win? Of course the candidate with better soft skills!
                                </p>
                                <p>Let us look at another scenario.</p>
                                <p>
                                    There are two companies offering similar product, vying for the same contract with a client. The first company’s representative is all technical and is not too good at people skills. The second company’s representative really listens, asks the right questions and matches the client’s needs with his employer’s offerings. No prizes for guessing which company gets the contract!
                                </p>
                                <p>
                                    <b>Soft skills are an essential part of personal and professional success, and they are not taught in any school or university.</b>
                                </p>
                            </div>
                            <h3 class="widget-title style-11 mt20 mb20">Some of our offerings</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="ul-checked">
                                        <li><i class="fa fa-check-circle"></i>Attitude</li>
                                        <li><i class="fa fa-check-circle"></i>Time Management</li>
                                        <li><i class="fa fa-check-circle"></i>Goal Setting</li>
                                        <li><i class="fa fa-check-circle"></i>Boosting Self Confidence</li>
                                        <li><i class="fa fa-check-circle"></i>Developing Empathy , Sensitivity</li>
                                        <li><i class="fa fa-check-circle"></i>Sustainability of Intent & Perseverance</li>
                                        <li><i class="fa fa-check-circle"></i>Organised working (How to set priorities in Life)</li>
                                        <li><i class="fa fa-check-circle"></i>Grooming and Etiquette</li>
                                        <li><i class="fa fa-check-circle"></i>Reducing Negativity & Developing Positive Attitude to Life</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="ul-checked">
                                        <li><i class="fa fa-check-circle"></i>Self Motivation</li>
                                        <li><i class="fa fa-check-circle"></i>Leadership</li>
                                        <li><i class="fa fa-check-circle"></i>Presentations Skills</li>
                                        <li><i class="fa fa-check-circle"></i>Communications Skills</li>
                                        <li><i class="fa fa-check-circle"></i>Anger & Stress Management</li>
                                        <li><i class="fa fa-check-circle"></i>Building Self Image & Self Efficacy</li>
                                        <li><i class="fa fa-check-circle"></i>Overcoming Fear of Failure & Rejection</li>
                                        <li><i class="fa fa-check-circle"></i>Fear of being Ridicule / Overcoming Shyness</li>
                                        <li><i class="fa fa-check-circle"></i>Conflict Management / Interpersonal Relationships</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                    <!-- col-md-12 -->
                </div>
                <!-- row -->
                <div class="widget millside-module-intro-9">
                    <h3 class="widget-title style-11 ">Nurture Has a Stronger Role</h3>
                    <div class="widget-content">
                        <p>
                            Nature has some role to play in how good a person’s soft skills are, but nurture has a stronger role to play.
                        </p>
                        <p>
                            Communication, friendliness, optimism, can-do attitude, every soft skill can be taught and strengthened through training.Soft Skills Training is something than cannot be ignored in a country like India where the education system completely ignores this aspect. For a company or corporate, their employees are the link between the company and the external world, including their clients.
                        </p>
                        <p>Thus, grooming employees should be considered by corporate houses as an important investment in their human resources.
                        </p>
                        <p>
                            Companies get dual benefits by enrolling their employees in a soft skills training programme. Firstly, the company reaps the benefits in terms of better client relations, better inter-departmental communications and an overall improvement in brand image. Secondly, the employees feel cared for, as the company is contributing to their professional and career growth, which in turn increases employee loyalty.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Empower’s Soft Skills Training Programs</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="widget millside-module-event-11 mb0">
                    <div class="mb30">
                        <p>
                            <b>Empower’s team has about four decades of experience in training people, with more than 400 successful training</b>
                        </p>
                    </div>
                    <div class="widget-content">
                        <ul class="row ul-mh">
                            @php
                            $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                            @endphp
                            @foreach($sessions as $session)
                            <li class="col-md-6 col-sm-12 col-xs-12">
                                <article class="entry-item">
                                    <div class="entry-thumb">
                                        <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                    </div>
                                    <div class="entry-content">
                                        <header class="entry-header style-01 ">
                                            <div class="entry-date-2">
                                                @php
                                                $day = date("d", strtotime($session->post_date));
                                                $month = date("M", strtotime($session->post_date));
                                                @endphp
                                                <p>{{ $day }}</p>
                                                <p>{{ $month }}</p>
                                            </div>
                                            <div>
                                                <h4 class="entry-title style-05 ">
                                                    <a>{{ $session->title }}</a>
                                                </h4>
                                                <p>
                                                    <a href="#">Posted in Corporate Training</a>
                                                </p>
                                            </div>
                                        </header>
                                        <div class="clearfix">
                                            <p>
                                                {{ $session->excerpt }}
                                            </p>
                                        </div>
                                    </div>
                                </article>
                            </li>
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/testimonials-for-outbound-training.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-12">
                                            <h3 class="widget-title style-11 mt20 mb20" style="padding-left:30px;line-height:20px;">Testimonials</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="testimonial-container">
                    <div class="dk-container">
                        <div class="cd-testimonials-wrapper cd-container">
                            <ul class="cd-testimonials">
                                @php
                                $testimonials = App\Models\Testimonial::where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                @endphp
                                @foreach($testimonials as $testimonial)
                                <li>
                                    <div class="testimonial-content">
                                        <p>
                                            @php
                                            echo $testimonial->content;
                                            if($testimonial->content_image) {
                                                echo $testimonial->contentImage();
                                            }
                                            @endphp
                                        </p>
                                        <div class="cd-author">
                                            @php
                                            echo $testimonial->authorImage();
                                            @endphp
                                            
                                            {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                            <ul class="cd-author-info">
                                                <li>{{ $testimonial->author }},<br><span>{{ $testimonial->designation }}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- cd-testimonials -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="widget millside-module-ads-5">
                            <div class="widget-content">
                                <img src="{{ asset('img/background/outbound-trainings.jpg') }}" alt="">
                                <div class="bg-green-2">
                                    <div class="row mtm30">
                                        <div class="col-md-1">
                                            
                                        </div>
                                        <!-- col-md-2 -->
                                        <div class="col-md-11 col-sm-12 col-xs-12">
                                            <div class=" part-2 ul-mh">
                                                <div class="icon-weather">
                                                    {{-- <img src="https://upsidethemes.net/demo/millside/html/images/p36/1.png" alt=""> --}}
                                                </div>
                                                <div>
                                                    <p>
                                                        Book you adventure now !
                                                        <button id="bookButton" class="btn btn-default">Book Now</button>
                                                    </p>
                                                    <p>Use the buttons on the right to check out our tee time specials</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-md-10 -->
                                    </div>
                                    <!-- row --> 
                                </div>
                            </div>
                        </div>
                        <!-- widget --> 
                    </div>
                </div>
            </div>
            <!-- col-md-8 -->
        </div>
        <!-- row --> 
    </div>
    <!-- container -->
</section>
<!-- kopa-area-11 -->

@endsection

@push('styles')
<style>
#bookButton {
    float: right;
    display: inline-block;
    margin-top: 13px;
}
</style>
@endpush


@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.1/jquery.flexslider.min.js"></script>
<script>
var wTitleTop = 0;
var wTitleBottom = 0;

function updateFormPos() {
    var scrollTop = $(this).scrollTop();
    // console.log('scrollTop', scrollTop);
    $("#widget-booking").removeClass('widget-fixed');

    if($(window).width() > 481) {
        if (scrollTop > wTitleTop) {
            $("#widget-booking").addClass('widget-fixed');
        } else {
            $("#widget-booking").removeClass('widget-fixed');
        }
    }
}

(function($) {
    // var scrollTop = $(this).scrollTop();
    // wTitleTop = parseInt($("#widget-booking .widget-title").offset().top);
    // wTitleTop = wTitleTop - scrollTop;

    wTitleTop = 830;
    wTitleBottom = 4950;

    // console.log('wTitleTop', wTitleTop);
    $(window).scroll(function() {
        updateFormPos();
    });

    // create testimonials slider
	$('.cd-testimonials-wrapper').flexslider({
		selector: ".cd-testimonials > li",
		animation: "slide",
		controlNav: true,
		slideshow: true,
		smoothHeight: true,
		start: function() {
			$('.cd-testimonials').children('li').css({
				'opacity': 1,
				'position': 'relative'
			});
		}
    });
    $("#bookButton").on("click", function() {
        $("input[name=name]").focus();
    });

    $("#bookingForm").validate({
        rules: {
        },
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        },
        submitHandler: function(form) {
            $.ajax({
                url: "{{ url('/save_session_booking') }}",
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit..');
                    $('#submit').prop('disabled', true);
                },
                success: function( data ) {
                    console.log(data);
                    if(data.status == "success") {
                        $('#msg').html(data.message);
                        $('#submit').prop('disabled', false);
                        $("#bookingForm")[0].reset();
                        document.location.href = "https://empowercamp.com/thankyou";
                    } else {
                        // $('#submit').html('<i class="fa fa-refresh fa-spin"></i> Submit');
                        $('#msg').html("Try Again :(");
                        $('#submit').prop('disabled', false);
                    }
                }
            });
            return false;
        }
    });

    updateFormPos();
})(window.jQuery);
</script>
@endpush
@extends('layouts.page')

@section('meta_title') Induction Outbound Camps | Empower Camp @endsection
@section('meta_description')  Looking for induction outbound camps near you? Equipped with affordability, top-notch features, experienced supervision, your hunt ends at Empower camp. Click now! @endsection
@section('meta_keywords')  @endsection
@section('no_footer', false)

@section('breadcrumb-image')
    <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}"
         alt="Corporate Outbound Trainings near Pune">
@endsection

@section('breadcrumb-content')
    <h3 class="with-subtitle">Induction Outbound Camps</h3>
    <h4 class="subtitle">Team building is awesome</h4>
    <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Trainings</span>
                                </a>
                            </span>
        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/induction-outbound-camps') }}" class="current-page">
                                    <span itemprop="title">Induction Outbound Camps</span>
                                </a>
                            </span>
    </div>
@endsection

@section('page-content')

    <section class="kopa-area sub-page kopa-area-25">
        <div class="container">
            <div class="row">
                <div class="main-col col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-intro-9">
                                <h3 class="widget-title style-11 ">Welcome Your Team Onboard with Empower Camp’s
                                    Induction Outbound Camps</h3>
                                <div class="widget-content">
                                    <p>
                                        A reliable onboarding process boosts productivity by over 70% and employee
                                        retention by approximately 82%. With an increased share of job openings, in
                                        recent times, retaining employees was never more critical. Subsequently, much
                                        emphasis is made on the induction process.</p>
                                    <p>Taking a cue from the boosted importance of in-boarding, at Empower Camp, we
                                        offer corporates with ideal induction outbound camps. Such camps are designed to
                                        ensure that the introductory platform of the company with its employee is kept
                                        informative and interactive.
                                    </p>
                                </div>
                                <h3 class="widget-title style-11 mt20 mb20">Benefits of Induction Outbound Camps</h3>

                                <p>The introduction to a company’s ethics and exposure is ideally essential and plays a
                                    crucial role in accelerating an employee’s professional development.</p>

                                <p>While this is an on-the-face benefit of our camps, it marks the outline of multiple
                                    benefits from both the company’s and individual’s end –</p>

                                <ol>
                                    <li><b>Company’s perspective</b>
                                        <ul>
                                            The induction is the first platform for employees to know about their
                                            enterprise. Such
                                            interaction ensures quality employee retention among other benefits that
                                            include –

                                            <li>It increases operational efficiency.</li>
                                            <li>The organisation saves up on time and money.</li>
                                            <li>It mitigates employee turnover.</li>
                                        </ul>
                                    </li>
                                    <li>
                                        Employee’s perspective
                                        <ul>
                                            It is essential that the newly joined employees come face-to-face with their
                                            enterprise.
                                            In this regard, our induction outbound camps extend benefits such as –
                                            <li>He/she feels valued and respected.</li>
                                            <li>It boosts communication.</li>
                                            <li>It offers required information about their company.</li>
                                            <li>It provides an outline of one’s professional prospect.</li>
                                        </ul>
                                    </li>

                                    <h3>Empower Camp - Empowering Corporates in their Team Endeavours!</h3>
                                    <p>
                                        At Empower Camp, we believe in ensuring a smooth journey of employee's inclusion
                                        into a
                                        corporate. Keeping in mind the chances of boosted organisational performances,
                                        we offer
                                        multiple add-ons to smoothen induction. It includes –</p>

                                    <ul>
                                        <li>Conference halls</li>
                                        <li>Activity areas</li>
                                        <li>Cottage tents, etc.</li>
                                    </ul>
                                </ol>
                                <p>
                                    These facilities substantiate our efforts at promoting teamwork, sans the cumbersome
                                    and
                                    mundane paperwork. Our induction camps are incorporated with fun-filled activities,
                                    promoting interesting interactions that boost an organisation’s productivity.
                                </p>

                            </div>
                            <!-- widget -->
                        </div>
                        <!-- col-md-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-slider-pro-trainings">
                                {{-- <h3 class="widget-title text-center style-11">Course Photo Gallery</h3> --}}
                                <div class="widget-content">
                                    <div class="slider-pro slider-pro-trainings">
                                        <div class="sp-slides">
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-4.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-5.jpg') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- widget-content -->
                            </div>
                            <!-- widget -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Training Options</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}');">
                                <span></span>
                            </div>
                            <h6>Residential Leadership and Team Building Camps at our camp site at Kolad.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}');"></div>
                            <h6>Residential Leadership and Team Building Camps at a venue of your choice.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}');">
                                <span></span></div>
                            <h6>Corporate Sales Meets and Conferences with a value adding element of Team Building.</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}');">
                                <span></span>
                            </div>
                            <h6>Indoor workshops/seminars/motivational lectures/psychometric assessments and analysis at
                                your premises.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}');"></div>
                            <h6>Off Sites.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}');">
                                <span></span></div>
                            <h6>Day Packages.</h6>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Latest Sessions</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget millside-module-event-11 mb0">
                        <div class="mb30">
                            <p>
                                Book a Corporate Training Session with Empower, and trust us, your employees will thank
                                you for it. Not only will they remember the learnings, this camp will form a part of
                                their dinner conversations for years to come.
                            </p>
                        </div>
                        <div class="widget-content">
                            <ul class="row ul-mh">
                                @php
                                    $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                                @endphp
                                @foreach($sessions as $session)
                                    <li class="col-md-6 col-sm-12 col-xs-12">
                                        <article class="entry-item">
                                            <div class="entry-thumb">
                                                <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                            </div>
                                            <div class="entry-content">
                                                <header class="entry-header style-01 ">
                                                    <div class="entry-date-2">
                                                        @php
                                                            $day = date("d", strtotime($session->post_date));
                                                            $month = date("M", strtotime($session->post_date));
                                                        @endphp
                                                        <p>{{ $day }}</p>
                                                        <p>{{ $month }}</p>
                                                    </div>
                                                    <div>
                                                        <h4 class="entry-title style-05 ">
                                                            <a>{{ $session->title }}</a>
                                                        </h4>
                                                        <p>
                                                            <a href="#">Posted in Corporate Training</a>
                                                        </p>
                                                    </div>
                                                </header>
                                                <div class="clearfix">
                                                    <p>
                                                        {{ $session->excerpt }}
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Testimonials</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial-container">
                        <div class="dk-container">
                            <div class="cd-testimonials-wrapper cd-container">
                                <ul class="cd-testimonials">
                                    @php
                                        $testimonials = App\Models\Testimonial::where("category", "Corporate")->where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                    @endphp
                                    @foreach($testimonials as $testimonial)
                                        <li>
                                            <div class="testimonial-content">
                                                <p>
                                                    @php
                                                        echo $testimonial->content;
                                                        if($testimonial->content_image) {
                                                            echo $testimonial->contentImage();
                                                        }
                                                    @endphp
                                                </p>
                                                <p>
                                                    @php
                                                        if($testimonial->youtube != "") {
                                                            $link = $testimonial->youtube;
                                                            $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
                                                            if (empty($video_id[1]))
                                                                $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

                                                            $video_id = explode("&", $video_id[1]); // Deleting any other params
                                                            $video_id = $video_id[0];
                                                            echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$video_id.'"></iframe>';
                                                        }
                                                    @endphp
                                                </p>
                                                <div class="cd-author">
                                                    @php
                                                        echo $testimonial->authorImage();
                                                    @endphp

                                                    {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                                    <ul class="cd-author-info">
                                                        <li>{{ $testimonial->author }},<br>
                                                            <span>@php echo $testimonial->designation @endphp</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- cd-testimonials -->
                        </div>
                    </div>
                </div>
                <!-- col-md-12 -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-11 -->

@endsection

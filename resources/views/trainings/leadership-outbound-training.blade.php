@extends('layouts.page')

@section('meta_title') Leadership outbound training | Empower Activity Camps @endsection
@section('meta_description')  Looking for the best leadership outbound training near you? Click for such affordable training sessions and witness a change in your team building capability! @endsection
@section('meta_keywords')  @endsection
@section('no_footer', false)

@section('breadcrumb-image')
    <img src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune.jpg') }}"
         alt="Corporate Outbound Trainings near Pune">
@endsection

@section('breadcrumb-content')
    <h3 class="with-subtitle">Leadership Outbound Training</h3>
    <h4 class="subtitle">Team building is awesome</h4>
    <div class="breadcrumb-content">
                            <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/') }}">
                                    <span itemprop="title">Trainings</span>
                                </a>
                            </span>
        <span itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                                <a itemprop="url" href="{{ url('/leadership-outbound-training') }}"
                                   class="current-page">
                                    <span itemprop="title">Leadership outbound training</span>
                                </a>
                            </span>
    </div>
@endsection

@section('page-content')
    <section class="kopa-area sub-page kopa-area-25">
        <div class="container">
            <div class="row">
                <div class="main-col col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-intro-9">
                                <h3 class="widget-title style-11 ">Empower Activity Camps – Awaking Latent Talent with
                                    Leadership Outbound Training</h3>
                                <div class="widget-content">
                                    <p>
                                        The capability of an organisation is a lid on the performance of an enterprise,
                                        according to the Law of the Lid by John C. Maxwell. Subsequently, at present,
                                        83% of businesses believe that it’s crucial to develop leaders at all levels.
                                    </p>

                                    <p>At Empower Camp, we cater to this increased demand for leading personas, and
                                        hence offer our affordable, streamlined and organised <b>leadership outbound
                                            training</b>. Such training aims to ensure that corporates are equipped with
                                        leaders
                                        to propel their business in the right direction.
                                    </p>
                                </div>
                                <h3 class="widget-title style-11 mt20 mb20">Why the Requisite for Leadership Outbound
                                    Training?
                                </h3>
                                <p>
                                    The role of a leader in a company has a significant impact on both employees and
                                    their company. In this regard, it is ideally essential to identify abilities,
                                    professional drive, and work ethic of an individual.
                                </p>
                                <p>
                                    Consequently, we endeavour to make this journey effortless for you by –
                                </p>
                                <ul class="ul-checked">
                                    <li><i class="fa fa-check-circle"></i>Nurturing future leaders</li>
                                    <li><i class="fa fa-check-circle"></i>Increasing productivity of the workforce</li>
                                    <li><i class="fa fa-check-circle"></i>Imparting
                                        <ul>
                                            <li><i class="fa fa-check-circle"></i>Self-leadership,</li>
                                            <li><i class="fa fa-check-circle"></i>Executive leadership,</li>
                                            <li><i class="fa fa-check-circle"></i>Group leadership, and</li>
                                            <li><i class="fa fa-check-circle"></i>Direct leadership.</li>
                                        </ul>
                                    </li>
                                    <li><i class="fa fa-check-circle"></i>Offering <b>leadership outbound training</b>
                                        that is equipped with
                                        elements
                                        such as –
                                        <ul>
                                            <li><i class="fa fa-check-circle"></i>Influencing and inspiring team
                                                members.
                                            </li>
                                            <li><i class="fa fa-check-circle"></i>Result accountability.</li>
                                            <li><i class="fa fa-check-circle"></i>Goal creation and strategy mapping.
                                            </li>
                                        </ul>
                                    </li>

                                    <li><i class="fa fa-check-circle"></i>Imbibing the requisite steps to awaken the
                                        latent leadership skills
                                        through –
                                        <ul>
                                            <li><i class="fa fa-check-circle"></i>Team evaluation and supervision.</li>
                                            <li><i class="fa fa-check-circle"></i>Team programme customisation.</li>
                                        </ul>
                                    </li>
                                </ul>

                                <h3>Add-Ons to our Leadership Outbound Training</h3>
                                <p>
                                    We acknowledge the importance of training future leaders. The ambience in which this
                                    training is delivered, is important too. We have top-notch facilities situated in a
                                    natural environment, far away from the distracting bustle of city life.
                                    Accommodation includes AC Cottages, Dormitories, and Swiss Cottage Tents, a large
                                    Conference Hall for discussions, and effective Safety Measures for all the adventure
                                    activities.
                                </p>
                                <p>Our company, with a team of in-house experts, who are Senior Army Veterans, is adept
                                    at highlighting a team’s leadership potential, and also bringing out the best in
                                    each team member in the organisation. This, in turn, benefits that organisation,
                                    making this individual a treasured employee and a true asset.
                                </p>
                            </div>
                            <!-- widget -->
                        </div>
                        <!-- col-md-12 -->
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-slider-pro-trainings">
                                {{-- <h3 class="widget-title text-center style-11">Course Photo Gallery</h3> --}}
                                <div class="widget-content">
                                    <div class="slider-pro slider-pro-trainings">
                                        <div class="sp-slides">
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-4.jpg') }}">
                                            </div>
                                            <div class="sp-slide">
                                                <img class="sp-image" alt=""
                                                     src="{{ asset('img/trainings/corporate-outbound-trainings-near-pune-5.jpg') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- widget-content -->
                            </div>
                            <!-- widget -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Training Options</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}');">
                                <span></span>
                            </div>
                            <h6>Residential Leadership and Team Building Camps at our camp site at Kolad.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}');"></div>
                            <h6>Residential Leadership and Team Building Camps at a venue of your choice.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}');">
                                <span></span></div>
                            <h6>Corporate Sales Meets and Conferences with a value adding element of Team Building.</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-1.jpg') }}');">
                                <span></span>
                            </div>
                            <h6>Indoor workshops/seminars/motivational lectures/psychometric assessments and analysis at
                                your premises.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-2.jpg') }}');"></div>
                            <h6>Off Sites.</h6>
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="img-rounded"
                                 style="background-image: url('{{ asset('img/trainings/corporate-outbound-trainings-near-pune-3.jpg') }}');">
                                <span></span></div>
                            <h6>Day Packages.</h6>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Latest Sessions</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="widget millside-module-event-11 mb0">
                        <div class="mb30">
                            <p>
                                Book a Corporate Training Session with Empower, and trust us, your employees will thank
                                you for it. Not only will they remember the learnings, this camp will form a part of
                                their dinner conversations for years to come.
                            </p>
                        </div>
                        <div class="widget-content">
                            <ul class="row ul-mh">
                                @php
                                    $sessions = App\Models\Session::where("training", "Corporate Training")->orderBy("post_date", "desc")->limit(6)->get();
                                @endphp
                                @foreach($sessions as $session)
                                    <li class="col-md-6 col-sm-12 col-xs-12">
                                        <article class="entry-item">
                                            <div class="entry-thumb">
                                                <img alt="{{ $session->title }}" src="{{ $session->bannerImage() }}">
                                            </div>
                                            <div class="entry-content">
                                                <header class="entry-header style-01 ">
                                                    <div class="entry-date-2">
                                                        @php
                                                            $day = date("d", strtotime($session->post_date));
                                                            $month = date("M", strtotime($session->post_date));
                                                        @endphp
                                                        <p>{{ $day }}</p>
                                                        <p>{{ $month }}</p>
                                                    </div>
                                                    <div>
                                                        <h4 class="entry-title style-05 ">
                                                            <a>{{ $session->title }}</a>
                                                        </h4>
                                                        <p>
                                                            <a href="#">Posted in Corporate Training</a>
                                                        </p>
                                                    </div>
                                                </header>
                                                <div class="clearfix">
                                                    <p>
                                                        {{ $session->excerpt }}
                                                    </p>
                                                </div>
                                            </div>
                                        </article>
                                    </li>
                                @endforeach

                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="widget millside-module-ads-5">
                                <div class="widget-content">
                                    <h3 class="widget-title style-11 mt20 mb20"
                                        style="padding-left:30px;line-height:20px;">Testimonials</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial-container">
                        <div class="dk-container">
                            <div class="cd-testimonials-wrapper cd-container">
                                <ul class="cd-testimonials">
                                    @php
                                        $testimonials = App\Models\Testimonial::where("category", "Corporate")->where("status", "Approved")->orderBy("post_date", "desc")->limit(10)->get();
                                    @endphp
                                    @foreach($testimonials as $testimonial)
                                        <li>
                                            <div class="testimonial-content">
                                                <p>
                                                    @php
                                                        echo $testimonial->content;
                                                        if($testimonial->content_image) {
                                                            echo $testimonial->contentImage();
                                                        }
                                                    @endphp
                                                </p>
                                                <p>
                                                    @php
                                                        if($testimonial->youtube != "") {
                                                            $link = $testimonial->youtube;
                                                            $video_id = explode("?v=", $link); // For videos like http://www.youtube.com/watch?v=...
                                                            if (empty($video_id[1]))
                                                                $video_id = explode("/v/", $link); // For videos like http://www.youtube.com/watch/v/..

                                                            $video_id = explode("&", $video_id[1]); // Deleting any other params
                                                            $video_id = $video_id[0];
                                                            echo '<iframe width="420" height="315" src="https://www.youtube.com/embed/'.$video_id.'"></iframe>';
                                                        }
                                                    @endphp
                                                </p>
                                                <div class="cd-author">
                                                    @php
                                                        echo $testimonial->authorImage();
                                                    @endphp

                                                    {{-- <img src="http://placehold.it/350x350/222222/222222" alt="Author image"> --}}
                                                    <ul class="cd-author-info">
                                                        <li>{{ $testimonial->author }},<br>
                                                            <span>@php echo $testimonial->designation @endphp</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- cd-testimonials -->
                        </div>
                    </div>
                </div>
                <!-- col-md-8 -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
    </section>
    <!-- kopa-area-11 -->

@endsection

Dear Admin,<br><br>

There is new inquiry:<br><br>

Name: {{ $name }}<br>
Email: {{ $email }}<br>
Phone: {{ $phone }}<br>
Organization: {{ $organization }}<br>
Message: {{ $message_str }}<br><br>

Best Regards,
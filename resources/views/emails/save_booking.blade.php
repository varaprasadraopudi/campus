Dear Admin,<br><br>

There is new booking:<br><br>

Name: {{ $name }}<br>
Email: {{ $email }}<br>
Phone: {{ $phone }}<br>
Organization: {{ $organization }}<br>
Message: {{ $message_str }}<br>
Source: {{ $source }}<br><br>

Best Regards,
<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'booking' => 'Booking',
    'bookings' => 'Bookings',
    'booking_listing' => 'Booking Listing',
    'booking_add' => 'Add Booking',
    'back_to_bookings' => 'Back to Bookings',
    'booking_view' => 'Booking View',
    'booking_edit' => 'Booking Edit',
];

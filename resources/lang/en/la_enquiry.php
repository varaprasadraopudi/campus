<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'enquiry' => 'Enquiry',
    'enquiries' => 'Enquiries',
    'enquiry_listing' => 'Enquiry Listing',
    'enquiry_add' => 'Add Enquiry',
    'back_to_enquiries' => 'Back to Enquiries',
    'enquiry_view' => 'Enquiry View',
    'enquiry_edit' => 'Enquiry Edit',
];

<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'backup' => 'Backup',
    'backups' => 'Backups',
    'backup_listing' => 'Backups listing',
    'create_backup' => 'Create Backup',
    'creating_backup' => 'Creating Backup',
    'backup_created' => 'Backup Created',
    'backup_creation_failed' => 'Backup creation failed',
    'backup_creation' => 'Backup Creation',
];

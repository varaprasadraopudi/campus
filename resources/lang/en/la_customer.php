<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'customer' => 'Customer',
    'customers' => 'Customers',
    'customer_listing' => 'Customer Listing',
    'customer_add' => 'Add Customer',
    'back_to_customers' => 'Back to Customers',
    'customer_view' => 'Customer View',
    'customer_edit' => 'Customer Edit',
];

<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'testimonial' => 'Testimonial',
    'testimonials' => 'Testimonials',
    'testimonial_listing' => 'Testimonial Listing',
    'testimonial_add' => 'Add Testimonial',
    'back_to_testimonials' => 'Back to Testimonials',
    'testimonial_view' => 'Testimonial View',
    'testimonial_edit' => 'Testimonial Edit',
];

<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'lalog' => 'Log',
    'lalogs' => 'Logs',
    'lalog_listing' => 'Log Listing',
    'lalog_add' => 'Add Log',
    'back_to_lalogs' => 'Back to Logs',
    'lalog_view' => 'Log View',
    'lalog_edit' => 'Log Edit'
];

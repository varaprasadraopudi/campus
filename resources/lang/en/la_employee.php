<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'employee' => 'Employee',
    'employees' => 'Employees',
    'employee_listing' => 'Employee Listing',
    'employee_add' => 'Add Employee',
    'back_to_employees' => 'Back to Employees',
    'employee_view' => 'Employee View',
    'employee_edit' => 'Employee Edit',
    'account_settings' => 'Account settings'
];

<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'permission' => 'Permission',
    'permissions' => 'Permissions',
    'permission_listing' => 'Permission Listing',
    'permission_add' => 'Add Permission',
    'back_to_permissions' => 'Back to Permissions',
    'permission_view' => 'Permission View',
    'permission_edit' => 'Permission Edit'
];

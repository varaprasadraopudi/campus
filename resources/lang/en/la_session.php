<?php
/**
 * Language generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

return [
    'session' => 'Session',
    'sessions' => 'Sessions',
    'session_listing' => 'Session Listing',
    'session_add' => 'Add Session',
    'back_to_sessions' => 'Back to Sessions',
    'session_view' => 'Session View',
    'session_edit' => 'Session Edit',
];

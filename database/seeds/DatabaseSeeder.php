<?php
/**
 * Code generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

use Illuminate\Database\Seeder;

use App\Models\Module;
use App\Models\ModuleFields;
use App\Models\ModuleFieldTypes;
use App\Models\Menu;
use App\Models\LAConfigs;
use App\Models\Department;
use App\Models\Upload;
use App\Models\BlogPost;

use App\Role;
use App\Permission;
use App\Models\Session;
use App\Models\Employee;
use App\User;
use App\Models\LALog;
use App\Models\Testimonial;


class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		
		/* ================ LaraAdmin Seeder Code ================ */
		
		// Generating Module Menus
		$modules = Module::all();
		$dashboardMenu = Menu::create([
			"name" => "Dashboard",
			"url" => "/",
			"icon" => "fa-home",
			"type" => 'custom',
			"parent" => 0,
			"hierarchy" => 0
		]);
        $teamMenu = Menu::create([
			"name" => "Team",
			"url" => "#",
			"icon" => "fa-group",
			"type" => 'custom',
			"parent" => 0,
			"hierarchy" => 1
		]);
		$blogMenu = Menu::create([
			"name" => "Blog",
			"url" => "#",
			"icon" => "fa-file-text-o",
			"type" => 'custom',
			"parent" => 0,
			"hierarchy" => 2
		]);
		
		foreach ($modules as $module) {
			$parent = 0;
			if($module->name != "Backups" && $module->name != "LALogs") {
				if(in_array($module->name, ["Users", "Departments", "Employees", "Roles", "Permissions"])) {
					$parent = $teamMenu->id;
				}
				if(in_array($module->name, ["Blog_posts", "Blog_categories"])) {
					$parent = $blogMenu->id;
				}
				Menu::create([
					"name" => $module->name,
					"url" => $module->name_db,
					"icon" => $module->fa_icon,
					"type" => 'module',
					"parent" => $parent
				]);
			}
		}
		
		// Create Administration Department
	    $deptAdmin = new Department;
		$deptAdmin->name = "Administration";
		$deptAdmin->tags = "[]";
		$deptAdmin->color = "#000";
		$deptAdmin->save();

        // Create Customer Department
	    $deptCustomer = new Department;
		$deptCustomer->name = "Customer";
		$deptCustomer->tags = "[]";
		$deptCustomer->color = "#000";
		$deptCustomer->save();

        // Create Super Admin Role
		$roleSA = new Role;
		$roleSA->name = "SUPER_ADMIN";
		$roleSA->display_name = "Super Admin";
		$roleSA->description = "Full Access Role";
        $roleSA->context_type = "Employee";
		$roleSA->parent = NULL;
		$roleSA->dept = $deptAdmin->id;
		$roleSA->save();

        // Create Admin Role
		$roleA = new Role;
		$roleA->name = "ADMIN";
		$roleA->display_name = "Admin";
		$roleA->description = "Admin Level Access Role";
        $roleA->context_type = "Employee";
		$roleA->parent = 1;
		$roleA->dept = $deptAdmin->id;
		$roleA->save();

        // Create Admin Role
		$roleC = new Role;
		$roleC->name = "CUSTOMER";
		$roleC->display_name = "Customer";
		$roleC->description = "Customer Level Access Role";
        $roleC->context_type = "Customer";
		$roleC->parent = $roleA->id;
		$roleC->dept = $deptCustomer->id;
		$roleC->save();

		// Set Full Access For Super Admin Role
		foreach ($modules as $module) {
			Module::setDefaultRoleAccess($module->id, $roleSA->id, "full");
		}
		// Set Full Access For Super Admin - Menu
		$menus = Menu::all();
		foreach ($menus as $menu) {
			$menu->roles()->attach($roleSA->id);
		}
		// Create Admin Panel Permission
		$perm = new Permission;
		$perm->name = "ADMIN_PANEL";
		$perm->display_name = "Admin Panel";
		$perm->description = "Admin Panel Permission";
		$perm->save();
		
		$roleSA->attachPermission($perm);
        $roleA->attachPermission($perm);

		// Generate LaraAdmin Default Configurations
		
		$laconfig = new LAConfigs;
		$laconfig->section = "General";
        $laconfig->label = "Sitename";
        $laconfig->key = "sitename";
		$laconfig->value = "Empower Camp";
        $laconfig->field_type = 16;
        $laconfig->minlength = 0;
        $laconfig->maxlength = 20;
        $laconfig->required = true;
		$laconfig->save();

		$laconfig = new LAConfigs;
        $laconfig->section = "General";
		$laconfig->label = "Sitename First Word";
        $laconfig->key = "sitename_part1";
		$laconfig->value = "Empower";
        $laconfig->field_type = 16;
        $laconfig->minlength = 0;
        $laconfig->maxlength = 20;
        $laconfig->required = false;
		$laconfig->save();
		
		$laconfig = new LAConfigs;
        $laconfig->section = "General";
        $laconfig->label = "Sitename Second Word";
		$laconfig->key = "sitename_part2";
		$laconfig->value = "Camp";
        $laconfig->field_type = 16;
        $laconfig->minlength = 0;
        $laconfig->maxlength = 20;
        $laconfig->required = false;
		$laconfig->save();
		
		$laconfig = new LAConfigs;
        $laconfig->section = "General";
		$laconfig->label = "Sitename Short (2/3 Characters)";
		$laconfig->key = "sitename_short";
		$laconfig->value = "EC";
        $laconfig->field_type = 16;
        $laconfig->minlength = 1;
        $laconfig->maxlength = 3;
        $laconfig->required = true;
		$laconfig->save();

		$laconfig = new LAConfigs;
        $laconfig->section = "General";
		$laconfig->label = "Site Description (160 Characters)";
		$laconfig->key = "site_description";
		$laconfig->value = "Veteran ARMY Officer's venture Empower Activity Camps specializes in ‘Corporate training’ in outdoor environment, 50 acres adventure camp near Mumbai and Pune.";
        $laconfig->field_type = 19;
        $laconfig->minlength = 0;
        $laconfig->maxlength = 160;
        $laconfig->required = false;
		$laconfig->save();

		// Display Configurations
		
		$laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Navbar Search Box";
		$laconfig->key = "topbar_search";
		$laconfig->value = false;
        $laconfig->field_type = 2;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
		$laconfig->save();
		
		$laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Show Navbar Messages";
		$laconfig->key = "show_messages";
		$laconfig->value = true;
        $laconfig->field_type = 2;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
		$laconfig->save();
		
		$laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Show Navbar Notifications";
		$laconfig->key = "show_notifications";
		$laconfig->value = true;
        $laconfig->field_type = 2;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
		$laconfig->save();
		
		$laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Show Navbar Tasks";
		$laconfig->key = "show_tasks";
		$laconfig->value = true;
        $laconfig->field_type = 2;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
		$laconfig->save();
		
		$laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Show Right SideBar";
		$laconfig->key = "show_rightsidebar";
		$laconfig->value = true;
        $laconfig->field_type = 2;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
		$laconfig->save();

        $laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Show Standby Chat Application";
		$laconfig->key = "show_standby_chat";
		$laconfig->value = false;
        $laconfig->field_type = 2;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
		$laconfig->save();
		
		$laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Skin / Theme Color";
        $laconfig->key = "skin";
		$laconfig->value = "skin-purple";
        $laconfig->field_type = 7;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
        $laconfig->popup_vals = '["skin-purple", "skin-blue", "skin-black", "skin-yellow", "skin-red", "skin-green", "skin-purple-light", "skin-blue-light", "skin-black-light", "skin-yellow-light", "skin-red-light", "skin-green-light"]';
		$laconfig->save();

		$laconfig = new LAConfigs;
        $laconfig->section = "Display";
		$laconfig->label = "Layout";
		$laconfig->key = "layout";
		$laconfig->value = "fixed";
        $laconfig->field_type = 7;
        $laconfig->minlength = NULL;
        $laconfig->maxlength = NULL;
        $laconfig->required = true;
        $laconfig->popup_vals = '["fixed", "sidebar-mini", "fixed-sidebar-mini", "layout-boxed", "layout-top-nav", "sidebar-collapse"]';
		$laconfig->save();

		// Admin Configurations

		$laconfig = new LAConfigs;
        $laconfig->section = "Admin";
		$laconfig->label = "default_email";
		$laconfig->key = "default_email";
		$laconfig->value = "test@example.com";
        $laconfig->field_type = 8;
        $laconfig->minlength = 5;
        $laconfig->maxlength = 100;
        $laconfig->required = true;
		$laconfig->save();

		// Module Generation Status
		
		$modules = Module::all();
		foreach ($modules as $module) {
			$module->is_gen=true;
			$module->save();	
		}

		// Sample Blog Posts
		// $uploadPostBanner = Upload::create([
        //     'name' => "Hello-World-by-Tim-Bogdanov.jpg",
        //     'path' =>  '/www/www/html/laplus/storage/uploads/Hello-World-by-Tim-Bogdanov.jpg',
        //     'extension' => 'jpg',
        //     'caption' => 'Photo by Tim Bogdanov on Unsplash',
        //     'user_id' => NULL,
        //     'hash' => 't5ipaasxz967pvle856719',
        //     'public' => 1
		// ]);

		// $album = array();
		// $album[] = json_encode($uploadPostBanner->id);


		// $post = new BlogPost;
        // $post->title = "Hello World";
		// $post->url = "hello-world";
		// $post->category_id = NULL;
		// $post->author_id = NULL;
		// $post->tags = '["Welcome", "LaraAdmin"]';
        // $post->post_date = "2017-11-20";
        // $post->excerpt = "Excerpt is a short extract from your post. This can be used as Post Description in Meta Tags.";
        // $post->album = json_encode($album);
		// $post->content = "Hello World from LaraAdmin Plus.";
		// $post->status = "Published";
		// $post->save();

		$uploadPostBanner = Upload::create([
            'name' => "Summer-Camp-Whatsapp-Promo-Vertical.png",
            'path' =>  '/www/www/html/laplus/storage/uploads/Summer-Camp-Whatsapp-Promo-Vertical.png',
            'extension' => 'png',
            'caption' => 'Photo by Tim Bogdanov on Unsplash',
            'user_id' => NULL,
            'hash' => 't5ipaasxz967pvle856788',
            'public' => 1
		]);

		$album = array();
		$album[] = json_encode($uploadPostBanner->id);

		$post = new BlogPost;
        $post->title = "Summer Camp 2018";
		$post->url = "summer-camp-2018";
		$post->category_id = NULL;
		$post->author_id = NULL;
		$post->tags = '["Summer Camp"]';
        $post->post_date = "2018-06-11";
        $post->excerpt = "";
        $post->album = json_encode($album);
		$post->content = '<p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">Summer holidays &nbsp;is time for your &nbsp;child/children&nbsp;&nbsp;to enjoy, have fun, explore new things, make new friends as well as it’s a great time to learn new things.</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">The greatest gifts that parents can give their children are independence and resiliency, and by selecting&nbsp;<em>OUR&nbsp;CAMPS</em>, you can give both. We can assure&nbsp;you all will see growth, maturity and confidence when he/she will return home. They will be more engaged, giving, and confident.</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">A program which is being loved by many children and parents over the past years.&nbsp;A program that helps your child to open up, learns new skills, prepares them for challenges and overcome them with less pressure &amp; more abilities and&nbsp;designed in lines with your children age group and learning capabilities; both physical and mental</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">To enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, ‘Processed Adventure’, Creativity, Practical education in an outdoor environment, and some cultural activities.</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">Going by our motto ‘learns ‘n leisure’ we have redefined the spellings of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN….N.</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><u>CAMPING AT EMPOWER STARTS WITH-</u></span></p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;">INTRODUCTIONS</span>: Knowing self and other members of the team</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;">TEAM FORMULATION AND FLAG MAKING</span>: Building brotherhood and a sense of belonging to a team and the importance of a flag</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;">MORNING PRAYERS.</span>&nbsp;The day starts with a universal prayer.</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;">HARIYALI:</span>&nbsp;An entertaining way to introduce the environment and physical training</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;">And the Campers gradually experience</span></p>';
		$post->status = "Published";
		$post->save();

		$uploadPostBanner = Upload::create([
            'name' => "EmpowerCamp-by-night.png",
            'path' =>  '/www/www/html/laplus/storage/uploads/EmpowerCamp-by-night.png',
            'extension' => 'png',
            'caption' => 'Photo by Tim Bogdanov on Unsplash',
            'user_id' => NULL,
            'hash' => 't5ipaasxz967pvle856889',
            'public' => 1
		]);

		$album = array();
		$album[] = json_encode($uploadPostBanner->id);

		$post = new BlogPost;
        $post->title = "Empower Is All Set To Welcome The New Year";
		$post->url = "welcome-the-new-year";
		$post->category_id = NULL;
		$post->author_id = NULL;
		$post->tags = '["New Year","Activity Camp"]';
        $post->post_date = "2018-01-01";
        $post->excerpt = "";
        $post->album = json_encode($album);
		$post->content = '<p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><em>Empower Activity Camps in Kolad is 50 acres of beautifully landscaped picturesque land in the midst of a hill and &nbsp;&nbsp;&nbsp;&nbsp; a lake.</em></span></p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><em>We are 2.15 Hrs &nbsp;from Vashi, Mumbai &amp; Chandni Chowk, Pune &nbsp;near Kolad on Mumbai – Goa Highway.</em></span></p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><em>&nbsp;</em></span><span style="font-weight: 700;"><em><u>Attractions</u></em></span></p><ul style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><li>Ideal place to usher in New Year with family (kids also welcome).</li><li>No dress code- Walk in casual attire.</li><li>Experience unlimited Funnnn…. under the open Skies in cool light of Stars.</li><li>Let your hair loose to the melodious music of DJ or enjoy sitting by the Bon Fire.</li><li>Veg, Non Veg, Jain food. (Jain food with prior notice)</li><li>Challenge yourself with a host of adventure activities or enjoy the fun activities organized by our trained team.</li></ul><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">A unique experience of a resort away from the humdrum of city life.</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><em><u>Accommodation/Packages</u></em></span></p><table border="1" width="786" style="background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px; height: 229px;"><tbody><tr><td width="284"><span style="font-weight: 700;">Accommodation</span></td><td width="210"><span style="font-weight: 700;">Rate<br></span>(per person &amp; all Inclusive)</td><td width="263"><span style="font-weight: 700;">Remarks</span></td></tr><tr><td width="284"><span style="font-weight: 700;">Cottage (AC)</span></td><td width="210" style="text-align: right;"><span style="font-weight: 700;">Rs. 6,375/-</span></td><td width="263">&nbsp;</td></tr><tr><td width="284"><span style="font-weight: 700;">Swiss Cottage Tents(Air Cooled)</span></td><td width="210" style="text-align: right;"><span style="font-weight: 700;">Rs. 5,775/-</span></td><td width="263">&nbsp;</td></tr><tr><td width="284"><span style="font-weight: 700;">Dormitory (AC)</span></td><td width="210" style="text-align: right;"><span style="font-weight: 700;">Rs. 4,775/-</span></td><td width="263">Modern Doms /15-18 Pax per Dom</td></tr></tbody></table><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;">&nbsp;</p><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><em><u>Cost Includes</u></em></span></p><ul style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><li>Check in 31<span style="position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;">st</span>&nbsp;Dec 2017 at 1200 hrs &amp; Check out 1<span style="position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;">st</span>&nbsp;Jan 2018 at 1200 hrs.</li><li>Stay &amp; all the meals from Day one lunch to Day two Breakfast (Day 2 lunch at extra cost)</li><li>New Year Eve celebration, DJ, Starters, Soft Drinks, Gala Dinner with starters (2 Veg.+2 non veg), Bonfire.</li><li>Taxes included &amp; Activities- Burma Bridge, Trekking, Target shooting, Dodge ball, River crossing and Treasure Hunt.</li></ul><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><em><u>Note</u></em></span></p><ul style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><li>Cottages on double occupancy &amp; can accommodate max four persons. Extra bed will be Rs 4700/-per person.</li><li>Swiss Cottage Tents on Quadruple occupancy. Double occupancy on extra cost.</li><li>Children up to 5 yrs are complimentary, 5-8yrs are on half tariff .</li><li>Adventure Activities other than stated above can be done on extra cost. Our Attractions are white water Raftng, Wall climbing &amp; Rappelling, Zip line, Raft Building &amp; Rowing &amp; many more. Some activities will require minimum no of &nbsp;&nbsp;&nbsp;</li><li>Other services like Telephone calls, Soft drinks (other than new year eve dinner), Juices etc will be on extra cost.</li><li>Additional night stay can be added on extra cost as per accommodation.</li></ul><p style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><span style="font-weight: 700;"><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Payment &amp; cancellation policy</u></em></span></p><ul style="color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;"><li>Booking on 100% advance Payment.</li><li>No refund will be applicable on cancellation after 25 Dec 2017.</li><li>Payment can be done by cheque or direct transfer in our bank account.</li><li><span style="font-weight: 700;">Bank Details:</span>&nbsp;Bank Name: ICICI Bank, Lokhandwala Branch, Bank Account No. – 026305000246, IFSC Code: ICIC0000263, Account Type: Current Account.</li></ul>';
		$post->status = "Published";
		$post->save();

		$uploadPostBanner = Upload::create([
            'name' => "outbound-training-camp-new-york-concluded.jpg",
            'path' =>  '/www/www/html/laplus/storage/uploads/outbound-training-camp-new-york-concluded.jpg',
            'extension' => 'jpg',
            'caption' => 'Photo by Tim Bogdanov on Unsplash',
            'user_id' => NULL,
            'hash' => 't5ipaasxz967pvle856799',
            'public' => 1
		]);

		$album = array();
		$album[] = json_encode($uploadPostBanner->id);

		$post = new BlogPost;
        $post->title = "New York Outbound Training Camp Concluded Successfully";
		$post->url = "outbound-training-camp";
		$post->category_id = NULL;
		$post->author_id = NULL;
		$post->tags = '["Outbound Training Camp"]';
        $post->post_date = "2018-02-11";
        $post->excerpt = "";
        $post->album = json_encode($album);
		$post->content = "Empower Activity Camp lead by Col Naval Kohli successfully conducted the Outbound Training Camp in New York for a leading MNC. Here are some of the stills from the camp:";
		$post->status = "Published";
		$post->save();

		$uploadPostBanner = Upload::create([
            'name' => "outbound-training-new-york-empower-camp.jpg",
            'path' =>  '/www/www/html/laplus/storage/uploads/outbound-training-new-york-empower-camp.jpg',
            'extension' => 'jpg',
            'caption' => 'Photo by Tim Bogdanov on Unsplash',
            'user_id' => NULL,
            'hash' => 't5ipaasxz967pvle856879',
            'public' => 1
		]);

		$album = array();
		$album[] = json_encode($uploadPostBanner->id);

		$post = new BlogPost;
        $post->title = "Now Empowering New York – Outbound Training In New York";
		$post->url = "new-york-outbound-training";
		$post->category_id = NULL;
		$post->author_id = NULL;
		$post->tags = '["Outbound Training"]';
        $post->post_date = "2018-05-15";
        $post->excerpt = "";
        $post->album = json_encode($album);
		$post->content = "Empower Camp is thrilled to announce our upcoming Outbound Training camp in New York from 15th-18th May, 2017.Stay tuned for more updates from the New York camp.Now Empower New York!";
		$post->status = "Published";
		$post->save();

		// =========== Super Admin Creation ===========

        $employeeSA = Employee::create([
            'name' => "Col. Naval Kohli",
            'designation' => "Super Admin",
            'gender' => 'Male',
            'phone_primary' => "7350558900",
            'phone_secondary' => "",
            'email_primary' => "digital@empowercamp.com",
            'email_secondary' => "",
            'profile_img' => NULL,
            'city' => "Pune",
            'address' => "",
            'about' => "",
            'date_birth' => NULL,
        ]);
        
        $userSA = User::create([
            'name' => "Col. Naval Kohli",
            'email' => "digital@empowercamp.com",
            'password' => bcrypt("EmpToSky123**"),
            'context_id' => $employeeSA->id
        ]);
        $role = Role::where('name', 'SUPER_ADMIN')->first();
        $userSA->attachRole($role);

        LALog::make("Employees.EMPLOYEE_CREATED", [
            'title' => "Employee ".$userSA->name." Created",
            'module_id' => 'Employees',
            'context_id' => $employeeSA->id,
            'content' => $employeeSA,
            'user_id' => 1,
            'notify_to' => "[]"
        ]);
        LALog::make("Users.USER_CREATED", [
            'title' => "User ".$userSA->name." Created",
            'module_id' => 'Users',
            'context_id' => $userSA->id,
            'content' => $userSA,
            'user_id' => $userSA->id,
            'notify_to' => "[]"
        ]);

		/* ================ Sessions ================ */

		$img = Upload::create([
            'name' => "Corporate-Training-for-Bank.jpg",
            'path' =>  '/www/www/html/laplus/storage/uploads/Corporate-Training-for-Bank.jpg',
            'extension' => 'jpg',
            'caption' => 'Corporate Training for Bank',
            'user_id' => NULL,
            'hash' => 't5ipaasxz967pvle856712',
            'public' => 1
		]);
		
		$session = new Session;
		$session->title = "Corporate Training for Bank";
		$session->pageurl = "Corporate-Training-for-Bank";
		$session->training = "Corporate Training";
		$session->excerpt = "Corporate Training with One of India's fastest growing bank";
		$session->banner = $img->id;
		$session->post_date = "2018-06-03";
        $session->content = "Corporate Training with One of India's fastest growing bank";
		$session->save();

		$img = Upload::create([
            'name' => "Outbound-Training-Institute.jpg",
            'path' =>  '/www/www/html/laplus/storage/uploads/Outbound-Training-Institute.jpg',
            'extension' => 'jpg',
            'caption' => 'Outbound Training for Institute',
            'user_id' => 1,
            'hash' => 't5ipaasxz967pvle856713',
            'public' => 1
		]);
		
		$session = new Session;
		$session->title = "Outbound Training for Business College";
		$session->pageurl = "Corporate-Training-for-Business-College";
		$session->training = "Corporate Training";
		$session->excerpt = "Outbound Training with Top Business College in Pune";
		$session->banner = NULL;
		$session->post_date = "2018-06-26";
        $session->content = "Outbound Training with Top Business College in Pune";
		$session->save();

		$img = Upload::create([
            'name' => "Outbound-Training-near-Pune.jpg",
            'path' =>  '/www/www/html/laplus/storage/uploads/Outbound-Training-near-Pune.jpg',
            'extension' => 'jpg',
            'caption' => 'Outbound Training near Pune',
            'user_id' => 1,
            'hash' => 't5ipaasxz967pvle856714',
            'public' => 1
		]);
		
		$session = new Session;
		$session->title = "Outbound Training near Pune";
		$session->pageurl = "Outbound-Training-near-Pune";
		$session->training = "Corporate Training";
		$session->excerpt = "Outbound Training near Pune";
		$session->banner = $img->id;
		$session->post_date = "2018-07-03";
        $session->content = "Outbound Training near Pune";
		$session->save();

		// - Upcoming

		$session = new Session;
		$session->title = "Team Outbound Training near Kolad";
		$session->pageurl = "Team-Outbound-Training-near-Kolad";
		$session->training = "Corporate Training";
		$session->excerpt = "Team Outbound Training near Kolad for Mahindra";
		$session->banner = $img->id;
		$session->post_date = "2018-11-03";
        $session->content = "Team Outbound Training near Kolads";
		$session->save();

		$session = new Session;
		$session->title = "Winter Adventure Camp near Pune";
		$session->pageurl = "Winter-Adventure-Camp-near-Pune";
		$session->training = "Corporate Training";
		$session->excerpt = "Winter Adventure Camp near Pune";
		$session->banner = $img->id;
		$session->post_date = "2018-10-23";
        $session->content = "Winter Adventure Camp near Pune for all age participents";
		$session->save();

		/* ================ Testimonials ================ */

		$testi = new Testimonial;
		$testi->title = "You totally deserve the aploases for making our trip such good";
		$testi->category = "Adventure";
		$testi->author = "Krunal Karia";
		$testi->designation = "Real Estate Advissor";
		$testi->content = "This is small feed back which I would like to give you on behalf of our family & specially from our bday boy.<br>As we visited with our family on 5th march to enjoy my nephew bday.";
		$testi->author_image = $img->id;
		$testi->content_image = NULL;
		$testi->youtube = "https://www.youtube.com/watch?v=GoEx-b8p9rc";
		$testi->post_date = "2018-06-12";
		$testi->save();

		$testi = new Testimonial;
		$testi->title = "Empower is Awesome !";
		$testi->category = "Adventure";
		$testi->author = "Kajal Dasani";
		$testi->designation = "Systems & finance,<br>Symbiosis Institute of Telecom Management";
		$testi->content = "I feel so privileged that i was a part of the Empower Camp and got an opportunity to gain so much from you! No flattering words sir but its a heart felt thank you and i feel lucky that was there under your guidance. It was very sweet of you that you remember my both names even now it brings a smile to my face.It was a lifetime experience and i will never forget those 4 fun-filled days where i became physically as well mentally stronger. Now i can take my decisions in a more firm way! No more mobile addiction that's the biggest take-away!";
		$testi->author_image = $img->id;
		$testi->content_image = NULL;
		$testi->youtube = "https://www.youtube.com/watch?v=GoEx-b8p9rc";
		$testi->post_date = "2018-06-12";
		$testi->save();

		$testi = new Testimonial;
		$testi->title = "An engaging session indeed";
		$testi->category = "Corporate";
		$testi->author = "ROHIT MALLICK";
		$testi->designation = "Regional Manager South,<br>Learning Services,<br>ITC GRAND CHOLA,Chennai";
		$testi->content = "Greetings from ITC Grand Chola ! Thank you for your email. It was indeed our privilege to organise a short programme delivered by you for our team members. Each one of them gave an extremely positive feedback for the programme and appeared to be highly motivated after attending your programme. I could make out that your session was indeed very engaging and every participant was actively involved in the activities that were being conducted.";
		$testi->author_image = NULL;
		$testi->content_image = NULL;
		$testi->youtube = "https://www.youtube.com/watch?v=GoEx-b8p9rc";
		$testi->post_date = "2018-07-03";
		$testi->save();

		$testi = new Testimonial;
		$testi->title = "Positive Transformation";
		$testi->category = "Corporate";
		$testi->author = "Sushavan Chatterjee";
		$testi->designation = "Area Sales Manager,<br>Nilkamal Ltd.";
		$testi->content = "first of all, I need to thank you for proffering us, such an alluring experience in between the lap of mother nature which we hardly get a chance to practice in our daily life.
		Secondly, I really boast of gaining all those outstanding experiences which we have acquired through your commendable trainings schedules.I honestly pledge to implement all the knowledge powered by you where ever relevant in both my “Professional”  as well as “Personal life” .......and I am sure it would bring out lots of positive transformation within me in the coming days.....Wish you and your entire team a great success.";
		$testi->author_image = NULL;
		$testi->content_image = NULL;
		$testi->youtube = "https://www.youtube.com/watch?v=GoEx-b8p9rc";
		$testi->post_date = "2018-07-03";
		$testi->save();

		$testi = new Testimonial;
		$testi->title = "They went as individuals but came back as 'one' Firestar";
		$testi->category = "Students";
		$testi->author = "Anup Pancha";
		$testi->designation = "COO,<br> Firestar Diamond";
		$testi->content = "Thanks for your Outbound Leadership and Team Building Training given to Sr. Firestar team.We had a good Time of Learning with Good Physical fitness activities and Games. I know your physical activities and games made them tired due to they are not habitual in their daily routine but I am Sure they All become Mentally Very Strong by learning lesson from all activities, what they can DO where they were feeling they can’t do before attempting those games and activates ...When they left from Mumbai to Kolad in bus, I saw them, they were from different dept and Location and when I saw them while returning, I found they all are from Firestar.Hoping to get continuous guidance from you to my team members time to time to become better than Before always at every stage in their Life.Thanks for your all your Hospitality with Fantastic Food .Please convey my sincere thanks to Anand , Pratap and all other team members whoever has directly or indirectly helped to make our training super successful.";
		$testi->author_image = NULL;
		$testi->content_image = NULL;
		$testi->youtube = "https://www.youtube.com/watch?v=GoEx-b8p9rc";
		$testi->post_date = "2018-07-03";
		$testi->save();

		// Call for Upload Directory Refresh
		Upload::update_local_upload_paths();

		/* ================ Call Other Seeders ================ */
		
	}
}

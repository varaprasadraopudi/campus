-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 22, 2019 at 03:06 PM
-- Server version: 5.6.41-84.1-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `empowziq_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Draft',
  `author_id` int(10) UNSIGNED DEFAULT NULL,
  `tags` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '[]',
  `post_date` date DEFAULT NULL,
  `excerpt` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `album` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `content` varchar(10000) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `title`, `url`, `category_id`, `status`, `author_id`, `tags`, `post_date`, `excerpt`, `album`, `content`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Summer Camp 2018', 'summer-camp-2018', NULL, 'Published', NULL, '[\"Summer Camp\"]', '2018-06-11', '', '[\"1\"]', '<p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">Summer holidays &nbsp;is time for your &nbsp;child/children&nbsp;&nbsp;to enjoy, have fun, explore new things, make new friends as well as it’s a great time to learn new things.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">The greatest gifts that parents can give their children are independence and resiliency, and by selecting&nbsp;<em>OUR&nbsp;CAMPS</em>, you can give both. We can assure&nbsp;you all will see growth, maturity and confidence when he/she will return home. They will be more engaged, giving, and confident.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">A program which is being loved by many children and parents over the past years.&nbsp;A program that helps your child to open up, learns new skills, prepares them for challenges and overcome them with less pressure &amp; more abilities and&nbsp;designed in lines with your children age group and learning capabilities; both physical and mental</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">To enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, ‘Processed Adventure’, Creativity, Practical education in an outdoor environment, and some cultural activities.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">Going by our motto ‘learns ‘n leisure’ we have redefined the spellings of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN….N.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><u>CAMPING AT EMPOWER STARTS WITH-</u></span></p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">INTRODUCTIONS</span>: Knowing self and other members of the team</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">TEAM FORMULATION AND FLAG MAKING</span>: Building brotherhood and a sense of belonging to a team and the importance of a flag</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">MORNING PRAYERS.</span>&nbsp;The day starts with a universal prayer.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">HARIYALI:</span>&nbsp;An entertaining way to introduce the environment and physical training</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">And the Campers gradually experience</span></p>', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(2, 'Empower Is All Set To Welcome The New Year', 'welcome-the-new-year', NULL, 'Published', NULL, '[\"New Year\",\"Activity Camp\"]', '2018-01-01', '', '[\"2\"]', '<p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>Empower Activity Camps in Kolad is 50 acres of beautifully landscaped picturesque land in the midst of a hill and &nbsp;&nbsp;&nbsp;&nbsp; a lake.</em></span></p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>We are 2.15 Hrs &nbsp;from Vashi, Mumbai &amp; Chandni Chowk, Pune &nbsp;near Kolad on Mumbai – Goa Highway.</em></span></p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>&nbsp;</em></span><span style=\"font-weight: 700;\"><em><u>Attractions</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Ideal place to usher in New Year with family (kids also welcome).</li><li>No dress code- Walk in casual attire.</li><li>Experience unlimited Funnnn…. under the open Skies in cool light of Stars.</li><li>Let your hair loose to the melodious music of DJ or enjoy sitting by the Bon Fire.</li><li>Veg, Non Veg, Jain food. (Jain food with prior notice)</li><li>Challenge yourself with a host of adventure activities or enjoy the fun activities organized by our trained team.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">A unique experience of a resort away from the humdrum of city life.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em><u>Accommodation/Packages</u></em></span></p><table border=\"1\" width=\"786\" style=\"background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px; height: 229px;\"><tbody><tr><td width=\"284\"><span style=\"font-weight: 700;\">Accommodation</span></td><td width=\"210\"><span style=\"font-weight: 700;\">Rate<br></span>(per person &amp; all Inclusive)</td><td width=\"263\"><span style=\"font-weight: 700;\">Remarks</span></td></tr><tr><td width=\"284\"><span style=\"font-weight: 700;\">Cottage (AC)</span></td><td width=\"210\" style=\"text-align: right;\"><span style=\"font-weight: 700;\">Rs. 6,375/-</span></td><td width=\"263\">&nbsp;</td></tr><tr><td width=\"284\"><span style=\"font-weight: 700;\">Swiss Cottage Tents(Air Cooled)</span></td><td width=\"210\" style=\"text-align: right;\"><span style=\"font-weight: 700;\">Rs. 5,775/-</span></td><td width=\"263\">&nbsp;</td></tr><tr><td width=\"284\"><span style=\"font-weight: 700;\">Dormitory (AC)</span></td><td width=\"210\" style=\"text-align: right;\"><span style=\"font-weight: 700;\">Rs. 4,775/-</span></td><td width=\"263\">Modern Doms /15-18 Pax per Dom</td></tr></tbody></table><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">&nbsp;</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em><u>Cost Includes</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Check in 31<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">st</span>&nbsp;Dec 2017 at 1200 hrs &amp; Check out 1<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">st</span>&nbsp;Jan 2018 at 1200 hrs.</li><li>Stay &amp; all the meals from Day one lunch to Day two Breakfast (Day 2 lunch at extra cost)</li><li>New Year Eve celebration, DJ, Starters, Soft Drinks, Gala Dinner with starters (2 Veg.+2 non veg), Bonfire.</li><li>Taxes included &amp; Activities- Burma Bridge, Trekking, Target shooting, Dodge ball, River crossing and Treasure Hunt.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em><u>Note</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Cottages on double occupancy &amp; can accommodate max four persons. Extra bed will be Rs 4700/-per person.</li><li>Swiss Cottage Tents on Quadruple occupancy. Double occupancy on extra cost.</li><li>Children up to 5 yrs are complimentary, 5-8yrs are on half tariff .</li><li>Adventure Activities other than stated above can be done on extra cost. Our Attractions are white water Raftng, Wall climbing &amp; Rappelling, Zip line, Raft Building &amp; Rowing &amp; many more. Some activities will require minimum no of &nbsp;&nbsp;&nbsp;</li><li>Other services like Telephone calls, Soft drinks (other than new year eve dinner), Juices etc will be on extra cost.</li><li>Additional night stay can be added on extra cost as per accommodation.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Payment &amp; cancellation policy</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Booking on 100% advance Payment.</li><li>No refund will be applicable on cancellation after 25 Dec 2017.</li><li>Payment can be done by cheque or direct transfer in our bank account.</li><li><span style=\"font-weight: 700;\">Bank Details:</span>&nbsp;Bank Name: ICICI Bank, Lokhandwala Branch, Bank Account No. – 026305000246, IFSC Code: ICIC0000263, Account Type: Current Account.</li></ul>', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(3, 'New York Outbound Training Camp Concluded Successfully', 'outbound-training-camp', NULL, 'Published', NULL, '[\"Outbound Training Camp\"]', '2018-02-11', '', '[\"3\"]', 'Empower Activity Camp lead by Col Naval Kohli successfully conducted the Outbound Training Camp in New York for a leading MNC. Here are some of the stills from the camp:', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(4, 'Now Empowering New York – Outbound Training In New York', 'new-york-outbound-training', NULL, 'Published', NULL, '[\"Outbound Training\"]', '2018-05-15', '', '[\"4\"]', 'Empower Camp is thrilled to announce our upcoming Outbound Training camp in New York from 15th-18th May, 2017.Stay tuned for more updates from the New York camp.Now Empower New York!', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `organization` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `training` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `name`, `email`, `phone_no`, `organization`, `training`, `message`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Ganesh Bhosale', 'gdb.sci123@gmail.com', '7350558900', 'Dwij IT Solutions', 'Special Packages for Summer Camp 2019', 'Test', NULL, '2019-05-03 06:04:13', '2019-05-03 06:04:13'),
(2, 'manish solanki', 'camanishsolanki@gmail.com', '9892623244', 'Self', 'Adventure and Leisure Capms', 'V 8 adults and 4 kids are looking to stay in your property for 2 nights, 10th May to 12 May 2019. Please do call us back!', NULL, '2019-05-03 11:02:21', '2019-05-03 11:02:21'),
(3, 'Rama Chaganti', 'ramachaganti@gmail.com', '7032658565', 'Self', 'Special Packages for Summer Camp 2019', '', NULL, '2019-05-05 02:00:48', '2019-05-05 02:00:48'),
(4, 'Rama Chaganti', 'ramachaganti@gmail.com', '7032658565', 'Self', 'Special Packages for Summer Camp 2019', 'Adventure beginners camp for parents and kids ', NULL, '2019-05-05 02:01:01', '2019-05-05 02:01:01'),
(5, 'Rama Chaganti', 'ramachaganti@gmail.com', '7032658565', 'Self', 'Special Packages for Summer Camp 2019', 'Adventure beginners camp for parents and kids ', NULL, '2019-05-05 02:01:03', '2019-05-05 02:01:03'),
(6, 'Rama Chaganti', 'ramachaganti@gmail.com', '7032658565', 'Self', 'Special Packages for Summer Camp 2019', 'Adventure beginners camp for parents and kids ', NULL, '2019-05-05 02:01:12', '2019-05-05 02:01:12'),
(7, 'Rama Chaganti', 'ramachaganti@gmail.com', '7032658565', 'Self', 'Special Packages for Summer Camp 2019', 'Adventure beginners camp for parents and kids ', NULL, '2019-05-05 02:01:13', '2019-05-05 02:01:13'),
(8, 'Rama Chaganti', 'ramachaganti@gmail.com', '7032658565', 'Self', 'Special Packages for Summer Camp 2019', 'Adventure beginners camp for parents and kids ', NULL, '2019-05-05 02:01:13', '2019-05-05 02:01:13'),
(9, 'Vasundhara Mehta', 'vasundhara.s.mehta@gmail.com', '8082096544', 'None', 'Adventure and Leisure Capms', 'I would like to book some activities for 3 adults for 10th/11th may. Could you send details along with cost?', NULL, '2019-05-05 17:04:59', '2019-05-05 17:04:59'),
(10, 'Amrutha Shetty', 'amyshetty@gmail.com', '9821899506', 'Accenture', 'EVENTS', 'Can you share the brochure. We are looking for team event options. can you share the details for 100+ team size', NULL, '2019-05-14 07:50:53', '2019-05-14 07:50:53'),
(11, 'Vishal', 'vishal.bajad@gmail.com', '08149086433', 'Confidential', 'FACILITIES', 'Hi Team, we are looking for team building activity with day night out. Please call me on 8149086433 ', NULL, '2019-05-22 10:59:47', '2019-05-22 10:59:47');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `organization` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Male',
  `phone_primary` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_secondary` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_primary` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `email_secondary` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_img` int(10) UNSIGNED DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `date_birth` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(2, 'Customer', '[]', '#000', NULL, '2019-05-03 05:54:37', '2019-05-03 05:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Male',
  `phone_primary` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_secondary` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_primary` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `email_secondary` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_img` int(10) UNSIGNED DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `date_birth` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `phone_primary`, `phone_secondary`, `email_primary`, `email_secondary`, `profile_img`, `city`, `address`, `about`, `date_birth`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Col. Naval Kohli', 'Super Admin', 'Male', '7350558900', '', 'digital@empowercamp.com', '', NULL, 'Pune', '', '', NULL, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `organization` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `training` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `name`, `email`, `phone_no`, `organization`, `training`, `message`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Ulia Haynes', 'ulia@get-online-visibility.com', '9013061554', 'Internet Marketing Company', '', 'We can put your website on 1st page of Google to drive relevant traffic to your site. Let us know if you would be interested in getting detailed proposal. We can also schedule a call & will be pleased to explain our services in detail. We look forward to hearing from you soon. Thanks!', NULL, '2019-05-04 07:59:03', '2019-05-04 07:59:03'),
(2, 'Rama Chaganti', 'ramachaganti@gmail.com', '7032658565', 'Self', '', 'Pls call me on options available for Family packages, 2 Kids (above 10-14) and parents ( 45 years)', NULL, '2019-05-05 03:10:04', '2019-05-05 03:10:04'),
(3, 'Manish Bablani', 'manishbablani@hotmail.com', '12344', 'Student', '', 'Hi,\r\nWould like to know any camp being organised in month of August.\r\nRegards Manish Bablani', NULL, '2019-05-17 08:56:19', '2019-05-17 08:56:19');

-- --------------------------------------------------------

--
-- Table structure for table `lalogs`
--

CREATE TABLE `lalogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `module_id` int(10) UNSIGNED DEFAULT NULL,
  `context_id` int(11) NOT NULL DEFAULT '0',
  `context2_id` int(11) NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lalogs`
--

INSERT INTO `lalogs` (`id`, `title`, `type`, `module_id`, `context_id`, `context2_id`, `content`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Employee Col. Naval Kohli Created', 'EMPLOYEE_CREATED', 4, 1, 0, '{\"name\":\"Col. Naval Kohli\",\"designation\":\"Super Admin\",\"gender\":\"Male\",\"phone_primary\":\"7350558900\",\"phone_secondary\":\"\",\"email_primary\":\"digital@empowercamp.com\",\"email_secondary\":\"\",\"profile_img\":null,\"city\":\"Pune\",\"address\":\"\",\"about\":\"\",\"date_birth\":null,\"updated_at\":\"2019-05-03 05:54:38\",\"created_at\":\"2019-05-03 05:54:38\",\"id\":1}', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(2, 'User Col. Naval Kohli Created', 'USER_CREATED', 1, 1, 0, '{\"name\":\"Col. Naval Kohli\",\"email\":\"digital@empowercamp.com\",\"context_id\":1,\"updated_at\":\"2019-05-03 05:54:38\",\"created_at\":\"2019-05-03 05:54:38\",\"id\":1}', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(3, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-04 05:21:52', '2019-05-04 05:21:52'),
(4, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-04 13:46:38', '2019-05-04 13:46:38'),
(5, 'User Col. Naval Kohli Logout', 'USER_LOGOUT', 1, 1, 0, '{}', 1, NULL, '2019-05-04 13:48:58', '2019-05-04 13:48:58'),
(6, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-09 08:36:15', '2019-05-09 08:36:15'),
(7, 'Upload Created', 'UPLOAD_CREATED', 2, 8, 0, '{\"name\":\"Summer-Camp-Event-2to4-Jun.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-09-092121-Summer-Camp-Event-2to4-Jun.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"9fl0eadwla9amhwjy7fu\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-09 09:21:21\",\"created_at\":\"2019-05-09 09:21:21\",\"id\":8}', 1, NULL, '2019-05-09 09:21:21', '2019-05-09 09:21:21'),
(8, 'Session Created', 'SESSION_CREATED', 12, 6, 0, '{\"id\":6,\"title\":\"Summer Camp 2019 (End of the Season)\",\"training\":\"Summer Camp for Kids\",\"excerpt\":\"Summer Camp for Kids\",\"banner\":\"8\",\"post_date\":null,\"content\":\"<h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camp Overview<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Empower Activity Camps is a passion driven training and adventure resort built on a 50 acre plot of picturesque expanse of table land adjoining a hill on one side and a lake on the other. Beautiful landscaped surroundings bring you in the lap of mother nature to energize , rejuvenate and relieve you from stresses of city life.The resort is owned and managed by retired senior army officers and hotel industry professionals, assisted by a team of dedicated trained staff.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Announcement!<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Summer holidays is time for your child\\/children to enjoy, have fun, explore new things, make new friends as well as it\\u2019s a great time to learn new things.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">The greatest gifts that parents can give their children are independence and resiliency, and by selecting OUR CAMPS, you can give both. We can assure you all will see growth, maturity and confidence when he\\/she will return home. They will be more engaged, giving, and confident.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">A program which is being loved by many children and parents over the past years. A program that helps your child to open up, learns new skills, prepares them for challenges and overcome them with less pressure &amp; more abilities and designed in lines with your children age group and learning capabilities; both physical and mental.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Accommodation<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Children will be staying in AC dormitories or air cooled Swiss cottage tents, all with attached wash room facilities. Boys and girls will be staying in segregated accommodation. Girls will be under close supervision of a lady trainer\\/ care taker. Camp has round the clock security.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Activities<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">To enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, \\u2018Processed Adventure\\u2019, Creativity, Practical education in an outdoor environment, and some cultural activities.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Going by our motto \\u2018learns \\u2018n leisure\\u2019 we have redefined the spellings of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN\\u2026.N.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camping at Empower starts with \\u2013<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">INTRODUCTIONS<\\/span>: Knowing self and other members of the team<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">TEAM FORMULATION AND FLAG MAKING<\\/span>: Building brotherhood and a sense of belonging to a team and the importance of a flag<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">MORNING PRAYERS.<\\/span>&nbsp;The day starts with a universal prayer.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">HARIYALI:<\\/span>&nbsp;An entertaining way to introduce the environment and physical training<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px;\\\">2 Nights \\u2013 3 Days at Rs. 6,499\\/- per child only (Age 8-18 yrs)<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">Includes:<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\\\"><li>Travel from Mumbai\\/ Navi Mumbai\\/ Pune to Empower Camps and back&nbsp;<\\/li><li>Veg Meals- from Day 1 lunch to Breakfast on Day 3&nbsp;<\\/li><li>Shared accommodation in AC Dormitories or Air-cooled tents (with attached western style washroom facilities)<\\/li><li>Boys and girls stay in separate accommodation with adequate care taken&nbsp;<\\/li><li>International standard safety gear, trained and qualified staff&nbsp;<\\/li><li>Govt. Taxes<\\/li><\\/ul><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">Cost Excludes:<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\\\"><li>Mineral Water \\/ Cold Drinks, etc.<\\/li><li>Food charges during Bus Journey.<\\/li><li>Personal expenditure of any kind.<\\/li><li>Medical expenses other than First aid.<\\/li><li>White Water Rafting Rs 1000\\/-per head<\\/li><li>Any other charges not specifically mentioned in \\u201cFees Include\\u201d.<\\/li><\\/ul><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camp Info<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Campsite distance from Mumbai \\u2013 130 Kms; Panvel \\u2013 70 kms; Harihareshwar \\u2013 80kms; Nagothane \\u2013 25 kms; Roha \\u2013 25 kms; MurudJanjira \\u2013 80 kms.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Safety<\\/h4><ul style=\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\\\"><li>Utmost care is taken to ensure the participants have a great time at the campus and no compromise is made on the safety and hygiene aspects.<\\/li><li>Our junior trainers are trained and certified.<\\/li><li>Equipment used for adventure is of European standards.<\\/li><\\/ul><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Propriety<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">The data furnished in this document and its attachments are not expected to be disclosed outside the account and may not be duplicated, used or disclosed in whole or part for any purpose other than to evaluate this information for conduct of this program.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Medical<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">We are fully equipped to provide First aid facilities at all activity locations along withtrained staff. Two local doctors (non MBBS) are available in a village 4 kms away.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">We have appropriate arrangements to transfer a patient to Gandhi Hospital,Koladwhich is 20 km away and takes approx. 30 mins to reach.<\\/p>\",\"deleted_at\":null,\"created_at\":\"2019-05-09 09:24:07\",\"updated_at\":\"2019-05-09 09:24:07\"}', 1, NULL, '2019-05-09 09:24:07', '2019-05-09 09:24:07'),
(9, 'Session Updated', 'SESSION_UPDATED', 12, 6, 0, '{\"post_date\":{\"old\":null,\"new\":\"2019-05-09\"},\"content\":{\"old\":\"<h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camp Overview<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Empower Activity Camps is a passion driven training and adventure resort built on a 50 acre plot of picturesque expanse of table land adjoining a hill on one side and a lake on the other. Beautiful landscaped surroundings bring you in the lap of mother nature to energize , rejuvenate and relieve you from stresses of city life.The resort is owned and managed by retired senior army officers and hotel industry professionals, assisted by a team of dedicated trained staff.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Announcement!<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Summer holidays is time for your child\\/children to enjoy, have fun, explore new things, make new friends as well as it\\u2019s a great time to learn new things.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">The greatest gifts that parents can give their children are independence and resiliency, and by selecting OUR CAMPS, you can give both. We can assure you all will see growth, maturity and confidence when he\\/she will return home. They will be more engaged, giving, and confident.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">A program which is being loved by many children and parents over the past years. A program that helps your child to open up, learns new skills, prepares them for challenges and overcome them with less pressure &amp; more abilities and designed in lines with your children age group and learning capabilities; both physical and mental.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Accommodation<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Children will be staying in AC dormitories or air cooled Swiss cottage tents, all with attached wash room facilities. Boys and girls will be staying in segregated accommodation. Girls will be under close supervision of a lady trainer\\/ care taker. Camp has round the clock security.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Activities<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">To enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, \\u2018Processed Adventure\\u2019, Creativity, Practical education in an outdoor environment, and some cultural activities.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Going by our motto \\u2018learns \\u2018n leisure\\u2019 we have redefined the spellings of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN\\u2026.N.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camping at Empower starts with \\u2013<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">INTRODUCTIONS<\\/span>: Knowing self and other members of the team<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">TEAM FORMULATION AND FLAG MAKING<\\/span>: Building brotherhood and a sense of belonging to a team and the importance of a flag<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">MORNING PRAYERS.<\\/span>&nbsp;The day starts with a universal prayer.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">HARIYALI:<\\/span>&nbsp;An entertaining way to introduce the environment and physical training<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px;\\\">2 Nights \\u2013 3 Days at Rs. 6,499\\/- per child only (Age 8-18 yrs)<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">Includes:<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\\\"><li>Travel from Mumbai\\/ Navi Mumbai\\/ Pune to Empower Camps and back&nbsp;<\\/li><li>Veg Meals- from Day 1 lunch to Breakfast on Day 3&nbsp;<\\/li><li>Shared accommodation in AC Dormitories or Air-cooled tents (with attached western style washroom facilities)<\\/li><li>Boys and girls stay in separate accommodation with adequate care taken&nbsp;<\\/li><li>International standard safety gear, trained and qualified staff&nbsp;<\\/li><li>Govt. Taxes<\\/li><\\/ul><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">Cost Excludes:<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\\\"><li>Mineral Water \\/ Cold Drinks, etc.<\\/li><li>Food charges during Bus Journey.<\\/li><li>Personal expenditure of any kind.<\\/li><li>Medical expenses other than First aid.<\\/li><li>White Water Rafting Rs 1000\\/-per head<\\/li><li>Any other charges not specifically mentioned in \\u201cFees Include\\u201d.<\\/li><\\/ul><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camp Info<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Campsite distance from Mumbai \\u2013 130 Kms; Panvel \\u2013 70 kms; Harihareshwar \\u2013 80kms; Nagothane \\u2013 25 kms; Roha \\u2013 25 kms; MurudJanjira \\u2013 80 kms.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Safety<\\/h4><ul style=\\\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\\\"><li>Utmost care is taken to ensure the participants have a great time at the campus and no compromise is made on the safety and hygiene aspects.<\\/li><li>Our junior trainers are trained and certified.<\\/li><li>Equipment used for adventure is of European standards.<\\/li><\\/ul><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Propriety<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">The data furnished in this document and its attachments are not expected to be disclosed outside the account and may not be duplicated, used or disclosed in whole or part for any purpose other than to evaluate this information for conduct of this program.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: &quot;Open Sans&quot;, sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Medical<\\/h4><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">We are fully equipped to provide First aid facilities at all activity locations along withtrained staff. Two local doctors (non MBBS) are available in a village 4 kms away.<\\/p><p style=\\\"margin-bottom: 14px; font-family: &quot;Open Sans&quot;, sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">We have appropriate arrangements to transfer a patient to Gandhi Hospital,Koladwhich is 20 km away and takes approx. 30 mins to reach.<\\/p>\",\"new\":\"<h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camp Overview<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Empower Activity Camps is a passion driven training and adventure resort built on a 50 acre plot of picturesque expanse of table land adjoining a hill on one side and a lake on the other. Beautiful landscaped surroundings bring you in the lap of mother nature to energize , rejuvenate and relieve you from stresses of city life.The resort is owned and managed by retired senior army officers and hotel industry professionals, assisted by a team of dedicated trained staff.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Announcement!<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Summer holidays is time for your child\\/children to enjoy, have fun, explore new things, make new friends as well as it\\u2019s a great time to learn new things.<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">The greatest gifts that parents can give their children are independence and resiliency, and by selecting OUR CAMPS, you can give both. We can assure you all will see growth, maturity and confidence when he\\/she will return home. They will be more engaged, giving, and confident.<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">A program which is being loved by many children and parents over the past years. A program that helps your child to open up, learns new skills, prepares them for challenges and overcome them with less pressure & more abilities and designed in lines with your children age group and learning capabilities; both physical and mental.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Accommodation<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Children will be staying in AC dormitories or air cooled Swiss cottage tents, all with attached wash room facilities. Boys and girls will be staying in segregated accommodation. Girls will be under close supervision of a lady trainer\\/ care taker. Camp has round the clock security.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Activities<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">To enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, \\u2018Processed Adventure\\u2019, Creativity, Practical education in an outdoor environment, and some cultural activities.<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Going by our motto \\u2018learns \\u2018n leisure\\u2019 we have redefined the spellings of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN\\u2026.N.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camping at Empower starts with \\u2013<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">INTRODUCTIONS<\\/span>: Knowing self and other members of the team<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">TEAM FORMULATION AND FLAG MAKING<\\/span>: Building brotherhood and a sense of belonging to a team and the importance of a flag<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">MORNING PRAYERS.<\\/span>\\u00a0The day starts with a universal prayer.<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">HARIYALI:<\\/span>\\u00a0An entertaining way to introduce the environment and physical training<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px;\\\">2 Nights \\u2013 3 Days at Rs. 6,499\\/- per child only (Age 8-18 yrs)<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">Includes:<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: \\\"Helvetica Neue\\\", Helvetica, Arial, sans-serif;\\\"><li>Travel from Mumbai\\/ Navi Mumbai\\/ Pune to Empower Camps and back\\u00a0<\\/li><li>Veg Meals- from Day 1 lunch to Breakfast on Day 3\\u00a0<\\/li><li>Shared accommodation in AC Dormitories or Air-cooled tents (with attached western style washroom facilities)<\\/li><li>Boys and girls stay in separate accommodation with adequate care taken\\u00a0<\\/li><li>International standard safety gear, trained and qualified staff\\u00a0<\\/li><li>Govt. Taxes<\\/li><\\/ul><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\"><span style=\\\"font-weight: 700;\\\">Cost Excludes:<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: \\\"Helvetica Neue\\\", Helvetica, Arial, sans-serif;\\\"><li>Mineral Water \\/ Cold Drinks, etc.<\\/li><li>Food charges during Bus Journey.<\\/li><li>Personal expenditure of any kind.<\\/li><li>Medical expenses other than First aid.<\\/li><li>White Water Rafting Rs 1000\\/-per head<\\/li><li>Any other charges not specifically mentioned in \\u201cFees Include\\u201d.<\\/li><\\/ul><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Camp Info<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">Campsite distance from Mumbai \\u2013 130 Kms; Panvel \\u2013 70 kms; Harihareshwar \\u2013 80kms; Nagothane \\u2013 25 kms; Roha \\u2013 25 kms; MurudJanjira \\u2013 80 kms.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Safety<\\/h4><ul style=\\\"color: rgb(51, 51, 51); font-family: \\\"Helvetica Neue\\\", Helvetica, Arial, sans-serif;\\\"><li>Utmost care is taken to ensure the participants have a great time at the campus and no compromise is made on the safety and hygiene aspects.<\\/li><li>Our junior trainers are trained and certified.<\\/li><li>Equipment used for adventure is of European standards.<\\/li><\\/ul><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Propriety<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">The data furnished in this document and its attachments are not expected to be disclosed outside the account and may not be duplicated, used or disclosed in whole or part for any purpose other than to evaluate this information for conduct of this program.<\\/p><h4 class=\\\"main-heads\\\" style=\\\"font-family: \\\"Open Sans\\\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\\\">Medical<\\/h4><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">We are fully equipped to provide First aid facilities at all activity locations along withtrained staff. Two local doctors (non MBBS) are available in a village 4 kms away.<\\/p><p style=\\\"margin-bottom: 14px; font-family: \\\"Open Sans\\\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\\\">We have appropriate arrangements to transfer a patient to Gandhi Hospital,Koladwhich is 20 km away and takes approx. 30 mins to reach.<\\/p>\"},\"updated_at\":{\"old\":\"2019-05-09 09:24:07\",\"new\":\"2019-05-09 09:28:13\"}}', 1, NULL, '2019-05-09 09:28:13', '2019-05-09 09:28:13'),
(10, 'Session Updated', 'SESSION_UPDATED', 12, 6, 0, '{\"post_date\":{\"old\":\"2019-05-09\",\"new\":\"2019-06-02\"},\"updated_at\":{\"old\":\"2019-05-09 09:28:13\",\"new\":\"2019-05-09 09:50:14\"}}', 1, NULL, '2019-05-09 09:50:14', '2019-05-09 09:50:14'),
(11, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-09 13:18:03', '2019-05-09 13:18:03'),
(12, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 8, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-09 09:21:21\",\"new\":\"2019-05-09 13:18:29\"}}', 1, NULL, '2019-05-09 13:18:29', '2019-05-09 13:18:29'),
(13, 'User Col. Naval Kohli Logout', 'USER_LOGOUT', 1, 1, 0, '{}', 1, NULL, '2019-05-09 13:18:47', '2019-05-09 13:18:47'),
(14, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-09 13:19:07', '2019-05-09 13:19:07'),
(15, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-10 05:47:51', '2019-05-10 05:47:51'),
(16, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-10 19:14:12', '2019-05-10 19:14:12'),
(17, 'Session Updated', 'SESSION_UPDATED', 12, 2, 0, '{\"banner\":{\"old\":null,\"new\":\"6\"},\"updated_at\":{\"old\":\"2019-05-03 05:54:38\",\"new\":\"2019-05-10 19:16:09\"}}', 1, NULL, '2019-05-10 19:16:09', '2019-05-10 19:16:09'),
(18, 'Session Updated', 'SESSION_UPDATED', 12, 3, 0, '{\"banner\":{\"old\":\"7\",\"new\":\"5\"},\"updated_at\":{\"old\":\"2019-05-03 05:54:38\",\"new\":\"2019-05-10 19:16:54\"}}', 1, NULL, '2019-05-10 19:16:54', '2019-05-10 19:16:54'),
(19, 'Upload Created', 'UPLOAD_CREATED', 2, 9, 0, '{\"name\":\"Empower-Album-Posting-Siemens1.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-10-192006-Empower-Album-Posting-Siemens1.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"eglqfogr24nzlahvq2kl\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-10 19:20:06\",\"created_at\":\"2019-05-10 19:20:06\",\"id\":9}', 1, NULL, '2019-05-10 19:20:06', '2019-05-10 19:20:06'),
(20, 'Session Updated', 'SESSION_UPDATED', 12, 4, 0, '{\"banner\":{\"old\":\"7\",\"new\":\"9\"},\"updated_at\":{\"old\":\"2019-05-03 05:54:38\",\"new\":\"2019-05-10 19:20:24\"}}', 1, NULL, '2019-05-10 19:20:24', '2019-05-10 19:20:24'),
(21, 'Session Updated', 'SESSION_UPDATED', 12, 6, 0, '{\"excerpt\":{\"old\":\"Summer Camp for Kids\",\"new\":\"Summer Camp for Kids 2019 (End of the Season)\"},\"updated_at\":{\"old\":\"2019-05-09 09:50:14\",\"new\":\"2019-05-10 19:22:11\"}}', 1, NULL, '2019-05-10 19:22:11', '2019-05-10 19:22:11'),
(22, 'Upload Created', 'UPLOAD_CREATED', 2, 10, 0, '{\"name\":\"Winter-Camp-Ad-2018.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-10-192653-Winter-Camp-Ad-2018.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"nr3awcpj7ihedcfpnns1\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-10 19:26:53\",\"created_at\":\"2019-05-10 19:26:53\",\"id\":10}', 1, NULL, '2019-05-10 19:26:53', '2019-05-10 19:26:53'),
(23, 'Upload Created', 'UPLOAD_CREATED', 2, 11, 0, '{\"name\":\"Winter-Camp-Ad-20181.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-10-192653-Winter-Camp-Ad-20181.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"mijqv8hvs6zxdiyl4pyx\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-10 19:26:53\",\"created_at\":\"2019-05-10 19:26:53\",\"id\":11}', 1, NULL, '2019-05-10 19:26:53', '2019-05-10 19:26:53'),
(24, 'Upload Created', 'UPLOAD_CREATED', 2, 12, 0, '{\"name\":\"Winter-Camp-Ad-20185.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-10-192655-Winter-Camp-Ad-20185.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"yrpato3ykmaqffodsewl\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-10 19:26:55\",\"created_at\":\"2019-05-10 19:26:55\",\"id\":12}', 1, NULL, '2019-05-10 19:26:55', '2019-05-10 19:26:55'),
(25, 'Upload Created', 'UPLOAD_CREATED', 2, 13, 0, '{\"name\":\"Winter-Camp-Ad-20182.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-10-192655-Winter-Camp-Ad-20182.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"okxi5tja7vvcluz3ptep\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-10 19:26:55\",\"created_at\":\"2019-05-10 19:26:55\",\"id\":13}', 1, NULL, '2019-05-10 19:26:55', '2019-05-10 19:26:55'),
(26, 'Session Updated', 'SESSION_UPDATED', 12, 5, 0, '{\"banner\":{\"old\":\"7\",\"new\":\"11\"},\"content\":{\"old\":\"Winter Adventure Camp near Pune for all age participents\",\"new\":\"Winter Adventure Camp near Pune for all age participants\"},\"updated_at\":{\"old\":\"2019-05-03 05:54:38\",\"new\":\"2019-05-10 19:27:28\"}}', 1, NULL, '2019-05-10 19:27:28', '2019-05-10 19:27:28'),
(27, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-11 03:00:04', '2019-05-11 03:00:04'),
(28, 'Upload Created', 'UPLOAD_CREATED', 2, 14, 0, '{\"name\":\"summer pack.jpg\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-11-044301-summer pack.jpg\",\"extension\":\"jpg\",\"caption\":\"\",\"hash\":\"aundjsoam8isgg28pzly\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-11 04:43:01\",\"created_at\":\"2019-05-11 04:43:01\",\"id\":14}', 1, NULL, '2019-05-11 04:43:01', '2019-05-11 04:43:01'),
(29, 'Session Created', 'SESSION_CREATED', 12, 7, 0, '{\"id\":7,\"title\":\"Special Packages For Summer Vacation 2019\",\"training\":\"Summer Special Packages\",\"excerpt\":\"Special Packages For Summer Vacation 2019\",\"banner\":\"14\",\"post_date\":\"2019-05-31\",\"content\":\"<p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\">NOTES:&nbsp;<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>White water rafting is an outsourced activity and is dependent on release of water from a dam. If it gets cancelled or anyone doesn\\u2019t want to do, Empower will not be responsible and no refund will take place. Transport for rafting from camp to rafting site &amp; back will be extra.<\\/li><li>All inclusions are part of package &amp; no refund will be given if any facility is not availed\\/activity is not done.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>CHILDREN POLICY:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Up to 5 years complimentary (No extra Bed)<\\/li><li>6-8 years half rate.<\\/li><li>8 years and above full rate. Kids who are under 14 cannot do rafting but can-do Zip line instead of that.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>OPTIONAL:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Starters, Mineral water &amp; Juices available at extra cost<\\/li><li>DJ Rs 12000\\/- and Music Rs. 5,000\\/-per night<\\/li><li>Driver\\u2019s\\/cleaners meals Rs. 1,000\\/-per person per day (buffet) and Rs 700\\/-per head (staff food).<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>NOTE<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Above package is applicable for the Corporate outings\\/offsites&nbsp;too.<\\/li><li>Any other Adventure Activity\\/ Training prog\\/Team Building prog will be charged extra and GST on it&nbsp;will be applicable.<\\/li><li>Full Payment in advance<\\/li><\\/ul>\",\"deleted_at\":null,\"created_at\":\"2019-05-11 04:43:30\",\"updated_at\":\"2019-05-11 04:43:30\"}', 1, NULL, '2019-05-11 04:43:30', '2019-05-11 04:43:30'),
(30, 'Session Updated', 'SESSION_UPDATED', 12, 7, 0, '{\"content\":{\"old\":\"<p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\">NOTES:&nbsp;<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>White water rafting is an outsourced activity and is dependent on release of water from a dam. If it gets cancelled or anyone doesn\\u2019t want to do, Empower will not be responsible and no refund will take place. Transport for rafting from camp to rafting site &amp; back will be extra.<\\/li><li>All inclusions are part of package &amp; no refund will be given if any facility is not availed\\/activity is not done.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>CHILDREN POLICY:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Up to 5 years complimentary (No extra Bed)<\\/li><li>6-8 years half rate.<\\/li><li>8 years and above full rate. Kids who are under 14 cannot do rafting but can-do Zip line instead of that.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>OPTIONAL:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Starters, Mineral water &amp; Juices available at extra cost<\\/li><li>DJ Rs 12000\\/- and Music Rs. 5,000\\/-per night<\\/li><li>Driver\\u2019s\\/cleaners meals Rs. 1,000\\/-per person per day (buffet) and Rs 700\\/-per head (staff food).<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>NOTE<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Above package is applicable for the Corporate outings\\/offsites&nbsp;too.<\\/li><li>Any other Adventure Activity\\/ Training prog\\/Team Building prog will be charged extra and GST on it&nbsp;will be applicable.<\\/li><li>Full Payment in advance<\\/li><\\/ul>\",\"new\":\"<p style=\\\"text-align: center; color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><img src=\\\"http:\\/\\/empowercamp.com\\/old\\/wp-content\\/uploads\\/2019\\/04\\/Summer-Camp-Special-Packages-2019.gif\\\" style=\\\"width: 546.943px; height: 559.9px; float: none;\\\"><span style=\\\"font-weight: 700;\\\"><br><\\/span><\\/p><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\">NOTES:&nbsp;<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>White water rafting is an outsourced activity and is dependent on release of water from a dam. If it gets cancelled or anyone doesn\\u2019t want to do, Empower will not be responsible and no refund will take place. Transport for rafting from camp to rafting site &amp; back will be extra.<\\/li><li>All inclusions are part of package &amp; no refund will be given if any facility is not availed\\/activity is not done.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>CHILDREN POLICY:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Up to 5 years complimentary (No extra Bed)<\\/li><li>6-8 years half rate.<\\/li><li>8 years and above full rate. Kids who are under 14 cannot do rafting but can-do Zip line instead of that.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>OPTIONAL:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Starters, Mineral water &amp; Juices available at extra cost<\\/li><li>DJ Rs 12000\\/- and Music Rs. 5,000\\/-per night<\\/li><li>Driver\\u2019s\\/cleaners meals Rs. 1,000\\/-per person per day (buffet) and Rs 700\\/-per head (staff food).<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>NOTE<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Above package is applicable for the Corporate outings\\/offsites&nbsp;too.<\\/li><li>Any other Adventure Activity\\/ Training prog\\/Team Building prog will be charged extra and GST on it&nbsp;will be applicable.<\\/li><li>Full Payment in advance<\\/li><\\/ul>\"},\"updated_at\":{\"old\":\"2019-05-11 04:43:30\",\"new\":\"2019-05-11 04:45:10\"}}', 1, NULL, '2019-05-11 04:45:10', '2019-05-11 04:45:10'),
(31, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-13 09:40:56', '2019-05-13 09:40:56');
INSERT INTO `lalogs` (`id`, `title`, `type`, `module_id`, `context_id`, `context2_id`, `content`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(32, 'Session Updated', 'SESSION_UPDATED', 12, 7, 0, '{\"content\":{\"old\":\"<p style=\\\"text-align: center; color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><img src=\\\"http:\\/\\/empowercamp.com\\/old\\/wp-content\\/uploads\\/2019\\/04\\/Summer-Camp-Special-Packages-2019.gif\\\" style=\\\"width: 546.943px; height: 559.9px; float: none;\\\"><span style=\\\"font-weight: 700;\\\"><br><\\/span><\\/p><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\">NOTES:&nbsp;<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>White water rafting is an outsourced activity and is dependent on release of water from a dam. If it gets cancelled or anyone doesn\\u2019t want to do, Empower will not be responsible and no refund will take place. Transport for rafting from camp to rafting site &amp; back will be extra.<\\/li><li>All inclusions are part of package &amp; no refund will be given if any facility is not availed\\/activity is not done.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>CHILDREN POLICY:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Up to 5 years complimentary (No extra Bed)<\\/li><li>6-8 years half rate.<\\/li><li>8 years and above full rate. Kids who are under 14 cannot do rafting but can-do Zip line instead of that.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>OPTIONAL:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Starters, Mineral water &amp; Juices available at extra cost<\\/li><li>DJ Rs 12000\\/- and Music Rs. 5,000\\/-per night<\\/li><li>Driver\\u2019s\\/cleaners meals Rs. 1,000\\/-per person per day (buffet) and Rs 700\\/-per head (staff food).<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>NOTE<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Above package is applicable for the Corporate outings\\/offsites&nbsp;too.<\\/li><li>Any other Adventure Activity\\/ Training prog\\/Team Building prog will be charged extra and GST on it&nbsp;will be applicable.<\\/li><li>Full Payment in advance<\\/li><\\/ul>\",\"new\":\"<p style=\\\"text-align: center; color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><img src=\\\"http:\\/\\/empowercamp.com\\/old\\/wp-content\\/uploads\\/2019\\/04\\/Summer-Camp-Special-Packages-2019.gif\\\" style=\\\"width: 546.943px; height: 559.9px; float: none;\\\"><span style=\\\"font-weight: 700;\\\"><br><\\/span><\\/p><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\">NOTES:\\u00a0<\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>White water rafting is an outsourced activity and is dependent on release of water from a dam. If it gets cancelled or anyone doesn\\u2019t want to do, Empower will not be responsible and no refund will take place. Transport for rafting from camp to rafting site & back will be extra.<\\/li><li>All inclusions are part of package & no refund will be given if any facility is not availed\\/activity is not done.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>CHILDREN POLICY:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Up to 5 years complimentary (No extra Bed)<\\/li><li>6-8 years half rate.<\\/li><li>8 years and above full rate. Kids who are under 14 cannot do rafting but can-do Zip line instead of that.<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>OPTIONAL:<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Starters, Mineral water & Juices available at extra cost<\\/li><li>DJ Rs 12000\\/- and Music Rs. 5,000\\/-per night<\\/li><li>Driver\\u2019s\\/cleaners meals Rs. 1,000\\/-per person per day (buffet) and Rs 700\\/-per head (staff food).<\\/li><\\/ul><p style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><span style=\\\"font-weight: 700;\\\"><u>NOTE<\\/u><\\/span><\\/p><ul style=\\\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\\\"><li>Above package is applicable for the Corporate outings\\/offsites\\u00a0too.<\\/li><li>Any other Adventure Activity\\/ Training prog\\/Team Building prog will be charged extra and GST on it\\u00a0will be applicable.<\\/li><li>Full Payment in advance<\\/li><\\/ul>\"},\"updated_at\":{\"old\":\"2019-05-11 04:45:10\",\"new\":\"2019-05-13 09:42:08\"}}', 1, NULL, '2019-05-13 09:42:08', '2019-05-13 09:42:08'),
(33, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-16 06:40:41', '2019-05-16 06:40:41'),
(34, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-20 09:06:29', '2019-05-20 09:06:29'),
(35, 'Upload Created', 'UPLOAD_CREATED', 2, 15, 0, '{\"name\":\"Monsoon-Special-Packages-2019-Event.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-20-090721-Monsoon-Special-Packages-2019-Event.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"i7lue6aho00v6vjdy14j\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-20 09:07:21\",\"created_at\":\"2019-05-20 09:07:21\",\"id\":15}', 1, NULL, '2019-05-20 09:07:21', '2019-05-20 09:07:21'),
(36, 'Session Created', 'SESSION_CREATED', 12, 8, 0, '{\"id\":8,\"title\":\"Monsoon Special Packages \",\"training\":\"Monsoon Packages\",\"excerpt\":\"Book Your Special Monsoon Packages start with Rs. 2,800\\/- and Get a Complimentary White Water Rafting Free\",\"banner\":\"15\",\"post_date\":null,\"content\":\"<p><b>SPECIAL PACKAGE FOR MONSOON- Jun-Jul 2019<\\/b><\\/p><p class=\\\"MsoNormal\\\" style=\\\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><b><u><span style=\\\"font-family:\\r\\n\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">1) STAY &amp; MEALS<\\/span><\\/u><\\/b><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Swiss cottage Tents (Air Cooled) -\\r\\n2800\\/-per head (Quadruple Occupancy)<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Cottage \\/ Dlx Swiss Cottage Tents\\r\\n(AC) - 3400\\/-per head (Quadruple Occupancy)<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Additional Rs 800 per head for Triple\\r\\nOccupancy and Rs 1000\\/-per head for double Occupancy. Inclusion will be same.<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Additional Night will cost etc.<\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><b><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">2)<u> &nbsp;PACKAGE&nbsp; INCLUDES:<\\/u><\\/span><\\/b><\\/p><ul style=\\\"margin-top:0in\\\" type=\\\"disc\\\">\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">1600 hrs Check In to 1600 hrs Check Out<o:p><\\/o:p><\\/span><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">One cycle of meals (High tea<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;color:windowtext\\\"=\\\"\\\">, D<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">inner, Morning Tea, Breakfast and Lunch)<o:p><\\/o:p><\\/span><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Activities- Trekking, Treasure\\r\\n     Hunt, Foot cricket, Burma Bridge, Wall climbing and Rappelling, Target\\r\\n     Shooting, Dodge ball and Camp Fire and <b>White water rafting on Day two. <\\/b><\\/span><b><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif\\\"=\\\"\\\"><o:p><\\/o:p><\\/span><\\/b><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;margin-bottom:8.0pt;line-height:107%;\\r\\n     mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";mso-fareast-language:en-in\\\"=\\\"\\\">Inclusive\\r\\n     of Taxes<o:p><\\/o:p><\\/span><\\/li>\\r\\n<\\/ul><p class=\\\"MsoNormal\\\"><b><u><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">PACKAGE NOTES :&nbsp; <o:p><\\/o:p><\\/span><\\/u><\\/b><\\/p><p class=\\\"MsoListParagraphCxSpFirst\\\" style=\\\"margin-bottom:8.0pt;mso-add-space:\\r\\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\\r\\nSymbol\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\\r\\n<\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">White water rafting is an outsourced activity and is dependent upon\\r\\nrelease of water from a dam. If it gets cancelled, reasons beyond our control, or\\r\\nanyone doesn\\u2019t want to do, Empower will not be held responsible and no refund\\r\\nwill be made. Transport for rafting from camp to rafting site &amp; back will\\r\\nbe extra.<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif\\\"=\\\"\\\"><o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoListParagraphCxSpLast\\\" style=\\\"margin-bottom:8.0pt;mso-add-space:\\r\\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\\r\\nSymbol\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\\r\\n<\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">All inclusions are part of package &amp; no refund will be given if any\\r\\nfacility is not availed \\/activity not done.<\\/span><\\/p>\",\"deleted_at\":null,\"created_at\":\"2019-05-20 09:26:25\",\"updated_at\":\"2019-05-20 09:26:25\"}', 1, NULL, '2019-05-20 09:26:25', '2019-05-20 09:26:25'),
(37, 'Session Updated', 'SESSION_UPDATED', 12, 8, 0, '{\"post_date\":{\"old\":null,\"new\":\"2019-06-01\"},\"content\":{\"old\":\"<p><b>SPECIAL PACKAGE FOR MONSOON- Jun-Jul 2019<\\/b><\\/p><p class=\\\"MsoNormal\\\" style=\\\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><b><u><span style=\\\"font-family:\\r\\n\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">1) STAY &amp; MEALS<\\/span><\\/u><\\/b><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Swiss cottage Tents (Air Cooled) -\\r\\n2800\\/-per head (Quadruple Occupancy)<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Cottage \\/ Dlx Swiss Cottage Tents\\r\\n(AC) - 3400\\/-per head (Quadruple Occupancy)<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Additional Rs 800 per head for Triple\\r\\nOccupancy and Rs 1000\\/-per head for double Occupancy. Inclusion will be same.<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Additional Night will cost etc.<\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><b><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">2)<u> &nbsp;PACKAGE&nbsp; INCLUDES:<\\/u><\\/span><\\/b><\\/p><ul style=\\\"margin-top:0in\\\" type=\\\"disc\\\">\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">1600 hrs Check In to 1600 hrs Check Out<o:p><\\/o:p><\\/span><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">One cycle of meals (High tea<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;color:windowtext\\\"=\\\"\\\">, D<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">inner, Morning Tea, Breakfast and Lunch)<o:p><\\/o:p><\\/span><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Activities- Trekking, Treasure\\r\\n     Hunt, Foot cricket, Burma Bridge, Wall climbing and Rappelling, Target\\r\\n     Shooting, Dodge ball and Camp Fire and <b>White water rafting on Day two. <\\/b><\\/span><b><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif\\\"=\\\"\\\"><o:p><\\/o:p><\\/span><\\/b><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;margin-bottom:8.0pt;line-height:107%;\\r\\n     mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";mso-fareast-language:en-in\\\"=\\\"\\\">Inclusive\\r\\n     of Taxes<o:p><\\/o:p><\\/span><\\/li>\\r\\n<\\/ul><p class=\\\"MsoNormal\\\"><b><u><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">PACKAGE NOTES :&nbsp; <o:p><\\/o:p><\\/span><\\/u><\\/b><\\/p><p class=\\\"MsoListParagraphCxSpFirst\\\" style=\\\"margin-bottom:8.0pt;mso-add-space:\\r\\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\\r\\nSymbol\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\\r\\n<\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">White water rafting is an outsourced activity and is dependent upon\\r\\nrelease of water from a dam. If it gets cancelled, reasons beyond our control, or\\r\\nanyone doesn\\u2019t want to do, Empower will not be held responsible and no refund\\r\\nwill be made. Transport for rafting from camp to rafting site &amp; back will\\r\\nbe extra.<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif\\\"=\\\"\\\"><o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoListParagraphCxSpLast\\\" style=\\\"margin-bottom:8.0pt;mso-add-space:\\r\\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\\r\\nSymbol\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\\r\\n<\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">All inclusions are part of package &amp; no refund will be given if any\\r\\nfacility is not availed \\/activity not done.<\\/span><\\/p>\",\"new\":\"<p><b>SPECIAL PACKAGE FOR MONSOON- Jun-Jul 2019<\\/b><\\/p><p class=\\\"MsoNormal\\\" style=\\\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><b><u><span style=\\\"font-family:\\r\\n\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">1) STAY & MEALS<\\/span><\\/u><\\/b><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Swiss cottage Tents (Air Cooled) -\\r\\n2800\\/-per head (Quadruple Occupancy)<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Cottage \\/ Dlx Swiss Cottage Tents\\r\\n(AC) - 3400\\/-per head (Quadruple Occupancy)<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Additional Rs 800 per head for Triple\\r\\nOccupancy and Rs 1000\\/-per head for double Occupancy. Inclusion will be same.<o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;\\r\\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\\r\\nmso-fareast-language:EN-IN\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0 <\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Additional Night will cost etc.<\\/span><\\/p><p class=\\\"MsoNormal\\\" style=\\\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\\\"><b><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">2)<u> \\u00a0PACKAGE\\u00a0 INCLUDES:<\\/u><\\/span><\\/b><\\/p><ul style=\\\"margin-top:0in\\\" type=\\\"disc\\\">\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">1600 hrs Check In to 1600 hrs Check Out<o:p><\\/o:p><\\/span><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">One cycle of meals (High tea<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;color:windowtext\\\"=\\\"\\\">, D<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" mso-fareast-language:en-in\\\"=\\\"\\\">inner, Morning Tea, Breakfast and Lunch)<o:p><\\/o:p><\\/span><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"line-height:107%;mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">Activities- Trekking, Treasure\\r\\n     Hunt, Foot cricket, Burma Bridge, Wall climbing and Rappelling, Target\\r\\n     Shooting, Dodge ball and Camp Fire and <b>White water rafting on Day two. <\\/b><\\/span><b><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif\\\"=\\\"\\\"><o:p><\\/o:p><\\/span><\\/b><\\/li>\\r\\n <li class=\\\"MsoNormal\\\" style=\\\"color:#444444;margin-bottom:8.0pt;line-height:107%;\\r\\n     mso-list:l5 level1 lfo5\\\"><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";mso-fareast-language:en-in\\\"=\\\"\\\">Inclusive\\r\\n     of Taxes<o:p><\\/o:p><\\/span><\\/li>\\r\\n<\\/ul><p class=\\\"MsoNormal\\\"><b><u><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";=\\\"\\\" color:#444444;mso-fareast-language:en-in\\\"=\\\"\\\">PACKAGE NOTES :\\u00a0 <o:p><\\/o:p><\\/span><\\/u><\\/b><\\/p><p class=\\\"MsoListParagraphCxSpFirst\\\" style=\\\"margin-bottom:8.0pt;mso-add-space:\\r\\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\\r\\nSymbol\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\r\\n<\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">White water rafting is an outsourced activity and is dependent upon\\r\\nrelease of water from a dam. If it gets cancelled, reasons beyond our control, or\\r\\nanyone doesn\\u2019t want to do, Empower will not be held responsible and no refund\\r\\nwill be made. Transport for rafting from camp to rafting site & back will\\r\\nbe extra.<\\/span><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif\\\"=\\\"\\\"><o:p><\\/o:p><\\/span><\\/p><p class=\\\"MsoListParagraphCxSpLast\\\" style=\\\"margin-bottom:8.0pt;mso-add-space:\\r\\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\\\"><!--[if !supportLists]--><span style=\\\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\\r\\nSymbol\\\">\\u00b7<span style=\\\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \\\" times=\\\"\\\" new=\\\"\\\" roman\\\";\\\"=\\\"\\\">\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\u00a0\\r\\n<\\/span><\\/span><!--[endif]--><span style=\\\"font-family:\\\" times=\\\"\\\" new=\\\"\\\" roman\\\",serif;=\\\"\\\" mso-fareast-font-family:\\\"times=\\\"\\\" roman\\\";color:#444444;mso-fareast-language:=\\\"\\\" en-in\\\"=\\\"\\\">All inclusions are part of package & no refund will be given if any\\r\\nfacility is not availed \\/activity not done.<\\/span><\\/p>\"},\"updated_at\":{\"old\":\"2019-05-20 09:26:25\",\"new\":\"2019-05-20 09:27:47\"}}', 1, NULL, '2019-05-20 09:27:47', '2019-05-20 09:27:47'),
(38, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-21 11:17:21', '2019-05-21 11:17:21'),
(39, 'Upload Created', 'UPLOAD_CREATED', 2, 16, 0, '{\"name\":\"Monsoon-Special-Packages-2019-Event.png\",\"path\":\"\\/home\\/empowziq\\/public_html\\/storage\\/uploads\\/2019-05-21-111850-Monsoon-Special-Packages-2019-Event.png\",\"extension\":\"png\",\"caption\":\"\",\"hash\":\"xcori4714hk26bsz0c0t\",\"public\":false,\"user_id\":1,\"updated_at\":\"2019-05-21 11:18:50\",\"created_at\":\"2019-05-21 11:18:50\",\"id\":16}', 1, NULL, '2019-05-21 11:18:50', '2019-05-21 11:18:50'),
(40, 'Session Updated', 'SESSION_UPDATED', 12, 8, 0, '{\"banner\":{\"old\":\"15\",\"new\":\"16\"},\"updated_at\":{\"old\":\"2019-05-20 09:27:47\",\"new\":\"2019-05-21 11:19:04\"}}', 1, NULL, '2019-05-21 11:19:04', '2019-05-21 11:19:04'),
(41, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-21 16:19:11', '2019-05-21 16:19:11'),
(42, 'Session Updated', 'SESSION_UPDATED', 12, 8, 0, '{\"excerpt\":{\"old\":\"Book Your Special Monsoon Packages start with Rs. 2,800\\/- and Get a Complimentary White Water Rafting Free\",\"new\":\"Book Your Special Monsoon Packages start with Rs. 2,800\\/- | Experience the Magic of Real White Water Rafting\"},\"updated_at\":{\"old\":\"2019-05-21 11:19:04\",\"new\":\"2019-05-21 16:20:26\"}}', 1, NULL, '2019-05-21 16:20:26', '2019-05-21 16:20:26'),
(43, 'User Col. Naval Kohli Login', 'USER_LOGIN', 1, 1, 0, '{}', 1, NULL, '2019-05-22 11:31:44', '2019-05-22 11:31:44'),
(44, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 16, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-21 11:18:50\",\"new\":\"2019-05-22 11:31:58\"}}', 1, NULL, '2019-05-22 11:31:58', '2019-05-22 11:31:58'),
(45, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 15, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-20 09:07:21\",\"new\":\"2019-05-22 11:32:01\"}}', 1, NULL, '2019-05-22 11:32:01', '2019-05-22 11:32:01'),
(46, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 14, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-11 04:43:01\",\"new\":\"2019-05-22 11:32:03\"}}', 1, NULL, '2019-05-22 11:32:03', '2019-05-22 11:32:03'),
(47, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 12, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-10 19:26:55\",\"new\":\"2019-05-22 11:32:06\"}}', 1, NULL, '2019-05-22 11:32:06', '2019-05-22 11:32:06'),
(48, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 13, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-10 19:26:55\",\"new\":\"2019-05-22 11:32:08\"}}', 1, NULL, '2019-05-22 11:32:08', '2019-05-22 11:32:08'),
(49, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 10, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-10 19:26:53\",\"new\":\"2019-05-22 11:32:10\"}}', 1, NULL, '2019-05-22 11:32:10', '2019-05-22 11:32:10'),
(50, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 11, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-10 19:26:53\",\"new\":\"2019-05-22 11:32:13\"}}', 1, NULL, '2019-05-22 11:32:13', '2019-05-22 11:32:13'),
(51, 'Upload is_public Updated', 'UPLOAD_UPDATED', 2, 9, 0, '{\"public\":{\"old\":\"0\",\"new\":true},\"updated_at\":{\"old\":\"2019-05-10 19:20:06\",\"new\":\"2019-05-22 11:32:15\"}}', 1, NULL, '2019-05-22 11:32:15', '2019-05-22 11:32:15');

-- --------------------------------------------------------

--
-- Table structure for table `lalog_user`
--

CREATE TABLE `lalog_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `lalog_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `notif_mail` tinyint(1) NOT NULL DEFAULT '0',
  `notif_mail_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `notif_socket` tinyint(1) NOT NULL DEFAULT '0',
  `notif_socket_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `la_configs`
--

CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_type` int(10) UNSIGNED DEFAULT NULL,
  `minlength` int(10) UNSIGNED DEFAULT NULL,
  `maxlength` int(10) UNSIGNED DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` varchar(5000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_configs`
--

INSERT INTO `la_configs` (`id`, `label`, `key`, `section`, `value`, `field_type`, `minlength`, `maxlength`, `required`, `popup_vals`, `created_at`, `updated_at`) VALUES
(1, 'Sitename', 'sitename', 'General', 'Empower Camp', 16, 0, 20, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(2, 'Sitename First Word', 'sitename_part1', 'General', 'Empower', 16, 0, 20, 0, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(3, 'Sitename Second Word', 'sitename_part2', 'General', 'Camp', 16, 0, 20, 0, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(4, 'Sitename Short (2/3 Characters)', 'sitename_short', 'General', 'EC', 16, 1, 3, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(5, 'Site Description (160 Characters)', 'site_description', 'General', 'Veteran ARMY Officer\'s venture Empower Activity Camps specializes in ‘Corporate training’ in outdoor environment, 50 acres adventure camp near Mumbai and Pune.', 19, 0, 160, 0, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(6, 'Navbar Search Box', 'topbar_search', 'Display', '0', 2, NULL, NULL, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(7, 'Show Navbar Messages', 'show_messages', 'Display', '1', 2, NULL, NULL, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(8, 'Show Navbar Notifications', 'show_notifications', 'Display', '1', 2, NULL, NULL, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(9, 'Show Navbar Tasks', 'show_tasks', 'Display', '1', 2, NULL, NULL, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(10, 'Show Right SideBar', 'show_rightsidebar', 'Display', '1', 2, NULL, NULL, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(11, 'Show Standby Chat Application', 'show_standby_chat', 'Display', '0', 2, NULL, NULL, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(12, 'Skin / Theme Color', 'skin', 'Display', 'skin-purple', 7, NULL, NULL, 1, '[\"skin-purple\", \"skin-blue\", \"skin-black\", \"skin-yellow\", \"skin-red\", \"skin-green\", \"skin-purple-light\", \"skin-blue-light\", \"skin-black-light\", \"skin-yellow-light\", \"skin-red-light\", \"skin-green-light\"]', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(13, 'Layout', 'layout', 'Display', 'fixed', 7, NULL, NULL, 1, '[\"fixed\", \"sidebar-mini\", \"fixed-sidebar-mini\", \"layout-boxed\", \"layout-top-nav\", \"sidebar-collapse\"]', '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(14, 'default_email', 'default_email', 'Admin', 'test@example.com', 8, 5, 100, 1, '', '2019-05-03 05:54:38', '2019-05-03 05:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `la_menus`
--

CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', '/', 'fa-home', 'custom', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(2, 'Team', '#', 'fa-group', 'custom', 0, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(3, 'Blog', '#', 'fa-file-text-o', 'custom', 0, 2, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(4, 'Users', 'users', 'fa-group', 'module', 2, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(5, 'Uploads', 'uploads', 'fa-files-o', 'module', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(6, 'Departments', 'departments', 'fa-tags', 'module', 2, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(7, 'Employees', 'employees', 'fa-group', 'module', 2, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(8, 'Customers', 'customers', 'fa-user', 'module', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(9, 'Roles', 'roles', 'fa-user-plus', 'module', 2, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(10, 'Permissions', 'permissions', 'fa-magic', 'module', 2, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(11, 'Messages', 'messages', 'fa-comments-o', 'module', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(12, 'Blog_categories', 'blog_categories', 'fa-list-ul', 'module', 3, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(13, 'Blog_posts', 'blog_posts', 'fa-file-text-o', 'module', 3, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(14, 'Sessions', 'sessions', 'fa-file-text-o', 'module', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(15, 'Testimonials', 'testimonials', 'fa-file-text-o', 'module', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(16, 'Bookings', 'bookings', 'fa-file-text-o', 'module', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(17, 'Enquiries', 'enquiries', 'fa-file-text-o', 'module', 0, 0, '2019-05-03 05:54:37', '2019-05-03 05:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '',
  `from` int(10) UNSIGNED DEFAULT NULL,
  `to` int(10) UNSIGNED DEFAULT NULL,
  `is_received` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(85, '2014_05_26_050000_create_modules_table', 1),
(86, '2014_05_26_055000_create_module_field_types_table', 1),
(87, '2014_05_26_060000_create_module_fields_table', 1),
(88, '2014_10_12_000000_create_users_table', 1),
(89, '2014_10_12_100000_create_password_resets_table', 1),
(90, '2014_12_01_000000_create_uploads_table', 1),
(91, '2016_05_26_064006_create_departments_table', 1),
(92, '2016_05_26_064007_create_employees_table', 1),
(93, '2016_05_26_064110_create_customers_table', 1),
(94, '2016_05_26_064446_create_roles_table', 1),
(95, '2016_07_05_115343_create_role_user_table', 1),
(96, '2016_07_07_134058_create_backups_table', 1),
(97, '2016_07_07_134058_create_menus_table', 1),
(98, '2016_07_07_294546_create_role_menu_table', 1),
(99, '2016_09_10_163337_create_permissions_table', 1),
(100, '2016_09_10_163520_create_permission_role_table', 1),
(101, '2016_09_22_105958_role_module_fields_table', 1),
(102, '2016_09_22_110008_role_module_table', 1),
(103, '2016_10_06_115413_create_la_configs_table', 1),
(104, '2016_12_09_121655_create_messages_table', 1),
(105, '2017_11_20_063000_create_blog_categories_table', 1),
(106, '2017_11_20_063206_create_blog_posts_table', 1),
(107, '2017_11_20_063216_create_sessions_table', 1),
(108, '2017_11_20_063217_create_testimonials_table', 1),
(109, '2017_11_20_063218_create_bookings_table', 1),
(110, '2017_11_20_063219_create_enquiries_table', 1),
(111, '2050_01_01_010000_create_la_logs_table', 1),
(112, '2050_01_01_010100_create_la_log_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2019-05-03 05:54:28', '2019-05-03 05:54:38'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2019-05-03 05:54:29', '2019-05-03 05:54:38'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2019-05-03 05:54:29', '2019-05-03 05:54:38'),
(4, 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', 1, '2019-05-03 05:54:29', '2019-05-03 05:54:38'),
(5, 'Customers', 'Customers', 'customers', 'name', 'Customer', 'CustomersController', 'fa-user', 1, '2019-05-03 05:54:30', '2019-05-03 05:54:38'),
(6, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2019-05-03 05:54:30', '2019-05-03 05:54:38'),
(7, 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', 1, '2019-05-03 05:54:31', '2019-05-03 05:54:38'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2019-05-03 05:54:31', '2019-05-03 05:54:38'),
(9, 'Messages', 'Messages', 'messages', 'content', 'Message', 'MessagesController', 'fa-comments-o', 1, '2019-05-03 05:54:33', '2019-05-03 05:54:38'),
(10, 'Blog_categories', 'Blog Categories', 'blog_categories', 'name', 'BlogCategory', 'BlogCategoriesController', 'fa-list-ul', 1, '2019-05-03 05:54:33', '2019-05-03 05:54:38'),
(11, 'Blog_posts', 'Blog Posts', 'blog_posts', 'title', 'BlogPost', 'BlogPostsController', 'fa-file-text-o', 1, '2019-05-03 05:54:34', '2019-05-03 05:54:38'),
(12, 'Sessions', 'Sessions', 'sessions', 'title', 'Session', 'SessionsController', 'fa-file-text-o', 1, '2019-05-03 05:54:34', '2019-05-03 05:54:38'),
(13, 'Testimonials', 'Testimonials', 'testimonials', 'title', 'Testimonial', 'TestimonialsController', 'fa-file-text-o', 1, '2019-05-03 05:54:34', '2019-05-03 05:54:38'),
(14, 'Bookings', 'Bookings', 'bookings', 'name', 'Booking', 'BookingsController', 'fa-file-text-o', 1, '2019-05-03 05:54:35', '2019-05-03 05:54:38'),
(15, 'Enquiries', 'Enquiries', 'enquiries', 'name', 'Enquiry', 'EnquiriesController', 'fa-file-text-o', 1, '2019-05-03 05:54:35', '2019-05-03 05:54:38'),
(16, 'LALogs', 'LALogs', 'lalogs', 'title', 'LALog', 'LALogsController', 'fa-eye', 1, '2019-05-03 05:54:35', '2019-05-03 05:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT '0',
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(11) NOT NULL DEFAULT '0',
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `listing_col` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `listing_col`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-05-03 05:54:28', '2019-05-03 05:54:28'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:28', '2019-05-03 05:54:28'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, 1, '2019-05-03 05:54:28', '2019-05-03 05:54:28'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, 0, '2019-05-03 05:54:28', '2019-05-03 05:54:28'),
(5, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(6, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(7, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(8, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(9, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(10, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(11, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(12, 'name', 'Name', 3, 16, 1, '', 1, 250, 1, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(13, 'tags', 'Tags', 3, 20, 0, '[]', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(14, 'color', 'Color', 3, 19, 0, '', 0, 50, 1, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(15, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(16, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(17, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(18, 'phone_primary', 'Primary Phone', 4, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(19, 'phone_secondary', 'Secondary Phone', 4, 14, 0, '', 5, 15, 0, '', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(20, 'email_primary', 'Primary Email', 4, 8, 1, '', 5, 250, 1, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(21, 'email_secondary', 'Secondary Email', 4, 8, 0, '', 5, 100, 0, '', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(22, 'profile_img', 'Profile Image', 4, 12, 0, '', 0, 0, 0, '', 0, 1, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(23, 'city', 'City', 4, 19, 0, '', 2, 50, 0, '', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(24, 'address', 'Address', 4, 1, 0, '', 4, 1000, 0, '', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, 'NULL', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:29', '2019-05-03 05:54:29'),
(27, 'name', 'Name', 5, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(28, 'designation', 'Designation', 5, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(29, 'organization', 'Organization', 5, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(30, 'gender', 'Gender', 5, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(31, 'phone_primary', 'Primary Phone', 5, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(32, 'phone_secondary', 'Secondary Phone', 5, 14, 0, '', 5, 15, 0, '', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(33, 'email_primary', 'Primary Email', 5, 8, 1, '', 5, 250, 1, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(34, 'email_secondary', 'Secondary Email', 5, 8, 0, '', 5, 100, 0, '', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(35, 'profile_img', 'Profile Image', 5, 12, 0, '', 0, 0, 0, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(36, 'city', 'City', 5, 19, 0, '', 2, 50, 0, '', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(37, 'address', 'Address', 5, 1, 0, '', 4, 1000, 0, '', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(38, 'about', 'About', 5, 19, 0, '', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(39, 'date_birth', 'Date of Birth', 5, 4, 0, 'NULL', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(40, 'name', 'Name', 6, 16, 1, '', 1, 250, 1, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(41, 'display_name', 'Display Name', 6, 19, 0, '', 0, 250, 1, '', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(42, 'description', 'Description', 6, 21, 0, '', 0, 1000, 0, '', 0, 0, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(43, 'parent', 'Parent Role', 6, 7, 0, '', 0, 0, 0, '@roles', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(44, 'context_type', 'Context Type', 6, 7, 0, 'Employee', 0, 0, 0, '[\"Employee\",\"Customer\"]', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(45, 'dept', 'Department', 6, 7, 0, '1', 0, 0, 0, '@departments', 0, 1, '2019-05-03 05:54:30', '2019-05-03 05:54:30'),
(46, 'name', 'Name', 7, 16, 1, '', 0, 250, 1, '', 0, 1, '2019-05-03 05:54:31', '2019-05-03 05:54:31'),
(47, 'file_name', 'File Name', 7, 19, 1, '', 0, 250, 1, '', 0, 1, '2019-05-03 05:54:31', '2019-05-03 05:54:31'),
(48, 'backup_size', 'File Size', 7, 19, 0, '0', 0, 10, 1, '', 0, 0, '2019-05-03 05:54:31', '2019-05-03 05:54:31'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, 1, '2019-05-03 05:54:31', '2019-05-03 05:54:31'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, 1, '2019-05-03 05:54:31', '2019-05-03 05:54:31'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, 0, '2019-05-03 05:54:31', '2019-05-03 05:54:31'),
(52, 'subject', 'Subject', 9, 16, 0, '', 0, 50, 0, '', 0, 1, '2019-05-03 05:54:33', '2019-05-03 05:54:33'),
(53, 'content', 'Content', 9, 19, 0, '', 1, 1000, 1, '', 0, 1, '2019-05-03 05:54:33', '2019-05-03 05:54:33'),
(54, 'from', 'From', 9, 7, 0, '', 0, 0, 1, '@users', 0, 1, '2019-05-03 05:54:33', '2019-05-03 05:54:33'),
(55, 'to', 'To', 9, 7, 0, '', 0, 0, 1, '@users', 0, 1, '2019-05-03 05:54:33', '2019-05-03 05:54:33'),
(56, 'is_received', 'Is Received', 9, 2, 0, 'false', 0, 0, 1, '', 0, 1, '2019-05-03 05:54:33', '2019-05-03 05:54:33'),
(57, 'name', 'Name', 10, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:33', '2019-05-03 05:54:33'),
(58, 'url', 'URL', 10, 19, 1, '', 5, 250, 1, '', 0, 0, '2019-05-03 05:54:33', '2019-05-03 05:54:33'),
(59, 'description', 'Description', 10, 19, 0, '', 0, 1000, 0, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(60, 'title', 'Title', 11, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(61, 'url', 'URL', 11, 19, 1, '', 5, 250, 1, '', 0, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(62, 'category_id', 'Category', 11, 7, 0, '', 0, 0, 0, '@blog_categories', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(63, 'status', 'Status', 11, 7, 0, 'Draft', 0, 0, 1, '[\"Draft\",\"Published\"]', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(64, 'author_id', 'Author', 11, 7, 0, '', 0, 0, 1, '@employees', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(65, 'tags', 'Tags', 11, 20, 0, '[]', 0, 0, 0, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(66, 'post_date', 'Date', 11, 4, 0, '', 0, 0, 1, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(67, 'excerpt', 'Excerpt', 11, 19, 0, '', 0, 200, 0, '', 0, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(68, 'album', 'Album', 11, 24, 0, '', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(69, 'content', 'Content', 11, 11, 0, '', 0, 10000, 0, '', 0, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(70, 'title', 'Title', 12, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(71, 'training', 'Training', 12, 19, 0, '', 0, 200, 1, '', 2, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(72, 'excerpt', 'Excerpt', 12, 19, 0, '', 0, 200, 1, '', 3, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(73, 'banner', 'Banner', 12, 12, 0, '', 0, 0, 1, '', 4, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(74, 'post_date', 'Date', 12, 4, 0, '', 0, 0, 1, '', 5, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(75, 'content', 'Content', 12, 11, 0, '', 0, 10000, 0, '', 6, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(76, 'title', 'Title', 13, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(77, 'category', 'Category', 13, 7, 0, 'Corporate', 0, 0, 1, '[\"Corporate\",\"Students\",\"Adventure\"]', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(78, 'author', 'Author', 13, 19, 1, '', 5, 250, 0, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(79, 'designation', 'Designation', 13, 19, 1, '', 5, 250, 0, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(80, 'content', 'Content', 13, 21, 0, '', 0, 0, 1, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(81, 'author_image', 'Author Image', 13, 12, 0, '', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(82, 'content_image', 'Content Image', 13, 12, 0, '', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(83, 'youtube', 'Youtube Link', 13, 23, 0, '', 40, 150, 0, '', 0, 0, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(84, 'post_date', 'Date', 13, 4, 0, '', 0, 0, 1, '', 0, 1, '2019-05-03 05:54:34', '2019-05-03 05:54:34'),
(85, 'name', 'Name', 14, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(86, 'email', 'Email', 14, 8, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(87, 'phone_no', 'Phone No.', 14, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(88, 'organization', 'Organization', 14, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(89, 'training', 'Training', 14, 19, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(90, 'message', 'Message', 14, 21, 0, '', 0, 0, 0, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(91, 'name', 'Name', 15, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(92, 'email', 'Email', 15, 8, 1, '', 5, 250, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(93, 'phone_no', 'Phone No.', 15, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(94, 'organization', 'Organization', 15, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(95, 'training', 'Training', 15, 19, 0, '', 5, 100, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(96, 'message', 'Message', 15, 21, 0, '', 0, 0, 0, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(97, 'title', 'Title', 16, 16, 0, '', 0, 256, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(98, 'type', 'Type', 16, 19, 0, '', 1, 256, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(99, 'module_id', 'Module', 16, 7, 0, '', 0, 0, 0, '@modules', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(100, 'context_id', 'Context', 16, 13, 0, '', 0, 11, 1, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(101, 'context2_id', 'Context2', 16, 13, 0, '0', 0, 11, 0, '', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(102, 'content', 'Content', 16, 21, 0, '{}', 0, 0, 0, '', 0, 0, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(103, 'user_id', 'User', 16, 7, 0, '', 0, 0, 1, '@users', 0, 1, '2019-05-03 05:54:35', '2019-05-03 05:54:35'),
(104, 'pageurl', 'URL', 12, 19, 0, '', 5, 100, 1, '', 1, 1, '2019-05-22 13:07:22', '2019-05-22 13:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `module_field_types`
--

CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(2, 'Checkbox', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(3, 'Currency', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(4, 'Date', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(5, 'Datetime', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(6, 'Decimal', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(7, 'Dropdown', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(8, 'Email', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(9, 'File', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(10, 'Float', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(11, 'HTML', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(12, 'Image', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(13, 'Integer', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(14, 'Mobile', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(15, 'Multiselect', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(16, 'Name', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(17, 'Password', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(18, 'Radio', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(19, 'String', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(20, 'Taginput', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(21, 'Textarea', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(22, 'TextField', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(23, 'URL', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(24, 'Files', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(25, 'Location', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(26, 'Color', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(27, 'Time', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(28, 'JSON', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(29, 'List', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(30, 'Duration', '2019-05-03 05:54:27', '2019-05-03 05:54:27'),
(31, 'Checklist', '2019-05-03 05:54:27', '2019-05-03 05:54:27');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `context_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Employee',
  `dept` int(10) UNSIGNED DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `context_type`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', NULL, 'Employee', 1, NULL, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(2, 'ADMIN', 'Admin', 'Admin Level Access Role', 1, 'Employee', 1, NULL, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(3, 'CUSTOMER', 'Customer', 'Customer Level Access Role', 2, 'Customer', 2, NULL, '2019-05-03 05:54:37', '2019-05-03 05:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(2, 1, 2, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(3, 1, 3, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(4, 1, 4, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(5, 1, 5, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(6, 1, 6, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(7, 1, 7, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(8, 1, 8, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(9, 1, 9, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(10, 1, 10, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(11, 1, 11, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(12, 1, 12, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(13, 1, 13, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(14, 1, 14, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(15, 1, 15, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(16, 1, 16, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(17, 1, 17, '2019-05-03 05:54:37', '2019-05-03 05:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(2, 1, 2, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(3, 1, 3, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(4, 1, 4, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(5, 1, 5, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(6, 1, 6, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(7, 1, 7, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(8, 1, 8, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(9, 1, 9, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(10, 1, 10, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(11, 1, 11, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(12, 1, 12, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(13, 1, 13, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(14, 1, 14, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(15, 1, 15, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(16, 1, 16, 1, 1, 1, 1, '2019-05-03 05:54:37', '2019-05-03 05:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `role_module_fields`
--

CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(2, 1, 2, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(3, 1, 3, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(4, 1, 4, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(5, 1, 5, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(6, 1, 6, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(7, 1, 7, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(8, 1, 8, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(9, 1, 9, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(10, 1, 10, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(11, 1, 11, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(12, 1, 12, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(13, 1, 13, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(14, 1, 14, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(15, 1, 15, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(16, 1, 16, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(17, 1, 17, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(18, 1, 18, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(19, 1, 19, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(20, 1, 20, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(21, 1, 21, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(22, 1, 22, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(23, 1, 23, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(24, 1, 24, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(25, 1, 25, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(26, 1, 26, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(27, 1, 27, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(28, 1, 28, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(29, 1, 29, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(30, 1, 30, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(31, 1, 31, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(32, 1, 32, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(33, 1, 33, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(34, 1, 34, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(35, 1, 35, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(36, 1, 36, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(37, 1, 37, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(38, 1, 38, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(39, 1, 39, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(40, 1, 40, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(41, 1, 41, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(42, 1, 42, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(43, 1, 43, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(44, 1, 44, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(45, 1, 45, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(46, 1, 46, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(47, 1, 47, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(48, 1, 48, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(49, 1, 49, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(50, 1, 50, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(51, 1, 51, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(52, 1, 52, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(53, 1, 53, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(54, 1, 54, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(55, 1, 55, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(56, 1, 56, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(57, 1, 57, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(58, 1, 58, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(59, 1, 59, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(60, 1, 60, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(61, 1, 61, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(62, 1, 62, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(63, 1, 63, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(64, 1, 64, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(65, 1, 65, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(66, 1, 66, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(67, 1, 67, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(68, 1, 68, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(69, 1, 69, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(70, 1, 70, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(71, 1, 71, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(72, 1, 72, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(73, 1, 73, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(74, 1, 74, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(75, 1, 75, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(76, 1, 76, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(77, 1, 77, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(78, 1, 78, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(79, 1, 79, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(80, 1, 80, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(81, 1, 81, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(82, 1, 82, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(83, 1, 83, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(84, 1, 84, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(85, 1, 85, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(86, 1, 86, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(87, 1, 87, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(88, 1, 88, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(89, 1, 89, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(90, 1, 90, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(91, 1, 91, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(92, 1, 92, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(93, 1, 93, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(94, 1, 94, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(95, 1, 95, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(96, 1, 96, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(97, 1, 97, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(98, 1, 98, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(99, 1, 99, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(100, 1, 100, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(101, 1, 101, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(102, 1, 102, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(103, 1, 103, 'write', '2019-05-03 05:54:37', '2019-05-03 05:54:37'),
(104, 1, 104, 'write', '2019-05-22 13:07:22', '2019-05-22 13:07:22');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pageurl` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `training` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `excerpt` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `banner` int(10) UNSIGNED DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `content` varchar(10000) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `title`, `pageurl`, `training`, `excerpt`, `banner`, `post_date`, `content`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Corporate Training for Bank', '', 'Corporate Training', 'Corporate Training with One of India\'s fastest growing bank', 5, '2018-06-03', 'Corporate Training with One of India\'s fastest growing bank', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(2, 'Outbound Training for Business College', '', 'Corporate Training', 'Outbound Training with Top Business College in Pune', 6, '2018-06-26', 'Outbound Training with Top Business College in Pune', NULL, '2019-05-03 05:54:38', '2019-05-10 19:16:09'),
(3, 'Outbound Training near Pune', '', 'Corporate Training', 'Outbound Training near Pune', 5, '2018-07-03', 'Outbound Training near Pune', NULL, '2019-05-03 05:54:38', '2019-05-10 19:16:54'),
(4, 'Team Outbound Training near Kolad', '', 'Corporate Training', 'Team Outbound Training near Kolad for Mahindra', 9, '2018-11-03', 'Team Outbound Training near Kolads', NULL, '2019-05-03 05:54:38', '2019-05-10 19:20:24'),
(5, 'Winter Adventure Camp near Pune', '', 'Corporate Training', 'Winter Adventure Camp near Pune', 11, '2018-10-23', 'Winter Adventure Camp near Pune for all age participants', NULL, '2019-05-03 05:54:38', '2019-05-10 19:27:28'),
(6, 'Summer Camp 2019 (End of the Season)', '', 'Summer Camp for Kids', 'Summer Camp for Kids 2019 (End of the Season)', 8, '2019-06-02', '<h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Camp Overview</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">Empower Activity Camps is a passion driven training and adventure resort built on a 50 acre plot of picturesque expanse of table land adjoining a hill on one side and a lake on the other. Beautiful landscaped surroundings bring you in the lap of mother nature to energize , rejuvenate and relieve you from stresses of city life.The resort is owned and managed by retired senior army officers and hotel industry professionals, assisted by a team of dedicated trained staff.</p><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Announcement!</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">Summer holidays is time for your child/children to enjoy, have fun, explore new things, make new friends as well as it’s a great time to learn new things.</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">The greatest gifts that parents can give their children are independence and resiliency, and by selecting OUR CAMPS, you can give both. We can assure you all will see growth, maturity and confidence when he/she will return home. They will be more engaged, giving, and confident.</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">A program which is being loved by many children and parents over the past years. A program that helps your child to open up, learns new skills, prepares them for challenges and overcome them with less pressure & more abilities and designed in lines with your children age group and learning capabilities; both physical and mental.</p><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Accommodation</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">Children will be staying in AC dormitories or air cooled Swiss cottage tents, all with attached wash room facilities. Boys and girls will be staying in segregated accommodation. Girls will be under close supervision of a lady trainer/ care taker. Camp has round the clock security.</p><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Activities</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">To enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, ‘Processed Adventure’, Creativity, Practical education in an outdoor environment, and some cultural activities.</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">Going by our motto ‘learns ‘n leisure’ we have redefined the spellings of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN….N.</p><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Camping at Empower starts with –</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\"><span style=\"font-weight: 700;\">INTRODUCTIONS</span>: Knowing self and other members of the team</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\"><span style=\"font-weight: 700;\">TEAM FORMULATION AND FLAG MAKING</span>: Building brotherhood and a sense of belonging to a team and the importance of a flag</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\"><span style=\"font-weight: 700;\">MORNING PRAYERS.</span> The day starts with a universal prayer.</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\"><span style=\"font-weight: 700;\">HARIYALI:</span> An entertaining way to introduce the environment and physical training</p><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px;\">2 Nights – 3 Days at Rs. 6,499/- per child only (Age 8-18 yrs)</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\"><span style=\"font-weight: 700;\">Includes:</span></p><ul style=\"color: rgb(51, 51, 51); font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\"><li>Travel from Mumbai/ Navi Mumbai/ Pune to Empower Camps and back </li><li>Veg Meals- from Day 1 lunch to Breakfast on Day 3 </li><li>Shared accommodation in AC Dormitories or Air-cooled tents (with attached western style washroom facilities)</li><li>Boys and girls stay in separate accommodation with adequate care taken </li><li>International standard safety gear, trained and qualified staff </li><li>Govt. Taxes</li></ul><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\"><span style=\"font-weight: 700;\">Cost Excludes:</span></p><ul style=\"color: rgb(51, 51, 51); font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\"><li>Mineral Water / Cold Drinks, etc.</li><li>Food charges during Bus Journey.</li><li>Personal expenditure of any kind.</li><li>Medical expenses other than First aid.</li><li>White Water Rafting Rs 1000/-per head</li><li>Any other charges not specifically mentioned in “Fees Include”.</li></ul><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Camp Info</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">Campsite distance from Mumbai – 130 Kms; Panvel – 70 kms; Harihareshwar – 80kms; Nagothane – 25 kms; Roha – 25 kms; MurudJanjira – 80 kms.</p><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Safety</h4><ul style=\"color: rgb(51, 51, 51); font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;\"><li>Utmost care is taken to ensure the participants have a great time at the campus and no compromise is made on the safety and hygiene aspects.</li><li>Our junior trainers are trained and certified.</li><li>Equipment used for adventure is of European standards.</li></ul><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Propriety</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">The data furnished in this document and its attachments are not expected to be disclosed outside the account and may not be duplicated, used or disclosed in whole or part for any purpose other than to evaluate this information for conduct of this program.</p><h4 class=\"main-heads\" style=\"font-family: \"Open Sans\", sans-serif; font-weight: bold; color: rgb(51, 51, 51); width: 778px; float: left;\">Medical</h4><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">We are fully equipped to provide First aid facilities at all activity locations along withtrained staff. Two local doctors (non MBBS) are available in a village 4 kms away.</p><p style=\"margin-bottom: 14px; font-family: \"Open Sans\", sans-serif; line-height: 21px; color: rgb(51, 51, 51); -webkit-font-smoothing: antialiased; text-align: justify;\">We have appropriate arrangements to transfer a patient to Gandhi Hospital,Koladwhich is 20 km away and takes approx. 30 mins to reach.</p>', NULL, '2019-05-09 09:24:07', '2019-05-10 19:22:11'),
(7, 'Special Packages For Summer Vacation 2019', '', 'Summer Special Packages', 'Special Packages For Summer Vacation 2019', 14, '2019-05-31', '<p style=\"text-align: center; color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><img src=\"http://empowercamp.com/old/wp-content/uploads/2019/04/Summer-Camp-Special-Packages-2019.gif\" style=\"width: 546.943px; height: 559.9px; float: none;\"><span style=\"font-weight: 700;\"><br></span></p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">NOTES: </span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>White water rafting is an outsourced activity and is dependent on release of water from a dam. If it gets cancelled or anyone doesn’t want to do, Empower will not be responsible and no refund will take place. Transport for rafting from camp to rafting site & back will be extra.</li><li>All inclusions are part of package & no refund will be given if any facility is not availed/activity is not done.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><u>CHILDREN POLICY:</u></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Up to 5 years complimentary (No extra Bed)</li><li>6-8 years half rate.</li><li>8 years and above full rate. Kids who are under 14 cannot do rafting but can-do Zip line instead of that.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><u>OPTIONAL:</u></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Starters, Mineral water & Juices available at extra cost</li><li>DJ Rs 12000/- and Music Rs. 5,000/-per night</li><li>Driver’s/cleaners meals Rs. 1,000/-per person per day (buffet) and Rs 700/-per head (staff food).</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><u>NOTE</u></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Above package is applicable for the Corporate outings/offsites too.</li><li>Any other Adventure Activity/ Training prog/Team Building prog will be charged extra and GST on it will be applicable.</li><li>Full Payment in advance</li></ul>', NULL, '2019-05-11 04:43:30', '2019-05-13 09:42:08'),
(8, 'Monsoon Special Packages ', '', 'Monsoon Packages', 'Book Your Special Monsoon Packages start with Rs. 2,800/- | Experience the Magic of Real White Water Rafting', 16, '2019-06-01', '<p><b>SPECIAL PACKAGE FOR MONSOON- Jun-Jul 2019</b></p><p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><u><span style=\"font-family:\r\n\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";color:#444444;=\"\" mso-fareast-language:en-in\"=\"\">1) STAY & MEALS</span></u></b></p><p class=\"MsoNormal\" style=\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Symbol;\r\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\r\nmso-fareast-language:EN-IN\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">       </span></span><!--[endif]--><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" color:#444444;mso-fareast-language:en-in\"=\"\">Swiss cottage Tents (Air Cooled) -\r\n2800/-per head (Quadruple Occupancy)<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Symbol;\r\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\r\nmso-fareast-language:EN-IN\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">       </span></span><!--[endif]--><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" color:#444444;mso-fareast-language:en-in\"=\"\">Cottage / Dlx Swiss Cottage Tents\r\n(AC) - 3400/-per head (Quadruple Occupancy)<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Symbol;\r\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\r\nmso-fareast-language:EN-IN\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">       </span></span><!--[endif]--><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" color:#444444;mso-fareast-language:en-in\"=\"\">Additional Rs 800 per head for Triple\r\nOccupancy and Rs 1000/-per head for double Occupancy. Inclusion will be same.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-left: 0.75in; text-indent: -0.25in; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><!--[if !supportLists]--><span style=\"font-family:Symbol;\r\nmso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol;color:#444444;\r\nmso-fareast-language:EN-IN\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">       </span></span><!--[endif]--><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" color:#444444;mso-fareast-language:en-in\"=\"\">Additional Night will cost etc.</span></p><p class=\"MsoNormal\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;=\"\" mso-fareast-font-family:\"times=\"\" roman\";color:#444444;mso-fareast-language:=\"\" en-in\"=\"\">2)<u>  PACKAGE  INCLUDES:</u></span></b></p><ul style=\"margin-top:0in\" type=\"disc\">\r\n <li class=\"MsoNormal\" style=\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\"><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" mso-fareast-language:en-in\"=\"\">1600 hrs Check In to 1600 hrs Check Out<o:p></o:p></span></li>\r\n <li class=\"MsoNormal\" style=\"color:#444444;line-height:107%;mso-list:l5 level1 lfo5\"><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" mso-fareast-language:en-in\"=\"\">One cycle of meals (High tea</span><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;color:windowtext\"=\"\">, D</span><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" mso-fareast-language:en-in\"=\"\">inner, Morning Tea, Breakfast and Lunch)<o:p></o:p></span></li>\r\n <li class=\"MsoNormal\" style=\"line-height:107%;mso-list:l5 level1 lfo5\"><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" color:#444444;mso-fareast-language:en-in\"=\"\">Activities- Trekking, Treasure\r\n     Hunt, Foot cricket, Burma Bridge, Wall climbing and Rappelling, Target\r\n     Shooting, Dodge ball and Camp Fire and <b>White water rafting on Day two. </b></span><b><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif\"=\"\"><o:p></o:p></span></b></li>\r\n <li class=\"MsoNormal\" style=\"color:#444444;margin-bottom:8.0pt;line-height:107%;\r\n     mso-list:l5 level1 lfo5\"><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;=\"\" mso-fareast-font-family:\"times=\"\" roman\";mso-fareast-language:en-in\"=\"\">Inclusive\r\n     of Taxes<o:p></o:p></span></li>\r\n</ul><p class=\"MsoNormal\"><b><u><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;mso-fareast-font-family:\"times=\"\" roman\";=\"\" color:#444444;mso-fareast-language:en-in\"=\"\">PACKAGE NOTES :  <o:p></o:p></span></u></b></p><p class=\"MsoListParagraphCxSpFirst\" style=\"margin-bottom:8.0pt;mso-add-space:\r\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span style=\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">      \r\n</span></span><!--[endif]--><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;=\"\" mso-fareast-font-family:\"times=\"\" roman\";color:#444444;mso-fareast-language:=\"\" en-in\"=\"\">White water rafting is an outsourced activity and is dependent upon\r\nrelease of water from a dam. If it gets cancelled, reasons beyond our control, or\r\nanyone doesn’t want to do, Empower will not be held responsible and no refund\r\nwill be made. Transport for rafting from camp to rafting site & back will\r\nbe extra.</span><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif\"=\"\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpLast\" style=\"margin-bottom:8.0pt;mso-add-space:\r\nauto;text-indent:-.25in;line-height:107%;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span style=\"font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:\r\nSymbol\">·<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \" times=\"\" new=\"\" roman\";\"=\"\">      \r\n</span></span><!--[endif]--><span style=\"font-family:\" times=\"\" new=\"\" roman\",serif;=\"\" mso-fareast-font-family:\"times=\"\" roman\";color:#444444;mso-fareast-language:=\"\" en-in\"=\"\">All inclusions are part of package & no refund will be given if any\r\nfacility is not availed /activity not done.</span></p>', NULL, '2019-05-20 09:26:25', '2019-05-21 16:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Corporate',
  `author` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `designation` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `author_image` int(10) UNSIGNED DEFAULT NULL,
  `content_image` int(10) UNSIGNED DEFAULT NULL,
  `youtube` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `title`, `category`, `author`, `designation`, `content`, `author_image`, `content_image`, `youtube`, `post_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'You totally deserve the aploases for making our trip such good', 'Adventure', 'Krunal Karia', 'Real Estate Advissor', 'This is small feed back which I would like to give you on behalf of our family & specially from our bday boy.<br>As we visited with our family on 5th march to enjoy my nephew bday.', 7, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-06-12', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(2, 'Empower is Awesome !', 'Adventure', 'Kajal Dasani', 'Systems & finance,<br>Symbiosis Institute of Telecom Management', 'I feel so privileged that i was a part of the Empower Camp and got an opportunity to gain so much from you! No flattering words sir but its a heart felt thank you and i feel lucky that was there under your guidance. It was very sweet of you that you remember my both names even now it brings a smile to my face.It was a lifetime experience and i will never forget those 4 fun-filled days where i became physically as well mentally stronger. Now i can take my decisions in a more firm way! No more mobile addiction that\'s the biggest take-away!', 7, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-06-12', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(3, 'An engaging session indeed', 'Corporate', 'ROHIT MALLICK', 'Regional Manager South,<br>Learning Services,<br>ITC GRAND CHOLA,Chennai', 'Greetings from ITC Grand Chola ! Thank you for your email. It was indeed our privilege to organise a short programme delivered by you for our team members. Each one of them gave an extremely positive feedback for the programme and appeared to be highly motivated after attending your programme. I could make out that your session was indeed very engaging and every participant was actively involved in the activities that were being conducted.', NULL, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-07-03', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(4, 'Positive Transformation', 'Corporate', 'Sushavan Chatterjee', 'Area Sales Manager,<br>Nilkamal Ltd.', 'first of all, I need to thank you for proffering us, such an alluring experience in between the lap of mother nature which we hardly get a chance to practice in our daily life.\n		Secondly, I really boast of gaining all those outstanding experiences which we have acquired through your commendable trainings schedules.I honestly pledge to implement all the knowledge powered by you where ever relevant in both my “Professional”  as well as “Personal life” .......and I am sure it would bring out lots of positive transformation within me in the coming days.....Wish you and your entire team a great success.', NULL, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-07-03', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(5, 'They went as individuals but came back as \'one\' Firestar', 'Students', 'Anup Pancha', 'COO,<br> Firestar Diamond', 'Thanks for your Outbound Leadership and Team Building Training given to Sr. Firestar team.We had a good Time of Learning with Good Physical fitness activities and Games. I know your physical activities and games made them tired due to they are not habitual in their daily routine but I am Sure they All become Mentally Very Strong by learning lesson from all activities, what they can DO where they were feeling they can’t do before attempting those games and activates ...When they left from Mumbai to Kolad in bus, I saw them, they were from different dept and Location and when I saw them while returning, I found they all are from Firestar.Hoping to get continuous guidance from you to my team members time to time to become better than Before always at every stage in their Life.Thanks for your all your Hospitality with Fantastic Food .Please convey my sincere thanks to Anand , Pratap and all other team members whoever has directly or indirectly helped to make our training super successful.', NULL, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-07-03', NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `extension` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `caption` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `user_id` int(10) UNSIGNED DEFAULT '1',
  `hash` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Summer-Camp-Whatsapp-Promo-Vertical.png', '/home/empowziq/public_html/storage/uploads/Summer-Camp-Whatsapp-Promo-Vertical.png', 'png', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856788', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(2, 'EmpowerCamp-by-night.png', '/home/empowziq/public_html/storage/uploads/EmpowerCamp-by-night.png', 'png', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856889', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(3, 'outbound-training-camp-new-york-concluded.jpg', '/home/empowziq/public_html/storage/uploads/outbound-training-camp-new-york-concluded.jpg', 'jpg', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856799', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(4, 'outbound-training-new-york-empower-camp.jpg', '/home/empowziq/public_html/storage/uploads/outbound-training-new-york-empower-camp.jpg', 'jpg', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856879', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(5, 'Corporate-Training-for-Bank.jpg', '/home/empowziq/public_html/storage/uploads/Corporate-Training-for-Bank.jpg', 'jpg', 'Corporate Training for Bank', NULL, 't5ipaasxz967pvle856712', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(6, 'Outbound-Training-Institute.jpg', '/home/empowziq/public_html/storage/uploads/Outbound-Training-Institute.jpg', 'jpg', 'Outbound Training for Institute', 1, 't5ipaasxz967pvle856713', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(7, 'Outbound-Training-near-Pune.jpg', '/home/empowziq/public_html/storage/uploads/Outbound-Training-near-Pune.jpg', 'jpg', 'Outbound Training near Pune', 1, 't5ipaasxz967pvle856714', 1, NULL, '2019-05-03 05:54:38', '2019-05-03 05:54:38'),
(8, 'Summer-Camp-Event-2to4-Jun.png', '/home/empowziq/public_html/storage/uploads/2019-05-09-092121-Summer-Camp-Event-2to4-Jun.png', 'png', '', 1, '9fl0eadwla9amhwjy7fu', 1, NULL, '2019-05-09 09:21:21', '2019-05-09 13:18:29'),
(9, 'Empower-Album-Posting-Siemens1.png', '/home/empowziq/public_html/storage/uploads/2019-05-10-192006-Empower-Album-Posting-Siemens1.png', 'png', '', 1, 'eglqfogr24nzlahvq2kl', 1, NULL, '2019-05-10 19:20:06', '2019-05-22 11:32:15'),
(10, 'Winter-Camp-Ad-2018.png', '/home/empowziq/public_html/storage/uploads/2019-05-10-192653-Winter-Camp-Ad-2018.png', 'png', '', 1, 'nr3awcpj7ihedcfpnns1', 1, NULL, '2019-05-10 19:26:53', '2019-05-22 11:32:10'),
(11, 'Winter-Camp-Ad-20181.png', '/home/empowziq/public_html/storage/uploads/2019-05-10-192653-Winter-Camp-Ad-20181.png', 'png', '', 1, 'mijqv8hvs6zxdiyl4pyx', 1, NULL, '2019-05-10 19:26:53', '2019-05-22 11:32:13'),
(12, 'Winter-Camp-Ad-20185.png', '/home/empowziq/public_html/storage/uploads/2019-05-10-192655-Winter-Camp-Ad-20185.png', 'png', '', 1, 'yrpato3ykmaqffodsewl', 1, NULL, '2019-05-10 19:26:55', '2019-05-22 11:32:06'),
(13, 'Winter-Camp-Ad-20182.png', '/home/empowziq/public_html/storage/uploads/2019-05-10-192655-Winter-Camp-Ad-20182.png', 'png', '', 1, 'okxi5tja7vvcluz3ptep', 1, NULL, '2019-05-10 19:26:55', '2019-05-22 11:32:08'),
(14, 'summer pack.jpg', '/home/empowziq/public_html/storage/uploads/2019-05-11-044301-summer pack.jpg', 'jpg', '', 1, 'aundjsoam8isgg28pzly', 1, NULL, '2019-05-11 04:43:01', '2019-05-22 11:32:03'),
(15, 'Monsoon-Special-Packages-2019-Event.png', '/home/empowziq/public_html/storage/uploads/2019-05-20-090721-Monsoon-Special-Packages-2019-Event.png', 'png', '', 1, 'i7lue6aho00v6vjdy14j', 1, NULL, '2019-05-20 09:07:21', '2019-05-22 11:32:01'),
(16, 'Monsoon-Special-Packages-2019-Event.png', '/home/empowziq/public_html/storage/uploads/2019-05-21-111850-Monsoon-Special-Packages-2019-Event.png', 'png', '', 1, 'xcori4714hk26bsz0c0t', 1, NULL, '2019-05-21 11:18:50', '2019-05-22 11:31:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `context_id`, `email`, `password`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Col. Naval Kohli', 1, 'digital@empowercamp.com', '$2y$10$nnnr5A.2oJB9.thXnZlc2uxfYfXtrO8hvcHrCBilfqJQ9hfg8Bsd2', '8MANfOvk0Offbwqi8M3v8E2XJZ2jRwraxFGHru5Q17yiO0gFitmGAekLhO2H', NULL, '2019-05-03 05:54:38', '2019-05-09 13:18:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `backups_name_unique` (`name`),
  ADD UNIQUE KEY `backups_file_name_unique` (`file_name`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_categories_url_unique` (`url`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_posts_url_unique` (`url`),
  ADD KEY `blog_posts_category_id_foreign` (`category_id`),
  ADD KEY `blog_posts_author_id_foreign` (`author_id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_primary_unique` (`email_primary`),
  ADD KEY `customers_profile_img_foreign` (`profile_img`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_primary_unique` (`email_primary`),
  ADD KEY `employees_profile_img_foreign` (`profile_img`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `enquiries_email_unique` (`email`);

--
-- Indexes for table `lalogs`
--
ALTER TABLE `lalogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lalogs_module_id_foreign` (`module_id`),
  ADD KEY `lalogs_user_id_foreign` (`user_id`);

--
-- Indexes for table `lalog_user`
--
ALTER TABLE `lalog_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lalog_user_lalog_id_foreign` (`lalog_id`),
  ADD KEY `lalog_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `la_configs`
--
ALTER TABLE `la_configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `la_configs_key_unique` (`key`),
  ADD KEY `la_configs_field_type_foreign` (`field_type`);

--
-- Indexes for table `la_menus`
--
ALTER TABLE `la_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_from_foreign` (`from`),
  ADD KEY `messages_to_foreign` (`to`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Indexes for table `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_menu_role_id_foreign` (`role_id`),
  ADD KEY `role_menu_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Indexes for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_banner_foreign` (`banner`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `testimonials_author_unique` (`author`),
  ADD UNIQUE KEY `testimonials_designation_unique` (`designation`),
  ADD KEY `testimonials_author_image_foreign` (`author_image`),
  ADD KEY `testimonials_content_image_foreign` (`content_image`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lalogs`
--
ALTER TABLE `lalogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `lalog_user`
--
ALTER TABLE `lalog_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `la_configs`
--
ALTER TABLE `la_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `la_menus`
--
ALTER TABLE `la_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

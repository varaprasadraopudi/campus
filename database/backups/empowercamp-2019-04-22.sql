-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 22, 2019 at 01:41 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `empowercamp`
--

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_categories`
--

CREATE TABLE `blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blog_posts`
--

CREATE TABLE `blog_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Draft',
  `author_id` int(10) UNSIGNED DEFAULT NULL,
  `tags` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '[]',
  `post_date` date DEFAULT NULL,
  `excerpt` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `album` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `content` varchar(10000) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog_posts`
--

INSERT INTO `blog_posts` (`id`, `title`, `url`, `category_id`, `status`, `author_id`, `tags`, `post_date`, `excerpt`, `album`, `content`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Summer Camp 2018', 'summer-camp-2018', NULL, 'Published', NULL, '[\"Summer Camp\"]', '2018-06-11', '', '[\"1\"]', '<p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">Summer holidays &nbsp;is time for your &nbsp;child/children&nbsp;&nbsp;to enjoy, have fun, explore new things, make new friends as well as it’s a great time to learn new things.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">The greatest gifts that parents can give their children are independence and resiliency, and by selecting&nbsp;<em>OUR&nbsp;CAMPS</em>, you can give both. We can assure&nbsp;you all will see growth, maturity and confidence when he/she will return home. They will be more engaged, giving, and confident.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">A program which is being loved by many children and parents over the past years.&nbsp;A program that helps your child to open up, learns new skills, prepares them for challenges and overcome them with less pressure &amp; more abilities and&nbsp;designed in lines with your children age group and learning capabilities; both physical and mental</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">To enrich the overall personality of participants by giving them exposure to Natural Environment, Experiential Learning, ‘Processed Adventure’, Creativity, Practical education in an outdoor environment, and some cultural activities.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">Some of the Activities planned are given below. They may not be followed rigidly as mood and energy levels of participants as well as weather conditions necessitate some modifications.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">Going by our motto ‘learns ‘n leisure’ we have redefined the spellings of FUN. In Empower we spell FUN as FUNNNNNNNNNNNN….N.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><u>CAMPING AT EMPOWER STARTS WITH-</u></span></p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">INTRODUCTIONS</span>: Knowing self and other members of the team</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">TEAM FORMULATION AND FLAG MAKING</span>: Building brotherhood and a sense of belonging to a team and the importance of a flag</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">MORNING PRAYERS.</span>&nbsp;The day starts with a universal prayer.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">HARIYALI:</span>&nbsp;An entertaining way to introduce the environment and physical training</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\">And the Campers gradually experience</span></p>', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'Empower Is All Set To Welcome The New Year', 'welcome-the-new-year', NULL, 'Published', NULL, '[\"New Year\",\"Activity Camp\"]', '2018-01-01', '', '[\"2\"]', '<p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>Empower Activity Camps in Kolad is 50 acres of beautifully landscaped picturesque land in the midst of a hill and &nbsp;&nbsp;&nbsp;&nbsp; a lake.</em></span></p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>We are 2.15 Hrs &nbsp;from Vashi, Mumbai &amp; Chandni Chowk, Pune &nbsp;near Kolad on Mumbai – Goa Highway.</em></span></p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>&nbsp;</em></span><span style=\"font-weight: 700;\"><em><u>Attractions</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Ideal place to usher in New Year with family (kids also welcome).</li><li>No dress code- Walk in casual attire.</li><li>Experience unlimited Funnnn…. under the open Skies in cool light of Stars.</li><li>Let your hair loose to the melodious music of DJ or enjoy sitting by the Bon Fire.</li><li>Veg, Non Veg, Jain food. (Jain food with prior notice)</li><li>Challenge yourself with a host of adventure activities or enjoy the fun activities organized by our trained team.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">A unique experience of a resort away from the humdrum of city life.</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em><u>Accommodation/Packages</u></em></span></p><table border=\"1\" width=\"786\" style=\"background-color: rgb(255, 255, 255); color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px; height: 229px;\"><tbody><tr><td width=\"284\"><span style=\"font-weight: 700;\">Accommodation</span></td><td width=\"210\"><span style=\"font-weight: 700;\">Rate<br></span>(per person &amp; all Inclusive)</td><td width=\"263\"><span style=\"font-weight: 700;\">Remarks</span></td></tr><tr><td width=\"284\"><span style=\"font-weight: 700;\">Cottage (AC)</span></td><td width=\"210\" style=\"text-align: right;\"><span style=\"font-weight: 700;\">Rs. 6,375/-</span></td><td width=\"263\">&nbsp;</td></tr><tr><td width=\"284\"><span style=\"font-weight: 700;\">Swiss Cottage Tents(Air Cooled)</span></td><td width=\"210\" style=\"text-align: right;\"><span style=\"font-weight: 700;\">Rs. 5,775/-</span></td><td width=\"263\">&nbsp;</td></tr><tr><td width=\"284\"><span style=\"font-weight: 700;\">Dormitory (AC)</span></td><td width=\"210\" style=\"text-align: right;\"><span style=\"font-weight: 700;\">Rs. 4,775/-</span></td><td width=\"263\">Modern Doms /15-18 Pax per Dom</td></tr></tbody></table><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\">&nbsp;</p><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em><u>Cost Includes</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Check in 31<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">st</span>&nbsp;Dec 2017 at 1200 hrs &amp; Check out 1<span style=\"position: relative; font-size: 12px; line-height: 0; vertical-align: baseline; top: -0.5em;\">st</span>&nbsp;Jan 2018 at 1200 hrs.</li><li>Stay &amp; all the meals from Day one lunch to Day two Breakfast (Day 2 lunch at extra cost)</li><li>New Year Eve celebration, DJ, Starters, Soft Drinks, Gala Dinner with starters (2 Veg.+2 non veg), Bonfire.</li><li>Taxes included &amp; Activities- Burma Bridge, Trekking, Target shooting, Dodge ball, River crossing and Treasure Hunt.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em><u>Note</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Cottages on double occupancy &amp; can accommodate max four persons. Extra bed will be Rs 4700/-per person.</li><li>Swiss Cottage Tents on Quadruple occupancy. Double occupancy on extra cost.</li><li>Children up to 5 yrs are complimentary, 5-8yrs are on half tariff .</li><li>Adventure Activities other than stated above can be done on extra cost. Our Attractions are white water Raftng, Wall climbing &amp; Rappelling, Zip line, Raft Building &amp; Rowing &amp; many more. Some activities will require minimum no of &nbsp;&nbsp;&nbsp;</li><li>Other services like Telephone calls, Soft drinks (other than new year eve dinner), Juices etc will be on extra cost.</li><li>Additional night stay can be added on extra cost as per accommodation.</li></ul><p style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><span style=\"font-weight: 700;\"><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Payment &amp; cancellation policy</u></em></span></p><ul style=\"color: rgb(51, 51, 51); font-family: Roboto; font-size: 16px;\"><li>Booking on 100% advance Payment.</li><li>No refund will be applicable on cancellation after 25 Dec 2017.</li><li>Payment can be done by cheque or direct transfer in our bank account.</li><li><span style=\"font-weight: 700;\">Bank Details:</span>&nbsp;Bank Name: ICICI Bank, Lokhandwala Branch, Bank Account No. – 026305000246, IFSC Code: ICIC0000263, Account Type: Current Account.</li></ul>', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 'New York Outbound Training Camp Concluded Successfully', 'outbound-training-camp', NULL, 'Published', NULL, '[\"Outbound Training Camp\"]', '2018-02-11', '', '[\"3\"]', 'Empower Activity Camp lead by Col Naval Kohli successfully conducted the Outbound Training Camp in New York for a leading MNC. Here are some of the stills from the camp:', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 'Now Empowering New York – Outbound Training In New York', 'new-york-outbound-training', NULL, 'Published', NULL, '[\"Outbound Training\"]', '2018-05-15', '', '[\"4\"]', 'Empower Camp is thrilled to announce our upcoming Outbound Training camp in New York from 15th-18th May, 2017.Stay tuned for more updates from the New York camp.Now Empower New York!', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `organization` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `training` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `organization` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Male',
  `phone_primary` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_secondary` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_primary` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `email_secondary` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_img` int(10) UNSIGNED DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `date_birth` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'Customer', '[]', '#000', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Male',
  `phone_primary` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_secondary` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_primary` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `email_secondary` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_img` int(10) UNSIGNED DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `date_birth` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `phone_primary`, `phone_secondary`, `email_primary`, `email_secondary`, `profile_img`, `city`, `address`, `about`, `date_birth`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Ganesh Bhosale', 'Super Admin', 'Male', '7350558900', '', 'hello@laraadmin.com', '', NULL, 'Pune', '', '', NULL, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `phone_no` varchar(15) COLLATE utf8_unicode_ci DEFAULT '',
  `organization` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `training` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lalogs`
--

CREATE TABLE `lalogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(256) COLLATE utf8_unicode_ci DEFAULT '',
  `module_id` int(10) UNSIGNED DEFAULT NULL,
  `context_id` int(11) NOT NULL DEFAULT '0',
  `context2_id` int(11) NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lalogs`
--

INSERT INTO `lalogs` (`id`, `title`, `type`, `module_id`, `context_id`, `context2_id`, `content`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Employee Ganesh Bhosale Created', 'EMPLOYEE_CREATED', 4, 1, 0, '{\"name\":\"Ganesh Bhosale\",\"designation\":\"Super Admin\",\"gender\":\"Male\",\"phone_primary\":\"7350558900\",\"phone_secondary\":\"\",\"email_primary\":\"hello@laraadmin.com\",\"email_secondary\":\"\",\"profile_img\":null,\"city\":\"Pune\",\"address\":\"\",\"about\":\"\",\"date_birth\":null,\"updated_at\":\"2019-04-22 11:36:25\",\"created_at\":\"2019-04-22 11:36:25\",\"id\":1}', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'User Ganesh Bhosale Created', 'USER_CREATED', 1, 1, 0, '{\"name\":\"Ganesh Bhosale\",\"email\":\"hello@laraadmin.com\",\"context_id\":1,\"updated_at\":\"2019-04-22 11:36:25\",\"created_at\":\"2019-04-22 11:36:25\",\"id\":1}', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `lalog_user`
--

CREATE TABLE `lalog_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `lalog_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `notif_mail` tinyint(1) NOT NULL DEFAULT '0',
  `notif_mail_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `notif_socket` tinyint(1) NOT NULL DEFAULT '0',
  `notif_socket_viewed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `la_configs`
--

CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_type` int(10) UNSIGNED DEFAULT NULL,
  `minlength` int(10) UNSIGNED DEFAULT NULL,
  `maxlength` int(10) UNSIGNED DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` varchar(5000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_configs`
--

INSERT INTO `la_configs` (`id`, `label`, `key`, `section`, `value`, `field_type`, `minlength`, `maxlength`, `required`, `popup_vals`, `created_at`, `updated_at`) VALUES
(1, 'Sitename', 'sitename', 'General', 'LaraAdmin Plus 1.0', 16, 0, 20, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'Sitename First Word', 'sitename_part1', 'General', 'LaraAdmin', 16, 0, 20, 0, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 'Sitename Second Word', 'sitename_part2', 'General', 'Plus 1.0', 16, 0, 20, 0, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 'Sitename Short (2/3 Characters)', 'sitename_short', 'General', 'LA+', 16, 1, 3, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 'Site Description (160 Characters)', 'site_description', 'General', 'LaraAdmin Plus is a unique Laravel Admin Panel for quick-start Admin based applications and boilerplate for CRM or CMS systems.', 19, 0, 160, 0, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(6, 'Navbar Search Box', 'topbar_search', 'Display', '0', 2, NULL, NULL, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(7, 'Show Navbar Messages', 'show_messages', 'Display', '1', 2, NULL, NULL, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(8, 'Show Navbar Notifications', 'show_notifications', 'Display', '1', 2, NULL, NULL, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(9, 'Show Navbar Tasks', 'show_tasks', 'Display', '1', 2, NULL, NULL, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(10, 'Show Right SideBar', 'show_rightsidebar', 'Display', '1', 2, NULL, NULL, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(11, 'Show Standby Chat Application', 'show_standby_chat', 'Display', '0', 2, NULL, NULL, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(12, 'Skin / Theme Color', 'skin', 'Display', 'skin-purple', 7, NULL, NULL, 1, '[\"skin-purple\", \"skin-blue\", \"skin-black\", \"skin-yellow\", \"skin-red\", \"skin-green\", \"skin-purple-light\", \"skin-blue-light\", \"skin-black-light\", \"skin-yellow-light\", \"skin-red-light\", \"skin-green-light\"]', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(13, 'Layout', 'layout', 'Display', 'fixed', 7, NULL, NULL, 1, '[\"fixed\", \"sidebar-mini\", \"fixed-sidebar-mini\", \"layout-boxed\", \"layout-top-nav\", \"sidebar-collapse\"]', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(14, 'default_email', 'default_email', 'Admin', 'test@example.com', 8, 5, 100, 1, '', '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `la_menus`
--

CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', '/', 'fa-home', 'custom', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'Team', '#', 'fa-group', 'custom', 0, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 'Blog', '#', 'fa-file-text-o', 'custom', 0, 2, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 'Users', 'users', 'fa-group', 'module', 2, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 'Uploads', 'uploads', 'fa-files-o', 'module', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(6, 'Departments', 'departments', 'fa-tags', 'module', 2, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(7, 'Employees', 'employees', 'fa-group', 'module', 2, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(8, 'Customers', 'customers', 'fa-user', 'module', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(9, 'Roles', 'roles', 'fa-user-plus', 'module', 2, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(10, 'Permissions', 'permissions', 'fa-magic', 'module', 2, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(11, 'Messages', 'messages', 'fa-comments-o', 'module', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(12, 'Blog_categories', 'blog_categories', 'fa-list-ul', 'module', 3, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(13, 'Blog_posts', 'blog_posts', 'fa-file-text-o', 'module', 3, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(14, 'Sessions', 'sessions', 'fa-file-text-o', 'module', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(15, 'Testimonials', 'testimonials', 'fa-file-text-o', 'module', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(16, 'Bookings', 'bookings', 'fa-file-text-o', 'module', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(17, 'Enquiries', 'enquiries', 'fa-file-text-o', 'module', 0, 0, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '',
  `from` int(10) UNSIGNED DEFAULT NULL,
  `to` int(10) UNSIGNED DEFAULT NULL,
  `is_received` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_05_26_050000_create_modules_table', 1),
(2, '2014_05_26_055000_create_module_field_types_table', 1),
(3, '2014_05_26_060000_create_module_fields_table', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2014_12_01_000000_create_uploads_table', 1),
(7, '2016_05_26_064006_create_departments_table', 1),
(8, '2016_05_26_064007_create_employees_table', 1),
(9, '2016_05_26_064110_create_customers_table', 1),
(10, '2016_05_26_064446_create_roles_table', 1),
(11, '2016_07_05_115343_create_role_user_table', 1),
(12, '2016_07_07_134058_create_backups_table', 1),
(13, '2016_07_07_134058_create_menus_table', 1),
(14, '2016_07_07_294546_create_role_menu_table', 1),
(15, '2016_09_10_163337_create_permissions_table', 1),
(16, '2016_09_10_163520_create_permission_role_table', 1),
(17, '2016_09_22_105958_role_module_fields_table', 1),
(18, '2016_09_22_110008_role_module_table', 1),
(19, '2016_10_06_115413_create_la_configs_table', 1),
(20, '2016_12_09_121655_create_messages_table', 1),
(21, '2017_11_20_063000_create_blog_categories_table', 1),
(22, '2017_11_20_063206_create_blog_posts_table', 1),
(23, '2017_11_20_063216_create_sessions_table', 1),
(24, '2017_11_20_063217_create_testimonials_table', 1),
(25, '2017_11_20_063218_create_bookings_table', 1),
(26, '2017_11_20_063219_create_enquiries_table', 1),
(27, '2050_01_01_010000_create_la_logs_table', 1),
(28, '2050_01_01_010100_create_la_log_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2019-04-22 06:06:23', '2019-04-22 06:06:25'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2019-04-22 06:06:23', '2019-04-22 06:06:25'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(4, 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(5, 'Customers', 'Customers', 'customers', 'name', 'Customer', 'CustomersController', 'fa-user', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(6, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(7, 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(9, 'Messages', 'Messages', 'messages', 'content', 'Message', 'MessagesController', 'fa-comments-o', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(10, 'Blog_categories', 'Blog Categories', 'blog_categories', 'name', 'BlogCategory', 'BlogCategoriesController', 'fa-list-ul', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(11, 'Blog_posts', 'Blog Posts', 'blog_posts', 'title', 'BlogPost', 'BlogPostsController', 'fa-file-text-o', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(12, 'Sessions', 'Sessions', 'sessions', 'title', 'Session', 'SessionsController', 'fa-file-text-o', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(13, 'Testimonials', 'Testimonials', 'testimonials', 'title', 'Testimonial', 'TestimonialsController', 'fa-file-text-o', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(14, 'Bookings', 'Bookings', 'bookings', 'name', 'Booking', 'BookingsController', 'fa-file-text-o', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(15, 'Enquiries', 'Enquiries', 'enquiries', 'name', 'Enquiry', 'EnquiriesController', 'fa-file-text-o', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25'),
(16, 'LALogs', 'LALogs', 'lalogs', 'title', 'LALog', 'LALogsController', 'fa-eye', 1, '2019-04-22 06:06:24', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT '0',
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(11) NOT NULL DEFAULT '0',
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `listing_col` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `listing_col`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, 0, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(5, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(6, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(7, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(8, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(9, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(10, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(11, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, 1, '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(12, 'name', 'Name', 3, 16, 1, '', 1, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(13, 'tags', 'Tags', 3, 20, 0, '[]', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(14, 'color', 'Color', 3, 19, 0, '', 0, 50, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(15, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(16, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(17, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(18, 'phone_primary', 'Primary Phone', 4, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(19, 'phone_secondary', 'Secondary Phone', 4, 14, 0, '', 5, 15, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(20, 'email_primary', 'Primary Email', 4, 8, 1, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(21, 'email_secondary', 'Secondary Email', 4, 8, 0, '', 5, 100, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(22, 'profile_img', 'Profile Image', 4, 12, 0, '', 0, 0, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(23, 'city', 'City', 4, 19, 0, '', 2, 50, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(24, 'address', 'Address', 4, 1, 0, '', 4, 1000, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, 'NULL', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(27, 'name', 'Name', 5, 16, 0, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(28, 'designation', 'Designation', 5, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(29, 'organization', 'Organization', 5, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(30, 'gender', 'Gender', 5, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(31, 'phone_primary', 'Primary Phone', 5, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(32, 'phone_secondary', 'Secondary Phone', 5, 14, 0, '', 5, 15, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(33, 'email_primary', 'Primary Email', 5, 8, 1, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(34, 'email_secondary', 'Secondary Email', 5, 8, 0, '', 5, 100, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(35, 'profile_img', 'Profile Image', 5, 12, 0, '', 0, 0, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(36, 'city', 'City', 5, 19, 0, '', 2, 50, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(37, 'address', 'Address', 5, 1, 0, '', 4, 1000, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(38, 'about', 'About', 5, 19, 0, '', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(39, 'date_birth', 'Date of Birth', 5, 4, 0, 'NULL', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(40, 'name', 'Name', 6, 16, 1, '', 1, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(41, 'display_name', 'Display Name', 6, 19, 0, '', 0, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(42, 'description', 'Description', 6, 21, 0, '', 0, 1000, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(43, 'parent', 'Parent Role', 6, 7, 0, '', 0, 0, 0, '@roles', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(44, 'context_type', 'Context Type', 6, 7, 0, 'Employee', 0, 0, 0, '[\"Employee\",\"Customer\"]', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(45, 'dept', 'Department', 6, 7, 0, '1', 0, 0, 0, '@departments', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(46, 'name', 'Name', 7, 16, 1, '', 0, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(47, 'file_name', 'File Name', 7, 19, 1, '', 0, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(48, 'backup_size', 'File Size', 7, 19, 0, '0', 0, 10, 1, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(52, 'subject', 'Subject', 9, 16, 0, '', 0, 50, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(53, 'content', 'Content', 9, 19, 0, '', 1, 1000, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(54, 'from', 'From', 9, 7, 0, '', 0, 0, 1, '@users', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(55, 'to', 'To', 9, 7, 0, '', 0, 0, 1, '@users', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(56, 'is_received', 'Is Received', 9, 2, 0, 'false', 0, 0, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(57, 'name', 'Name', 10, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(58, 'url', 'URL', 10, 19, 1, '', 5, 250, 1, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(59, 'description', 'Description', 10, 19, 0, '', 0, 1000, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(60, 'title', 'Title', 11, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(61, 'url', 'URL', 11, 19, 1, '', 5, 250, 1, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(62, 'category_id', 'Category', 11, 7, 0, '', 0, 0, 0, '@blog_categories', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(63, 'status', 'Status', 11, 7, 0, 'Draft', 0, 0, 1, '[\"Draft\",\"Published\"]', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(64, 'author_id', 'Author', 11, 7, 0, '', 0, 0, 1, '@employees', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(65, 'tags', 'Tags', 11, 20, 0, '[]', 0, 0, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(66, 'post_date', 'Date', 11, 4, 0, '', 0, 0, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(67, 'excerpt', 'Excerpt', 11, 19, 0, '', 0, 200, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(68, 'album', 'Album', 11, 24, 0, '', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(69, 'content', 'Content', 11, 11, 0, '', 0, 10000, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(70, 'title', 'Title', 12, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(71, 'training', 'Training', 12, 19, 0, '', 0, 200, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(72, 'excerpt', 'Excerpt', 12, 19, 0, '', 0, 200, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(73, 'banner', 'Banner', 12, 12, 0, '', 0, 0, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(74, 'post_date', 'Date', 12, 4, 0, '', 0, 0, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(75, 'content', 'Content', 12, 11, 0, '', 0, 10000, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(76, 'title', 'Title', 13, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(77, 'category', 'Category', 13, 7, 0, 'Corporate', 0, 0, 1, '[\"Corporate\",\"Students\",\"Adventure\"]', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(78, 'author', 'Author', 13, 19, 1, '', 5, 250, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(79, 'designation', 'Designation', 13, 19, 1, '', 5, 250, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(80, 'content', 'Content', 13, 21, 0, '', 0, 0, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(81, 'author_image', 'Author Image', 13, 12, 0, '', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(82, 'content_image', 'Content Image', 13, 12, 0, '', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(83, 'youtube', 'Youtube Link', 13, 23, 0, '', 40, 150, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(84, 'post_date', 'Date', 13, 4, 0, '', 0, 0, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(85, 'name', 'Name', 14, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(86, 'email', 'Email', 14, 8, 1, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(87, 'phone_no', 'Phone No.', 14, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(88, 'organization', 'Organization', 14, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(89, 'training', 'Training', 14, 19, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(90, 'message', 'Message', 14, 21, 0, '', 0, 0, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(91, 'name', 'Name', 15, 16, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(92, 'email', 'Email', 15, 8, 1, '', 5, 250, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(93, 'phone_no', 'Phone No.', 15, 14, 0, '', 5, 15, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(94, 'organization', 'Organization', 15, 19, 0, '', 0, 50, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(95, 'training', 'Training', 15, 19, 0, '', 5, 100, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(96, 'message', 'Message', 15, 21, 0, '', 0, 0, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(97, 'title', 'Title', 16, 16, 0, '', 0, 256, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(98, 'type', 'Type', 16, 19, 0, '', 1, 256, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(99, 'module_id', 'Module', 16, 7, 0, '', 0, 0, 0, '@modules', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(100, 'context_id', 'Context', 16, 13, 0, '', 0, 11, 1, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(101, 'context2_id', 'Context2', 16, 13, 0, '0', 0, 11, 0, '', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(102, 'content', 'Content', 16, 21, 0, '{}', 0, 0, 0, '', 0, 0, '2019-04-22 06:06:24', '2019-04-22 06:06:24'),
(103, 'user_id', 'User', 16, 7, 0, '', 0, 0, 1, '@users', 0, 1, '2019-04-22 06:06:24', '2019-04-22 06:06:24');

-- --------------------------------------------------------

--
-- Table structure for table `module_field_types`
--

CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(2, 'Checkbox', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(3, 'Currency', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(4, 'Date', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(5, 'Datetime', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(6, 'Decimal', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(7, 'Dropdown', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(8, 'Email', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(9, 'File', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(10, 'Float', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(11, 'HTML', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(12, 'Image', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(13, 'Integer', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(14, 'Mobile', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(15, 'Multiselect', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(16, 'Name', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(17, 'Password', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(18, 'Radio', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(19, 'String', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(20, 'Taginput', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(21, 'Textarea', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(22, 'TextField', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(23, 'URL', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(24, 'Files', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(25, 'Location', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(26, 'Color', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(27, 'Time', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(28, 'JSON', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(29, 'List', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(30, 'Duration', '2019-04-22 06:06:23', '2019-04-22 06:06:23'),
(31, 'Checklist', '2019-04-22 06:06:23', '2019-04-22 06:06:23');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `context_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Employee',
  `dept` int(10) UNSIGNED DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `context_type`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', NULL, 'Employee', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'ADMIN', 'Admin', 'Admin Level Access Role', 1, 'Employee', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 'CUSTOMER', 'Customer', 'Customer Level Access Role', 2, 'Customer', 2, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id`, `role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 1, 2, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 1, 3, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 1, 4, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 1, 5, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(6, 1, 6, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(7, 1, 7, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(8, 1, 8, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(9, 1, 9, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(10, 1, 10, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(11, 1, 11, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(12, 1, 12, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(13, 1, 13, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(14, 1, 14, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(15, 1, 15, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(16, 1, 16, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(17, 1, 17, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 1, 2, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 1, 3, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 1, 4, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 1, 5, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(6, 1, 6, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(7, 1, 7, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(8, 1, 8, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(9, 1, 9, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(10, 1, 10, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(11, 1, 11, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(12, 1, 12, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(13, 1, 13, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(14, 1, 14, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(15, 1, 15, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(16, 1, 16, 1, 1, 1, 1, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `role_module_fields`
--

CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 1, 2, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 1, 3, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 1, 4, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 1, 5, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(6, 1, 6, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(7, 1, 7, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(8, 1, 8, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(9, 1, 9, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(10, 1, 10, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(11, 1, 11, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(12, 1, 12, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(13, 1, 13, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(14, 1, 14, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(15, 1, 15, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(16, 1, 16, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(17, 1, 17, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(18, 1, 18, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(19, 1, 19, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(20, 1, 20, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(21, 1, 21, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(22, 1, 22, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(23, 1, 23, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(24, 1, 24, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(25, 1, 25, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(26, 1, 26, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(27, 1, 27, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(28, 1, 28, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(29, 1, 29, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(30, 1, 30, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(31, 1, 31, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(32, 1, 32, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(33, 1, 33, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(34, 1, 34, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(35, 1, 35, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(36, 1, 36, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(37, 1, 37, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(38, 1, 38, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(39, 1, 39, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(40, 1, 40, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(41, 1, 41, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(42, 1, 42, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(43, 1, 43, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(44, 1, 44, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(45, 1, 45, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(46, 1, 46, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(47, 1, 47, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(48, 1, 48, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(49, 1, 49, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(50, 1, 50, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(51, 1, 51, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(52, 1, 52, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(53, 1, 53, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(54, 1, 54, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(55, 1, 55, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(56, 1, 56, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(57, 1, 57, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(58, 1, 58, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(59, 1, 59, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(60, 1, 60, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(61, 1, 61, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(62, 1, 62, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(63, 1, 63, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(64, 1, 64, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(65, 1, 65, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(66, 1, 66, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(67, 1, 67, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(68, 1, 68, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(69, 1, 69, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(70, 1, 70, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(71, 1, 71, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(72, 1, 72, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(73, 1, 73, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(74, 1, 74, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(75, 1, 75, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(76, 1, 76, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(77, 1, 77, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(78, 1, 78, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(79, 1, 79, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(80, 1, 80, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(81, 1, 81, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(82, 1, 82, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(83, 1, 83, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(84, 1, 84, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(85, 1, 85, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(86, 1, 86, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(87, 1, 87, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(88, 1, 88, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(89, 1, 89, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(90, 1, 90, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(91, 1, 91, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(92, 1, 92, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(93, 1, 93, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(94, 1, 94, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(95, 1, 95, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(96, 1, 96, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(97, 1, 97, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(98, 1, 98, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(99, 1, 99, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(100, 1, 100, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(101, 1, 101, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(102, 1, 102, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(103, 1, 103, 'write', '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `training` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `excerpt` varchar(200) COLLATE utf8_unicode_ci DEFAULT '',
  `banner` int(10) UNSIGNED DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `content` varchar(10000) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `title`, `training`, `excerpt`, `banner`, `post_date`, `content`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Corporate Training for Bank', 'Corporate Training', 'Corporate Training with One of India\'s fastest growing bank', 5, '2018-06-03', 'Corporate Training with One of India\'s fastest growing bank', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'Outbound Training for Business College', 'Corporate Training', 'Outbound Training with Top Business College in Pune', NULL, '2018-06-26', 'Outbound Training with Top Business College in Pune', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 'Outbound Training near Pune', 'Corporate Training', 'Outbound Training near Pune', 7, '2018-07-03', 'Outbound Training near Pune', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 'Team Outbound Training near Kolad', 'Corporate Training', 'Team Outbound Training near Kolad for Mahindra', 7, '2018-11-03', 'Team Outbound Training near Kolads', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 'Winter Adventure Camp near Pune', 'Corporate Training', 'Winter Adventure Camp near Pune', 7, '2018-10-23', 'Winter Adventure Camp near Pune for all age participents', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Corporate',
  `author` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `designation` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `author_image` int(10) UNSIGNED DEFAULT NULL,
  `content_image` int(10) UNSIGNED DEFAULT NULL,
  `youtube` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `title`, `category`, `author`, `designation`, `content`, `author_image`, `content_image`, `youtube`, `post_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'You totally deserve the aploases for making our trip such good', 'Adventure', 'Krunal Karia', 'Real Estate Advissor', 'This is small feed back which I would like to give you on behalf of our family & specially from our bday boy.<br>As we visited with our family on 5th march to enjoy my nephew bday.', 7, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-06-12', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'Empower is Awesome !', 'Adventure', 'Kajal Dasani', 'Systems & finance,<br>Symbiosis Institute of Telecom Management', 'I feel so privileged that i was a part of the Empower Camp and got an opportunity to gain so much from you! No flattering words sir but its a heart felt thank you and i feel lucky that was there under your guidance. It was very sweet of you that you remember my both names even now it brings a smile to my face.It was a lifetime experience and i will never forget those 4 fun-filled days where i became physically as well mentally stronger. Now i can take my decisions in a more firm way! No more mobile addiction that\'s the biggest take-away!', 7, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-06-12', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 'An engaging session indeed', 'Corporate', 'ROHIT MALLICK', 'Regional Manager South,<br>Learning Services,<br>ITC GRAND CHOLA,Chennai', 'Greetings from ITC Grand Chola ! Thank you for your email. It was indeed our privilege to organise a short programme delivered by you for our team members. Each one of them gave an extremely positive feedback for the programme and appeared to be highly motivated after attending your programme. I could make out that your session was indeed very engaging and every participant was actively involved in the activities that were being conducted.', NULL, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-07-03', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 'Positive Transformation', 'Corporate', 'Sushavan Chatterjee', 'Area Sales Manager,<br>Nilkamal Ltd.', 'first of all, I need to thank you for proffering us, such an alluring experience in between the lap of mother nature which we hardly get a chance to practice in our daily life.\n		Secondly, I really boast of gaining all those outstanding experiences which we have acquired through your commendable trainings schedules.I honestly pledge to implement all the knowledge powered by you where ever relevant in both my “Professional”  as well as “Personal life” .......and I am sure it would bring out lots of positive transformation within me in the coming days.....Wish you and your entire team a great success.', NULL, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-07-03', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 'They went as individuals but came back as \'one\' Firestar', 'Students', 'Anup Pancha', 'COO,<br> Firestar Diamond', 'Thanks for your Outbound Leadership and Team Building Training given to Sr. Firestar team.We had a good Time of Learning with Good Physical fitness activities and Games. I know your physical activities and games made them tired due to they are not habitual in their daily routine but I am Sure they All become Mentally Very Strong by learning lesson from all activities, what they can DO where they were feeling they can’t do before attempting those games and activates ...When they left from Mumbai to Kolad in bus, I saw them, they were from different dept and Location and when I saw them while returning, I found they all are from Firestar.Hoping to get continuous guidance from you to my team members time to time to become better than Before always at every stage in their Life.Thanks for your all your Hospitality with Fantastic Food .Please convey my sincere thanks to Anand , Pratap and all other team members whoever has directly or indirectly helped to make our training super successful.', NULL, NULL, 'https://www.youtube.com/watch?v=GoEx-b8p9rc', '2018-07-03', NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `extension` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `caption` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `user_id` int(10) UNSIGNED DEFAULT '1',
  `hash` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Summer-Camp-Whatsapp-Promo-Vertical.png', '/Applications/XAMPP/xamppfiles/htdocs/empowercamp/storage/uploads/Summer-Camp-Whatsapp-Promo-Vertical.png', 'png', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856788', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(2, 'EmpowerCamp-by-night.png', '/Applications/XAMPP/xamppfiles/htdocs/empowercamp/storage/uploads/EmpowerCamp-by-night.png', 'png', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856889', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(3, 'outbound-training-camp-new-york-concluded.jpg', '/Applications/XAMPP/xamppfiles/htdocs/empowercamp/storage/uploads/outbound-training-camp-new-york-concluded.jpg', 'jpg', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856799', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(4, 'outbound-training-new-york-empower-camp.jpg', '/Applications/XAMPP/xamppfiles/htdocs/empowercamp/storage/uploads/outbound-training-new-york-empower-camp.jpg', 'jpg', 'Photo by Tim Bogdanov on Unsplash', NULL, 't5ipaasxz967pvle856879', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(5, 'Corporate-Training-for-Bank.jpg', '/Applications/XAMPP/xamppfiles/htdocs/empowercamp/storage/uploads/Corporate-Training-for-Bank.jpg', 'jpg', 'Corporate Training for Bank', NULL, 't5ipaasxz967pvle856712', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(6, 'Outbound-Training-Institute.jpg', '/Applications/XAMPP/xamppfiles/htdocs/empowercamp/storage/uploads/Outbound-Training-Institute.jpg', 'jpg', 'Outbound Training for Institute', 1, 't5ipaasxz967pvle856713', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25'),
(7, 'Outbound-Training-near-Pune.jpg', '/Applications/XAMPP/xamppfiles/htdocs/empowercamp/storage/uploads/Outbound-Training-near-Pune.jpg', 'jpg', 'Outbound Training near Pune', 1, 't5ipaasxz967pvle856714', 1, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(11) NOT NULL DEFAULT '0',
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `context_id`, `email`, `password`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Ganesh Bhosale', 1, 'hello@laraadmin.com', '$2y$10$Ao/E5BbzLgjcWScqsfmN0O8QhaG0YNlthFKtIFMPeKVpi7At4S4eq', NULL, NULL, '2019-04-22 06:06:25', '2019-04-22 06:06:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `backups_name_unique` (`name`),
  ADD UNIQUE KEY `backups_file_name_unique` (`file_name`);

--
-- Indexes for table `blog_categories`
--
ALTER TABLE `blog_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_categories_url_unique` (`url`);

--
-- Indexes for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blog_posts_url_unique` (`url`),
  ADD KEY `blog_posts_category_id_foreign` (`category_id`),
  ADD KEY `blog_posts_author_id_foreign` (`author_id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bookings_email_unique` (`email`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_primary_unique` (`email_primary`),
  ADD KEY `customers_profile_img_foreign` (`profile_img`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_primary_unique` (`email_primary`),
  ADD KEY `employees_profile_img_foreign` (`profile_img`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `enquiries_email_unique` (`email`);

--
-- Indexes for table `lalogs`
--
ALTER TABLE `lalogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lalogs_module_id_foreign` (`module_id`),
  ADD KEY `lalogs_user_id_foreign` (`user_id`);

--
-- Indexes for table `lalog_user`
--
ALTER TABLE `lalog_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lalog_user_lalog_id_foreign` (`lalog_id`),
  ADD KEY `lalog_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `la_configs`
--
ALTER TABLE `la_configs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `la_configs_key_unique` (`key`),
  ADD KEY `la_configs_field_type_foreign` (`field_type`);

--
-- Indexes for table `la_menus`
--
ALTER TABLE `la_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_from_foreign` (`from`),
  ADD KEY `messages_to_foreign` (`to`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Indexes for table `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_menu_role_id_foreign` (`role_id`),
  ADD KEY `role_menu_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Indexes for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_banner_foreign` (`banner`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `testimonials_author_unique` (`author`),
  ADD UNIQUE KEY `testimonials_designation_unique` (`designation`),
  ADD KEY `testimonials_author_image_foreign` (`author_image`),
  ADD KEY `testimonials_content_image_foreign` (`content_image`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_categories`
--
ALTER TABLE `blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog_posts`
--
ALTER TABLE `blog_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lalogs`
--
ALTER TABLE `lalogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lalog_user`
--
ALTER TABLE `lalog_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `la_configs`
--
ALTER TABLE `la_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `la_menus`
--
ALTER TABLE `la_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog_posts`
--
ALTER TABLE `blog_posts`
  ADD CONSTRAINT `blog_posts_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `blog_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_profile_img_foreign` FOREIGN KEY (`profile_img`) REFERENCES `uploads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_profile_img_foreign` FOREIGN KEY (`profile_img`) REFERENCES `uploads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lalogs`
--
ALTER TABLE `lalogs`
  ADD CONSTRAINT `lalogs_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lalogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lalog_user`
--
ALTER TABLE `lalog_user`
  ADD CONSTRAINT `lalog_user_lalog_id_foreign` FOREIGN KEY (`lalog_id`) REFERENCES `lalogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lalog_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `la_configs`
--
ALTER TABLE `la_configs`
  ADD CONSTRAINT `la_configs_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_from_foreign` FOREIGN KEY (`from`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `messages_to_foreign` FOREIGN KEY (`to`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD CONSTRAINT `role_menu_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `la_menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_menu_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_banner_foreign` FOREIGN KEY (`banner`) REFERENCES `uploads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD CONSTRAINT `testimonials_author_image_foreign` FOREIGN KEY (`author_image`) REFERENCES `uploads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `testimonials_content_image_foreign` FOREIGN KEY (`content_image`) REFERENCES `uploads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
/**
 * Migration generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Module;

class CreateTestimonialsTable extends Migration
{
    /**
     * Migration generate Module Table Schema by LaraAdmin
     *
     * @return void
     */
    public function up()
    {
        Module::generate("Testimonials", 'testimonials', 'title', 'fa-file-text-o', [
            [
                "colname" => "title",
                "label" => "Title",
                "field_type" => "Name",
                "unique" => false,
                "defaultvalue" => "",
                "minlength" => 5,
                "maxlength" => 100,
                "required" => true,
                "listing_col" => true
            ], [
                "colname" => "category",
                "label" => "Category",
                "field_type" => "Dropdown",
                "unique" => false,
                "defaultvalue" => "Corporate",
                "required" => true,
                "listing_col" => true,
                "popup_vals" => ["Corporate", "Students", "Adventure"]
            ], [
                "colname" => "author",
                "label" => "Author",
                "field_type" => "String",
                "unique" => false,
                "defaultvalue" => "",
                "minlength" => 5,
                "maxlength" => 150,
                "required" => false,
                "listing_col" => true
            ], [
                "colname" => "designation",
                "label" => "Designation",
                "field_type" => "String",
                "unique" => false,
                "defaultvalue" => "",
                "minlength" => 5,
                "maxlength" => 150,
                "required" => false,
                "listing_col" => true
            ], [
                "colname" => "content",
                "label" => "Content",
                "field_type" => "Textarea",
                "unique" => false,
                "defaultvalue" => "",
                "required" => true,
                "listing_col" => true
            ], [
                "colname" => "author_image",
                "label" => "Author Image",
                "field_type" => "Image",
                "unique" => false,
                "defaultvalue" => NULL,
                "required" => false,
                "listing_col" => false
            ], [
                "colname" => "content_image",
                "label" => "Content Image",
                "field_type" => "Image",
                "unique" => false,
                "defaultvalue" => NULL,
                "required" => false,
                "listing_col" => false
            ], [
                "colname" => "youtube",
                "label" => "Youtube Link",
                "field_type" => "URL",
                "unique" => false,
                "defaultvalue" => "",
                "minlength" => 40,
                "maxlength" => 150,
                "required" => false,
                "listing_col" => false
            ], [
                "colname" => "post_date",
                "label" => "Date",
                "field_type" => "Date",
                "unique" => false,
                "defaultvalue" => NULL,
                "required" => true,
                "listing_col" => true
            ], 
        ]);
        
        /*
        Module::generate("Module_Name", "Table_Name", "view_column_name" "Fields_Array");

        Field Format:
        [
            "colname" => "name",
            "label" => "Name",
            "field_type" => "Name",
            "unique" => false,
            "defaultvalue" => "John Doe",
            "minlength" => 5,
            "maxlength" => 100,
            "required" => true,
            "listing_col" => true,
            "popup_vals" => ["Employee", "Client"]
        ]
        # Format Details: Check http://laraadmin.com/docs/migrations_cruds#schema-ui-types
        
        colname: Database column name. lowercase, words concatenated by underscore (_)
        label: Label of Column e.g. Name, Cost, Is Public
        field_type: It defines type of Column in more General way.
        unique: Whether the column has unique values. Value in true / false
        defaultvalue: Default value for column.
        minlength: Minimum Length of value in integer.
        maxlength: Maximum Length of value in integer.
        required: Is this mandatory field in Add / Edit forms. Value in true / false
        listing_col: Is allowed to show in index page datatable.
        popup_vals: These are values for MultiSelect, TagInput and Radio Columns. Either connecting @tables or to list []
        */
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('testimonials')) {
            Schema::drop('testimonials');
        }
    }
}

<?php
/**
 * Migration generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Module;

class CreateLALogUserTable extends Migration
{
    /**
     * Migration generate Module Table Schema by LaraAdmin
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lalog_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('lalog_id')->unsigned();
			$table->foreign('lalog_id')->references('id')->on('lalogs')->onUpdate('cascade')->onDelete('cascade');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->boolean('notif_mail')->default(false);
            $table->boolean('notif_mail_viewed')->default(false);
            $table->boolean('notif_socket')->default(false);
            $table->boolean('notif_socket_viewed')->default(false);
			$table->timestamps();
		});
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('lalog_user')) {
            Schema::drop('lalog_user');
        }
    }
}

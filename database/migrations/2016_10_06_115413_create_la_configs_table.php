<?php
/**
 * Migration generated using LaraAdmin
 * Help: http://laraadmin.com
 * LaraAdmin is open-sourced software licensed under the MIT license.
 * Developed by: Dwij IT Solutions
 * Developer Website: http://dwijitsolutions.com
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('la_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label', 100);
            $table->string('key', 50)->unique();
            $table->string('section', 100)->default("");
            $table->string('value', 5000)->nullable()->default(NULL);
            $table->integer('field_type')->unsigned()->nullable()->default(NULL);
            $table->foreign('field_type')->references('id')->on('module_field_types')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('minlength')->unsigned()->nullable()->default(NULL);
            $table->integer('maxlength')->unsigned()->nullable()->default(NULL);
            $table->boolean('required')->default(false);
            $table->string('popup_vals', 5000)->default("");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('la_configs');
    }
}

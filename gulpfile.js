const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.less('laraadmin/LaraAdmin.less', 'public/la-assets/css');
    
    mix.less('bootstrap/bootstrap.less', 'public/la-assets/css');

    mix.less('laraadmin/skins/_all-skins.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-black.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-black-light.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-blue.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-blue-light.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-green.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-green-light.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-purple.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-purple-light.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-red.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-red-light.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-yellow.less', 'public/la-assets/css/skins');
    mix.less('laraadmin/skins/skin-yellow-light.less', 'public/la-assets/css/skins');
    
    // mix.webpack('app.js');
});
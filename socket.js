require('dotenv').config();
var http = require('http');
var querystring = require('querystring');

var app_url = process.env.APP_URL;
var socket_port = process.env.SOCKET_PORT;
var laplus_id = process.env.LAPLUS_ID;
var socket_channel = 'private-laraadmin-channel-'+laplus_id;

var server = require('http').Server();
var io = require('socket.io')(server);
var Redis = require('ioredis');

var redis = new Redis();

redis.subscribe(socket_channel);

// Send Messages from Server to Client
redis.on('message', function(channel, data) {
    data = JSON.parse(data);

    console.log("redis.on", channel, data.data.type, data.data.message.id, ": " + data.data.message.from + " -> " + data.data.message.to);

    // Send Data to Client/User Browser
    io.emit("private-laraadmin-" + laplus_id + "-user-" + data.data.message.to + "-channel", data.data);
});

// Socket Connection Authentication
io.use(function(socket, next) {
    console.log("io.use", app_url, " == ",socket.handshake.headers.referer, socket.handshake.query.user_id);

    if(socket.handshake.headers.referer.indexOf(app_url) !== -1) {
        io.emit(socket_channel, {
            type: 'UserJoined',
            user_id: socket.handshake.query.user_id
        });
        return next();
    }
    next(new Error('Authentication error'));
});

// Get Client Messages and BroadCast
io.on('connection', function(socket) {
    // TODO: Understand param socket. Helpful in Channel Auth
    console.log("io.on.connection", socket.handshake.query.user_id);

    socket.on(socket_channel, function(message) {
        console.log("socket.on", message.type, message.message_id);
        
        // Handle Message acknowledgement
        if(message.type == "UserMessageAck") {
            
            // Save Message Ack in Database
            var options = {
                path: message.app_url + '/messages/ack/' + message.message_id,
                method: 'GET',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            };
            // console.log("socket.on.UserMessageAck", options);
            var req = http.request(options, function(res) {
                res.setEncoding('utf8');
                /*
                res.on('data', (chunk) => {
                    console.log(`BODY: ${chunk}`);
                });
                res.on('end', () => {
                    console.log('No more data in response.');
                });
                */
            });
            req.write(querystring.stringify({ message }));
            req.end();

            // Send Ack Back to From / Source user
            var ackMessage = {
                type: "UserMessageAckDone",
                user: message.user_ack_from,
                message_id: message.message_id
            }
            io.emit("private-laraadmin-" + laplus_id + "-user-" + message.user_ack_to + "-channel", ackMessage);
        }
        // Emit commented to send messages via Laravel
        // io.emit(socket_channel, message);
    });
});

console.log("Starting Socket.IO Server on port "+socket_port+" and channel "+socket_channel+"... ");

server.listen({
    port: socket_port
});

// Express Server
// var app = require('express')();
// app.get('/', function(request, response) {
//     response.sendFile(__dirname + '/index.html');
// });
<?php

use App\Helpers\LAHelper;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/* ================== Homepage ================== */

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get(config('laraadmin.blogRoute'), 'LA\BlogPostsController@show_blog');
Route::get(config('laraadmin.blogRoute') . '/{post_url}', 'LA\BlogPostsController@show_post');

Route::get('/category/blogs', function () {
    return redirect('/blog');
}); 

Route::get('/category/{category_url}', 'LA\BlogPostsController@show_category');

/* ================== Forms ================== */

Route::post('/save_session_booking', 'HomeController@save_session_booking');

Route::post('/save_enquiry', 'HomeController@save_enquiry');

Route::post('/save_testimonial', 'HomeController@save_testimonial');

/* ================== Pages ================== */

Route::get('/sample', function () {
    return View('sample');
});

Route::get('/corporate-outbound-training', function () {
    return View('trainings.corporate-outbound-training');
});

Route::get('/leadership-outbound-training', function () {
    return View('trainings.leadership-outbound-training');
});

Route::get('/team-building-outbound-training', function () {
    return View('trainings.team-building-outbound-training');
});

Route::get('/induction-outbound-camps', function () {
    return View('trainings.induction-outbound-camps');
});

Route::get('/corporate-social-responsibility', function () {
    return View('trainings.corporate-social-responsibility');
});

Route::get('/empowering-students', function () {
    return View('trainings.empowering-students');
});

Route::get('/workshops', function () {
    return View('trainings.workshops');
});

Route::get('/summer-camps', function () {
    return redirect('/summer-camp');
});

Route::get('/adventure-and-leisure-camps', function () {
    return View('adventure.adventure-and-leisure-camps');
});

Route::get('/white-water-rafting-kolad', function () {
    return View('adventure.white-water-rafting-kolad');
});

Route::get('/testimonials', function () {
    return View('testimonials');
});

Route::get('/about-us', function () {
    return view('about-us');
});

Route::get('/col-naval-kohli', function () {
    return view('team.col-naval-kohli');
});

Route::get('/anil-bhasin', function () {
    return view('team.anil-bhasin');
});

Route::get('/facilities', function () {
    return view('facilities');
});

Route::get('/contact-us', function () {
    return view('contact-us');
});

Route::get('/blog', function () {
    return view('blog.blogs');
});


Route::get('/blog_post/{id}', function ($id) {
    $blog_post = App\Models\Blogpost::find($id);
    return view('blog.blog_post')->with('blog_post', $blog_post);
});

Route::get('/events', function () {
    return view('events.events');
});

Route::get('/event/{pageurl}', function ($pageurl) {
    if(is_numeric($pageurl)) {
        $session = App\Models\Session::find($pageurl);
    } else {
        $pageurl = trim($pageurl, "/");
        $session = App\Models\Session::where("pageurl", $pageurl)->first();
    }
    if(isset($session->id)) {
        return view('events.event')->with('session', $session);
    } else {
        abort(404, 'Page not found');
    }
});

Route::get('/event-category/{category}', function ($category) {
    $sessions = App\Models\Session::where("training", $category)->get();
    return view('events.category', [
        'category' => $category,
        'sessions' => $sessions
    ]);
});

Route::get('/winter-camp', function () {
    return view('camps.winter-camp');
});

Route::get('/summer-camp', function () {
    return view('camps.summer-camp');
});

Route::get('/special-packages-for-summer-camp-2019', function () {
    return view('camps.special-packages-for-summer-camp-2019');
});

Route::get('/monsoon-adventure-camps', function () {
    return view('camps.monsoon-adventure-camps');
});

Route::get('/our-privacy-policy', function () {
    return view('our-privacy-policy');
});

Route::get('/faq', function () {
    return view('faq');
});

Route::get('/site-map-empower', function () {
    return view('site-map-empower');
});

Route::get('/summer-camps-for-kids-2017', function () {
    return view('camps.summer-camps-for-kids-2017');
});

Route::get('/thankyou', function () {
    return view('thankyou');
});

/* ================== Redirect ================== */

Route::get('/education', function () {
    return redirect('/empowering-students');
});

Route::get('/about', function () {
    return redirect('/about-us');
});

Route::get('/adventure', function () {
    return redirect('/adventure-and-leisure-camps');
});

Route::get('/privacy-policy', function () {
    return redirect('/our-privacy-policy');
});

Route::get('/faqs', function () {
    return redirect('/faq');
});

Route::get('/camp-activity-area', function () {
    return redirect('/facilities');
});

Route::get('/terms-of-use', function () {
    return redirect('/home');
});

Route::get('/site-map', function () {
    return redirect('/site-map-empower');
});

Route::get('/summer-camps-for-kids-2017-dates-announced', function () {
    return redirect('/summer-camps-for-kids-2017');
});

Route::get('/new-york-outbound-training-camp-concluded-successfully', function () {
    return redirect('/blog_post/3');
});

Route::get('/corporate-outbound-training-camps-soft-skill-training-team-building/nggallery/thumbnails', function () {
    return redirect('/corporate-outbound-training');
});

Route::get('/corporate-training', function () {
    return redirect('/corporate-outbound-training');
});

Route::get('/soft-skills-n3-2', function () {
    return redirect('/corporate-outbound-training');
});

Route::get('/white-water-rafting-2', function () {
    return redirect('/white-water-rafting-kolad');
});

Route::get('/dcc-dynamic-challenge-course-for-corporates', function () {
    return redirect('/home');
});/* === only photo wich have on home page==== */

Route::get('/tag/experiential-learning', function () {
    return redirect('/corporate-outbound-training');
});

Route::get('/tag/outbound-training', function () {
    return redirect('/corporate-outbound-training');
});

Route::get('/author/santosh', function () {
    return redirect('/blog');
});

Route::get('/tag/leadership-training', function () {
    return redirect('/corporate-outbound-training');/* === page is ready waiting for content==== */
});

Route::get('/gol-hut-a-1-2', function () {
    return redirect('/facilities');
}); 

Route::get('/raft-building-empower-camp', function () {
    return redirect('/adventure-and-leisure-camps');
});

Route::get('/anil-bhasin-empower-3', function () {
    return redirect('/anil-bhasin');
});

/* ================== Auth ================== */

Route::auth();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/customers/register', 'Auth\RegisterController@showCustomerRegistrationForm');

/* ================== Testing Function ================== */

Route::get('/test', function() {
    
});

/* ================== Access Uploaded Files ================== */

Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/* ================== Call LaraAdmin Routes  ================== */

require __DIR__.'/admin_routes.php';
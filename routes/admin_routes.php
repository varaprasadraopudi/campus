<?php

/*
|--------------------------------------------------------------------------
| Super Admin Routes
|--------------------------------------------------------------------------
*/

/**
 * Connect routes with ADMIN_PANEL permission(for security) and 'App\Http\Controllers\LA' namespace
 * and '/admin' url.
 */
Route::group([
    'namespace' => 'LA',
    'as' => config('laraadmin.adminRoute').'.',
    'middleware' => ['web', 'auth', 'permission:ADMIN_PANEL', 'role:SUPER_ADMIN']
], function () {
    
    /* ================== Modules ================== */
    Route::resource(config('laraadmin.adminRoute') . '/modules', 'ModuleController');
    Route::resource(config('laraadmin.adminRoute') . '/module_fields', 'FieldController');
    Route::get(config('laraadmin.adminRoute') . '/module_generate_crud/{model_id}', 'ModuleController@generate_crud');
    Route::get(config('laraadmin.adminRoute') . '/module_generate_migr/{model_id}', 'ModuleController@generate_migr');
    Route::get(config('laraadmin.adminRoute') . '/module_generate_update/{model_id}', 'ModuleController@generate_update');
    Route::get(config('laraadmin.adminRoute') . '/module_generate_migr_crud/{model_id}', 'ModuleController@generate_migr_crud');
    Route::get(config('laraadmin.adminRoute') . '/modules/{model_id}/set_view_col/{column_name}', 'ModuleController@set_view_col');
    Route::post(config('laraadmin.adminRoute') . '/save_role_module_permissions/{id}', 'ModuleController@save_role_module_permissions');
    Route::get(config('laraadmin.adminRoute') . '/save_module_field_sort/{model_id}', 'ModuleController@save_module_field_sort');
    Route::get(config('laraadmin.adminRoute') . '/module_fields/{id}/delete', 'FieldController@destroy');
    Route::post(config('laraadmin.adminRoute') . '/get_module_files/{module_id}', 'ModuleController@get_module_files');
    Route::post(config('laraadmin.adminRoute') . '/module_update', 'ModuleController@update');
    Route::post(config('laraadmin.adminRoute') . '/module_field_listing_show', 'FieldController@module_field_listing_show_ajax');
    
    /* ================== Code Editor ================== */
    Route::get(config('laraadmin.adminRoute') . '/lacodeeditor', function () {
        if(file_exists(resource_path("views/la/editor/index.blade.php"))) {
            return redirect(config('laraadmin.adminRoute') . '/laeditor');
        } else {
            // show install code editor page
            return View('la.editor.install');
        }
    });
    
    /* ================== Menu Editor ================== */
    Route::resource(config('laraadmin.adminRoute') . '/la_menus', 'MenuController');
    Route::post(config('laraadmin.adminRoute') . '/la_menus/update_hierarchy', 'MenuController@update_hierarchy');
    Route::post(config('laraadmin.adminRoute') . '/save_role_menu_permissions/{id}', 'MenuController@save_role_menu_permissions');
    
    /* ================== Configuration ================== */
    Route::resource(config('laraadmin.adminRoute') . '/la_configs', 'LAConfigController');
    Route::post(config('laraadmin.adminRoute') . '/la_configs/edit_save/{id}', 'LAConfigController@edit_save');
    Route::get(config('laraadmin.adminRoute') . '/la_configs/ajax_destroy/{id}', 'LAConfigController@ajax_destroy');
    
    Route::group([
        'middleware' => 'role'
    ], function () {
        /*
        Route::get(config('laraadmin.adminRoute') . '/menu', [
            'as'   => 'menu',
            'uses' => 'LAController@index'
        ]);
        */
    });
});

/*
|--------------------------------------------------------------------------
| Node.js Routes
|--------------------------------------------------------------------------
*/

Route::get('/messages/ack/{message_id}', 'LA\MessagesController@messageAck');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'as' => config('laraadmin.adminRoute').'.',
    'middleware' => ['auth', 'permission:ADMIN_PANEL']
], function () {

    /* ================== General ================== */

    Route::post(config('laraadmin.adminRoute') . '/check_unique_val/{field_id}', 'LA\FieldController@check_unique_val');
    Route::get(config('laraadmin.adminRoute') . '/quick_add_form/{module_id}', 'LA\ModuleController@quick_add_form');
    Route::post(config('laraadmin.adminRoute') . '/quick_add_form_save/{module_id}', 'LA\ModuleController@quick_add_form_save');
    Route::post(config('laraadmin.adminRoute') . '/checklist_update', 'LA\ModuleController@checklist_update');
	
	/* ================== Dashboard ================== */
	
	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');

    /* ================== LALogs ================== */
    Route::resource(config('laraadmin.adminRoute') . '/lalogs', 'LA\LALogsController');
    Route::get(config('laraadmin.adminRoute') . '/lalog_dt_ajax', 'LA\LALogsController@dtajax');
    Route::post(config('laraadmin.adminRoute') . '/get_lalog_details/{id}', 'LA\LALogsController@get_lalog_details');
	
	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
    Route::get(config('laraadmin.adminRoute') . '/update_local_upload_paths', 'LA\UploadsController@update_local_upload_paths');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
    Route::get(config('laraadmin.adminRoute') . '/hierarchy_view', 'LA\RolesController@hierarchy_view');
    Route::post(config('laraadmin.adminRoute') . '/update_role_hierarchy', 'LA\RolesController@update_role_hierarchy');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');

    /* ================== Customers ================== */
    Route::resource(config('laraadmin.adminRoute') . '/customers', 'LA\CustomersController');
    Route::get(config('laraadmin.adminRoute') . '/customer_dt_ajax', 'LA\CustomersController@dtajax');
	
	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');

    /* ================== search ================== */
    Route::get(config('laraadmin.adminRoute') . '/find/{module_name}', 'LA\SearchController@find');

    /* ================== Messages ================== */
    Route::resource(config('laraadmin.adminRoute') . '/messages', 'LA\MessagesController');
    Route::get(config('laraadmin.adminRoute') . '/messages/user/{id}', 'LA\MessagesController@get_old_messages');
    Route::get(config('laraadmin.adminRoute') . '/messages/user_profile/{id}', 'LA\MessagesController@get_user_profile');    

    /* ================== Blog_categories ================== */
    Route::resource(config('laraadmin.adminRoute') . '/blog_categories', 'LA\BlogCategoriesController');
    Route::get(config('laraadmin.adminRoute') . '/blog_category_dt_ajax', 'LA\BlogCategoriesController@dtajax');

    /* ================== Blog_posts ================== */
    Route::resource(config('laraadmin.adminRoute') . '/blog_posts', 'LA\BlogPostsController');
    Route::get(config('laraadmin.adminRoute') . '/blog_post_dt_ajax', 'LA\BlogPostsController@dtajax');

    /* ================== Sessions ================== */
    Route::resource(config('laraadmin.adminRoute') . '/sessions', 'LA\SessionsController');
    Route::get(config('laraadmin.adminRoute') . '/session_dt_ajax', 'LA\SessionsController@dtajax');

    /* ================== Testimonials ================== */
    Route::resource(config('laraadmin.adminRoute') . '/testimonials', 'LA\TestimonialsController');
    Route::get(config('laraadmin.adminRoute') . '/testimonial_status_update/{test_id}/{status}', 'LA\TestimonialsController@status_update');
    Route::get(config('laraadmin.adminRoute') . '/testimonial_dt_ajax', 'LA\TestimonialsController@dtajax');

    /* ================== Bookings ================== */
    Route::resource(config('laraadmin.adminRoute') . '/bookings', 'LA\BookingsController');
    Route::get(config('laraadmin.adminRoute') . '/booking_status_update/{booking_id}/{status}', 'LA\BookingsController@status_update');
    Route::get(config('laraadmin.adminRoute') . '/booking_dt_ajax', 'LA\BookingsController@dtajax');

    /* ================== Enquiries ================== */
    Route::resource(config('laraadmin.adminRoute') . '/enquiries', 'LA\EnquiriesController');
    Route::get(config('laraadmin.adminRoute') . '/enquiry_status_update/{enquiry_id}/{status}', 'LA\EnquiriesController@status_update');
    Route::get(config('laraadmin.adminRoute') . '/enquiry_dt_ajax', 'LA\EnquiriesController@dtajax');
});
